/******************************************************************************
    Atomic Library
     Copyright (C)  2019-2020 Renesas Electronics Corporation.All rights reserved.

    [File] atmlib_cnn_sub.c

******************************************************************************/
#include <stdint.h>
#include <string.h>

#include "r_atmlib_prot.h"

#include "atmlib_cnn.h"


/* const table for initializing current_reg  */
R_ATMLIB_CNN_STATIC const R_ATMLIB_CNNInitCurReg atmlib_cnn_initCurRegTbl[] =
{	/*	offset_addr,						num,	initial_val,  */
	{	R_ATMLIB_CNN_SLSP,					1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_MAX_VAL_THRES,			1U,		0x0000FFFFU,  },
	{	R_ATMLIB_CNN_MIN_VAL_THRES,			1U,		0x0000FFFFU,  },
	{	R_ATMLIB_CNN_MAXSUM_CNT_THRES,		1U,		0xFFFFFFFFU,  },
	{	R_ATMLIB_CNN_MINSUM_CNT_THRES,		1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_SYNCC00_03,		 	1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_SYNCC04_07,			1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_SYNCC08_11,			1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_SYNCC12_15,			1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_PERFCNTCTRL_BASE,	 	4U,		0x00000000U,  },
	{	R_ATMLIB_CNN_PPERFCNT_BASE,		 	4U,		0x00000000U,  },
	{	R_ATMLIB_CNN_ARICSR,				1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_ARIE,				   	1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD_STRIDE_STARTPOS,	1U,		0x00110000U,  }, 
	{	R_ATMLIB_CNN_BIAS,					1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_ARI_LEN,			  	1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_MULT_POST_SFT,			1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_MULT_VAL,				1U,		0x00000001U,  }, 
	{	R_ATMLIB_CNN_PADD00_01,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD02_03,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD04_05,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD06_07,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD08_09,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD10_11,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD12_13,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_PADD14_15,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_STRIDE00_03,			1U,		0x11111111U,  }, 
	{	R_ATMLIB_CNN_STRIDE04_07,			1U,		0x11111111U,  }, 
	{	R_ATMLIB_CNN_STRIDE08_11,			1U,		0x11111111U,  }, 
	{	R_ATMLIB_CNN_STRIDE12_15,			1U,		0x11111111U,  }, 
	{	R_ATMLIB_CNN_START_POS_00_07,		1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_START_POS_08_15,		1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS00_01,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS02_03,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS04_05,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS06_07,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS08_09,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS10_11,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS12_13,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS14_15,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS16_17,				1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS18_19,			   	1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS20_21,		    	1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS22_23,		 	    1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS24_25,		        1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS26_27,		  	    1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS28_29,		  	    1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_BIAS30_31,		  	    1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_SFTI,			 	    1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_SFTC,			     	1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_SFTB,			     	1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_SFTM,				    1U,		0x00000000U,  }, 
	{	R_ATMLIB_CNN_SFTI_CH_BASE,		    16U,	0x00000000U,  },
	{	R_ATMLIB_CNN_SFTC_CH_BASE,			32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_R0,					1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_R1,					1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_R2,					1U,		0x00000000U,  },
	{	R_ATMLIB_CNN_ARI_BND,				1U,		0x80007FFFU,  },
	{	R_ATMLIB_CNN_LUT_POST_SFT,			1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_OCFP_TABLE,			128U,	0x00000000U,  },
	{	R_ATMLIB_CNN_UV_TABLE,			    128U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMAISA,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAICO,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIL,				    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIST,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIMAG,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIFM,			    1U,	    0x00000004U,  },
	{	R_ATMLIB_CNN_DMAIOA,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIE,					1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIS,				    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAISA_CH_BASE,	    16U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIL_CH_BASE,		    16U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIST_CH_BASE,		16U,    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIMAG00_03,			1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIMAG04_07,			1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIMAG08_11,			1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIMAG12_15,			1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAIFM00_07,			1U,	    0x44444444U,  },
	{	R_ATMLIB_CNN_DMAIFM08_15,			1U,	    0x44444444U,  },
	{	R_ATMLIB_CNN_DMAIOA_CH_BASE,		16U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCSA,				1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCCO,				1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCL,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCST,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCFM,			    1U,	    0x00000004U,  },
	{	R_ATMLIB_CNN_DMA3DCOA,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCE,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCS,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCSA_CH_BASE,		32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCL_CH_BASE,		32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCST_CH_BASE,		32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG00_03,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG04_04,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG08_11,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG12_15,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG16_19,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG20_23,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG24_27,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCMAG28_31,		1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMA3DCFM00_07,			1U,	    0x44444444U,  },
	{	R_ATMLIB_CNN_DMA3DCFM08_15,			1U,	    0x44444444U,  },
	{	R_ATMLIB_CNN_DMA3DCFM16_23,			1U,	    0x44444444U,  },
	{	R_ATMLIB_CNN_DMA3DCFM24_31,			1U,	    0x44444444U,  },
	{	R_ATMLIB_CNN_DMA3DCOA_CH_BASE,		32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOSA,				1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOCO,				1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOL,				    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOST,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOFM,			    1U,	    0x00000004U,  },
	{	R_ATMLIB_CNN_DMAOOA,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOE,					1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOS,					1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOSA_CH_BASE,		32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMAOOA_CH_BASE,	    32U,	0x00000000U,  },
	{	R_ATMLIB_CNN_DMARFESA,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMARFISA,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMARFL,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_DMARFCTRL,			    1U,	    0x00000000U,  },
	{	R_ATMLIB_CNN_LUT_BASE,			    256U,	0x00000000U,  },
	{	R_ATMLIB_CNN_WB0_BASE,			    4096U,	0x00000000U,  }
};


/* const table for disable omission current_reg  */
R_ATMLIB_CNN_STATIC const R_ATMLIB_CNNDisableCurReg atmlib_cnn_disableCurRegTbl[] =
{	/*	offset_addr,						num		*/
	{	R_ATMLIB_CNN_SLSP,					1U		},
	{	R_ATMLIB_CNN_PPERFCNT_BASE,			4U      },
	{	R_ATMLIB_CNN_BIAS,		            1U		},
	{	R_ATMLIB_CNN_DMAIOA,		        1U	    },
	{	R_ATMLIB_CNN_DMAIS,		            1U	    },
	{	R_ATMLIB_CNN_DMAIOA_CH_BASE,	    16U	    },
	{	R_ATMLIB_CNN_DMA3DCOA,		        1U	    },
	{	R_ATMLIB_CNN_DMA3DCS,		        1U	    },
	{	R_ATMLIB_CNN_DMA3DCOA_CH_BASE,	    32U	    },
	{	R_ATMLIB_CNN_DMAOOA,		        1U	    },
	{	R_ATMLIB_CNN_DMAOS,		            1U	    },
	{	R_ATMLIB_CNN_DMAOOA_CH_BASE,	    32U	    },
	{	R_ATMLIB_CNN_DMARFCTRL, 			1U		},
};


/************************************************
 Static function prototype definitions
************************************************/
R_ATMLIB_CNN_STATIC uint32_t atmlib_CNNCheckCurReg( const R_ATMLIB_CNNCurrentReg *current_reg, uint16_t dst_start_pos, uint16_t dst_length, const uint32_t *data );
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckStatus(const R_ATMLIB_CLData	*cldata, R_ATMLIB_CNNEVT_GROUP	event_group);
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckFmtConvertDMAIC(R_ATMLIB_CNNImageFormat img_fmt, R_ATMLIB_CNNARIFormat ari_fmt);
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckSPMCImgStrDMAI(uint16_t img_stride, R_ATMLIB_CNNImageFormat img_fmt, R_ATMLIB_CNNKernelMode kernel_mode);
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckSPMCImgStrDMAO(uint16_t img_stride, R_ATMLIB_CNNImageFormat img_fmt, R_ATMLIB_CNNARIFormat ari_fmt, R_ATMLIB_CNNKernelMode kernel_mode);
R_ATMLIB_CNN_STATIC uint32_t atmlib_CNNCalculateARILen(const R_ATMLIB_CNNDMAIChParam dmai_ch, const R_ATMLIB_CNNKernelMode kernel_mode, uint8_t dil_conv, uint16_t *ari_xlen, uint16_t *ari_ylen);
R_ATMLIB_CNN_STATIC void atmlib_CNNPresetDataDMAI(const R_ATMLIB_CNNDMAIParam *dmai_param, uint32_t *padd_stride_startpos, uint32_t *dmail, uint32_t *dmaist, uint32_t *dmaimag, uint32_t *dmaifm );
R_ATMLIB_CNN_STATIC void atmlib_CNNPresetDataDMAC(const R_ATMLIB_CNNDMACParam *dmac_param, uint32_t *dmacl, uint32_t *dmacst, uint32_t *dmacmag, uint32_t *dmacfm);

/******************************************************************************
 [ FuncName ]	atmlib_CNNInitializeCL
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				addr							: The address of CL memory
				size							: The size of CL memory
				cnn_data						: The pointer to the R_ATMLIB_CNNData structure
 [ Returns ]	none
 [ Function ]	Initialize the command list information (R_ATMLIB_CLData structure)
 [ Note ]		The caller of this function shall assure that the arguments are allocated memory.
******************************************************************************/
void atmlib_CNNInitializeCL(
	R_ATMLIB_CLData		*cldata,
	uint32_t			*addr,
	uint32_t			size,
	R_ATMLIB_CNNData	*cnn_data
)
{
	/* set the CL type */
	cldata->cltype = R_ATMLIB_CLTYPE_CNN;

	/* set the CL size */
	cldata->size = size;

	/* set the addresses of CL */
	cldata->cur_addr = addr;
	cldata->top_addr = addr;
	cldata->cnn_data = cnn_data;

	/* initialize CNN data */
	atmlib_CNNInitializeCnnData( cldata->cnn_data );

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNInitializeCnnData
 [ Params ]		cnn_data						: The pointer to the R_ATMLIB_CNNData structure
 [ Returns ]	none
 [ Function ]	Initialize the CNN data (R_ATMLIB_CNNData structure).
 [ Note ]		The caller of this function shall assure that the arguments are allocated memory.
******************************************************************************/
void atmlib_CNNInitializeCnnData(
	R_ATMLIB_CNNData	*cnn_data
)
{
	uint32_t					loop_cnt;
	uint32_t					loop_cnt2;
	R_ATMLIB_CNNInitCurReg		*init_tbl;
	R_ATMLIB_CNNDisableCurReg	*disable_tbl;

	/* initialize local variables */

	/************************************************/
	/* Initialize current register's information	*/
	/************************************************/
	/* initialize all elements of current_reg with 0 */
	for( loop_cnt = 0U; loop_cnt < R_ATMLIB_CNN_REG_MAXNUM; loop_cnt++ )
	{
		/* initlialize value & attr */
		cnn_data->current_reg[loop_cnt].value		= 0U;
		cnn_data->current_reg[loop_cnt].attr		= R_ATMLIB_CNN_REGCACHE_SET | R_ATMLIB_CNN_REGCACHE_ENABLE;
	}

	/* initialize some elements of current_reg with atmlib_cnn_initCurRegTbl */
	for( loop_cnt = 0U; loop_cnt < (sizeof(atmlib_cnn_initCurRegTbl) / sizeof(atmlib_cnn_initCurRegTbl[0])); loop_cnt++ )
	{
		/* get the table address of initialize */
		init_tbl = (R_ATMLIB_CNNInitCurReg *)(&(atmlib_cnn_initCurRegTbl[loop_cnt]));
		/* initialize the cuurent_reg by the specified size */
		for( loop_cnt2 = 0; loop_cnt2 < init_tbl->num; loop_cnt2++ )
		{
			/* initlialize value */
			cnn_data->current_reg[(init_tbl->offset_addr / 4U) + loop_cnt2].value	= init_tbl->initial_val;
		}
	}

	/* initialize some elements of current_reg with atmlib_cnn_disableCurRegTbl */
	for( loop_cnt = 0U; loop_cnt < (sizeof(atmlib_cnn_disableCurRegTbl) / sizeof(atmlib_cnn_disableCurRegTbl[0])); loop_cnt++ )
	{
		/* get the table address of disable */
		disable_tbl = (R_ATMLIB_CNNDisableCurReg *)(&(atmlib_cnn_disableCurRegTbl[loop_cnt]));
		/* initialize the cuurent_reg by the specified size */
		for( loop_cnt2 = 0; loop_cnt2 < disable_tbl->num; loop_cnt2++ )
		{
			/* initlialize attr */
			cnn_data->current_reg[(disable_tbl->offset_addr / 4U) + loop_cnt2].attr	&= ~R_ATMLIB_CNN_REGCACHE_ENABLE;
		}
	}

	/************************************************/
	/* Initialize channel's information */
	/************************************************/
	/* initialize the channel_props (zero clear) */
	(void)memset( &cnn_data->channel_props, 0, sizeof(cnn_data->channel_props) );

	/************************************************/
	/* Initialize other information					*/
	/************************************************/
	/* initialize setup_mode_dmai/dmac/dmao (default fast) */
	cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_FAST;
	cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_FAST;
	cnn_data->setup_mode_dmao = R_ATMLIB_CNN_CHSET_FAST;

	/* initialize ari_format/next_ari_format */
	cnn_data->ari_format 	  = R_ATMLIB_CNN_ARIFORMAT_8S;
	cnn_data->next_ari_format = R_ATMLIB_CNN_ARIFORMAT_8S;

	/* initialize dmai_frac/dmac_frac (zero clear) */
	(void)memset( &cnn_data->dmai_frac, 0, sizeof(cnn_data->dmai_frac) );
	(void)memset( &cnn_data->dmac_frac, 0, sizeof(cnn_data->dmac_frac) );

	/* initialize label information */
	(void)memset( &cnn_data->label_info, 0, sizeof(cnn_data->label_info) );

	/* initialize ofs_addr_wup */
	cnn_data->ofs_addr_wup = NULL;

	/* initialize base address information */
	(void)memset( &cnn_data->baddr_info, 0, sizeof(cnn_data->baddr_info) );

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckStatus
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				event_group						: The group of CNN events (Category of CNN API)
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Check the status of the CL information (R_ATMLIB_CLData structure).
 [ Note ]		The caller of this function shall assure that the arguments are allocated memory.
******************************************************************************/
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckStatus(
	const R_ATMLIB_CLData	*cldata,
	R_ATMLIB_CNNEVT_GROUP	event_group
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	/* const table of CNN(CL) status */
	R_ATMLIB_CNN_STATIC const R_ATMLIB_CNNEnableControl atmlib_cnn_statePermissionTbl[R_ATMLIB_CNNEVT_GROUP_COUNT][R_ATMLIB_CLSTATE_COUNT] =
		{	/*	UNINIT,					READY,					CONFIG,					FINAL					*/
			{	R_ATMLIB_CNN_ENABLE,	R_ATMLIB_CNN_ENABLE,	R_ATMLIB_CNN_DISABLE,	R_ATMLIB_CNN_ENABLE		},	/* R_ATMLIB_CNNEVT_GROUP_A	*/
			{	R_ATMLIB_CNN_DISABLE,	R_ATMLIB_CNN_ENABLE,	R_ATMLIB_CNN_DISABLE,	R_ATMLIB_CNN_DISABLE	},	/* R_ATMLIB_CNNEVT_GROUP_B	*/
			{	R_ATMLIB_CNN_DISABLE,	R_ATMLIB_CNN_ENABLE,	R_ATMLIB_CNN_DISABLE,	R_ATMLIB_CNN_DISABLE	},	/* R_ATMLIB_CNNEVT_GROUP_E	*/
		};

	/* initialize local variables */
	ret = R_ATMLIB_E_NG_STATE;

	/* check whether the CL state is valid */
	if( cldata->state < R_ATMLIB_CLSTATE_COUNT )
	{
		/* check whether the permission is enable */
		if( atmlib_cnn_statePermissionTbl[event_group][cldata->state] == R_ATMLIB_CNN_ENABLE )
		{
			/* set R_ATMLIB_E_OK in return value */
			ret = R_ATMLIB_E_OK;
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNUpdateStatus
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				clstate							: State of the command list
 [ Returns ]	none
 [ Function ]	Update the status of the CL information (R_ATMLIB_CLData structure).
 [ Note ]		none
******************************************************************************/
void atmlib_CNNUpdateStatus(
	R_ATMLIB_CLData		*cldata,
	R_ATMLIB_CLState	clstate
)
{
	/* update CL status */
	cldata->state = clstate;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamCL
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				event_group						: The group of CNN events (Category of CNN API)
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG					: Value of the members of the argument cldata is out of range
 [ Function ]	Check the parameters of command list information (R_ATMLIB_CLData structure)
 [ Note ]		The caller of this function shall assure that the arguments are allocated memory.
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamCL(
	const R_ATMLIB_CLData	*cldata,
	R_ATMLIB_CNNEVT_GROUP	event_group
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initialize local variables */
	ret = R_ATMLIB_E_OK;

	/* check the CL type */
	if( cldata->cltype != R_ATMLIB_CLTYPE_CNN )
	{
		/* set R_ATMLIB_E_NG_CLTYPE in return value */
		ret = R_ATMLIB_E_NG_CLTYPE;
	}
	/* check the CL size */
	else if( (cldata->size < 1U) || (cldata->size > R_ATMLIB_MAX_CL_LENGTH) )
	{
		/* set R_ATMLIB_E_NG_CLSIZE in return value */
		ret = R_ATMLIB_E_NG_CLSIZE;
	}
	/* check the addresses of CL */
	else if( (cldata->cur_addr == NULL) || (cldata->top_addr == NULL) || (cldata->cnn_data == NULL) )
	{
		/* set R_ATMLIB_E_NG in return value */
		ret = R_ATMLIB_E_NG;
	}
	else
	{
		/* check the CL state */
		ret = atmlib_CNNCheckStatus( cldata, event_group );
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckRemainCLSize
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				size							: Size to be written to the CL
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
 [ Function ]	Check the remain size of the CL.
 [ Note ]		none.
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckRemainCLSize(
	const R_ATMLIB_CLData	*cldata,
	uint32_t				size
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initialize local variables */
	ret	= R_ATMLIB_E_OK;

	/* check the remain size of CL */
	if( size > ((uint32_t)((uintptr_t)&cldata->top_addr[cldata->size] - (uintptr_t)cldata->cur_addr) / 4U) )
	{
		/* set R_ATMLIB_E_NG_CLSIZE in return value */
		ret = R_ATMLIB_E_NG_CLSIZE;
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckCurReg
 [ Params ]		current_reg						: The pointer to the current_reg
				dst_start_pos					: Destination start position of the current_reg (4 bytes unit)
				dst_length						: Length of the current_reg & the data to be compared
				data							: The pointer to the data to be compared
 [ Returns ]	0U								: No difference
				1U								: With difference
 [ Function ]	Check wheter the current regs (member current_reg of the cnn_data) has update.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC uint32_t atmlib_CNNCheckCurReg(
	const R_ATMLIB_CNNCurrentReg	*current_reg,
	uint16_t						dst_start_pos,
	uint16_t						dst_length,
	const uint32_t					*data
)
{
	uint32_t	ret;
	uint32_t	loop_cnt;

	/* initialize local variables */
	ret			= 0U;
	loop_cnt	= 0U;

	/* check difference the specified size  */
	for( loop_cnt = 0U; loop_cnt < dst_length; loop_cnt++ )
	{
		/* check difference between the current_reg and the data */
		if( ((current_reg[dst_start_pos + loop_cnt].attr & R_ATMLIB_CNN_REGCACHE_ENABLE) != R_ATMLIB_CNN_REGCACHE_ENABLE)	||		/* case of cache disable	*/
			((current_reg[dst_start_pos + loop_cnt].attr & R_ATMLIB_CNN_REGCACHE_SET)    != R_ATMLIB_CNN_REGCACHE_SET)		||		/* case of unset			*/
			 (current_reg[dst_start_pos + loop_cnt].value								 != data[loop_cnt]) )						/* case of with difference	*/
		{
			/* set 1 to the return code (case of with difference or cache is [disable / unset]) */
			ret = 1U;
			break;
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNUnsetCurReg
 [ Params ]		current_reg						: The pointer to the current_reg
				dst_start_pos					: Destination start position of the current_reg
				dst_length						: Length of the current_reg & the data to be compared
 [ Returns ]	none
 [ Function ]	Set the status "unsetting state" in current regs (member current_reg of the cnn_data).
 [ Note ]		none
******************************************************************************/
void atmlib_CNNUnsetCurReg(
    R_ATMLIB_CNNCurrentReg *current_reg, 
    uint16_t dst_start_pos, 
    uint16_t dst_length
)
{
	uint32_t	loop_cnt;

	/* initialize local variables */
	loop_cnt	= 0U;

	/* unset the current_reg from dst_start_pos to dst_start_pos + dst_length */
	for( loop_cnt = 0U; loop_cnt < dst_length; loop_cnt++ )
	{
		/* update the current_reg's attr */
		current_reg[dst_start_pos + loop_cnt].attr &= ~R_ATMLIB_CNN_REGCACHE_SET;
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNAddCLCommon
 [ Params ]		cldata							: Pointer to the CL info
				data							: Pointer to the write data to the CL
				data_num						: Number of the write data
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
 [ Function ]	Write the commands in the CL.(common processing part)
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNAddCLCommon(
	R_ATMLIB_CLData		*cldata,
	const uint32_t		*data,
	uint32_t			data_num
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				loop_cnt;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	loop_cnt	= 0U;

	/* check the remain size of the CL */
	ret = atmlib_CNNCheckRemainCLSize( cldata, data_num );
	/* check the result of atmlib_CNNCheckRemainCLSize() */
	if( ret == R_ATMLIB_E_OK )
	{
		/* write the data to CL  for the number of the data_num */
		for( loop_cnt = 0U; loop_cnt < data_num; loop_cnt++ )
		{
			/* write the data to CL & update the cur_addr */
			*(cldata->cur_addr) = data[loop_cnt];	/* write the data to CL	*/
			(cldata->cur_addr)++;					/* update the cur_addr	*/
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNAddCL_WPR
 [ Params ]		cldata							: Pointer to the CL info
				add								: Offset of writes register start address
				data							: Pointer to the write data to the CL
				data_num						: Number of the write data
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
 [ Function ]	Write the commands in the CL.(dedicated processing for WPR command)
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNAddCL_WPR(
	R_ATMLIB_CLData		*cldata,
	uint16_t			add,
	const uint32_t		*data,
	uint8_t				data_num
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				size_total;
	uint32_t				loop_cnt;
	uint32_t				add_flag;
	R_ATMLIB_CNNCurrentReg	*cur_reg;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	size_total	= 1U + (uint32_t)data_num;
	loop_cnt	= 0U;
	cur_reg		= cldata->cnn_data->current_reg;

	/* check the remain size of the CL */
	ret = atmlib_CNNCheckRemainCLSize( cldata, size_total );
	/* check the result of atmlib_CNNCheckRemainCLSize() */
	if( ret == R_ATMLIB_E_OK )
	{
		/* check addtion is required */
		add_flag = atmlib_CNNCheckCurReg( cur_reg, add, data_num, data );
		/* check if addition is required */
		if( add_flag == 1U )
		{
			/* write the opc to CL & update the cur_addr */
			*(cldata->cur_addr) = R_ATMLIB_CNN_OPC_WPR | ((uint32_t)add << 8U) | (uint32_t)data_num;
			(cldata->cur_addr)++;

			/* write the data to CL for the number of the data_num */
			for( loop_cnt = 0U; loop_cnt < data_num; loop_cnt++ )
			{
				/* write the data to CL & update the CL information */
				*(cldata->cur_addr)				= data[loop_cnt];				/* write the data to CL				*/
				cur_reg[add + loop_cnt].value	= data[loop_cnt];				/* write the data to current_reg	*/
				cur_reg[add + loop_cnt].attr	|= R_ATMLIB_CNN_REGCACHE_SET;	/* update current_reg's flag		*/
				(cldata->cur_addr)++;											/* update the cur_addr				*/
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNAddCL_WRI
 [ Params ]		cldata							: Pointer to the CL info
				dst_start_add					: Start address of destination register
				imm								: Immediate value
				dst_length						: Number of destination register
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
 [ Function ]	Append the WRI command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNAddCL_WRI(
	R_ATMLIB_CLData		*cldata,
	uint16_t			dst_start_add,
	int16_t				imm,
	uint8_t				dst_length
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				loop_cnt;
	uint32_t				add_flag;
	R_ATMLIB_CNNCurrentReg	*cur_reg;
	uint32_t                data[64];

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	loop_cnt	= 0U;
	add_flag	= 0U;
	cur_reg		= cldata->cnn_data->current_reg;
	(void)memset( data, 0U, sizeof(data) );

	/* write the imm to data arrary for the number of the dst_length */
	for( loop_cnt = 0U; loop_cnt < dst_length; loop_cnt++ )
	{
		/* update the data arrary */
		data[loop_cnt] = (uint32_t)imm;
	}

	/* check the remain size of the CL */
	ret = atmlib_CNNCheckRemainCLSize( cldata, 1U );
	/* check the result of atmlib_CNNCheckRemainCLSize() */
	if( ret == R_ATMLIB_E_OK )
	{
		/* check addtion is required */
		add_flag = atmlib_CNNCheckCurReg( cur_reg, dst_start_add, dst_length, data );
		/* check the requiring addtion */
		if( add_flag == 1U )
		{
			/* write the opc to CL & update the cur_addr, offset 0x2000 is added by HW automaticly */
			*(cldata->cur_addr)	= R_ATMLIB_CNN_OPC_WRI | (((uint32_t)dst_length & 0x0000003FU) << 18U) | (((uint32_t)(dst_start_add - 0x800) & 0x000001FFU) << 9U) | ((uint32_t)imm & 0x000001FFU) ;
			(cldata->cur_addr)++;

			/* write the data to CL for the number of the dst_length */
			for( loop_cnt = 0U; loop_cnt < (uint32_t)dst_length; loop_cnt++ )
			{
				/* update the CL information */
				cur_reg[(uint32_t)dst_start_add + loop_cnt].value	= (uint32_t)imm;				/* write the data to current_reg	*/
				cur_reg[(uint32_t)dst_start_add + loop_cnt].attr	|= R_ATMLIB_CNN_REGCACHE_SET;	/* update current_reg's flag		*/
			}
		}
	}

	return ret;
}

		
/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamDMAI
 [ Params ]		dmai_param						: DMAI parameters
 [ Returns ]	R_ATMLIB_E_OK					: on success 
				R_ATMLIB_E_NG_CH_KERNEL_MODE	: Member kernel_mode is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE		: Member data_mode is out of range
				R_ATMLIB_E_NG_COMB_SPMC_K333	: Combination of data_mode (R_ATMLIB_CNN_DATA_SPMC) & kernel_mode(R_ATMLIB_CNN_KERNEL_3_3_3) is  invalid
				R_ATMLIB_E_NG_COUNT				: Member param_count/ch_count is out of range
				R_ATMLIB_E_NG_COMB_CNT_SPMC		: Combination of param_count and data_mode (R_ATMLIB_CNN_DATA_SCMP) is invalid
				R_ATMLIB_E_NG_COMB_CNT_SCMP_KM	: Combination of param_count,data_mode (R_ATMLIB_CNN_DATA_SCMP) & kernel_mode(not R_ATMLIB_CNN_KERNEL_1_5_5) is  invalid
				R_ATMLIB_E_NG_CH_DIL_CONV		: Member dil_conv is out of range
				R_ATMLIB_E_NG_CH_CO				: Member channel_offset is out of range
				R_ATMLIB_E_NG_CH_IMG_FMT		: Member image_format is out of range
				R_ATMLIB_E_NG_CH_ID				: Member id is out of range
				R_ATMLIB_E_NG_CH_PTR			: Member ptr is out of range
				R_ATMLIB_E_NG_CH_IMG_STR		: Member image_stride is out of range
				R_ATMLIB_E_NG_CH_START_POS		: Member start_pos is out of range
				R_ATMLIB_E_NG_CH_PAD_L			: Member pad_l is out of range
				R_ATMLIB_E_NG_CH_PAD_R			: Member pad_r is out of range
				R_ATMLIB_E_NG_CH_PAD_T			: Member pad_t is out of range
				R_ATMLIB_E_NG_CH_PAD_B			: Member pad_b is out of range
				R_ATMLIB_E_NG_CH_STR_X			: Member stride_x is out of range
				R_ATMLIB_E_NG_CH_STR_Y			: Member stride_y is out of range
				R_ATMLIB_E_NG_CH_MAG_TYPE		: Member mag_type is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_X		: Member repeat_skip_x is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_y		: Member repeat_skip_y is out of range
				R_ATMLIB_E_NG_CH_IMG_X			: Member image_x is out of range
				R_ATMLIB_E_NG_CH_IMG_Y			: Member image_y is out of range
				R_ATMLIB_E_NG_CONV_XLEN			: ari_xlen is out of range or not all channels have the same ari_xlen
				R_ATMLIB_E_NG_CONV_YLEN			: ari_ylen is out of range or not all channels have the same ari_ylen
 [ Function ]	Check the members of the DMAI parameter (R_ATMLIB_CNNDMAIParam structure).
 [ Note ]		none.
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamDMAI(
	R_ATMLIB_CLData 			*cldata,
	const R_ATMLIB_CNNDMAIParam *dmai_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				cal_arilen_ret;
	uint8_t                 loop_cnt;
	uint16_t				ari_xlen[16];
	uint16_t				ari_ylen[16];

	/* initialize local variables */
	ret				= R_ATMLIB_E_OK;
	cal_arilen_ret 	= 0U;
	loop_cnt    	= 0U;
	(void)memset( ari_xlen, 0U, sizeof(ari_xlen) );
	(void)memset( ari_xlen, 0U, sizeof(ari_xlen) );

	/* check the member param_count of the dmai_param */
	if( (dmai_param->param_count < 1U) || 
		(dmai_param->param_count > R_ATMLIB_CNN_DMAI_MAX_CH)  
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member param_count of the dmai_param */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (dmai_param->param_count > R_ATMLIB_CNN_DMAI_MAX_CH / 2U)
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmai_param */
	else if( (dmai_param->ch_count < 1U) || 
			 (dmai_param->ch_count > R_ATMLIB_CNN_DMAI_MAX_CH)  
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmai_param */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (dmai_param->ch_count > R_ATMLIB_CNN_DMAI_MAX_CH / 2U)
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmai_param */
	else if( (dmai_param->param_count > 1U) && 
		     (dmai_param->param_count !=  dmai_param->ch_count) 
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member kernel_mode of the dmai_param */
	else if( (dmai_param->kernel_mode >= R_ATMLIB_CNN_KERNEL_END) || (dmai_param->kernel_mode == 3U) )
	{
		ret = R_ATMLIB_E_NG_CH_KERNEL_MODE;
	}
	/* check the member data_mode of the dmai_param */
	else if(dmai_param->data_mode >= R_ATMLIB_CNN_DATA_END)
	{
		ret =  R_ATMLIB_E_NG_CH_DATA_MODE;
	}
	/* check combination of kernel_mode and data_mode */
	else if( (dmai_param->data_mode == R_ATMLIB_CNN_DATA_SPMC) &&
			 (dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_3_3_3)
	)
	{
		ret = R_ATMLIB_E_NG_COMB_SPMC_K333;
	}
	/* check the member param_count of the dmai_param */
	else if( (dmai_param->data_mode == R_ATMLIB_CNN_DATA_SPMC) && 
			 (dmai_param->param_count != 1U)
	)
	{
		ret =  R_ATMLIB_E_NG_COMB_CNT_SPMC;
	}
	/* check combination of kernel_mode, data_mode and param_count */
	else if( (dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) && 
			 (dmai_param->kernel_mode != R_ATMLIB_CNN_KERNEL_1_5_5) &&
			 (dmai_param->param_count != 1U)
	)
	{
		ret =  R_ATMLIB_E_NG_COMB_CNT_SCMP_KM;
	}
	/* check the member dil_conv of the dmai_param */
	else if( dmai_param->dil_conv > R_ATMLIB_CNN_MAX_DILATED_DISTANCE )
	{
		ret =  R_ATMLIB_E_NG_CH_DIL_CONV;
	}
	/* check the member channel_offset of the dmai_param */
	else if( dmai_param->channel_offset > R_ATMLIB_CNN_MAX_CH_OFST )
	{
		ret =  R_ATMLIB_E_NG_CH_CO;
	}
	/* check the member channel_offset of the dmai_param */
	else if( (dmai_param->param_count == 1U) && 
			 (dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			 (dmai_param->channel_offset == 0U)
	)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	else
	{
		/* check whether the data_mode is SCMP */
		if (dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* loop for the member ch_count of the dmai_param */
			for( loop_cnt = 0U; loop_cnt < dmai_param->ch_count; loop_cnt++ )
			{
				/* check the member id of the dmai_param */
				if( (dmai_param->ch_id[loop_cnt] < R_ATMLIB_CNN_DMAI0)  ||
					(dmai_param->ch_id[loop_cnt] > R_ATMLIB_CNN_DMAI15)
				)
				{
					ret =  R_ATMLIB_E_NG_CH_ID;
				}
				/* check the member id of the dmai_param */
				else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
						(dmai_param->ch_id[loop_cnt] % 2U != 0)
				)
				{
					ret =  R_ATMLIB_E_NG_CH_ID;
				}
				else
				{
					/* DO NOTHING */
				}
			}
		}
		/* check whether the ret is R_ATMLIB_E_OK */
		if (ret == R_ATMLIB_E_OK)
		{
			/* loop for the member param_count of the dmai_param */
			for( loop_cnt = 0U; loop_cnt < dmai_param->param_count; loop_cnt++ )
			{
				/* check the member image_format of the dmai_param */
				if(dmai_param->dmai_ch[loop_cnt].image_format >= R_ATMLIB_IMG_END)
				{
					ret =  R_ATMLIB_E_NG_CH_IMG_FMT;
				}
				/* check the member ptr of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].ptr == 0U) ||
						 (dmai_param->dmai_ch[loop_cnt].ptr == R_ATMLIB_CNN_MEM_INVALID) 
				)
				{
					ret = R_ATMLIB_E_NG_CH_PTR;
				}
				/* check the member image_stride of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].image_stride < R_ATMLIB_CNN_MIN_IMG_STRIDE )
				{
					ret =  R_ATMLIB_E_NG_CH_IMG_STR;
				}
				/* check the member start_pos of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].start_pos > R_ATMLIB_CNN_MAX_START_POS )
				{
					ret =  R_ATMLIB_E_NG_CH_START_POS;
				}
				/* check the member pad_l of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].pad_l > R_ATMLIB_CNN_MAX_PADLR )
				{
					ret =  R_ATMLIB_E_NG_CH_PAD_L;
				}
				/* check the member pad_r of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].pad_r > R_ATMLIB_CNN_MAX_PADLR )
				{
					ret =  R_ATMLIB_E_NG_CH_PAD_R;
				}
				/* check the member pad_t of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].pad_t > R_ATMLIB_CNN_MAX_PADTB )
				{
					ret =  R_ATMLIB_E_NG_CH_PAD_T;
				}
				/* check the member pad_b of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].pad_b > R_ATMLIB_CNN_MAX_PADTB )
				{
					ret =  R_ATMLIB_E_NG_CH_PAD_B;
				}
				/* check the member stride_x of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].stride_x < R_ATMLIB_CNN_MIN_CONV_STRIDE) || 
						(dmai_param->dmai_ch[loop_cnt].stride_x > R_ATMLIB_CNN_MAX_CONV_STRIDE)
				)
				{
					ret =  R_ATMLIB_E_NG_CH_STR_X;
				}
				/* check the member stride_y of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].stride_y < R_ATMLIB_CNN_MIN_CONV_STRIDE) || 
						(dmai_param->dmai_ch[loop_cnt].stride_y > R_ATMLIB_CNN_MAX_CONV_STRIDE)
				)
				{
					ret =  R_ATMLIB_E_NG_CH_STR_Y;
				}
				/* check the member mag_type of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].mag_type >= R_ATMLIB_CNN_MAG_END )
				{
					ret =  R_ATMLIB_E_NG_CH_MAG_TYPE;
				}
				/* check the member repeat_skip_x of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].repeat_skip_x > R_ATMLIB_CNN_MAX_RPT_SKP )
				{
					ret =  R_ATMLIB_E_NG_CH_RPT_SKP_X;
				}
				/* check the member repeat_skip_x of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG) && 
						(dmai_param->dmai_ch[loop_cnt].repeat_skip_x != R_ATMLIB_CNN_MIN_RPT_SKP )
				)
				{
					ret =  R_ATMLIB_E_NG_CH_RPT_SKP_X;
				}
				/* check the member repeat_skip_y of the dmai_param */
				else if( dmai_param->dmai_ch[loop_cnt].repeat_skip_y > R_ATMLIB_CNN_MAX_RPT_SKP )
				{
					ret =  R_ATMLIB_E_NG_CH_RPT_SKP_Y;
				}
				/* check the member repeat_skip_y of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG) && 
						(dmai_param->dmai_ch[loop_cnt].repeat_skip_y != R_ATMLIB_CNN_MIN_RPT_SKP )
				)
				{
					ret =  R_ATMLIB_E_NG_CH_RPT_SKP_Y;
				}
				/* check the member image_x of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].image_x < R_ATMLIB_CNN_MIN_WIDTH) ||
						((dmai_param->dmai_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG)    && (dmai_param->dmai_ch[loop_cnt].image_x > R_ATMLIB_CNN_MAX_WIDTH)) ||
						((dmai_param->dmai_ch[loop_cnt].mag_type == R_ATMLIB_CNN_SKP)      && (dmai_param->dmai_ch[loop_cnt].image_x > (R_ATMLIB_CNN_MAX_WIDTH * ((uint16_t)dmai_param->dmai_ch[loop_cnt].repeat_skip_x + 1U))))	||
						((dmai_param->dmai_ch[loop_cnt].mag_type >= R_ATMLIB_CNN_RPT_ZERO) && (dmai_param->dmai_ch[loop_cnt].image_x > (R_ATMLIB_CNN_MAX_WIDTH / ((uint16_t)dmai_param->dmai_ch[loop_cnt].repeat_skip_x + 1U)))) )
				{
					ret =  R_ATMLIB_E_NG_CH_IMG_X;
				}
				/* check the member image_y of the dmai_param */
				else if( (dmai_param->dmai_ch[loop_cnt].image_y < R_ATMLIB_CNN_MIN_HEIGHT) ||
						((dmai_param->dmai_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG)    && (dmai_param->dmai_ch[loop_cnt].image_y > R_ATMLIB_CNN_MAX_HEIGHT )) ||
						((dmai_param->dmai_ch[loop_cnt].mag_type == R_ATMLIB_CNN_SKP)      && (dmai_param->dmai_ch[loop_cnt].image_y > (R_ATMLIB_CNN_MAX_HEIGHT * ((uint16_t)dmai_param->dmai_ch[loop_cnt].repeat_skip_y + 1U)))) ||
						((dmai_param->dmai_ch[loop_cnt].mag_type >= R_ATMLIB_CNN_RPT_ZERO) && (dmai_param->dmai_ch[loop_cnt].image_y > (R_ATMLIB_CNN_MAX_HEIGHT / ((uint16_t)dmai_param->dmai_ch[loop_cnt].repeat_skip_y + 1U)))) )
				{
					ret =  R_ATMLIB_E_NG_CH_IMG_Y;
				}
				else
				{
					/**
					Redmine #248902 
					ARI_LEN must be always for all channels the same. 
					It is user responsibility if using slow setup mode for the DMAI parameters 
					to guarantee that the resulting ARI_LEN is same. 
					Otherwise, the architecture behavior is undefined and can hang up.
					*/

					/* calcaulate the ARI channel width and ARI channel height */
					cal_arilen_ret = atmlib_CNNCalculateARILen(dmai_param->dmai_ch[loop_cnt], dmai_param->kernel_mode, dmai_param->dil_conv, ari_xlen + loop_cnt, ari_ylen + loop_cnt);
					/* check ARI channel width -- ari_xlen */
					if( (cal_arilen_ret == 1U) ||
						(ari_xlen[0] != ari_xlen[loop_cnt]) || 
						(ari_xlen[loop_cnt] > R_ATMLIB_CNN_MAX_WIDTH)
					)
					{
						ret =  R_ATMLIB_E_NG_CONV_XLEN;
					}
					/* check ARI channel height */
					else if((cal_arilen_ret == 2U) ||
							(ari_ylen[0] != ari_ylen[loop_cnt]) || 
							(ari_ylen[loop_cnt] > R_ATMLIB_CNN_MAX_HEIGHT)
					)
					{
						ret =  R_ATMLIB_E_NG_CONV_YLEN;
					}
					else
					{
						/* update the channel_props for DMAI (ARI_LEN) */
						cldata->cnn_data->channel_props.ARI_LEN = (((uint32_t)ari_xlen[0])            & 0x000000FFU)	|
																  (((uint32_t)ari_ylen[0]    <<  16U) & 0x0FFF0000U);
					}
				}
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamDMAC
 [ Params ]		dmac_param  					: DMAC parameters
 [ Returns ]	R_ATMLIB_E_OK					: on success 
				R_ATMLIB_E_NG_CH_DATA_MODE		: Member kernel_mode is out of range
				R_ATMLIB_E_NG_CH_CO				: Member channel_offset is out of range
				R_ATMLIB_E_NG_COUNT				: Member param_count/ch_count is out of range
				R_ATMLIB_E_NG_COMB_CNT_SPMC		: Combination of param_count and data_mode(R_ATMLIB_CNN_DATA_SCMP) is invalid
				R_ATMLIB_E_NG_CH_IMG_FMT		: Member image_format is out of range
				R_ATMLIB_E_NG_CH_ID				: Member id is out of range
				R_ATMLIB_E_NG_CH_PTR			: Member ptr is out of range
				R_ATMLIB_E_NG_CH_IMG_STR		: Member image_stride is out of range
				R_ATMLIB_E_NG_CH_MAG_TYPE		: Member mag_type is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_X		: Member repeat_skip_x is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_Y		: Member repeat_skip_y is out of range
				R_ATMLIB_E_NG_CH_IMG_X			: Member image_x is out of range
				R_ATMLIB_E_NG_CH_IMG_Y			: Member image_y is out of range
 [ Function ]	Check the members of the DMAC parameter (R_ATMLIB_CNNDMACParam structure).
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamDMAC(
	R_ATMLIB_CLData 			*cldata,
	const R_ATMLIB_CNNDMACParam *dmac_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint8_t loop_cnt;

	/* initialize local variables */
	ret	     = R_ATMLIB_E_OK;
	loop_cnt = 0U;

	/* check the member param_count of the dmac_param */
	if( (dmac_param->param_count < 1U) ||
		(dmac_param->param_count > R_ATMLIB_CNN_DMAC_MAX_CH)
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member param_count of the dmac_param */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (dmac_param->param_count > R_ATMLIB_CNN_DMAC_MAX_CH / 2U)
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmac_param */
	else if( (dmac_param->ch_count < 1U) ||
			 (dmac_param->ch_count > R_ATMLIB_CNN_DMAC_MAX_CH)
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmac_param */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (dmac_param->ch_count > R_ATMLIB_CNN_DMAC_MAX_CH / 2U)
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmac_param */
	else if( (dmac_param->param_count > 1U) && 
		     (dmac_param->param_count !=  dmac_param->ch_count) 
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member data_mode of the dmac_param */
	else if( dmac_param->data_mode >= R_ATMLIB_CNN_DATA_END )
	{
		ret = R_ATMLIB_E_NG_CH_DATA_MODE;
	}
	/* check the member param_count of the dmac_param */
	else if( (dmac_param->data_mode == R_ATMLIB_CNN_DATA_SPMC) && 
			 (dmac_param->param_count != 1U)
	)
	{
		ret = R_ATMLIB_E_NG_COMB_CNT_SPMC;
	}
	/* check the member channel_offset of the dmac_param */
	else if( dmac_param->channel_offset > R_ATMLIB_CNN_MAX_CH_OFST )
	{
		ret =  R_ATMLIB_E_NG_CH_CO;
	}
	/* check the member channel_offset of the dmac_param */
	else if( (dmac_param->param_count == 1U) && 
			 (dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			 (dmac_param->channel_offset == 0U)
	)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	else
	{
		/* check whether the data_mode is SCMP */
		if (dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* loop for the member ch_count of the dmac_param */
			for( loop_cnt = 0U; loop_cnt < dmac_param->ch_count; loop_cnt++ )
			{
				/* check the member id of the dmac_param */
				if( (dmac_param->ch_id[loop_cnt] < R_ATMLIB_CNN_DMAC0)  ||
					(dmac_param->ch_id[loop_cnt] > R_ATMLIB_CNN_DMAC31)
				)
				{
					ret = R_ATMLIB_E_NG_CH_ID;
				}
				/* check the member id of the dmac_param */
				else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) &&
						(dmac_param->ch_id[loop_cnt] % 2U != 0)
				)
				{
					ret = R_ATMLIB_E_NG_CH_ID;
				}
				else
				{
					/* DO NOTHING */
				}
			}
		}

		/* check whether the ret is R_ATMLIB_E_OK */
		if (ret == R_ATMLIB_E_OK)
		{
			/* loop for the member param_count of the dmac_param */
			for( loop_cnt = 0U; loop_cnt < dmac_param->param_count; loop_cnt++ )
			{
				/* check the member image_format of the dmac_param */
				if(dmac_param->dmac_ch[loop_cnt].image_format >= R_ATMLIB_IMG_END)
				{
					ret = R_ATMLIB_E_NG_CH_IMG_FMT;
				}
				/* check the member ptr of the dmac_param */
				else if((dmac_param->dmac_ch[loop_cnt].ptr == 0U) || 
						(dmac_param->dmac_ch[loop_cnt].ptr == R_ATMLIB_CNN_MEM_INVALID)
				)
				{
					ret = R_ATMLIB_E_NG_CH_PTR;
				}
				/* check the member image_stride of the dmac_param */
				else if( dmac_param->dmac_ch[loop_cnt].image_stride < R_ATMLIB_CNN_MIN_IMG_STRIDE ) 
				{
					ret = R_ATMLIB_E_NG_CH_IMG_STR;
				}
				/* check the member mag_type of the dmac_param */
				else if( dmac_param->dmac_ch[loop_cnt].mag_type >= R_ATMLIB_CNN_MAG_END )
				{
					ret = R_ATMLIB_E_NG_CH_MAG_TYPE;
				}
				/* check the member repeat_skip_x of the dmac_param */
				else if( dmac_param->dmac_ch[loop_cnt].repeat_skip_x > R_ATMLIB_CNN_MAX_RPT_SKP )
				{
					ret = R_ATMLIB_E_NG_CH_RPT_SKP_X;
				}
				/* check the member repeat_skip_x of the dmac_param */
				else if( (dmac_param->dmac_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG) && 
						(dmac_param->dmac_ch[loop_cnt].repeat_skip_x != R_ATMLIB_CNN_MIN_RPT_SKP )
				)
				{
					ret =  R_ATMLIB_E_NG_CH_RPT_SKP_X;
				}
				/* check the member repeat_skip_y of the dmac_param */
				else if( dmac_param->dmac_ch[loop_cnt].repeat_skip_y > R_ATMLIB_CNN_MAX_RPT_SKP )
				{
					ret = R_ATMLIB_E_NG_CH_RPT_SKP_Y;
				}
				/* check the member repeat_skip_y of the dmac_param */
				else if( (dmac_param->dmac_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG) && 
						(dmac_param->dmac_ch[loop_cnt].repeat_skip_y != R_ATMLIB_CNN_MIN_RPT_SKP )
				)
				{
					ret =  R_ATMLIB_E_NG_CH_RPT_SKP_Y;
				}
				/* check the member image_x of the dmac_param */
				else if( ( dmac_param->dmac_ch[loop_cnt].image_x < R_ATMLIB_CNN_MIN_WIDTH) ||
						((dmac_param->dmac_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG)    && (dmac_param->dmac_ch[loop_cnt].image_x > R_ATMLIB_CNN_MAX_WIDTH)) ||
						((dmac_param->dmac_ch[loop_cnt].mag_type == R_ATMLIB_CNN_SKP)      && (dmac_param->dmac_ch[loop_cnt].image_x > (R_ATMLIB_CNN_MAX_WIDTH * ((uint16_t)dmac_param->dmac_ch[loop_cnt].repeat_skip_x + 1U))))	||
						((dmac_param->dmac_ch[loop_cnt].mag_type >= R_ATMLIB_CNN_RPT_ZERO) && (dmac_param->dmac_ch[loop_cnt].image_x > (R_ATMLIB_CNN_MAX_WIDTH / ((uint16_t)dmac_param->dmac_ch[loop_cnt].repeat_skip_x + 1U)))) )
				{
					ret = R_ATMLIB_E_NG_CH_IMG_X;
				}
				/* check the member image_y of the dmac_param */
				else if( (dmac_param->dmac_ch[loop_cnt].image_y < R_ATMLIB_CNN_MIN_HEIGHT) ||
						((dmac_param->dmac_ch[loop_cnt].mag_type == R_ATMLIB_CNN_NOMAG)    && (dmac_param->dmac_ch[loop_cnt].image_y > R_ATMLIB_CNN_MAX_HEIGHT )) ||
						((dmac_param->dmac_ch[loop_cnt].mag_type == R_ATMLIB_CNN_SKP)      && (dmac_param->dmac_ch[loop_cnt].image_y > (R_ATMLIB_CNN_MAX_HEIGHT * ((uint16_t)dmac_param->dmac_ch[loop_cnt].repeat_skip_y + 1U))))	||
						((dmac_param->dmac_ch[loop_cnt].mag_type >= R_ATMLIB_CNN_RPT_ZERO) && (dmac_param->dmac_ch[loop_cnt].image_y > (R_ATMLIB_CNN_MAX_HEIGHT / ((uint16_t)dmac_param->dmac_ch[loop_cnt].repeat_skip_y + 1U)))) )
				{
					ret = R_ATMLIB_E_NG_CH_IMG_Y;
				}
				else
				{
					/* DO NOTHING */
				}
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamDMAO
 [ Params ]		dmao_param						: DMAO parameters
 [ Returns ]	R_ATMLIB_E_OK					: on success 
				R_ATMLIB_E_NG_CH_KERNEL_MODE	: Member kernel_mode is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE		: Member data_mode is out of range
				R_ATMLIB_E_NG_CH_CO				: Member channel_offset is out of range
				R_ATMLIB_E_NG_CH_IMG_FMT		: Member image_format is out of range
				R_ATMLIB_E_NG_CH_IMG_STR		: Member image_stride is out of range
				R_ATMLIB_E_NG_CH_IMG_X			: Member image_x is out of range
				R_ATMLIB_E_NG_CH_IMG_Y			: Member image_y is out of range
				R_ATMLIB_E_NG_COUNT				: Member param_count/ch_count is out of range
				R_ATMLIB_E_NG_COMB_CNT_SPMC		: Combination of param_count and data_mode(R_ATMLIB_CNN_DATA_SCMP) is invalid
				R_ATMLIB_E_NG_CH_ID				: Member id is out of range
				R_ATMLIB_E_NG_CH_PTR			: Member ptr is out of range
 [ Function ]	Check the members of the DMAO parameter (R_ATMLIB_CNNDMAOParam structure).
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamDMAO(
	R_ATMLIB_CLData 			*cldata,
	const R_ATMLIB_CNNDMAOParam *dmao_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint8_t loop_cnt;

	/* initialize local variables */
	ret	     = R_ATMLIB_E_OK;
	loop_cnt = 0U;

	/* check the member ch_count of the dmao_param */
	if( (dmao_param->ch_count < 1U) ||
		(dmao_param->ch_count > R_ATMLIB_CNN_DMAO_MAX_CH)
	) 
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmao_param */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (dmao_param->ch_count > R_ATMLIB_CNN_DMAO_MAX_CH / 2U)
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member param_count of the dmao_param */
	else if( (dmao_param->param_count < 1U) ||
			 (dmao_param->param_count > R_ATMLIB_CNN_DMAO_MAX_CH)
	) 
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member param_count of the dmao_param */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (dmao_param->param_count > R_ATMLIB_CNN_DMAO_MAX_CH / 2U)
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_count of the dmao_param */
	else if( (dmao_param->param_count > 1U) && 
		     (dmao_param->param_count !=  dmao_param->ch_count) 
	)
	{
		ret =  R_ATMLIB_E_NG_COUNT;
	}
	/* check the member next_layer_kernel_mode of the dmao_param */
	else if( dmao_param->next_layer_kernel_mode >= R_ATMLIB_CNN_KERNEL_END )
	{
		ret = R_ATMLIB_E_NG_CH_KERNEL_MODE;
	}
	/* check the member data_mode of the dmao_param */
	else if( dmao_param->data_mode >= R_ATMLIB_CNN_DATA_END )
	{
		ret = R_ATMLIB_E_NG_CH_DATA_MODE;
	}
	/* check the combination of param_count and data_mode */
    else if( (dmao_param->data_mode == R_ATMLIB_CNN_DATA_SPMC) && 
	    	 (dmao_param->param_count != 1U)
	)
	{
		ret = R_ATMLIB_E_NG_COMB_CNT_SPMC;
	}
	/* check the member channel_offset of the dmao_param */
	else if( dmao_param->channel_offset > R_ATMLIB_CNN_MAX_CH_OFST)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	/* check the member channel_offset of the dmao_param */
	else if( (dmao_param->param_count == 1U) && 
			 (dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			 (dmao_param->channel_offset == 0U)
	)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	/* check the member image_format of the dmao_param */
	else if( dmao_param->image_format >= R_ATMLIB_IMG_END )
	{
		ret = R_ATMLIB_E_NG_CH_IMG_FMT;
	}
	/* check the member image_stride of the dmao_param */
	else if( dmao_param->image_stride < R_ATMLIB_CNN_MIN_IMG_STRIDE )
	{
		ret = R_ATMLIB_E_NG_CH_IMG_STR;
	}
	/* check the member image_x of the dmao_param */
	else if( (dmao_param->image_x < R_ATMLIB_CNN_MIN_WIDTH) || 
			 (dmao_param->image_x > R_ATMLIB_CNN_MAX_WIDTH) 
	)
	{
		ret = R_ATMLIB_E_NG_CH_IMG_X;
	}
	/* check the member image_y of the dmao_param */
	else if( (dmao_param->image_y < R_ATMLIB_CNN_MIN_HEIGHT) || 
			 (dmao_param->image_y > R_ATMLIB_CNN_MAX_HEIGHT)
	)
	{
		ret = R_ATMLIB_E_NG_CH_IMG_Y;
	}
	else
	{
		/* check whether the data_mode is SCMP */
		if (dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* loop for the member ch_count of the dmao_param */
			for( loop_cnt = 0U; loop_cnt < dmao_param->ch_count; loop_cnt++ )
			{
				/* check the member id of the dmao_param */
				if( (dmao_param->ch_id[loop_cnt] < R_ATMLIB_CNN_DMAO0)  ||
					(dmao_param->ch_id[loop_cnt] > R_ATMLIB_CNN_DMAO31)
				)
				{
					ret = R_ATMLIB_E_NG_CH_ID;
				}
				/* check the member id of the dmao_param */
				else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) &&
						(dmao_param->ch_id[loop_cnt] % 2U != 0)
				)
				{
					ret = R_ATMLIB_E_NG_CH_ID;
				}
				else
				{
					/* DO NOTHING */
				}
			}
		}

		/* check whether the ret is R_ATMLIB_E_OK */
		if (ret == R_ATMLIB_E_OK)
		{
			/* loop for the member param_count of the dmao_param */
			for( loop_cnt = 0U; loop_cnt < dmao_param->param_count; loop_cnt++ )
			{
				/* check the member ptr of the dmao_param */
				if( (dmao_param->ptr[loop_cnt] == 0U) ||
					(dmao_param->ptr[loop_cnt] == R_ATMLIB_CNN_MEM_INVALID)
				) 
				{
					ret = R_ATMLIB_E_NG_CH_PTR;
				}/* do nothing for else branch */
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamARI
 [ Params ]     ari_param							: ARI parameters
 				channel_info						: channel statistic info
 [ Returns ]	R_ATMLIB_E_OK					    : on success
				R_ATMLIB_E_NG_CONV_ARI_FMT			: Member next_ari_format/ari_format of ari_param is out of range
				R_ATMLIB_E_NG_CONV_ACTI_MODE		: Member acti_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_LOWER_VAL		: Member lower_bound of ari_param is out of range
				R_ATMLIB_E_NG_CONV_MULT_VAL			: Member mult_val of ari_param is out of range
				R_ATMLIB_E_NG_CONV_LUT_LIN_INT		: Member lut_lin_int of ari_param is out of range
				R_ATMLIB_E_NG_CONV_PAD_MODE			: Member pad_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_BIAS				: Member bias_en is of ari_param out of range
				R_ATMLIB_E_NG_CONV_XYPOOL_MODE		: Member xypool_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_XPOOL			: Member xpool of ari_param is out of range
				R_ATMLIB_E_NG_CONV_YPOOL			: Member ypool of ari_param is out of range
				R_ATMLIB_E_NG_CONV_CPOOL_MODE		: Member cpool_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_LABEL_INFO		: Member label_info of ari_param is out of range
				R_ATMLIB_E_NG_CONV_MAXOUT_GROUP		: Member maxout_group of ari_param is out of range
				R_ATMLIB_E_NG_CONV_SFTMRM			: Member sftm_round_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_CPOOLUI			: Member cpool_ui of ari_param is out of range
				R_ATMLIB_E_NG_COUNT					: Member ari_cnt of ari_param is out of range
				R_ATMLIB_E_NG_CONV_CHGLOBSUM		: Member glob_sum of channel_info is out of range
				R_ATMLIB_E_NG_CONV_CHMAXMIN			: Member channel_max_min of channel_info is out of range
				R_ATMLIB_E_NG_CH_ID					: Member ch_id of ari_param is out of range
 [ Function ]	Check the members of the ari parameter and channel info (R_ATMLIB_CNNARIParam structure and R_ATMLIB_CNNChannelStatistic structure)
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamARI(
	 const R_ATMLIB_CNNARIParam *ari_param,
	 const R_ATMLIB_CNNChannelStatistic *channel_info
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint8_t loop_cnt;

	/* initialize local variables */
	ret	     = R_ATMLIB_E_OK;
	loop_cnt = 0U;

	/* check the member ari_format of the ari_param */
	if( ari_param->ari_format >= R_ATMLIB_CNN_ARIFORMAT_END )
	{
		ret = R_ATMLIB_E_NG_CONV_ARI_FMT;
	}
	/* check the member acti_mode of the ari_param */
	else if( ari_param->acti_mode >= R_ATMLIB_CNN_ACTFUNC_END )
	{
		ret = R_ATMLIB_E_NG_CONV_ACTI_MODE;
	}
	/* check the member lower_bound of the ari_param */
	else if( ari_param->lower_bound > R_ATMLIB_CNN_MAX_LOWERBOUND )
	{
		ret = R_ATMLIB_E_NG_CONV_LOWER_VAL;
	}
	/* check the member mult_val of the ari_param */
	else if( ari_param->mult_val == R_ATMLIB_CNN_MULT_VAL_INVALID )
	{
		ret = R_ATMLIB_E_NG_CONV_MULT_VAL;
	}
	/* check the member lut_lin_int of the ari_param */
	else if( ari_param->lut_lin_int >= R_ATMLIB_CNN_ENABLECONTROL_END )
	{
		ret = R_ATMLIB_E_NG_CONV_LUT_LIN_INT;
	}
	/* check the member pad_mode of the ari_param */
	else if( ari_param->pad_mode >= R_ATMLIB_CNN_PAD_END )
	{
		ret = R_ATMLIB_E_NG_CONV_PAD_MODE;
	}
	/* check the member bias_en of the ari_param */
	else if( ari_param->bias_en >= R_ATMLIB_CNN_ENABLECONTROL_END )
	{
		ret = R_ATMLIB_E_NG_CONV_BIAS;
	}
	/* check the member xypool_mode of the ari_param */
	else if( ari_param->xypool_mode >= R_ATMLIB_CNN_XYPOOL_END )
	{
		ret = R_ATMLIB_E_NG_CONV_XYPOOL_MODE;
	}
	/* check the member xpool of the ari_param */
	else if( (ari_param->xpool < R_ATMLIB_CNN_MIN_XYPOOL) || 
			 (ari_param->xpool > R_ATMLIB_CNN_MAX_XYPOOL) 
	)
	{
		ret = R_ATMLIB_E_NG_CONV_XPOOL;
	}
	/* check the member ypool of the ari_param */
	else if( (ari_param->ypool < R_ATMLIB_CNN_MIN_XYPOOL) || 
		     (ari_param->ypool > R_ATMLIB_CNN_MAX_XYPOOL) 
	)
	{
		ret = R_ATMLIB_E_NG_CONV_YPOOL;
	}
	/* check the member cpool_mode of the ari_param */
	else if( ari_param->cpool_mode >= R_ATMLIB_CNN_CPOOL_END )
	{
		ret = R_ATMLIB_E_NG_CONV_CPOOL_MODE;
	}
	/* check the member label_info of the ari_param */
	else if( ari_param->label_info >= R_ATMLIB_CNN_LABELBIT_END )
	{
		ret = R_ATMLIB_E_NG_CONV_LABEL_INFO;
	}
	/* check the member maxout_group of the ari_param */
	else if( ari_param->maxout_group >= R_ATMLIB_CNN_MAXGROUP_END )
	{
		ret = R_ATMLIB_E_NG_CONV_MAXOUT_GROUP;
	}
	/* check the member next_ari_format of the ari_param */
	else if( ari_param->next_ari_format >= R_ATMLIB_CNN_ARIFORMAT_END )
	{
		ret = R_ATMLIB_E_NG_CONV_ARI_FMT;
	}
	/* check the member ari_cnt of the ari_param */
	else if( ari_param->sftm_round_mode >= R_ATMLIB_CNN_ENABLECONTROL_END )
	{
		ret = R_ATMLIB_E_NG_CONV_SFTMRM;
	}
	/* check the member cpool_ui of the ari_param */
	else if( ari_param->cpool_ui >= R_ATMLIB_CNN_CPOOLUI_END )
	{
		ret = R_ATMLIB_E_NG_CONV_CPOOLUI;
	}
	/* check the member ari_cnt of the ari_param */
	else if( (ari_param->ari_cnt < 1U) ||
			 (ari_param->ari_cnt > R_ATMLIB_CNN_ARI_MAX_CH)
	) 
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ari_cnt of the ari_param */
	else if( (ari_param->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (ari_param->ari_cnt > R_ATMLIB_CNN_ARI_MAX_CH / 2U )
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member glob_sum of the channel_info */
	else if( (channel_info != NULL) && (channel_info->glob_sum >= R_ATMLIB_CNN_ENABLECONTROL_END) )
	{
		ret = R_ATMLIB_E_NG_CONV_CHGLOBSUM;
	}
	/* check the member channel_max_min of the channel_info */
	else if( (channel_info != NULL) && (channel_info->channel_max_min >= R_ATMLIB_CNN_ENABLECONTROL_END) )
	{
		ret = R_ATMLIB_E_NG_CONV_CHMAXMIN;
	}
	else
	{
		/* loop for the member ari_cnt of the ari_param */
		for( loop_cnt = 0U; loop_cnt < ari_param->ari_cnt; loop_cnt++ )
		{
			/* check the member ch_id of the ari_param */
			if( (ari_param->ch_id[loop_cnt] < R_ATMLIB_CNN_ARI0)  ||
				(ari_param->ch_id[loop_cnt] > R_ATMLIB_CNN_ARI31)
			)
			{
				ret = R_ATMLIB_E_NG_CH_ID;
			}
			/* check the member ch_id of the ari_param */
			else if( (ari_param->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			         (ari_param->ch_id[loop_cnt]  % 2U != 0)
			)
			{
				ret = R_ATMLIB_E_NG_CH_ID;
			}
			else
			{
				/* DO NOTHING */
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNJudgeSetupModeDMAI
 [ Params ]		cldata					: The pointer to the R_ATMLIB_CLData structure
				dmai_ch 				: DMAI channel parameters
				param_count				: DMAI parameter count
 [ Returns ]	none
 [ Function ]	Judge setup mode(fast or slow) for DMAI
 [ Note ]		none
******************************************************************************/
void atmlib_CNNJudgeSetupModeDMAI(
	R_ATMLIB_CLData 			  *cldata,
	const R_ATMLIB_CNNDMAIChParam *dmai_ch,
	uint8_t						  param_count
)
{
	uint8_t loop_cnt;

	/* initialize local variables */
	loop_cnt 		= 1U;

	/* loop for the member param_count */
	for( loop_cnt = 1U; loop_cnt < param_count; loop_cnt++ )
	{
		/* check wether all the image_x values are the same */
		if( dmai_ch[0].image_x != dmai_ch[loop_cnt].image_x )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the image_y values are the same */
		if( dmai_ch[0].image_y != dmai_ch[loop_cnt].image_y )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the image_stride values are the same */
		if( dmai_ch[0].image_stride != dmai_ch[loop_cnt].image_stride )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the mag_type values are the same */
		if( dmai_ch[0].mag_type != dmai_ch[loop_cnt].mag_type )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the repeat_skip_x values are the same */
		if( dmai_ch[0].repeat_skip_x != dmai_ch[loop_cnt].repeat_skip_x )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the repeat_skip_y values are the same */
		if( dmai_ch[0].repeat_skip_y != dmai_ch[loop_cnt].repeat_skip_y )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the image_format values are the same */
		if( dmai_ch[0].image_format != dmai_ch[loop_cnt].image_format )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the start_pos values are the same */
		if( dmai_ch[0].start_pos != dmai_ch[loop_cnt].start_pos )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the stride_x values are the same */
		if( dmai_ch[0].stride_x != dmai_ch[loop_cnt].stride_x )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the stride_y values are the same */
		if( dmai_ch[0].stride_y != dmai_ch[loop_cnt].stride_y )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the pad_b values are the same */
		if( dmai_ch[0].pad_b != dmai_ch[loop_cnt].pad_b )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the pad_t values are the same */
		if( dmai_ch[0].pad_t != dmai_ch[loop_cnt].pad_t )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the pad_l values are the same */
		if( dmai_ch[0].pad_l != dmai_ch[loop_cnt].pad_l )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the pad_r values are the same */
		if( dmai_ch[0].pad_r != dmai_ch[loop_cnt].pad_r )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the data_fracs values are the same */
		if( dmai_ch[0].dmai_frac != dmai_ch[loop_cnt].dmai_frac )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether the latter ptr is smaller than the former ptr */
		if( dmai_ch[loop_cnt].ptr < dmai_ch[loop_cnt - 1].ptr )
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether the channel offsets are sequence of equal differences
		and difference between two adjacent channel offsets should be the multi of 4kB(0x1000) and max is 4MB(0x00400000) */
		if( (dmai_ch[1].ptr - dmai_ch[0].ptr) != (dmai_ch[loop_cnt].ptr - dmai_ch[loop_cnt - 1U].ptr) ||
			((dmai_ch[loop_cnt].ptr - dmai_ch[loop_cnt - 1U].ptr) % 0x1000U != 0) ||
			((dmai_ch[loop_cnt].ptr - dmai_ch[loop_cnt - 1U].ptr) > 0x400000)
		)
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether all the baddr_state are the same */
		if( cldata->cnn_data->baddr_info.baddr_dmai[0].baddr_state != cldata->cnn_data->baddr_info.baddr_dmai[loop_cnt].baddr_state)
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether all the offest address are the same */
		if( (cldata->cnn_data->baddr_info.baddr_dmai[0].baddr_state == R_ATMLIB_CNN_BADDRSTAT_SET1ST) &&
			((dmai_ch[0].ptr - cldata->cnn_data->baddr_info.baddr_dmai[0].baddr) != (dmai_ch[loop_cnt].ptr - cldata->cnn_data->baddr_info.baddr_dmai[loop_cnt].baddr))
		)
		{
			cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNJudgeSetupModeDMAC
 [ Params ]		cldata					: The pointer to the R_ATMLIB_CLData structure
				dmac_param				: DMAC parameters
				param_count				: DMAC parameter count
 [ Returns ]	none
 [ Function ]	Judge setup mode(fast or slow) for DMAC
 [ Note ]		none
******************************************************************************/
void atmlib_CNNJudgeSetupModeDMAC(
	R_ATMLIB_CLData				  *cldata,
	const R_ATMLIB_CNNDMACChParam *dmac_ch,
	uint8_t						  param_count
)
{
	uint8_t loop_cnt;

	/* initialize local variables */
	loop_cnt = 1U;

	/* loop for the member param_count */
	for( loop_cnt = 1U; loop_cnt < param_count; loop_cnt++ )
	{
		/* check wether all the image_x values are the same */
		if( dmac_ch[0].image_x != dmac_ch[loop_cnt].image_x )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the image_y values are the same */
		if( dmac_ch[0].image_y != dmac_ch[loop_cnt].image_y )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the image_stride values are the same */
		if( dmac_ch[0].image_stride != dmac_ch[loop_cnt].image_stride )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the mag_type values are the same */
		if( dmac_ch[0].mag_type != dmac_ch[loop_cnt].mag_type )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the repeat_skip_x values are the same */
		if( dmac_ch[0].repeat_skip_x != dmac_ch[loop_cnt].repeat_skip_x )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the repeat_skip_y values are the same */
		if( dmac_ch[0].repeat_skip_y != dmac_ch[loop_cnt].repeat_skip_y )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the image_format values are the same */
		if( dmac_ch[0].image_format != dmac_ch[loop_cnt].image_format )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check wether all the data_fracs values are the same */
		if( dmac_ch[0].dmac_frac != dmac_ch[loop_cnt].dmac_frac )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether the latter ptr is smaller than the former ptr */
		if( dmac_ch[loop_cnt].ptr < dmac_ch[loop_cnt - 1].ptr )
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether the channel offsets are sequence of equal differences
		   and difference between two adjacent channel offsets should be the multi of 4kB(0x1000) and max is 4MB(0x00400000) */
		if( (dmac_ch[1].ptr - dmac_ch[0].ptr != dmac_ch[loop_cnt].ptr - dmac_ch[loop_cnt - 1U].ptr) ||
			((dmac_ch[loop_cnt].ptr - dmac_ch[loop_cnt - 1U].ptr) % 0x1000U != 0) ||
			((dmac_ch[loop_cnt].ptr - dmac_ch[loop_cnt - 1U].ptr) > 0x400000)
		)
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether all the baddr_state are the same */
		if( cldata->cnn_data->baddr_info.baddr_dmac[0].baddr_state != cldata->cnn_data->baddr_info.baddr_dmac[loop_cnt].baddr_state)
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether all the offest address are the same */
		if( (cldata->cnn_data->baddr_info.baddr_dmac[0].baddr_state == R_ATMLIB_CNN_BADDRSTAT_SET1ST) &&
			((dmac_ch[0].ptr - cldata->cnn_data->baddr_info.baddr_dmac[0].baddr) != (dmac_ch[loop_cnt].ptr - cldata->cnn_data->baddr_info.baddr_dmac[loop_cnt].baddr))
		)
		{
			cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNJudgeSetupModeDMAO
 [ Params ]		cldata					: The pointer to the R_ATMLIB_CLData structure
				dmao_ch 				: DMAO channel parameters
				param_count				: DMAO parameter count
 [ Returns ]	none
 [ Function ]	Judge setup mode(fast or slow) for DMAO
 [ Note ]		none
******************************************************************************/
void atmlib_CNNJudgeSetupModeDMAO(
	R_ATMLIB_CLData *cldata,
	const uint32_t  *ptr,
	uint8_t			param_count
)
{
	uint8_t loop_cnt;

	/* initialize local variables */
	loop_cnt = 1U;

	/* loop for the member param_count */
	for( loop_cnt = 1U; loop_cnt < param_count; loop_cnt++ )
	{
		/* check whether the latter ptr is smaller than the former ptr */
		if( ptr[loop_cnt] < ptr[loop_cnt - 1U] )
		{
			cldata->cnn_data->setup_mode_dmao = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether the channel offsets are sequence of equal differences
		   and difference between two adjacent channel offsets should be the multi of 4kB(0x1000) and max is 4MB(0x00400000) */
		if( (ptr[1] - ptr[0] != ptr[loop_cnt] - ptr[loop_cnt - 1U]) ||
			((ptr[loop_cnt] - ptr[loop_cnt - 1U]) % 0x1000U != 0)   ||
			((ptr[loop_cnt] - ptr[loop_cnt - 1U]) > 0x400000)
		)
		{
			cldata->cnn_data->setup_mode_dmao = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether all the baddr_state are the same */
		if( cldata->cnn_data->baddr_info.baddr_dmao[0].baddr_state != cldata->cnn_data->baddr_info.baddr_dmao[loop_cnt].baddr_state)
		{
			cldata->cnn_data->setup_mode_dmao = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
		/* check whether all the offest address are the same */
		if( (cldata->cnn_data->baddr_info.baddr_dmao[0].baddr_state == R_ATMLIB_CNN_BADDRSTAT_SET1ST) &&
			((ptr[0] - cldata->cnn_data->baddr_info.baddr_dmao[0].baddr) != (ptr[loop_cnt] - cldata->cnn_data->baddr_info.baddr_dmao[loop_cnt].baddr))
		)
		{
			cldata->cnn_data->setup_mode_dmao = R_ATMLIB_CNN_CHSET_SLOW;
			break;
		}
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckFmtConvertDMAIC
 [ Params ]     img_fmt				     			: Image format
 				ari_param   						: ARI format
 [ Returns ]	R_ATMLIB_E_OK					    : on success
				R_ATMLIB_E_NG               		: Format conversion from image format to ari format are invalid
 [ Function ]	Check whether format conversions from image format to ari format are supported.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckFmtConvertDMAIC(
	R_ATMLIB_CNNImageFormat img_fmt, 
	R_ATMLIB_CNNARIFormat ari_fmt
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initilize local varibles */
	ret = R_ATMLIB_E_OK;

	/* const table of format conversion status */
	R_ATMLIB_CNN_STATIC const R_ATMLIB_RETURN_VALUE atmlib_cnn_Foramt_Matrix_Tbl[R_ATMLIB_IMG_END][R_ATMLIB_CNN_ARIFORMAT_END] =
	{   /*  R_ATMLIB_CNN_ARIFORMAT_8S   R_ATMLIB_CNN_ARIFORMAT_8U	R_ATMLIB_CNN_ARIFORMAT_16S   R_ATMLIB_CNN_ARIFORMAT_16U */
		{	R_ATMLIB_E_OK, 			 	R_ATMLIB_E_NG,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_NG,}, /* R_ATMLIB_IMG_4S  */
		{	R_ATMLIB_E_OK, 			 	R_ATMLIB_E_OK, 				R_ATMLIB_E_OK, 				 R_ATMLIB_E_OK,}, /* R_ATMLIB_IMG_4U  */
		{	R_ATMLIB_E_OK, 	 		 	R_ATMLIB_E_NG,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_NG,}, /* R_ATMLIB_IMG_8S  */
		{	R_ATMLIB_E_NG,              R_ATMLIB_E_OK,	            R_ATMLIB_E_OK, 				 R_ATMLIB_E_OK,}, /* R_ATMLIB_IMG_8U  */
		{	R_ATMLIB_E_NG,              R_ATMLIB_E_NG,   			R_ATMLIB_E_OK, 	 			 R_ATMLIB_E_NG,}, /* R_ATMLIB_IMG_16S */
		{	R_ATMLIB_E_NG,              R_ATMLIB_E_NG,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_OK,}, /* R_ATMLIB_IMG_16U */
	};

	/* check wether format conversions from img_fmt to ari_param are supported */
	ret = atmlib_cnn_Foramt_Matrix_Tbl[img_fmt][ari_fmt];

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckSPMCImgStrDMAI
 [ Params ]     img_stride                          : image stride
 				img_fmt				     			: image format
 				kernel_mode   						: kernel mode
 [ Returns ]	R_ATMLIB_E_OK					    : on success
				R_ATMLIB_E_NG               		: DMAI img_stride is invalid
 [ Function ]	For DMAI SPMC mode, the stride is set as multiple of 16byte (1*5x5 mode), 32byte (2*3x3 mode) or 64byte (4*1x1 mode) 
				Check whether img stride setting meets this requirement.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckSPMCImgStrDMAI(
	uint16_t img_stride,
	R_ATMLIB_CNNImageFormat img_fmt,
	R_ATMLIB_CNNKernelMode kernel_mode
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint16_t                alignment;
	uint8_t                 kernel_index;
	uint8_t					imgfm_index;

	/* initialize local variables */
	ret          = R_ATMLIB_E_OK;
	alignment    = 1U;
	kernel_index = 0U;
	imgfm_index	 = 0U;

	/* check if kernel_mode is 1*5x5 mode */
	if( kernel_mode == R_ATMLIB_CNN_KERNEL_1_5_5 )
	{
		/* if kernel_mode is 1*5x5 mode, set kernel_index to 0U */
		kernel_index = 0U;
	}
	/* check if kernel_index is 2*3x3 mode */
	else if( kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3 )
	{
		/* if kernel_mode is 2*3x3 mode, set kernel_index to 1U */
		kernel_index = 1U;
	}
	else
	{
		/* if kernel_mode is 4*1x1 mode, set kernel_index to 2U */
		kernel_index = 2U;
	}
	
	/* if img_fmt is R_ATMLIB_IMG_8S/8U/16S/16U, set imgfm_index to 0U/1U/2U/3U */
	imgfm_index = (uint8_t)(img_fmt - 2U);

	/* const table of stride aligment */
	R_ATMLIB_CNN_STATIC const uint16_t atmlib_cnn_Stride_Matrix_Tbl[R_ATMLIB_IMG_END - 2U][R_ATMLIB_CNN_KERNEL_END - 1U] =
	{   /* R_ATMLIB_CNN_KERNEL_1_5_5  R_ATMLIB_CNN_KERNEL_2_3_3  R_ATMLIB_CNN_KERNEL_4_1_1 */
		{	16U,					  32U, 						 64U  },  /* R_ATMLIB_IMG_8S  */
		{	16U,					  32U, 						 64U  },  /* R_ATMLIB_IMG_8U  */
		{	8U,  					  16U, 						 32U  },  /* R_ATMLIB_IMG_16S */
		{	8U,  					  16U, 						 32U  },  /* R_ATMLIB_IMG_16U */
	};

	/* For SPMC mode, the stride is set as multiple of 16byte (1*5x5 mode), 32byte (2*3x3 mode) or 64byte (4*1x1 mode)
	   Check wether img_stride setting meets this requirement.*/
	alignment = atmlib_cnn_Stride_Matrix_Tbl[imgfm_index][kernel_index];
	ret = (img_stride % alignment == 0) ? R_ATMLIB_E_OK : R_ATMLIB_E_NG;

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckCombParamDMAI
 [ Params ]     cldata								: The pointer to the R_ATMLIB_CLData structure
 				dmai_param				     		: DMAI parameters
 [ Returns ]	R_ATMLIB_E_OK					    : on success
 				R_ATMLIB_E_NG_COMB_KM_DIL			: Combination of dil_conv and kern_mode is invalid
				R_ATMLIB_E_NG_CH_CO					: Combination of channel_offset,param_count and setup_mode_dmai
				R_ATMLIB_E_NG_COMB_SPMC_DATAMIX		: Combination of image_format and ari_fmt in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_DATAMIX		: Combination of image_format and ari_fmt in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_FM_ST		: Combination of image_format and image_stride in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SPMC_FM_ST		: Combination of image_format and image_stride in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_FM_IMGX			: Combination of image_format and image_x is invalid
				R_ATMLIB_E_NG_COMB_STPOS_PADL		: Combination of start_pos and pad_l is invalid
				R_ATMLIB_E_NG_COMB_KM_STX			: Combination of kernel_mode and stride_x is invalid
				R_ATMLIB_E_NG_COMB_KM_STY			: Combination of kernel_mode and stride_y is invalid
				R_ATMLIB_E_NG_COMB_KM_STPOS			: Combination of kernel_mode and start_pos is invalid
				R_ATMLIB_E_NG_COMB_KM_PADL			: Combination of kernel_mode and pad_l is invalid
				R_ATMLIB_E_NG_COMB_KM_PADR			: Combination of kernel_mode and pad_r is invalid
				R_ATMLIB_E_NG_COMB_KM_PADT			: Combination of kernel_mode and pad_t is invalid
				R_ATMLIB_E_NG_COMB_KM_PADB			: Combination of kernel_mode and pad_b is invalid
				R_ATMLIB_E_NG_COMB_KM_MAG			: Combination of kernel_mode and mag_type is invalid
 [ Function ]	Check combinaion members of the DMAI parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckCombParamDMAI(
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNDMAIParam *dmai_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	R_ATMLIB_RETURN_VALUE	fmt_convt_result;
	R_ATMLIB_RETURN_VALUE	img_str_result;
	uint8_t 				loop_cnt;
	uint8_t 				loop_len;

	/* initialize local variables */
	ret 	 		  = R_ATMLIB_E_OK;
	fmt_convt_result  = R_ATMLIB_E_OK;
	img_str_result 	  = R_ATMLIB_E_OK;
	loop_cnt 		  = 0U;
	loop_len 	      = 1U;

	/* check combination of dil_conv and kern_mode. 2_3x3 and 4_1x1 mode cannot support dil_conv */
	if( ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3) && (dmai_param->dil_conv > R_ATMLIB_CNN_MIN_DILATED_DISTANCE)) ||
		((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) && (dmai_param->dil_conv > R_ATMLIB_CNN_MIN_DILATED_DISTANCE))
	)
	{
		ret = R_ATMLIB_E_NG_COMB_KM_DIL;
	}
	/* check combination of setup_mode, param_count and channel_offset */
	else if( (cldata->cnn_data->setup_mode_dmai == R_ATMLIB_CNN_CHSET_FAST) && 
			 (dmai_param->param_count > 1U) &&
			 ((dmai_param->channel_offset * 0x1000U) != (dmai_param->dmai_ch[1].ptr - dmai_param->dmai_ch[0].ptr)) 
	)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	else
	{
		/* set loop_len to 1U when setup_mode_dmai is fast or set to dmai_param->param_count */
		loop_len = (cldata->cnn_data->setup_mode_dmai == R_ATMLIB_CNN_CHSET_FAST ) ? (uint8_t)1U : dmai_param->param_count ;
		for( loop_cnt = 0; loop_cnt < loop_len; loop_cnt++ )
		{
			/* check combination of image_format and image_x. R_ATMLIB_IMG_4U/4S, image_x must event number */
			if( (dmai_param->dmai_ch[loop_cnt].image_format <= R_ATMLIB_IMG_4U) && 
				(dmai_param->dmai_ch[loop_cnt].image_x % 2U != 0U)
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FM_IMGX;
			}
			/* check combination of kernel_mode and stride_x.only R_ATMLIB_CNN_KERNEL_1_5_5 support stride_x settings */
			else if( (dmai_param->kernel_mode != R_ATMLIB_CNN_KERNEL_1_5_5) && 
					 (dmai_param->dmai_ch[loop_cnt].stride_x > R_ATMLIB_CNN_MIN_CONV_STRIDE)
			)
			{
				ret = R_ATMLIB_E_NG_COMB_KM_STX;
			}
			/* check combination of kernel_mode and stride_y.only R_ATMLIB_CNN_KERNEL_1_5_5 support stride_y settings */
			else if( (dmai_param->kernel_mode != R_ATMLIB_CNN_KERNEL_1_5_5) && 
					 (dmai_param->dmai_ch[loop_cnt].stride_y > R_ATMLIB_CNN_MIN_CONV_STRIDE)
			)
			{
				ret = R_ATMLIB_E_NG_COMB_KM_STY;
			}
			/* check combination of kernel_mode and start_pos.only R_ATMLIB_CNN_KERNEL_1_5_5 support start_pos settings */
			else if( (dmai_param->kernel_mode != R_ATMLIB_CNN_KERNEL_1_5_5) && 
					 (dmai_param->dmai_ch[loop_cnt].start_pos > R_ATMLIB_CNN_MIN_START_POS) 
			) 
			{
				ret = R_ATMLIB_E_NG_COMB_KM_STPOS;
			}
			/* check combination of start_pos and pad_l. when start_pos>0, any padding on left side is not used */
			else if( (dmai_param->dmai_ch[loop_cnt].start_pos > R_ATMLIB_CNN_MIN_START_POS) && 
					 (dmai_param->dmai_ch[loop_cnt].pad_l > 0U)
			)
			{
				ret = R_ATMLIB_E_NG_COMB_STPOS_PADL;
			}
			/* check combination of kernel_mode and pad_l 
			   R_ATMLIB_CNN_KERNEL_3_3_3 and R_ATMLIB_CNN_KERNEL_2_3_3 only supprt pad_l 0~1
			   R_ATMLIB_CNN_KERNEL_4_1_1 not support pad mode */
			else if( ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_3_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_l > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_l > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) && (dmai_param->dmai_ch[loop_cnt].pad_l > 0U))
			)
			{
				ret = R_ATMLIB_E_NG_COMB_KM_PADL;
			}
			/* check combination of kernel_mode and pad_r 
			   R_ATMLIB_CNN_KERNEL_3_3_3 and R_ATMLIB_CNN_KERNEL_2_3_3 only supprt pad_r 0~1
			   R_ATMLIB_CNN_KERNEL_4_1_1 not support pad mode */
			else if( ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_3_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_r > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_r > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) && (dmai_param->dmai_ch[loop_cnt].pad_r > 0U))
			)
			{
				ret = R_ATMLIB_E_NG_COMB_KM_PADR;
			}
			/* check combination of kernel_mode and pad_t 
			   R_ATMLIB_CNN_KERNEL_3_3_3 and R_ATMLIB_CNN_KERNEL_2_3_3 only supprt pad_t 0~1
			   R_ATMLIB_CNN_KERNEL_4_1_1 not support pad mode */
			else if( ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_3_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_t > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_t > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) && (dmai_param->dmai_ch[loop_cnt].pad_t > 0U))
			)
			{
				ret = R_ATMLIB_E_NG_COMB_KM_PADT;
			}
			/* check combination of kernel_mode and pad_b 
			   R_ATMLIB_CNN_KERNEL_3_3_3 and R_ATMLIB_CNN_KERNEL_2_3_3 only supprt pad_b 0~1
			   R_ATMLIB_CNN_KERNEL_4_1_1 not support pad mode */
			else if( ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_3_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_b > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3) && (dmai_param->dmai_ch[loop_cnt].pad_b > 1U)) ||
					 ((dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) && (dmai_param->dmai_ch[loop_cnt].pad_b > 0U)) 
			)
			{
				ret = R_ATMLIB_E_NG_COMB_KM_PADB;
			}
			else 
			{
				/* check combination of image_format and ari_fmt in SCMP mode. */
				if(dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
				{
					/* check whether format conversions from image format to ari format are supported */
					fmt_convt_result = atmlib_CNNCheckFmtConvertDMAIC(dmai_param->dmai_ch[loop_cnt].image_format,
																	cldata->cnn_data->ari_format);
					if(fmt_convt_result != R_ATMLIB_E_OK)
					{
						ret = R_ATMLIB_E_NG_COMB_SCMP_DATAMIX;
					}
					else if( (dmai_param->dmai_ch[loop_cnt].image_format <= R_ATMLIB_IMG_4U) && 
							(dmai_param->dmai_ch[loop_cnt].image_stride % 2U != 0U)
					)
					{
						ret = R_ATMLIB_E_NG_COMB_SCMP_FM_ST;
					}
					else
					{
						/* DO NOTHING */
					}
				}
				/* check combination of image_format and image_stride in SPMC mode.  */
				else
				{
					/* check combination of kernel_mode and mag_type 
						2*3x3 and 4*1x1 in SPMC mode only support R_ATMLIB_CNN_NOMAG
					*/
					if( (dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3) &&
						(dmai_param->dmai_ch[loop_cnt].mag_type != R_ATMLIB_CNN_NOMAG)
					)
					{
						ret = R_ATMLIB_E_NG_COMB_KM_MAG;
					}
					/* check combination of kernel_mode and mag_type 
					   2*3x3 and 4*1x1 in SPMC mode only support R_ATMLIB_CNN_NOMAG
					*/
					else if( (dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) && 
							 (dmai_param->dmai_ch[loop_cnt].mag_type != R_ATMLIB_CNN_NOMAG)
					)
					{
						ret = R_ATMLIB_E_NG_COMB_KM_MAG;
					}
					else
					{
						/* check whether img stride setting meets "the stride is set as multiple of 16byte (1*5x5 mode), 32byte (2*3x3 mode) or 64byte (4*1x1 mode)" */
						img_str_result = atmlib_CNNCheckSPMCImgStrDMAI(dmai_param->dmai_ch[loop_cnt].image_stride, 
																	dmai_param->dmai_ch[loop_cnt].image_format, 
																	dmai_param->kernel_mode);
						/* check combination of image_format and ari_fmt in SPMC mode. data format convert is not support in SPMC mode. redmine #243474 */
						if( (uint8_t)(dmai_param->dmai_ch[loop_cnt].image_format) != (uint8_t)(cldata->cnn_data->ari_format + 2U) )
						{
							ret = R_ATMLIB_E_NG_COMB_SPMC_DATAMIX;
						}
						/* check whether img stride setting meets "the stride is set as multiple of 16byte (1*5x5 mode), 32byte (2*3x3 mode) or 64byte (4*1x1 mode)" */
						else if(img_str_result != R_ATMLIB_E_OK)
						{
							ret = R_ATMLIB_E_NG_COMB_SPMC_FM_ST;
						}
						else
						{
							/* DO NOTHING */
						}
					}
				}
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckCombParamDMAC
 [ Params ]     cldata								: The pointer to the R_ATMLIB_CLData structure
 				dmac_param				     		: DMAC parameters
 [ Returns ]	R_ATMLIB_E_OK					    : on success
 				R_ATMLIB_E_NG_CH_CO					: Combination of channel_offset,param_count and setup_mode_dmac
				R_ATMLIB_E_NG_COMB_SPMC_DATAMIX		: Combination of image_format and ari_fmt in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_DATAMIX		: Combination of image_format and ari_fmt in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_FM_ST		: Combination of image_format and image_stride in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SPMC_FM_ST		: Combination of image_format and image_stride in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_FM_IMGX			: Combination of image_format and image_x is invalid
 [ Function ]	Check combinaion members of the DMAC parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckCombParamDMAC(
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNDMACParam *dmac_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	R_ATMLIB_RETURN_VALUE	fmt_convt_result;
	uint8_t                 loop_cnt;
	uint8_t					loop_len;

	/* initialize local variables */
	ret 	 		 = R_ATMLIB_E_OK;
	fmt_convt_result = R_ATMLIB_E_OK;
	loop_cnt 		 = 0U;
	loop_len 		 = 1U;

	/* check combination of setup_mode, param_count and channel_offset */
	if( (cldata->cnn_data->setup_mode_dmac == R_ATMLIB_CNN_CHSET_FAST) && 
		(dmac_param->param_count > 1U) &&
		((dmac_param->channel_offset * 0x1000U) != (dmac_param->dmac_ch[1].ptr - dmac_param->dmac_ch[0].ptr)) 
	)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	else
	{
		/* set loop_len to 1U when setup_mode_dmac is fast or set to dmac_param->param_count */
		loop_len = (cldata->cnn_data->setup_mode_dmac == R_ATMLIB_CNN_CHSET_FAST ) ? (uint8_t)1U : dmac_param->param_count ;
		for(loop_cnt = 0; loop_cnt < loop_len; loop_cnt++)
		{
			/* check combination of image_format and image_x. R_ATMLIB_IMG_4U/4S, image_x must event number */
			if( (dmac_param->dmac_ch[loop_cnt].image_format <= R_ATMLIB_IMG_4U) && 
				(dmac_param->dmac_ch[loop_cnt].image_x % 2U != 0U)
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FM_IMGX;
			}
			else
			{
				/* check if the data_mode dmac_param is SCMP mode. */
				if( dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP ) 
				{
					/* check whether format conversions from image format to ari format are supported */
					fmt_convt_result = atmlib_CNNCheckFmtConvertDMAIC(dmac_param->dmac_ch[loop_cnt].image_format,
																	cldata->cnn_data->ari_format);
					if(fmt_convt_result != R_ATMLIB_E_OK)
					{
						ret = R_ATMLIB_E_NG_COMB_SCMP_DATAMIX;
					}
					/* check combination of image_format and image_stride in SCMP mode. R_ATMLIB_CNN_DATA_SCMP & R_ATMLIB_IMG_4U/4S, image_stride must event number */
					else if( (dmac_param->dmac_ch[loop_cnt].image_format <= R_ATMLIB_IMG_4U) && 
							 (dmac_param->dmac_ch[loop_cnt].image_stride % 2U != 0U)
					)
					{
						ret = R_ATMLIB_E_NG_COMB_SCMP_FM_ST;
					}
					else
					{
						/* DO NOTHING */
					}
				}
				/* check if the data_mode dmac_param is SPMC mode. */
				else
				{
					/* check combination of image_format and ari_fmt in SPMC mode. data format convert is not support in SPMC mode. redmine #243474 */
					if( (uint8_t)(dmac_param->dmac_ch[loop_cnt].image_format) != (uint8_t)(cldata->cnn_data->ari_format + 2U) )
					{
						ret = R_ATMLIB_E_NG_COMB_SPMC_DATAMIX;
					}
					/* check combination of image_format and image_stride in SPMC mode. */
					else if((dmac_param->dmac_ch[loop_cnt].image_format <= R_ATMLIB_IMG_8U) && 
							(dmac_param->dmac_ch[loop_cnt].image_stride % 32U != 0U) 
					)
					{
						ret = R_ATMLIB_E_NG_COMB_SPMC_FM_ST;
					}
					else
					{
						/* check combination of image_format and image_stride in SPMC mode. */
						if(dmac_param->dmac_ch[loop_cnt].image_stride % 16U != 0U)
						{
							ret = R_ATMLIB_E_NG_COMB_SPMC_FM_ST;
						}
					}
				}
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckSPMCImgStrDMAO
 [ Params ]     img_stride                          : image stride
 				img_fmt				     			: image format
				ari_fmt								: ARI format
 				kernel_mode   						: kernel mode
 [ Returns ]	R_ATMLIB_E_OK					    : on success
				R_ATMLIB_E_NG               		: DMAO img_stride is invalid
 [ Function ]	For DMAO SPMC mode, the stride is set as multiple of 16byte (1*5x5 mode) or 64byte (4*1x1 mode)  
				Check whether img stride setting meets this requirement.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC R_ATMLIB_RETURN_VALUE atmlib_CNNCheckSPMCImgStrDMAO(
	uint16_t img_stride,
	R_ATMLIB_CNNImageFormat img_fmt,
	R_ATMLIB_CNNARIFormat ari_fmt,
	R_ATMLIB_CNNKernelMode kernel_mode
)
{
	R_ATMLIB_RETURN_VALUE	ret ;
	uint16_t 				alignment;
	uint8_t 				arifmt_index;
	uint8_t 				imgfmt_index;
	uint8_t 				kernel_index;

	/* initialize local variables */
	ret 			= R_ATMLIB_E_OK;
	alignment		= 1U;
	arifmt_index	= 0U;
	imgfmt_index	= 0U;
	kernel_index	= 0U;

	/* if ari_fmt is 16bit, set arifmt_index to 1U or set arifmt_index to 0U */
	arifmt_index = (ari_fmt >= R_ATMLIB_CNN_ARIFORMAT_16S) ? 1U : 0U;
	/* if kernel_mode is 4*1x1 mode, set kernel_index to 1U or set kernel_index to 0U */
	kernel_index = (kernel_mode == R_ATMLIB_CNN_KERNEL_4_1_1) ? 1U : 0U;
	/* check if img_fmt is 4bit */
	if( img_fmt == R_ATMLIB_IMG_4S || img_fmt == R_ATMLIB_IMG_4U )
	{
		/* if img_fmt is 4bit, set imgfmt_index to 0U */
		imgfmt_index = 0U;
	}
	/* check if img_fmt is 8bit */
	else if( img_fmt == R_ATMLIB_IMG_8S || img_fmt == R_ATMLIB_IMG_8U )
	{
		/* if img_fmt is 8bit, set imgfmt_index to 1U */
		imgfmt_index = 1U;
	}
	else
	{
		/* if img_fmt is 16bit, set imgfmt_index to 2U */
		imgfmt_index = 2U;
	}

	/* const table of stride aligment */
	R_ATMLIB_CNN_STATIC const uint16_t atmlib_cnn_Stride_Matrix_Tbl[2][3][2] =
	{   /* R_ATMLIB_CNN_ARIFORMAT_8S/8U */
		{   /* Not R_ATMLIB_CNN_KERNEL_4_1_1    R_ATMLIB_CNN_KERNEL_4_1_1 */
			{  64U, 					    	128U },  /* R_ATMLIB_IMG_4S/4U   */
			{  32U, 					    	64U  },  /* R_ATMLIB_IMG_8S/8U   */
			{  32U, 							32U  },  /* R_ATMLIB_IMG_16S/16U */
		},
		/* R_ATMLIB_CNN_ARIFORMAT_16S/16U */
		{	/* Not R_ATMLIB_CNN_KERNEL_4_1_1    R_ATMLIB_CNN_KERNEL_4_1_1 */
			{  32U,								128U },  /* R_ATMLIB_IMG_4S/4U   */
			{  16U,								64U  },  /* R_ATMLIB_IMG_8S/8U   */
			{  16U, 							32U  },  /* R_ATMLIB_IMG_16S/16U */
		}
	};

	/* For SPMC mode, the stride is set as multiple of 16byte (1*5x5 mode) or 64byte (4*1x1 mode)  
	   Check wether img_stride setting meets this requirement. */
	alignment = atmlib_cnn_Stride_Matrix_Tbl[arifmt_index][imgfmt_index][kernel_index];
	ret = (img_stride % alignment == 0U) ? R_ATMLIB_E_OK : R_ATMLIB_E_NG;

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckCombParamDMAO
 [ Params ]     cldata								: The pointer to the R_ATMLIB_CLData structure
 				dmao_param				     		: DMAO parameters
 [ Returns ]	R_ATMLIB_E_OK					    : on success
				R_ATMLIB_E_NG_COMB_SCMP_FM_ST		: Combination of image_format and image_stride in SCMP mode is invalid
				R_ATMLIB_E_NG_CH_CO 				: Combination of channel_offset,param_count and setup_mode_dmac
				R_ATMLIB_E_NG_COMB_SPMC_FM_ST		: Combination of image_format, ari_format and image_stride in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_FM_IMGX			: Combination of image_format and image_x is invalid
 [ Function ]	Check combinaion members of the DMAO parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckCombParamDMAO(
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNDMAOParam *dmao_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	R_ATMLIB_RETURN_VALUE	img_str_result;

	/* initialize local variables */
	ret 		   = R_ATMLIB_E_OK;
	img_str_result = R_ATMLIB_E_OK;

	/* check combination of image_format and image_x. R_ATMLIB_IMG_4U/4S, image_x must event number */
	if( (dmao_param->image_format <= R_ATMLIB_IMG_4U) && 
	    (dmao_param->image_x % 2U != 0U)
	)
	{
		ret = R_ATMLIB_E_NG_COMB_FM_IMGX;
	}
	/* check combination of image_format and image_stride in SCMP mode. R_ATMLIB_CNN_DATA_SCMP & R_ATMLIB_IMG_4U/4S, image_stride must event number */
	else if( (dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) && 
			 (dmao_param->image_format <= R_ATMLIB_IMG_4U) && 
			 (dmao_param->image_stride % 2U != 0U)
	)
	{
		ret = R_ATMLIB_E_NG_COMB_SCMP_FM_ST;
	}
	/* check combination of setup_mode, param_count and channel_offset */
	else if( (cldata->cnn_data->setup_mode_dmao == R_ATMLIB_CNN_CHSET_FAST) && 
			 (dmao_param->param_count > 1U) &&
			 ((dmao_param->channel_offset * 0x1000U) != (dmao_param->ptr[1] - dmao_param->ptr[0])) 
	)
	{
		ret = R_ATMLIB_E_NG_CH_CO;
	}
	/* check combination of image_format, ari_format and image_stride in SPMC mode. */
	else if( dmao_param->data_mode == R_ATMLIB_CNN_DATA_SPMC )
	{
		/* check wether the stride is set as multiple of 16byte (1*5x5 mode) or 64byte (4*1x1 mode) */
		img_str_result = atmlib_CNNCheckSPMCImgStrDMAO(	dmao_param->image_stride,
		 											   	dmao_param->image_format,
														cldata->cnn_data->ari_format,
													   	dmao_param->next_layer_kernel_mode);
		if(img_str_result != R_ATMLIB_E_OK)
		{
			ret = R_ATMLIB_E_NG_COMB_SPMC_FM_ST;
		}/* do nothing for else branch */
	}
	else
	{
		/* DO NOTHING */
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamWeightDMARF
 [ Params ]		dmarf_param     				: DMARF parameters
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_DMARFFM   		: Member format is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE		: Member data_mode is out of range
				R_ATMLIB_E_NG_COMB_RFFM_ARIFM	: Combination of next_ari_format and DMARF format is invalid
				R_ATMLIB_E_NG_RF_EXSA			: Member external_sa is out of range
				R_ATMLIB_E_NG_RF_INSA			: Member internal_sa is out of range
				R_ATMLIB_E_NG_RF_LEN			: Member length is out of range
				R_ATMLIB_E_NG_RF_STAPOS			: Member start_pos is out of range
				R_ATMLIB_E_NG_RF_ENDPOS			: Member end_pos is out of range
 [ Function ]	Check weight DMARF parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamWeightDMARF(
	R_ATMLIB_CLData				 *cldata,
	const R_ATMLIB_CNNDMARFParam *dmarf_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initialize local variables */
	ret 			 = R_ATMLIB_E_OK;

	/* check the member format of the dmarf_param */
	if( dmarf_param->format >= R_ATMLIB_CNN_DATAFM_END )
	{
		ret = R_ATMLIB_E_NG_DMARFFM;
	}
	/* check the member data_mode of the dmarf_param */
	else if(dmarf_param->data_mode >= R_ATMLIB_CNN_DATA_END )
	{
		ret = R_ATMLIB_E_NG_CH_DATA_MODE;
	}
	/* check the member external_sa of the dmarf_param */
	else if( (dmarf_param->external_sa == 0U) || 
			 (dmarf_param->external_sa == R_ATMLIB_CNN_MEM_INVALID) 
	)
	{
		ret = R_ATMLIB_E_NG_RF_EXSA;
	}
	/* check the member internal_sa of the dmarf_param in SCMP mode */
	else if( (dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			 ((dmarf_param->internal_sa < R_ATMLIB_CNN_WB0_BASE) || (dmarf_param->internal_sa > R_ATMLIB_CNN_MAX_WEIGHT_ISA))
	)								      
	{
		ret = R_ATMLIB_E_NG_RF_INSA;
	}
	/* check the member internal_sa of the dmarf_param in SCMP mode */
	else if( (dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			 ((dmarf_param->internal_sa - R_ATMLIB_CNN_WB0_BASE) % 32U != 0U)
	)
	{
		ret = R_ATMLIB_E_NG_RF_INSA;
	}
	/* check the member length of the dmarf_param in SCMP mode */
	else if(  (dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			  ((dmarf_param->length < R_ATMLIB_CNN_MIN_WEIGHT_DMARFL) || (dmarf_param->length > R_ATMLIB_CNN_MAX_WEIGHT_DMARFL))
	)
	{
		ret = R_ATMLIB_E_NG_RF_LEN;
	}
	/* check the member length of the dmarf_param in SCMP mode */
	else if(  (dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) &&
			  ((dmarf_param->internal_sa + dmarf_param->length * 64U) > (uint16_t)0xC000)
	)
	{
		ret = R_ATMLIB_E_NG_RF_LEN;
	}
	/* check the member start_pos of the dmarf_param in SPMC mode */
	else if( (dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SPMC) &&
			 (dmarf_param->start_pos > R_ATMLIB_CNN_MAX_DMARF_SPMC_POS)
	)
	{
		ret = R_ATMLIB_E_NG_RF_STAPOS;
	}
	/* check the member end_pos of the dmarf_param in SPMC mode */
	else if( (dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SPMC) &&
			 ((dmarf_param->end_pos > R_ATMLIB_CNN_MAX_DMARF_SPMC_POS) || (dmarf_param->end_pos < dmarf_param->start_pos))
	)
	{
		ret = R_ATMLIB_E_NG_RF_ENDPOS;
	}
	else
	{
		/* const table of format conversion status */
		R_ATMLIB_CNN_STATIC const R_ATMLIB_RETURN_VALUE atmlib_cnn_Foramt_Matrix_Tbl[R_ATMLIB_CNN_DATAFM_END][R_ATMLIB_CNN_ARIFORMAT_END] =
		{   /*  R_ATMLIB_CNN_ARIFORMAT_8S    R_ATMLIB_CNN_ARIFORMAT_8U	R_ATMLIB_CNN_ARIFORMAT_16S   R_ATMLIB_CNN_ARIFORMAT_16U */
			{	R_ATMLIB_E_NG, 				 R_ATMLIB_E_NG,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DMARF_16U */
			{	R_ATMLIB_E_NG, 				 R_ATMLIB_E_NG, 			R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,},	/* R_ATMLIB_CNN_DMARF_16S */
			{	R_ATMLIB_E_NG,			 	 R_ATMLIB_E_OK,   			R_ATMLIB_E_OK, 				 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DMARF_8U  */
			{	R_ATMLIB_E_OK,               R_ATMLIB_E_NG,				R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,},	/* R_ATMLIB_CNN_DMARF_8S  */
			{	R_ATMLIB_E_OK,               R_ATMLIB_E_OK,   			R_ATMLIB_E_OK, 	 			 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DMARF_4U  */
			{	R_ATMLIB_E_OK,               R_ATMLIB_E_NG,   			R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,}	/* R_ATMLIB_CNN_DMARF_4S  */
		};
		/* check the combination of the dmarf_foramt and next_ari_fmt */
		if( atmlib_cnn_Foramt_Matrix_Tbl[dmarf_param->format][cldata->cnn_data->next_ari_format] != R_ATMLIB_E_OK )
		{
			ret = R_ATMLIB_E_NG_COMB_RFFM_ARIFM;
		}/* do nothing for else branch */
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamLUTDMARF
 [ Params ]	    dmarf                           : Pointer to the DMARF structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
 				R_ATMLIB_E_NG_DMARFFM			: Member format is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE		: Member data_mode is out of range
				R_ATMLIB_E_NG_RF_EXSA			: Member external_sa is out of range
				R_ATMLIB_E_NG_RF_INSA			: Member internal_sa is out of range
				R_ATMLIB_E_NG_RF_LEN			: Member length is out of range
				R_ATMLIB_E_NG_COMB_RFFM_ARIFM	: Combination of ari_format and DMARF format is invalid
 [ Function ]	Check LUT DMARF parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamLUTDMARF(
	R_ATMLIB_CLData				 *cldata,
	const R_ATMLIB_CNNDMARFParam *dmarf_param
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				dmarffm_index;

	/* initialize local variables */
	ret = R_ATMLIB_E_OK;

	/* check the member format of the dmarf_param */
	if( (dmarf_param->format < R_ATMLIB_CNN_DATAFM_8U) || 
		(dmarf_param->format > R_ATMLIB_CNN_DATAFM_8S)  
	)
	{
		ret = R_ATMLIB_E_NG_DMARFFM;
	}
	/* check the member data_mode of the dmarf_param */
	else if( dmarf_param->data_mode != R_ATMLIB_CNN_DATA_SCMP )
	{
		ret = R_ATMLIB_E_NG_CH_DATA_MODE;
	}
	/* check the member external_sa of the dmarf_param */
	else if( dmarf_param->external_sa == 0U || (dmarf_param->external_sa == R_ATMLIB_CNN_MEM_INVALID))
	{
		ret = R_ATMLIB_E_NG_RF_EXSA;
	}
	/* check the member internal_sa of the dmarf_param */
	else if( (dmarf_param->internal_sa < R_ATMLIB_CNN_LUT_BASE) || 
			 (dmarf_param->internal_sa > R_ATMLIB_CNN_MAX_LUT_ISA)
	)
	{
		ret = R_ATMLIB_E_NG_RF_INSA;
	}
	/* check the member internal_sa of the dmarf_param */
	else if( (dmarf_param->internal_sa - R_ATMLIB_CNN_LUT_BASE) % 256U != 0 )
	{
		ret = R_ATMLIB_E_NG_RF_INSA;
	}
	/* check the member length of the dmarf_param */
	else if( (dmarf_param->length < R_ATMLIB_CNN_MIN_LUT_DMARFL) ||
			 (dmarf_param->length > R_ATMLIB_CNN_MAX_LUT_DMARFL)
	)
	{
		ret = R_ATMLIB_E_NG_RF_LEN;
	}
	/* check the member length of the dmarf_param */
	else if( (dmarf_param->internal_sa + dmarf_param->length * 64U) > (uint16_t)0x7500 )
	{
		ret = R_ATMLIB_E_NG_RF_LEN;
	}
	else
	{
		/* const table of format conversion status */
		R_ATMLIB_CNN_STATIC const R_ATMLIB_RETURN_VALUE atmlib_cnn_Foramt_Matrix_Tbl[2][R_ATMLIB_CNN_ARIFORMAT_END] =
		{   /*  R_ATMLIB_CNN_ARIFORMAT_8S    R_ATMLIB_CNN_ARIFORMAT_8U	R_ATMLIB_CNN_ARIFORMAT_16S   R_ATMLIB_CNN_ARIFORMAT_16U */
			{	R_ATMLIB_E_NG,			 	 R_ATMLIB_E_OK,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DATAFM_8U  */
			{	R_ATMLIB_E_OK,               R_ATMLIB_E_NG,				R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,},	/* R_ATMLIB_CNN_DATAFM_8S  */
		};

		/* if DMARF format is 8U set dmarffm_index to 0 or set to 1 */
		dmarffm_index = (dmarf_param->format == R_ATMLIB_CNN_DATAFM_8U) ? 0U : 1U;
		/* check the combination of the dmarf_foramt and ari_format */
		if( atmlib_cnn_Foramt_Matrix_Tbl[dmarffm_index][cldata->cnn_data->ari_format] != R_ATMLIB_E_OK )
		{
			ret = R_ATMLIB_E_NG_COMB_RFFM_ARIFM;
		}/* do nothing for else branch */
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamWeightWPR
 [ Params ]		weight_info     				: Pointer to the R_ATMLIB_CNNWeightInfo structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_COUNT				: Member ch_cnt is out of range
				R_ATMLIB_E_NG_WB_FMT			: Member weight_format is out of range
				R_ATMLIB_E_NG_CH_ID				: Member in_id/out_id is out of range
				R_ATMLIB_E_NG_COMB_WBFM_ARIFM	: Combination of weigth_format and ari_format is invalid
				R_ATMLIB_E_NG_WB_VALUE          : Member value is out of range
 [ Function ]	Check weight WPR parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamWeightWPR(
	R_ATMLIB_CLData 			 *cldata,
	const R_ATMLIB_CNNWeightInfo *weight_info
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				loop_cnt;
	uint32_t				loop_cnt2;

	/* initialize local variables */
	ret 	  = R_ATMLIB_E_OK;
	loop_cnt  = 0U;
	loop_cnt2 = 0U;

	/* const table of format conversion status */
	R_ATMLIB_CNN_STATIC const R_ATMLIB_RETURN_VALUE atmlib_cnn_Foramt_Matrix_Tbl[R_ATMLIB_CNN_DATAFM_END][R_ATMLIB_CNN_ARIFORMAT_END] =
	{   /*  R_ATMLIB_CNN_ARIFORMAT_8S    R_ATMLIB_CNN_ARIFORMAT_8U	R_ATMLIB_CNN_ARIFORMAT_16S   R_ATMLIB_CNN_ARIFORMAT_16U */
		{	R_ATMLIB_E_NG, 				 R_ATMLIB_E_NG,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DATAFM_16U */
		{	R_ATMLIB_E_NG, 				 R_ATMLIB_E_NG, 			R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,},	/* R_ATMLIB_CNN_DATAFM_16S */
		{	R_ATMLIB_E_NG,			 	 R_ATMLIB_E_OK,   			R_ATMLIB_E_OK, 				 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DATAFM_8U  */
		{	R_ATMLIB_E_OK,               R_ATMLIB_E_NG,				R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,},	/* R_ATMLIB_CNN_DATAFM_8S  */
		{	R_ATMLIB_E_OK,               R_ATMLIB_E_OK,   			R_ATMLIB_E_OK, 	 			 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DATAFM_4U  */
		{	R_ATMLIB_E_OK,               R_ATMLIB_E_NG,   			R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,}	/* R_ATMLIB_CNN_DATAFM_4S  */
	};

	/* check the member ch_cnt of the weight_info */
	if( (weight_info->ch_cnt < 1U) || 
		(weight_info->ch_cnt > R_ATMLIB_CNN_WEIGHT_MAX_CH) 
	) 
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	/* check the member ch_cnt of the weight_info */
	else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
			 (weight_info->ch_cnt > (R_ATMLIB_CNN_WEIGHT_MAX_CH / 4U)) 
	)
	{
		ret = R_ATMLIB_E_NG_COUNT;
	}
	else if( weight_info->weight_format >= R_ATMLIB_CNN_DATAFM_END )
	{
		ret = R_ATMLIB_E_NG_WB_FMT;
	}
	/* check wether format conversions from weight_format to ari_format are supported */
	else if( atmlib_cnn_Foramt_Matrix_Tbl[weight_info->weight_format][cldata->cnn_data->ari_format] != R_ATMLIB_E_OK )
	{
		ret = R_ATMLIB_E_NG_COMB_WBFM_ARIFM;
	}
	else
	{
		/* loop for the ch_cnt of weight_info */
		for(loop_cnt = 0; loop_cnt < weight_info->ch_cnt; loop_cnt++)
		{	
			/* check the member in_id of the weight_info */
			if( (weight_info->ch_weight[loop_cnt].in_id < R_ATMLIB_CNN_DMAI0) ||
			    (weight_info->ch_weight[loop_cnt].in_id > R_ATMLIB_CNN_DMAI15)
			)
			{
				ret = R_ATMLIB_E_NG_CH_ID;
			}
			/* check the member out_id of the weight_info */
			else if( (weight_info->ch_weight[loop_cnt].out_id > R_ATMLIB_CNN_ARI31) || 
					 (weight_info->ch_weight[loop_cnt].out_id < R_ATMLIB_CNN_ARI0)
			)
			{
				ret = R_ATMLIB_E_NG_CH_ID;
			}
			/* check the member in_id of the weight_info */
			else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
				  	 (weight_info->ch_weight[loop_cnt].in_id % 2U != 0U)
			)
			{
				ret = R_ATMLIB_E_NG_CH_ID;
			}
			/* check the member out_id of the weight_info */
			else if( (cldata->cnn_data->ari_format >= R_ATMLIB_CNN_ARIFORMAT_16S) && 
				     (weight_info->ch_weight[loop_cnt].out_id % 2U != 0U)
			)
			{
				ret = R_ATMLIB_E_NG_CH_ID;
			}
			else
			{
				/* loop for the filter size */
				for(loop_cnt2 = 0; loop_cnt2 < R_ATMLIB_CNN_FILTER_SIZE; loop_cnt2++)
				{
					/* check whether the weight_format is 4s */
					if(weight_info->weight_format == R_ATMLIB_CNN_DATAFM_4S)
					{
						/* check the member value of the weight_info */
						if( (weight_info->ch_weight[loop_cnt].value[loop_cnt2] > (int32_t)(0x00000007)) ||
						    (weight_info->ch_weight[loop_cnt].value[loop_cnt2] < (int32_t)(0xFFFFFFF8))
						)
						{
							ret = R_ATMLIB_E_NG_WB_VALUE;
						}
					}
					/* check whether the weight_format is 4u */
					else if(weight_info->weight_format == R_ATMLIB_CNN_DATAFM_4U)
					{
						/* check the member value of the weight_info */
						if( (weight_info->ch_weight[loop_cnt].value[loop_cnt2] > (int32_t)(0x0000000F)) ||
						    (weight_info->ch_weight[loop_cnt].value[loop_cnt2] < (int32_t)(0x00000000))
						)
						{
							ret = R_ATMLIB_E_NG_WB_VALUE;
						}
					}
					/* check whether the weight_format is 8s */
					else if(weight_info->weight_format == R_ATMLIB_CNN_DATAFM_8S)
					{
						/* check the member value of the weight_info */
						if( (weight_info->ch_weight[loop_cnt].value[loop_cnt2] > (int32_t)(0x0000007F)) ||
						    (weight_info->ch_weight[loop_cnt].value[loop_cnt2] < (int32_t)(0xFFFFFF80) )
						)
						{
							ret = R_ATMLIB_E_NG_WB_VALUE;
						}
					}
					/* check whether the weight_format is 8u */
					else if(weight_info->weight_format == R_ATMLIB_CNN_DATAFM_8U)
					{
						/* check the member value of the weight_info */
						if( (weight_info->ch_weight[loop_cnt].value[loop_cnt2] > (int32_t)(0x000000FF)) ||
						    (weight_info->ch_weight[loop_cnt].value[loop_cnt2] < (int32_t)(0x00000000))
						)
						{
							ret = R_ATMLIB_E_NG_WB_VALUE;
						}
					}
					/* check whether the weight_format is 16s */
					else if(weight_info->weight_format == R_ATMLIB_CNN_DATAFM_16S)
					{
						/* check the member value of the weight_info */
						if( (weight_info->ch_weight[loop_cnt].value[loop_cnt2] > (int32_t)(0x00007FFF)) ||
						    (weight_info->ch_weight[loop_cnt].value[loop_cnt2] < (int32_t)(0xFFFF8000))
						)
						{
							ret = R_ATMLIB_E_NG_WB_VALUE;
						}
					}
					else
					{
						/* check the member value of the weight_info */
						if( (weight_info->ch_weight[loop_cnt].value[loop_cnt2] > (int32_t)(0x0000FFFF)) ||
						    (weight_info->ch_weight[loop_cnt].value[loop_cnt2] < (int32_t)(0x00000000))
						)
						{
							ret = R_ATMLIB_E_NG_WB_VALUE;
						}
					}
				}
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamLUTWPR
 [ Params ]		lut_info                        : Pointer to the DMARF structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_LUT_FMT			: Member lut_format is out of range
				R_ATMLIB_E_NG_LUT_VALUE			: Member value is out of range
				R_ATMLIB_E_NG_COMB_LUTFM_ARIFM	: Combination of lut_format and ari_format is invalid
 [ Function ]	Check LUT WPR parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamLUTWPR(
	R_ATMLIB_CLData 			*cldata,
	const R_ATMLIB_CNNLUTInfo	*lut_info
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				lutfm_index;
	uint16_t				loop_cnt;

	/* initialize local variables */
	ret 	  	= R_ATMLIB_E_OK;
	lutfm_index = 0U;
	loop_cnt 	= 0U;

	/* const table of format conversion status */
	R_ATMLIB_CNN_STATIC const R_ATMLIB_RETURN_VALUE atmlib_cnn_Foramt_Matrix_Tbl[2][R_ATMLIB_CNN_ARIFORMAT_END] =
	{   /*  R_ATMLIB_CNN_ARIFORMAT_8S    R_ATMLIB_CNN_ARIFORMAT_8U	R_ATMLIB_CNN_ARIFORMAT_16S   R_ATMLIB_CNN_ARIFORMAT_16U */
		{	R_ATMLIB_E_NG,			 	 R_ATMLIB_E_OK,   			R_ATMLIB_E_NG, 				 R_ATMLIB_E_OK,},	/* R_ATMLIB_CNN_DATAFM_8U  */
		{	R_ATMLIB_E_OK,               R_ATMLIB_E_NG,				R_ATMLIB_E_OK, 				 R_ATMLIB_E_NG,},	/* R_ATMLIB_CNN_DATAFM_8S  */
	};

	/* if lut_format is 8U set lutfm_index to 0 or set to 1 */
	lutfm_index = (lut_info->lut_format == R_ATMLIB_CNN_DATAFM_8U) ? 0U : 1U;

	/* check the member lut_format of the lut_info */
	if((lut_info->lut_format != R_ATMLIB_CNN_DATAFM_8U) && 
	   (lut_info->lut_format != R_ATMLIB_CNN_DATAFM_8S) 
	)
	{
		ret = R_ATMLIB_E_NG_LUT_FMT;
	}
	/* check wether format conversions from lut_format to ari_format are supported */
	else if( atmlib_cnn_Foramt_Matrix_Tbl[lutfm_index][cldata->cnn_data->ari_format] != R_ATMLIB_E_OK )
	{
		ret = R_ATMLIB_E_NG_COMB_LUTFM_ARIFM;
	}
	else
	{
		/* loop for the LUT table size */
		for(loop_cnt = 0; loop_cnt < R_ATMLIB_CNN_LUTTABLE_SIZE; loop_cnt++)
		{
			/* check whether the lut_format is 8s */
			if( lut_info->lut_format == R_ATMLIB_CNN_DATAFM_8S)
			{
				/* check the member value of the lut_info */
				if( (lut_info->value[loop_cnt] > (int16_t)(0x007F)) ||
					(lut_info->value[loop_cnt] < (int16_t)(0xFF80) )
				)
				{
					ret = R_ATMLIB_E_NG_LUT_VALUE;
				}
			}
			/* check whether the lut_format is 8u */
			else
			{
				/* check the member value of the lut_info */
				if( (lut_info->value[loop_cnt] > (int16_t)(0x00FF)) ||
					(lut_info->value[loop_cnt] < (int16_t)(0x0000))
				)
				{
					ret = R_ATMLIB_E_NG_LUT_VALUE;
				}
			}
		}		
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckParamCounter
 [ Params ]		perform_counter					: The pointer to R_ATMLIB_CNNPerformCounter structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_COUNTER_NUM	    : Member counter_num is out of range
				R_ATMLIB_E_NG_COUNTER_ID		: Member counter_id is out of range
 				R_ATMLIB_E_NG_COUNTER_CTRL   	: Member counter_en is out of range
				R_ATMLIB_E_NG_COUNTER_STRSN		: Member stall_reason is out of range
 				R_ATMLIB_E_NG_COUNTER_FUNC	    : Member func is out of range
				R_ATMLIB_E_NG_COUNTER_UNITNO	: Member unit_no is out of range
 				R_ATMLIB_E_NG_COM_UNITNO_FUNC	: Combination of unit_no and func is invalid
				R_ATMLIB_E_NG_COM_FUNC_STRSN	: Combination of stall_reason and func is invalid
 [ Function ]	Check paramcounter parameter.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamCounter(const R_ATMLIB_CNNPerformCounter *perform_counter)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint8_t					loop_cnt;

	/* initialize local variables */
	ret 	 = R_ATMLIB_E_OK;
	loop_cnt = 0U;

	/* check the member counter_cnt of the perform_counter */
	if( (perform_counter->counter_cnt < 1U) || 
		(perform_counter->counter_cnt > R_ATMLIB_CNN_PERFORMCNT_NUM) 
	)
	{
		ret = R_ATMLIB_E_NG_COUNTER_NUM;
	}
	else
	{
		/* loop for the counter_cnt of the perform_counter */
		for( loop_cnt = 0; loop_cnt < perform_counter->counter_cnt; loop_cnt++ )
		{
			/* check the member counter_id of the perform_counter */
			if( perform_counter->counter_info[loop_cnt].counter_id >= R_ATMLIB_CNN_COUNTER_ID_END )
			{
				ret = R_ATMLIB_E_NG_COUNTER_ID;
			}
			/* check the member counter_en of the perform_counter */
			else if( perform_counter->counter_info[loop_cnt].counter_en >= R_ATMLIB_CNN_ENABLECONTROL_END )
			{
				ret = R_ATMLIB_E_NG_COUNTER_CTRL;
			}
			/* check the member stall_reason of the perform_counter */
			else if( perform_counter->counter_info[loop_cnt].stall_reason >= R_ATMLIB_CNN_STALL_END )
			{
				ret = R_ATMLIB_E_NG_COUNTER_STRSN;
			}
			/* check the member func of the perform_counter */
			else if( perform_counter->counter_info[loop_cnt].func >= R_ATMLIB_CNN_CNT_FUNC_END )
			{
				ret = R_ATMLIB_E_NG_COUNTER_FUNC;
			}
			/* check the member unit_num of the perform_counter */
			else if( perform_counter->counter_info[loop_cnt].unit_num > 32U )
			{
				ret = R_ATMLIB_E_NG_COUNTER_UNITNO;
			}
			/* check combination of func and unit_num 
				For CL, ARI and RF, only unit number setting 0 is allowed. */
			else if(  (perform_counter->counter_info[loop_cnt].func >= R_ATMLIB_CNN_CNT_FUNC_ARIB) &&
					  (perform_counter->counter_info[loop_cnt].unit_num != 0U)
			)
			{
				ret = R_ATMLIB_E_NG_COMB_UNITNO_FUNC;
			}
			/* check combination of func and unit_num 
				For DMAI, unit number setting 0-15 is allowed for selection of a single unit and 
				32 is allowed for selection of all units. */
			else if( ((perform_counter->counter_info[loop_cnt].func >= R_ATMLIB_CNN_CNT_FUNC_DMAIB) && (perform_counter->counter_info[loop_cnt].func <= R_ATMLIB_CNN_CNT_FUNC_DMAIS) ) &&
					  (perform_counter->counter_info[loop_cnt].unit_num > 15U && perform_counter->counter_info[loop_cnt].unit_num != 32U) 
			)
			{
				ret = R_ATMLIB_E_NG_COMB_UNITNO_FUNC;
			}
			/* check combination of func and stall_reason
				Stall reason for DMAI DMA3DC DMAO : 001 010 */
			else if( (perform_counter->counter_info[loop_cnt].func == R_ATMLIB_CNN_CNT_FUNC_DMAIS) &&
					 ((perform_counter->counter_info[loop_cnt].stall_reason < R_ATMLIB_CNN_STALL_DASINK) || (perform_counter->counter_info[loop_cnt].stall_reason > R_ATMLIB_CNN_STALL_DASRC_SYNCS) )
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FUNC_STRSN;
			}
			/* check combination of func and stall_reason
				Stall reason for DMAI DMA3DC DMAO : 001 010 */
			else if( (perform_counter->counter_info[loop_cnt].func == R_ATMLIB_CNN_CNT_FUNC_DMACS) &&
					 ((perform_counter->counter_info[loop_cnt].stall_reason < R_ATMLIB_CNN_STALL_DASINK) || (perform_counter->counter_info[loop_cnt].stall_reason > R_ATMLIB_CNN_STALL_DASRC_SYNCS) )
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FUNC_STRSN;
			}
			/* check combination of func and stall_reason
				Stall reason for DMAI DMA3DC DMAO : 001 010 */
			else if( (perform_counter->counter_info[loop_cnt].func == R_ATMLIB_CNN_CNT_FUNC_DMAOS) &&
					 ((perform_counter->counter_info[loop_cnt].stall_reason < R_ATMLIB_CNN_STALL_DASINK) || (perform_counter->counter_info[loop_cnt].stall_reason > R_ATMLIB_CNN_STALL_DASRC_SYNCS) )
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FUNC_STRSN;
			}
			/* check combination of func and stall_reason
				Stall reason for DMARF CL : 001 010 */
			else if((perform_counter->counter_info[loop_cnt].func == R_ATMLIB_CNN_CNT_FUNC_CLS) &&
					((perform_counter->counter_info[loop_cnt].stall_reason < R_ATMLIB_CNN_STALL_DASINK) || (perform_counter->counter_info[loop_cnt].stall_reason > R_ATMLIB_CNN_STALL_DASRC_SYNCS) )
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FUNC_STRSN;
			}
			/* check combination of func and stall_reason
				Stall reason for DMARF CL : 001 010 */
			else if((perform_counter->counter_info[loop_cnt].func == R_ATMLIB_CNN_CNT_FUNC_DMARFS) &&
					((perform_counter->counter_info[loop_cnt].stall_reason < R_ATMLIB_CNN_STALL_DASINK) || (perform_counter->counter_info[loop_cnt].stall_reason > R_ATMLIB_CNN_STALL_DASRC_SYNCS) )
			)
			{
				ret = R_ATMLIB_E_NG_COMB_FUNC_STRSN;
			}
			else
			{
				/* DO NOTHING */
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCalculateARILen
 [ Params ]		dmai_ch						: DMAI channel parameters
				kernel_mode					: Kernel mode
				dil_conv					: Dilated convolution distance
				ari_xlen					: Pointer to ARI channel width
				ari_ylen					: Pointer to ARI channel height
 [ Returns ]	0U							: success
				1U							: Kernel_size is greater than ari_ylen
				2U							: Kernel_size is greater than ari_ylen
 [ Function ]	Calculate ARI width and height
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC uint32_t atmlib_CNNCalculateARILen(
	const R_ATMLIB_CNNDMAIChParam   	dmai_ch,
	const R_ATMLIB_CNNKernelMode        kernel_mode,
	uint8_t                             dil_conv,
	uint16_t							*ari_xlen,
	uint16_t							*ari_ylen
)
{
	uint8_t  kernel_size;
	uint32_t ret;

	/* initialize local variables */
	kernel_size = 0U;
	ret			= 0U;

	/* set kernel_size to 5U when kernel_mode is R_ATMLIB_CNN_KERNEL_1_5_5 */
	if( kernel_mode == R_ATMLIB_CNN_KERNEL_1_5_5 )
	{
		kernel_size = 5U;
	}
	/* set kernel_size to 3U when kernel_mode is R_ATMLIB_CNN_KERNEL_2_3_3 or R_ATMLIB_CNN_KERNEL_3_3_3  */
	else if( kernel_mode == R_ATMLIB_CNN_KERNEL_2_3_3 || kernel_mode == R_ATMLIB_CNN_KERNEL_3_3_3 )
	{
		kernel_size = 3U;
	}
	/* set kernel_size to 1U when kernel_mode is R_ATMLIB_CNN_KERNEL_4_1_1 */
	else
	{
		kernel_size = 1U;
	}

	/* taking mag_type--R_ATMLIB_CNN_SKP into account when calculate the ari_len */
	if( dmai_ch.mag_type == R_ATMLIB_CNN_SKP )
	{
		*ari_xlen = (uint16_t)(dmai_ch.image_x / (uint16_t)(dmai_ch.repeat_skip_x + 1U));
		*ari_ylen = (uint16_t)(dmai_ch.image_y / (uint16_t)(dmai_ch.repeat_skip_y + 1U));
	}
	/* taking mag_type--R_ATMLIB_CNN_RPT* into account when calculate the ari_len */
	else if( dmai_ch.mag_type == R_ATMLIB_CNN_RPT_ZERO ||
			 dmai_ch.mag_type == R_ATMLIB_CNN_RPT_IDENTITY
	)
	{
		*ari_xlen = (uint16_t)(dmai_ch.image_x * (uint16_t)(dmai_ch.repeat_skip_x + 1U));
		*ari_ylen = (uint16_t)(dmai_ch.image_y * (uint16_t)(dmai_ch.repeat_skip_y + 1U));
	}
	/* taking mag_type--R_ATMLIB_CNN_NOMAG into account when calculate the ari_len */
	else
	{
		*ari_xlen = dmai_ch.image_x;
		*ari_ylen = dmai_ch.image_y;
	}

	/* taking dil_conv into account when calculate the ari_len */
	*ari_xlen = (uint16_t)(*ari_xlen / (dil_conv + 1U));

	/* taking pad_l/r/t/b and start_pos into account when calculate the ari_len */
	*ari_xlen = (uint16_t)(*ari_xlen + dmai_ch.pad_l + dmai_ch.pad_r - dmai_ch.start_pos);
	*ari_ylen = (uint16_t)(*ari_ylen + dmai_ch.pad_t + dmai_ch.pad_b);

	/* check whether the kernel_size is greater than (*ari_xlen + (dmai_ch.stride_x -1U) */
	if( (*ari_xlen + (dmai_ch.stride_x - 1U)) < kernel_size )
	{
		ret = 1U;
	}
	/* check whether the kernel_size is greater than (*ari_ylen + (dmai_ch.stride_y -1U) */
	else if( (*ari_ylen + (dmai_ch.stride_y - 1U)) < kernel_size  )
	{
		ret = 2U;
	}
	else
	{
		/* taking stride_x and kernel_size into account when calculate the ari_len */
		*ari_xlen = (uint16_t)((*ari_xlen + (dmai_ch.stride_x - 1U) - kernel_size) / dmai_ch.stride_x + 1U);
		*ari_ylen = (uint16_t)((*ari_ylen + (dmai_ch.stride_y - 1U) - kernel_size) / dmai_ch.stride_y + 1U);
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupFastDMAI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmai_param						: DMAI parameters  
				clofs_imgaddr					: Image address offset of writes in CL data
 [ Returns ]	none
 [ Function ]   Add DMAI channel setup information to the command list in fast setup mode.
 [ Note ]       none
******************************************************************************/
void atmlib_CNNSetupFastDMAI(
	R_ATMLIB_CLData 			*cldata, 
	const R_ATMLIB_CNNDMAIParam *dmai_param, 
	uint32_t					*clofs_dmai
)
{
	uint32_t				DMAIE;
	uint32_t				PADD_STRIDE_STARTPOS;
	uint32_t				DMAIOA;
	uint16_t				dmaisa_index;
	uint8_t					wpr_n;
	uint16_t				add;
	uint32_t				*temp_cur_addr;
	uint32_t				data[4];
	uint8_t					ch_num;
	uint8_t					loop_cnt;

	/* initialize local variables */
	DMAIE						= 0U;
	PADD_STRIDE_STARTPOS		= 0U;
	DMAIOA						= 0U;
	dmaisa_index				= 0U;
	wpr_n						= 0U;
	add							= 0U;
	ch_num						= 0U;
	loop_cnt					= 0U;

	/* check if the data_mode is R_ATMLIB_CNN_DATA_SPMC */
	if( dmai_param->data_mode == R_ATMLIB_CNN_DATA_SPMC )
	{
		/* update the channel_props for DMAI (DMAIE) */
		cldata->cnn_data->channel_props.DMAIE = (uint32_t)1U;
	}
	else
	{
		for( loop_cnt = 0U; loop_cnt < dmai_param->ch_count; loop_cnt++ )
		{
			/* calculate the ch_num */
			ch_num = (uint8_t)(dmai_param->ch_id[loop_cnt]);
			/* update the channel_props for DMAI (DMAIE) */
			DMAIE |= (uint32_t)(1U << ch_num);
		}
		/* update the channel_props for DMAI (DMAIE) */
		cldata->cnn_data->channel_props.DMAIE = DMAIE;
	}
	/* update the dmai_frac */
	cldata->cnn_data->dmai_frac[0] = dmai_param->dmai_ch[0].dmai_frac;

	/* update the channel_props for DMAI (DMAIS) */
	cldata->cnn_data->channel_props.DMAIS = (((uint32_t)dmai_param->data_mode)                   & 0x00000001U)	|
											(((uint32_t)1U								  << 1U) & 0x00000002U)	|
											(((uint32_t)dmai_param->kernel_mode           << 2U) & 0x0000001CU);

	/* make preset data to the CL (PADD_STRIDE_STARTPOS) */
	PADD_STRIDE_STARTPOS =  (((uint32_t)dmai_param->dmai_ch[0].pad_t          ) & 0x00000007U)	|
						    (((uint32_t)dmai_param->dmai_ch[0].pad_b    <<  3U) & 0x00000038U)	|
							(((uint32_t)dmai_param->dmai_ch[0].pad_l    <<  6U) & 0x000007C0U)	|
							(((uint32_t)dmai_param->dmai_ch[0].pad_r    << 11U) & 0x0000F800U)  |
							(((uint32_t)dmai_param->dmai_ch[0].stride_x << 16U) & 0x00070000U)  |
							(((uint32_t)dmai_param->dmai_ch[0].stride_y << 20U) & 0x00700000U)  |
							(((uint32_t)dmai_param->dmai_ch[0].start_pos<< 24U) & 0x07000000U)  |
							(((uint32_t)dmai_param->dil_conv            << 28U) & 0x30000000U);
	/* fix the dest address */
	add		= (uint16_t)(R_ATMLIB_CNN_PADD_STRIDE_STARTPOS / 4U);
	/* fix write size in CL (PADD_STRIDE_STARTPOS) */
	wpr_n	= 1U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, &PADD_STRIDE_STARTPOS, wpr_n );

	/* initialize writting data */
	(void)memset( data, 0U, sizeof(data) );
	/* make preset data to the CL (DMAL/DMAIST/DMAIMAG/DMAIFM) */ 
	data[0] = (((uint32_t)dmai_param->dmai_ch[0].image_y       << 16U) & 0x7FFF0000U) |
			  ( (uint32_t)dmai_param->dmai_ch[0].image_x               & 0x000003FFU);
	data[1] = (((uint32_t)dmai_param->dmai_ch[0].image_stride        ) & 0x0000FFFFU);
	data[2] = (((uint32_t)dmai_param->dmai_ch[0].mag_type      << 6U ) & 0x000000C0U) |
			  (((uint32_t)dmai_param->dmai_ch[0].repeat_skip_y << 3U ) & 0x00000038U) |
			  (((uint32_t)dmai_param->dmai_ch[0].repeat_skip_x       ) & 0x00000007U);
	data[3] = (((uint32_t)dmai_param->dmai_ch[0].image_format        ) & 0x00000007U);
	
	/* fix the dest address */
	add		= (uint16_t)(R_ATMLIB_CNN_DMAIL / 4U);
	/* fix write size in CL (DMAL/DMAIST/DMAIMAG/DMAIFM) */
	wpr_n	= 4U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );

	/* caliculate index of DMAISA */
	dmaisa_index = (uint16_t)(R_ATMLIB_CNN_DMAISA / 4U);

	/* check the base address setting state */
    switch( cldata->cnn_data->baddr_info.baddr_dmai[0].baddr_state )
	{
	case R_ATMLIB_CNN_BADDRSTAT_UNSET:
		/* make preset data to the CL (DMAISA) */
		data[0] = (uint32_t)(dmai_param->dmai_ch[0].ptr);
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMAICO) */
			data[1] = (uint32_t)(dmai_param->channel_offset);
			/* fix write size in CL (DMAISA/DMAICO) */
			wpr_n = 2U;
		}
		else
		{
			/* fix write size in CL (DMAISA) */
			wpr_n = 1U;
		}
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMAISA / 4U);
		/* backup the current address of the CL */
		temp_cur_addr = cldata->cur_addr;
		/* add the WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );

		/* check whether add WPR command and need notify application */
		if( (temp_cur_addr != cldata->cur_addr)	&&
			(clofs_dmai != NULL) )
		{
			/* set image address of clofs_dmai */
			clofs_dmai[0] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
		}
		break;

	case R_ATMLIB_CNN_BADDRSTAT_SET1ST:
		/* make preset data to the CL (DMAISA) */
		data[0] = (uint32_t)(cldata->cnn_data->baddr_info.baddr_dmai[0].baddr);
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMAICO) */
			data[1] = (uint32_t)(dmai_param->channel_offset);
			/* fix write size in CL (DMAISA/DMAICO) */
			wpr_n = 2U;
		}
		else
		{
			/* fix write size in CL (DMAISA) */
			wpr_n = 1U;
		}
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMAISA / 4U);
		/* backup the current address of the CL */
		temp_cur_addr	= cldata->cur_addr;
		/* add the WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );

		/* check whether add WPR command and need notify application */
		if( (temp_cur_addr != cldata->cur_addr)	&&
			(clofs_dmai != NULL) )
		{
			clofs_dmai[0] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
		}

		/* make preset data to the CL (DMAIOA) */
		DMAIOA  = (uint32_t)(dmai_param->dmai_ch[0].ptr - cldata->cnn_data->baddr_info.baddr_dmai[0].baddr);
		/* fix the dest address and fix write size in CL (DMAIOA) */   
		add		= (uint16_t)(R_ATMLIB_CNN_DMAIOA / 4U);
		wpr_n	= 1U;

		/* add the WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAIOA, wpr_n );

		/* update the current_reg */
		cldata->cnn_data->current_reg[dmaisa_index].value	= cldata->cnn_data->current_reg[dmaisa_index].value + DMAIOA;
		cldata->cnn_data->current_reg[dmaisa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;

		/* update the base address setting state */
		cldata->cnn_data->baddr_info.baddr_dmai[0].baddr_state = R_ATMLIB_CNN_BADDRSTAT_SET2ND;
		break;

	case R_ATMLIB_CNN_BADDRSTAT_SET2ND:
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMAICO) */
			data[0] = (uint32_t)(dmai_param->channel_offset);
			/* fix write size in CL (DMAICO) */
			wpr_n = 1U;
			/* fix the dest address */
			add		= (uint16_t)(R_ATMLIB_CNN_DMAICO / 4U);
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
		}

		/* make preset data to the CL (DMAIOA) */
		DMAIOA  = (uint32_t)(dmai_param->dmai_ch[0].ptr - cldata->cnn_data->current_reg[dmaisa_index].value);   
		/* fix the dest address and fix write size in CL (DMAIOA) */     
		add		= (uint16_t)(R_ATMLIB_CNN_DMAIOA / 4U);
		wpr_n	= 1U;

		/* add the WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAIOA, wpr_n );

		/* update the current_reg */
		cldata->cnn_data->current_reg[dmaisa_index].value	= cldata->cnn_data->current_reg[dmaisa_index].value + DMAIOA;
		cldata->cnn_data->current_reg[dmaisa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
		break;

	default:
		/* DO NOTHING */
		break;
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNPresetDataDMAI
 [ Params ]		dmai_param					: DMAI parameters
				padd_stride_startpos		: Register which have PADD, STRIDE and START_POS information 
				dmail						: Register which have DMAIL information
				dmaist						: Register which have DMAIST information
				dmaimag						: Register which have DMAIMAG information
				dmaifm						: Register which have DMAIFM information
 [ Returns ]	none
 [ Function ]	Make the preset data for DMAI
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC void atmlib_CNNPresetDataDMAI(
	const R_ATMLIB_CNNDMAIParam  	*dmai_param,
	uint32_t						*padd_stride_startpos, 
	uint32_t						*dmail,
	uint32_t						*dmaist,
	uint32_t						*dmaimag,
	uint32_t						*dmaifm
)
{
	uint8_t 				loop_cnt;
	uint8_t					ch_num;
	uint32_t				PADD[16];
	uint32_t				PADD_COMB[8];
	uint32_t				STRIDE[16];
	uint32_t				STRIDE_COMB[4];
	uint32_t				START_POS[16];
	uint32_t				START_POS_COMB[2];
	uint32_t                DMAIL[16];
	uint32_t                DMAIST[16];
	uint32_t                DMAIMAG[16];
	uint32_t                DMAIMAG_COMB[4];
	uint32_t 				DMAIFM[16];
	uint32_t 				DMAIFM_COMB[2];

	/* initialize local variables */
	loop_cnt = 0U;
	ch_num   = 0U;
	(void)memset( PADD,            0U, 			sizeof(PADD)           );
	(void)memset( PADD_COMB,       0U, 			sizeof(PADD_COMB)      );
	(void)memset( STRIDE,          0U, 			sizeof(STRIDE)         );
	(void)memset( STRIDE_COMB,     0U, 			sizeof(STRIDE_COMB)    );
	(void)memset( START_POS,	   0U, 			sizeof(START_POS)      );
	(void)memset( START_POS_COMB,  0U, 			sizeof(START_POS_COMB) );
	(void)memset( DMAIL,           0U, 			sizeof(DMAIL)          );
	(void)memset( DMAIST,          0U, 			sizeof(DMAIST)   	   );
	(void)memset( DMAIMAG,         0U, 			sizeof(DMAIMAG)        );
	(void)memset( DMAIMAG_COMB,    0U, 			sizeof(DMAIMAG_COMB)   );
	(void)memset( DMAIFM,	       0U, 			sizeof(DMAIFM)         );
	(void)memset( DMAIFM_COMB,     0U, 			sizeof(DMAIFM_COMB)    );

	/* loop for the ch_count of dmai_param */
	for(loop_cnt = 0; loop_cnt < dmai_param->ch_count; loop_cnt++ )
	{
		/* calculate the channel number of the dmai */
		ch_num =  (uint8_t)(dmai_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAI0);

		/* make preset data to the CL(PADD00_01~PADD14_15) */
		PADD[ch_num]      = (((uint32_t)dmai_param->dmai_ch[loop_cnt].pad_t)                & 0x00000007U) |
							(((uint32_t)dmai_param->dmai_ch[loop_cnt].pad_b   <<  3U)       & 0x00000038U) |
							(((uint32_t)dmai_param->dmai_ch[loop_cnt].pad_l   <<  6U)       & 0x000007C0U) |
							(((uint32_t)dmai_param->dmai_ch[loop_cnt].pad_r   << 11U)       & 0x0000F800U);

		/* make preset data to the CL(STRIDE00_03~STRIDE12_15) */
		STRIDE[ch_num]    = (((uint32_t)dmai_param->dmai_ch[loop_cnt].stride_x)             & 0x00000007U) |
						    (((uint32_t)dmai_param->dmai_ch[loop_cnt].stride_y <<  4U)      & 0x00000070U);

		/* make preset data to the CL(START_POS_08~START_POS_08_15) */
		START_POS[ch_num] = (((uint32_t)dmai_param->dmai_ch[loop_cnt].start_pos) 		    & 0x00000007U);

		/* make preset data to the CL(DMAIL00~DMAIL15) */
		DMAIL[ch_num]     = (((uint32_t)dmai_param->dmai_ch[loop_cnt].image_x)  	 		& 0x000003FFU) |
						    (((uint32_t)dmai_param->dmai_ch[loop_cnt].image_y << 16U)       & 0x7FFF0000U);
		/* make preset data to the CL(DMAIST00~DMAIST15) */
		DMAIST[ch_num]    = (((uint32_t)dmai_param->dmai_ch[loop_cnt].image_stride)         & 0x0000FFFFU);
		/* make preset data to the CL(DMAIMAG00_03~DMAIMAG12_15) */
		DMAIMAG[ch_num]   = (((uint32_t)dmai_param->dmai_ch[loop_cnt].repeat_skip_x)  	    & 0x00000007U) |
						    (((uint32_t)dmai_param->dmai_ch[loop_cnt].repeat_skip_y << 3U)  & 0x00000038U) |
						    (((uint32_t)dmai_param->dmai_ch[loop_cnt].mag_type      << 6U)  & 0x000000C0U);
		/* make preset data to the CL(DMAIFM00_07~DMAIFM08_15) */
		DMAIFM[ch_num]    = (((uint32_t)dmai_param->dmai_ch[loop_cnt].image_format)  		& 0x00000007U);
	}

	/* make preset data to the CL(PADD00_01~PADD14_15) */
	for(loop_cnt = 0; loop_cnt < R_ATMLIB_CNN_DMAI_MAX_CH / 2U; loop_cnt++ )
	{
		PADD_COMB[loop_cnt] = ( (PADD[loop_cnt*2U + 1] << 16U) & 0xFFFF0000U ) |
						 	    (PADD[loop_cnt*2U]             & 0x0000FFFFU );
	}
	/* make preset data to the CL (DMAIST00~DMAIST15)/(DMAIMAG00_03~DMAIMAG12_15) */
	for(loop_cnt = 0; loop_cnt < R_ATMLIB_CNN_DMAI_MAX_CH / 4U; loop_cnt++ )
	{
		STRIDE_COMB[loop_cnt] =  ( (STRIDE[loop_cnt*4U + 3] << 24U) & 0xFF000000U) |
								 ( (STRIDE[loop_cnt*4U + 2] << 16U) & 0x00FF0000U) | 
								 ( (STRIDE[loop_cnt*4U + 1] << 8U ) & 0x0000FF00U) | 
						 		 (  STRIDE[loop_cnt*4U]             & 0x000000FFU);

		DMAIMAG_COMB[loop_cnt] = (  (DMAIMAG[loop_cnt*4U + 3] << 24U) & 0xFF000000U) | 
						 		 (	(DMAIMAG[loop_cnt*4U + 2] << 16U) & 0x00FF0000U) | 
						 		 (	(DMAIMAG[loop_cnt*4U + 1] << 8U ) & 0x0000FF00U) |
						 		 (   DMAIMAG[loop_cnt*4U]             & 0x000000FFU);

	}
	/* make preset data to the CL (STRIDE00_03~STRIDE12_15)/(DMAIFM00_07~DMAIFM08_15) */
	for(loop_cnt = 0; loop_cnt < R_ATMLIB_CNN_DMAI_MAX_CH / 8U; loop_cnt++ )
	{
		START_POS_COMB[loop_cnt] =  ( (START_POS[loop_cnt*8U + 7U] << 28U) & 0xF0000000U) | 
									( (START_POS[loop_cnt*8U + 6U] << 24U) & 0x0F000000U) | 
									( (START_POS[loop_cnt*8U + 5U] << 20U) & 0x00F00000U) | 
									( (START_POS[loop_cnt*8U + 4U] << 16U) & 0x000F0000U) | 
									( (START_POS[loop_cnt*8U + 3U] << 12U) & 0x0000F000U) | 
									( (START_POS[loop_cnt*8U + 2U] << 8U ) & 0x00000F00U) | 
									( (START_POS[loop_cnt*8U + 1U] << 4U ) & 0x000000F0U) | 
									(  START_POS[loop_cnt*8U]              & 0x0000000FU);
		
		DMAIFM_COMB[loop_cnt] = ( (DMAIFM[loop_cnt*8U + 7U] << 28U) & 0xF0000000U) | 
								( (DMAIFM[loop_cnt*8U + 6U] << 24U) & 0x0F000000U) | 
								( (DMAIFM[loop_cnt*8U + 5U] << 20U) & 0x00F00000U) | 
								( (DMAIFM[loop_cnt*8U + 4U] << 16U) & 0x000F0000U) | 
								( (DMAIFM[loop_cnt*8U + 3U] << 12U) & 0x0000F000U) | 
								( (DMAIFM[loop_cnt*8U + 2U] << 8U ) & 0x00000F00U) | 
								( (DMAIFM[loop_cnt*8U + 1U] << 4U ) & 0x000000F0U) | 
								(  DMAIFM[loop_cnt*8U]              & 0x0000000FU);

	}

	/* make preset data to the CL */
	memcpy(padd_stride_startpos,      PADD_COMB,      8U*sizeof(uint32_t));
	memcpy(padd_stride_startpos + 8U, STRIDE_COMB,    4U*sizeof(uint32_t));
	memcpy(padd_stride_startpos + 12U,START_POS_COMB, 2U*sizeof(uint32_t));

	memcpy(dmail,   DMAIL,        16U*sizeof(uint32_t));
	memcpy(dmaist,  DMAIST,       16U*sizeof(uint32_t));
	memcpy(dmaimag, DMAIMAG_COMB, 4U *sizeof(uint32_t));
	memcpy(dmaifm,  DMAIFM_COMB,  2U *sizeof(uint32_t));

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupSlowDMAI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmai_param						: DMAI parameters 
				clofs_imgaddr					: Image address offset of writes in CL data
 [ Returns ]	none
 [ Function ]   Add DMAI channel setup information to the command list in slow setup mode.
 [ Note ]       none
******************************************************************************/
void atmlib_CNNSetupSlowDMAI(
	R_ATMLIB_CLData 			*cldata, 
	const R_ATMLIB_CNNDMAIParam *dmai_param, 
	uint32_t					*clofs_dmai
)
{
	uint32_t				DMAIE;
	uint32_t				DIL_CONV;
	uint32_t				dmai_frac[16];
	uint32_t				PADD_STRIDE_STARTPOS[14];
	uint32_t				DMAISA;
	uint32_t				DMAIL[16];
	uint32_t				DMAIST[16];
	uint32_t				DMAIMAG[4];
	uint32_t				DMAIFM[2];
	uint32_t				DMAIOA;
	uint8_t					wpr_n;
	uint16_t				add;
	uint32_t				*temp_cur_addr;
	uint8_t					ch_num;
	uint16_t				dmaisa_index;
	uint8_t					loop_cnt;

	/* initialize local variables */
	DMAIE			= 0U;
	DIL_CONV		= 0U;
	DMAISA			= 0U;
	DMAIOA			= 0U;
	wpr_n			= 0U;
	add				= 0U;
	ch_num			= 0U;
	dmaisa_index	= 0U;
	loop_cnt		= 0U;

	/* initialize writting data */
	(void)memset( dmai_frac, 			0U, sizeof(dmai_frac) );
	(void)memset( PADD_STRIDE_STARTPOS, 0U, sizeof(PADD_STRIDE_STARTPOS) );
	(void)memset( DMAIL, 				0U, sizeof(DMAIL) );
	(void)memset( DMAIST, 				0U, sizeof(DMAIST) );
	(void)memset( DMAIMAG, 				0U, sizeof(DMAIMAG) );
	(void)memset( DMAIFM, 				0U, sizeof(DMAIFM) );

	for(loop_cnt = 0; loop_cnt < dmai_param->ch_count; loop_cnt++ )
	{
		ch_num 			  = (uint8_t)(dmai_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAI0);
		dmai_frac[ch_num] = dmai_param->dmai_ch[loop_cnt].dmai_frac;
	}
	/* store the parameters of dmai_frac */
	memcpy(cldata->cnn_data->dmai_frac, dmai_frac, 16U*sizeof(uint32_t));

	/* update the channel_props for DMAI (DMAIS) */
	cldata->cnn_data->channel_props.DMAIS = (((uint32_t)dmai_param->data_mode)          & 0x00000001U)	|
											(((uint32_t)0U						 << 1U) & 0x00000002U)	|
											(((uint32_t)dmai_param->kernel_mode  << 2U) & 0x0000001CU);

	/* make preset data to the CL (PADD_STRIDE_STARTPOS) */
	DIL_CONV =  (((uint32_t)dmai_param->dil_conv << 28U) & 0x30000000U) | 0x00110000U;
	/* fix the dest address */
	add		= (uint16_t)(R_ATMLIB_CNN_PADD_STRIDE_STARTPOS / 4U);
	/* fix write size in CL (PADD_STRIDE_STARTPOS) */
	wpr_n	= 1U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, &DIL_CONV, wpr_n );

	/* make preset data to the CL (PADD00_01~PADD14_15/STRIDE00_03~STRIDE12_15/START_POS_00_07~START_POS_08_15/DMAIL00~DMAIL15/DMAIST00~DMIST15/DMAIMAG00_03~DMAIMAG12_15/DMAIFM00_07/DMAIFM08_15) */
	atmlib_CNNPresetDataDMAI( dmai_param, PADD_STRIDE_STARTPOS, DMAIL, DMAIST, DMAIMAG, DMAIFM);

	/* fix the dest address */
	add	  = R_ATMLIB_CNN_PADD00_01 / 4U;
	/* fix write size in CL (PADD00_01~PADD14_15/STRIDE00_03~STRIDE12_15/START_POS_00_07~START_POS_08_15) */
	wpr_n = 14U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, PADD_STRIDE_STARTPOS, wpr_n );

	/* fix the dest address */
	add   = R_ATMLIB_CNN_DMAIL_CH_BASE / 4U;
	/* fix write size in CL (DMAIL00~DMAIL15) */
	wpr_n = 16U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMAIL, wpr_n );

	/* fix the dest address */
	add   = R_ATMLIB_CNN_DMAIST_CH_BASE / 4U;
	/* fix write size in CL (DMAIST00~DMIST15) */
	wpr_n = 16U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMAIST, wpr_n );

	/* fix the dest address */
	add   = R_ATMLIB_CNN_DMAIMAG00_03 / 4U;
	/* fix write size in CL (DMAIMAG00_03~DMAIMAG12_15) */
	wpr_n = 4U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMAIMAG, wpr_n );

	/* fix the dest address */
	add   = R_ATMLIB_CNN_DMAIFM00_07 / 4U;
	/* fix write size in CL (DMAIFM00_07~DMAIFM08_15) */
	wpr_n = 2U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMAIFM, wpr_n );

	/* loop for the ch_count of the dmai_param */
	for(loop_cnt = 0U; loop_cnt < dmai_param->ch_count; loop_cnt++ )
	{
		/* calculate the ch_num */
		ch_num = (uint8_t)(dmai_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAI0);
		/* update the channel_props for DMAI (DMAIE) */
		DMAIE |= (uint32_t)(1U << ch_num);
		/* calculate index of DMAISA(ch_num) */
		dmaisa_index = (uint16_t)((R_ATMLIB_CNN_DMAISA_CH_BASE + (R_ATMLIB_CNN_DMAISA_CH_OFST * ch_num)) / 4U);
		
		/* check the base address setting state */
		switch( cldata->cnn_data->baddr_info.baddr_dmai[ch_num].baddr_state )
		{
		case R_ATMLIB_CNN_BADDRSTAT_UNSET:
			/* make preset data to the CL (DMAISA(ch_num)) */
			DMAISA  = dmai_param->dmai_ch[ch_num].ptr;
			/* fix write size in CL (DMAISA(ch_num)) */
			wpr_n   = 1U;
			/* fix the dest address */
			add		= dmaisa_index;
			/* backup the current address of the CL */
			temp_cur_addr = cldata->cur_addr;
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAISA, wpr_n );

			/* check whether add WPR command and need notify application */
			if( (temp_cur_addr != cldata->cur_addr)	&&
				(clofs_dmai != NULL) )
			{
				/* set image address of clofs_dmai */
				clofs_dmai[ch_num] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
			}
			break;

		case R_ATMLIB_CNN_BADDRSTAT_SET1ST:
			/* make preset data to the CL (DMAISA(ch_num)) */
			DMAISA  = (uint32_t)(cldata->cnn_data->baddr_info.baddr_dmai[ch_num].baddr);
			/* fix write size in CL (DMAISA(ch_num)) */
			wpr_n   = 1U;
			/* fix the dest address */
			add		= (uint16_t)dmaisa_index;
			/* backup the current address of the CL */
			temp_cur_addr	= cldata->cur_addr;
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAISA, wpr_n );

			/* check whether add WPR command and need notify application */
			if( (temp_cur_addr != cldata->cur_addr)	&&
				(clofs_dmai != NULL) )
			{
				/* set image address of clofs_dmai */
				clofs_dmai[ch_num] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
			}

			/* make preset data to the CL (DMAIOA(ch_num)) */
			DMAIOA  = (uint32_t)(dmai_param->dmai_ch[ch_num].ptr - cldata->cnn_data->baddr_info.baddr_dmai[ch_num].baddr);  
			/* fix the dest address */    
			add		= (uint16_t)((R_ATMLIB_CNN_DMAIOA_CH_BASE + (R_ATMLIB_CNN_DMAIOA_CH_OFST * ch_num)) / 4U);
			/* fix write size in CL (DMAIOA(ch_num)) */
			wpr_n	= 1U;

			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAIOA, wpr_n );

			/* update the current_reg */
			cldata->cnn_data->current_reg[dmaisa_index].value	= cldata->cnn_data->current_reg[dmaisa_index].value + DMAIOA;
			cldata->cnn_data->current_reg[dmaisa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
			/* update the base address setting state */
			cldata->cnn_data->baddr_info.baddr_dmai[ch_num].baddr_state = R_ATMLIB_CNN_BADDRSTAT_SET2ND;
			break;

		case R_ATMLIB_CNN_BADDRSTAT_SET2ND:
			/* make preset data to the CL (DMAIOA(ch_num)) */
			DMAIOA  = (uint32_t)(dmai_param->dmai_ch[ch_num].ptr - cldata->cnn_data->current_reg[dmaisa_index].value);     
			/* fix the dest address */ 
			add		= (uint16_t)((R_ATMLIB_CNN_DMAIOA_CH_BASE + (R_ATMLIB_CNN_DMAIOA_CH_OFST * ch_num)) / 4U);;
			/* fix write size in CL (DMAIOA(ch_num)) */
			wpr_n	= 1U;

			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAIOA, wpr_n );

			/* update the current_reg */
			cldata->cnn_data->current_reg[dmaisa_index].value	= cldata->cnn_data->current_reg[dmaisa_index].value + DMAIOA;
			cldata->cnn_data->current_reg[dmaisa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
			break;

		default:
			/* DO NOTHING */
			break;
		}
	}

	/* update the channel_props for DMAI (DMAIE) */
	cldata->cnn_data->channel_props.DMAIE = DMAIE;

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupFastDMAC
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmac_param						: DMAC parameters
				clofs_imgaddr					: Image address offset of writes in CL data
 [ Returns ]	none
 [ Function ]   Add DMAC channel setup information to the command list in fast setup mode.
 [ Note ]       none
******************************************************************************/
void atmlib_CNNSetupFastDMAC(
	R_ATMLIB_CLData 			*cldata, 
	const R_ATMLIB_CNNDMACParam *dmac_param,
	uint32_t					*clofs_dmac
)
{
	uint32_t				DMA3DCE;
	uint32_t				DMA3DCOA;
	uint16_t				dmacsa_index;
	uint8_t					wpr_n;
	uint16_t				add;
	uint32_t				*temp_cur_addr;
	uint32_t				data[4];
	uint8_t					loop_cnt;
	uint8_t					ch_num;

	/* initialize local variables */
	DMA3DCE			= 0U;
	DMA3DCOA		= 0U;
	dmacsa_index	= 0U;
	wpr_n			= 0U;
	add				= 0U;
	ch_num			= 0U;
	loop_cnt		= 0U;

	/* check if the data_mode is R_ATMLIB_CNN_DATA_SPMC */
	if( dmac_param->data_mode == R_ATMLIB_CNN_DATA_SPMC )
	{
		/* update the channel_props for DMA3DC (DMA3DCE) */
		cldata->cnn_data->channel_props.DMA3DCE = (uint32_t)1U;;
	}
	else
	{
		for( loop_cnt = 0U; loop_cnt < dmac_param->ch_count; loop_cnt++ )
		{
			/* calculate the ch_num */
			ch_num = (uint8_t)(dmac_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAC0);
			/* update the channel_props for DMA3DC (DMA3DCE) */
			DMA3DCE |= (uint32_t)(1U << ch_num);
		}
		/* update the channel_props for DMA3DC (DMA3DCE) */
		cldata->cnn_data->channel_props.DMA3DCE = DMA3DCE;
	}
	/* update the dmac_frac */
	cldata->cnn_data->dmac_frac[0] = dmac_param->dmac_ch[0].dmac_frac;

	/* update the channel_props for DMAI (DMA3DCS) */
	cldata->cnn_data->channel_props.DMA3DCS = (((uint32_t)dmac_param->data_mode)                   & 0x00000001U)	|
			  								  (((uint32_t)cldata->cnn_data->setup_mode_dmac << 1U) & 0x00000002U);
	/* initialize writting data */
	(void)memset( data, 0U, sizeof(data) );
	/* make preset data to the CL (DMACL/DMACST/DMACMAG/DMACFM) */
	data[0] = (((uint32_t)dmac_param->dmac_ch[0].image_y       << 16U) & 0x7FFF0000U) |
			  ( (uint32_t)dmac_param->dmac_ch[0].image_x               & 0x000003FFU);
	data[1] = (((uint32_t)dmac_param->dmac_ch[0].image_stride        ) & 0x0000FFFFU);
	data[2] = (((uint32_t)dmac_param->dmac_ch[0].mag_type      << 6U ) & 0x000000C0U)	|
			  (((uint32_t)dmac_param->dmac_ch[0].repeat_skip_y << 3U ) & 0x00000038U)	|
			  (((uint32_t)dmac_param->dmac_ch[0].repeat_skip_x       ) & 0x00000007U);
	data[3] = (((uint32_t)dmac_param->dmac_ch[0].image_format        ) & 0x00000007U);

	/* fix the dest address */
	add		= (uint16_t)(R_ATMLIB_CNN_DMA3DCL / 4U);
	/* fix write size in CL (DMACL/DMACST/DMACMAG/DMACFM) */
	wpr_n	= 4U;
	/* add WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
	/* caliculate index of DMA3DCSA */
	dmacsa_index = (uint16_t)(R_ATMLIB_CNN_DMA3DCSA / 4U);

	/* check the base address setting state */
    switch( cldata->cnn_data->baddr_info.baddr_dmac[0].baddr_state )
	{
	case R_ATMLIB_CNN_BADDRSTAT_UNSET:
		/* make preset data to the CL (DMA3DCSA) */
		data[0] = (uint32_t)(dmac_param->dmac_ch[0].ptr);
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMA3DCCO) */
			data[1] = (uint32_t)(dmac_param->channel_offset);
			/* fix write size in CL (DMA3DCSA/DMA3DCCO) */
			wpr_n = 2U;
		}
		else
		{
			/* fix write size in CL (DMA3DCSA) */
			wpr_n = 1U;
		}
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMA3DCSA / 4U);
		/* backup the current address of the CL */
		temp_cur_addr = cldata->cur_addr;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
		/* check whether add WPR command and need notify application */
		if( (temp_cur_addr != cldata->cur_addr)	&&
			(clofs_dmac != NULL) )
		{
			/* set image address of clofs_dmac */
			clofs_dmac[0] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
		}
		break;

	case R_ATMLIB_CNN_BADDRSTAT_SET1ST:
		/* make preset data to the CL (DMA3DCSA) */
		data[0] = (uint32_t)(cldata->cnn_data->baddr_info.baddr_dmac[0].baddr);
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMA3DCOA) */
			data[1] = (uint32_t)(dmac_param->channel_offset);
			/* fix write size in CL (DMA3DCSA/DMA3DCOA) */
			wpr_n = 2U;
		}
		else
		{
			/* fix write size in CL (DMA3DCSA) */
			wpr_n = 1U;
		}
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMA3DCSA / 4U);
		/* backup the current address of the CL */
		temp_cur_addr	= cldata->cur_addr;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
		/* check whether add WPR command and need notify application */
		if( (temp_cur_addr != cldata->cur_addr)	&&
			(clofs_dmac != NULL) )
		{
			/* set image address of clofs_dmac */
			clofs_dmac[0] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
		}

		/* make preset data to the CL (DMA3DCOA) */
		DMA3DCOA = (uint32_t)(dmac_param->dmac_ch[0].ptr - cldata->cnn_data->baddr_info.baddr_dmac[0].baddr);  
		/* fix the dest address */    
		add		 = (uint16_t)(R_ATMLIB_CNN_DMA3DCOA / 4U);
		/* fix write size in CL (DMA3DCOA) */
		wpr_n	 = 1U;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &DMA3DCOA, wpr_n );

		/* update the current_reg */
		cldata->cnn_data->current_reg[dmacsa_index].value	= cldata->cnn_data->current_reg[dmacsa_index].value + DMA3DCOA;
		cldata->cnn_data->current_reg[dmacsa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
		/* update the base address setting state */
		cldata->cnn_data->baddr_info.baddr_dmac[0].baddr_state = R_ATMLIB_CNN_BADDRSTAT_SET2ND;
		break;

	case R_ATMLIB_CNN_BADDRSTAT_SET2ND:
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMA3DCCO) */
			data[0] = (uint32_t)(dmac_param->channel_offset);
			/* fix write size in CL (DMA3DCCO) */
			wpr_n = 1U;
			/* fix the dest address */
			add		= (uint16_t)(R_ATMLIB_CNN_DMA3DCCO / 4U);
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
		}

		/* make preset data to the CL (DMA3DCOA) */
		DMA3DCOA = (uint32_t)(dmac_param->dmac_ch[0].ptr - cldata->cnn_data->current_reg[dmacsa_index].value);  
		/* fix the dest address */      
		add		 = (uint16_t)(R_ATMLIB_CNN_DMA3DCOA / 4U);
		/* fix write size in CL (DMA3DCOA) */
		wpr_n	 = 1U;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &DMA3DCOA, wpr_n );

		/* update the current_reg */
		cldata->cnn_data->current_reg[dmacsa_index].value	= cldata->cnn_data->current_reg[dmacsa_index].value + DMA3DCOA;
		cldata->cnn_data->current_reg[dmacsa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
		break;

	default:
		/* DO NOTHING */
		break;
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNPresetDataDMAC
 [ Params ]		dmac_param					: DMAC parameters
				dmacl						: Register which have DMA3DCL information
				dmacst						: Register which have DMA3DCST information
				dmacmag						: Register which have DMA3DCMAG information
				dmacfm						: Register which have DMA3DCFM information
 [ Returns ]	none
 [ Function ]	Make the preset data for DMAC
 [ Note ]		none
******************************************************************************/
R_ATMLIB_CNN_STATIC void atmlib_CNNPresetDataDMAC(
	const R_ATMLIB_CNNDMACParam  	*dmac_param,
	uint32_t						*dmacl,
	uint32_t						*dmacst,
	uint32_t						*dmacmag,
	uint32_t						*dmacfm
)
{
	uint8_t					loop_cnt;
	uint8_t					ch_num;
	uint32_t                DMACL[32];
	uint32_t                DMACST[32];
	uint32_t                DMACMAG[32];
	uint32_t                DMACMAG_COMB[8];
	uint32_t 				DMACFM[32];
	uint32_t 				DMACFM_COMB[4];

	/* initialize local variables */
	loop_cnt = 0U;
	ch_num	 = 0U;
	(void)memset( DMACL,           0U, sizeof(DMACL)          );
	(void)memset( DMACST,          0U, sizeof(DMACST)   	  );
	(void)memset( DMACMAG,         0U, sizeof(DMACMAG)        );
	(void)memset( DMACMAG_COMB,    0U, sizeof(DMACMAG_COMB)   );
	(void)memset( DMACFM,	       0U, sizeof(DMACFM)         );
	(void)memset( DMACFM_COMB,     0U, sizeof(DMACFM_COMB)    );

	/* make preset data to the CL (DMA3DCL00~DMA3DCL31/DMA3DCST00~DMA3DCST31/DMA3DCMAG00_03~DMA3DCMAG28_31/DMA3DCFM00_07~DMA3DCFM24_31)*/
	for( loop_cnt = 0; loop_cnt < dmac_param->ch_count; loop_cnt++ )
	{
		ch_num =  (uint8_t)(dmac_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAC0);

		DMACL[ch_num]     = (((uint32_t)dmac_param->dmac_ch[loop_cnt].image_x) 		        & 0x000003FFU) |
						    (((uint32_t)dmac_param->dmac_ch[loop_cnt].image_y << 16U)       & 0x07FF0000U);

		DMACST[ch_num]    = (((uint32_t)dmac_param->dmac_ch[loop_cnt].image_stride)  		& 0x0000FFFFU);

		DMACMAG[ch_num]   = (((uint32_t)dmac_param->dmac_ch[loop_cnt].repeat_skip_x) 	 	& 0x00000007U) |
						    (((uint32_t)dmac_param->dmac_ch[loop_cnt].repeat_skip_y << 3U)  & 0x00000038U) |
						    (((uint32_t)dmac_param->dmac_ch[loop_cnt].mag_type      << 6U)  & 0x000000C0U);

		DMACFM[ch_num]    = (((uint32_t)dmac_param->dmac_ch[loop_cnt].image_format)  		& 0x00000007U);
	}
	/* make preset data to the CL */
	for(loop_cnt = 0; loop_cnt < R_ATMLIB_CNN_DMAC_MAX_CH / 4U; loop_cnt++ )
	{
		DMACMAG_COMB[loop_cnt] = ( (DMACMAG[loop_cnt*4U + 3] << 24U) & 0xFF000000U) | 
						 		 ( (DMACMAG[loop_cnt*4U + 2] << 16U) & 0x00FF0000U) | 
						  		 ( (DMACMAG[loop_cnt*4U + 1] << 8U ) & 0x0000FF00U) |
						  		 (  DMACMAG[loop_cnt*4U]             & 0x000000FFU);

	}
	/* make preset data to the CL */
	for(loop_cnt = 0; loop_cnt < R_ATMLIB_CNN_DMAC_MAX_CH / 8U; loop_cnt++ )
	{
		DMACFM_COMB[loop_cnt] = ( (DMACFM[loop_cnt*8U + 7U] << 28U) & 0xF0000000U) | 
						    	( (DMACFM[loop_cnt*8U + 6U] << 24U) & 0x0F000000U) | 
								( (DMACFM[loop_cnt*8U + 5U] << 20U) & 0x00F00000U) | 
								( (DMACFM[loop_cnt*8U + 4U] << 16U) & 0x000F0000U) | 
								( (DMACFM[loop_cnt*8U + 3U] << 12U) & 0x0000F000U) | 
								( (DMACFM[loop_cnt*8U + 2U] << 8U ) & 0x00000F00U) | 
								( (DMACFM[loop_cnt*8U + 1U] << 4U ) & 0x000000F0U) | 
								(  DMACFM[loop_cnt*8U]              & 0x0000000FU);

	}

	/* make preset data to the CL */
	memcpy(dmacl,       DMACL,        32U*sizeof(uint32_t));
	memcpy(dmacst,      DMACST,       32U*sizeof(uint32_t));
	memcpy(dmacmag,     DMACMAG_COMB, 8U *sizeof(uint32_t));
	memcpy(dmacfm,      DMACFM_COMB,  4U *sizeof(uint32_t));

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupSlowDMAC
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmac_param						: DMAC parameters
				clofs_imgaddr					: Image address offset of writes in CL data
 [ Returns ]	none
 [ Function ]   Add DMAC channel setup information to the command list in slow setup mode.
 [ Note ]       none
******************************************************************************/
void atmlib_CNNSetupSlowDMAC(
	R_ATMLIB_CLData 			*cldata, 
	const R_ATMLIB_CNNDMACParam *dmac_param,  
	uint32_t 					*clofs_dmac
)
{
	uint8_t					dmac_frac[32];
	uint32_t				DMA3DCE;
	uint32_t				DMA3DCSA;
	uint32_t				DMA3DCL[32];
	uint32_t				DMA3DCST[32];
	uint32_t				DMA3DCMAG[8];
	uint32_t				DMA3DCFM[4];
	uint32_t				DMA3DCOA;
	uint8_t					wpr_n;
	uint16_t				add;
	uint32_t				*temp_cur_addr;
	uint8_t					ch_num;
	uint16_t				dmacsa_index;
	uint8_t					loop_cnt;

	/* initialize local variables */
	DMA3DCE			= 0U;
	DMA3DCSA		= 0U;
	DMA3DCOA		= 0U;
	dmacsa_index	= 0U;
	wpr_n			= 0U;
	add				= 0U;
	ch_num			= 0U;
	loop_cnt		= 0U;

	/* initialize writting data */
	(void)memset( dmac_frac, 0U, sizeof(dmac_frac)   );
	(void)memset( DMA3DCL,   0U, sizeof(DMA3DCL)   );
	(void)memset( DMA3DCST,  0U, sizeof(DMA3DCST)  );
	(void)memset( DMA3DCMAG, 0U, sizeof(DMA3DCMAG) );
	(void)memset( DMA3DCFM,  0U, sizeof(DMA3DCFM)  );

	for(loop_cnt = 0; loop_cnt < dmac_param->ch_count; loop_cnt++ )
	{
		ch_num 			  = (uint8_t)(dmac_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAC0);
		dmac_frac[ch_num] = dmac_param->dmac_ch[loop_cnt].dmac_frac;
	}
	/* store the parameters of dmac_frac */
	memcpy(cldata->cnn_data->dmac_frac, dmac_frac, 16U*sizeof(uint32_t));

	/* update the channel_props for DMA3DC (DMA3DCS) */
	cldata->cnn_data->channel_props.DMA3DCS = (((uint32_t)dmac_param->data_mode)                   & 0x00000001U) |
											  (((uint32_t)cldata->cnn_data->setup_mode_dmac << 1U) & 0x00000002U);

	/* make preset data to the CL (DMA3DCL00~DMA3DCL31/DMA3DCST00~DMA3DCST31/DMA3DCMAG00_03~DMA3DCMAG28_31/DMA3DCFM00_07~DMA3DCFM24_31)*/
	atmlib_CNNPresetDataDMAC( dmac_param, DMA3DCL, DMA3DCST, DMA3DCMAG, DMA3DCFM );

	/* fix the dest address */
	add = R_ATMLIB_CNN_DMA3DCL_CH_BASE / 4U;
	/* fix write size in CL (DMA3DCL00~DMA3DCL31) */
	wpr_n = 32U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMA3DCL, wpr_n );

	/* fix the dest address */
	add = R_ATMLIB_CNN_DMA3DCST_CH_BASE / 4U;
	/* fix write size in CL (DMA3DCST00~DMA3DCST31) */
	wpr_n = 32U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMA3DCST, wpr_n );

	/* fix the dest address */
	add = R_ATMLIB_CNN_DMA3DCMAG00_03 / 4U;
	/* fix write size in CL (DMA3DCMAG00_03~DMA3DCMAG28_31) */
	wpr_n = 8U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMA3DCMAG, wpr_n );

	/* fix the dest address */
	add = R_ATMLIB_CNN_DMA3DCFM00_07 / 4U;
	/* fix write size in CL (DMA3DCFM00_07~DMA3DCFM24_31) */
	wpr_n = 4U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, DMA3DCFM, wpr_n );

	/* loop for the ch_count of the dmac_param */
	for( loop_cnt = 0; loop_cnt < dmac_param->ch_count; loop_cnt++ )
	{
		/* calculate the ch_num */
		ch_num = (uint8_t)(dmac_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAC0);
		/* update the channel_props for DMA3DC (DMA3DCE) */
		DMA3DCE |= (uint32_t)(1U << ch_num);
		/* caliculate index of DMA3DCSA(ch_num) */
		dmacsa_index	= (uint16_t)((R_ATMLIB_CNN_DMA3DCSA_CH_BASE + (R_ATMLIB_CNN_DMA3DCSA_CH_OFST * ch_num)) / 4U);

		/* check the base address setting state */
		switch( cldata->cnn_data->baddr_info.baddr_dmac[ch_num].baddr_state )
		{
		case R_ATMLIB_CNN_BADDRSTAT_UNSET:
			/* make preset data to the CL (DMA3DCSA(ch_num)) */
			DMA3DCSA  = dmac_param->dmac_ch[ch_num].ptr;
			/* fix write size in CL (DMA3DCSA(ch_num)) */
			wpr_n   = 1U;
			/* fix the dest address */
			add		= dmacsa_index;
			/* backup the current address of the CL */
			temp_cur_addr = cldata->cur_addr;
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMA3DCSA, wpr_n );

			/* check whether add WPR command and need notify application */
			if( (temp_cur_addr != cldata->cur_addr)	&&
				(clofs_dmac != NULL) )
			{
				/* set image address of clofs_dmac */
				clofs_dmac[ch_num] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
			}
			break;

		case R_ATMLIB_CNN_BADDRSTAT_SET1ST:
			/* make preset data to the CL (DMA3DCSA(ch_num)) */
			DMA3DCSA  = (uint32_t)(cldata->cnn_data->baddr_info.baddr_dmac[ch_num].baddr);
			/* fix write size in CL (DMA3DCSA(ch_num)) */
			wpr_n   = 1U;
			/* fix the dest address */
			add		= dmacsa_index;
			/* backup the current address of the CL */
			temp_cur_addr	= cldata->cur_addr;
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMA3DCSA, wpr_n );
			/* check whether add WPR command and need notify application */
			if( (temp_cur_addr != cldata->cur_addr)	&&
				(clofs_dmac != NULL) )
			{
				/* set image address of clofs_dmac */
				clofs_dmac[ch_num] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
			}

			/* make preset data to the CL (DMA3DCOA(ch_num)) */
			DMA3DCOA = (uint32_t)(dmac_param->dmac_ch[ch_num].ptr - cldata->cnn_data->baddr_info.baddr_dmac[ch_num].baddr);
			/* fix the dest address */     
			add		 = (uint16_t)((R_ATMLIB_CNN_DMA3DCOA_CH_BASE + (R_ATMLIB_CNN_DMA3DCOA_CH_OFST * ch_num)) / 4U);
			/* fix write size in CL (DMA3DCOA(ch_num)) */
			wpr_n	 = 1U;
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMA3DCOA, wpr_n );

			/* update the current_reg */
			cldata->cnn_data->current_reg[dmacsa_index].value	= cldata->cnn_data->current_reg[dmacsa_index].value + DMA3DCOA;
			cldata->cnn_data->current_reg[dmacsa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
			/* update the base address setting state */
			cldata->cnn_data->baddr_info.baddr_dmac[ch_num].baddr_state = R_ATMLIB_CNN_BADDRSTAT_SET2ND;
			break;

		case R_ATMLIB_CNN_BADDRSTAT_SET2ND:
			/* make preset data to the CL (DMA3DCOA(ch_num)) */
			DMA3DCOA  = (uint32_t)(dmac_param->dmac_ch[ch_num].ptr - cldata->cnn_data->current_reg[dmacsa_index].value);    
			/* fix the dest address */
			add		  = (uint16_t)((R_ATMLIB_CNN_DMA3DCOA_CH_BASE + (R_ATMLIB_CNN_DMA3DCOA_CH_OFST * ch_num)) / 4U);;
			/* fix write size in CL (DMA3DCOA(ch_num)) */
			wpr_n	  = 1U;
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMA3DCOA, wpr_n );

			/* update the current_reg */
			cldata->cnn_data->current_reg[dmacsa_index].value	= cldata->cnn_data->current_reg[dmacsa_index].value + DMA3DCOA;
			cldata->cnn_data->current_reg[dmacsa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
			break;

		default:
			/* DO NOTHING */
			break;
		}
	}

	/* update the channel_props for DMA3DC (DMA3DCE) */
	cldata->cnn_data->channel_props.DMA3DCE = DMA3DCE;

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupFastDMAO
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmao_param						: DMAO parameters
				clofs_imgaddr					: Image address offset of writes in CL data
 [ Returns ]	none
 [ Function ]   Add DMAO channel setup information to the command list in fast setup mode.
 [ Note ]       none
******************************************************************************/
void atmlib_CNNSetupFastDMAO(
	R_ATMLIB_CLData 			*cldata, 
	const R_ATMLIB_CNNDMAOParam *dmao_param, 
	uint32_t 					*clofs_dmao
)
{
	uint32_t				DMAOE;
	uint32_t				DMAOOA;
	uint32_t				next_layer_kernel_mode;
	uint32_t				DMAOS_411;
	uint32_t				dmaosa_index;
	uint8_t					wpr_n;
	uint16_t				add;
	uint32_t				*temp_cur_addr;
	uint32_t				data[3];
	uint8_t					loop_cnt;
	uint8_t					ch_num;

	/* initialize local variables */
	DMAOE				   	= 0U;
	DMAOOA				   	= 0U;
	next_layer_kernel_mode 	= 0U;
	DMAOS_411				= 0U;
	dmaosa_index		   	= 0U;
	wpr_n					= 0U;
	add						= 0U;
	ch_num					= 0U;
	loop_cnt				= 0U;

	/* check if the data_mode is R_ATMLIB_CNN_DATA_SPMC */
	if( dmao_param->data_mode == R_ATMLIB_CNN_DATA_SPMC )
	{
		/* update the channel_props for DMAO (DMAOE) */
		cldata->cnn_data->channel_props.DMAOE = (uint32_t)1U;
	}
	else
	{
		for( loop_cnt = 0U; loop_cnt < dmao_param->ch_count; loop_cnt++ )
		{
			/* calculate the ch_num */
			ch_num = (uint8_t)(dmao_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAO0);
			/* update the channel_props for DMAO (DMAOE) */
			DMAOE |= (uint32_t)(1U << ch_num);
		}
		/* update the channel_props for DMAO (DMAOE) */
		cldata->cnn_data->channel_props.DMAOE = DMAOE;
	}

	/* judge whether the next_layer_kernel_mode is 4*1x1 */
	next_layer_kernel_mode = (uint32_t)((dmao_param->next_layer_kernel_mode  == R_ATMLIB_CNN_KERNEL_4_1_1) ? 1U : 0U);
	/* check whether the next layer is SPMC & 4x1x1 */
	if(next_layer_kernel_mode == 1U && dmao_param->data_mode == R_ATMLIB_CNN_DATA_SPMC)
	{
		/* set the DMAOS_411 to 1U */
		DMAOS_411 = 1U;
	}
	else
	{
		/* set the DMAOS_411 to 0U */
		DMAOS_411 = 0U;
	}

	/* update the channel_props for DMAO (DMAOS) */
	cldata->cnn_data->channel_props.DMAOS = (((uint32_t)dmao_param->data_mode)                    & 0x00000001U)	|
											(((uint32_t)cldata->cnn_data->setup_mode_dmao  << 1U) & 0x00000002U)	|
											((DMAOS_411									   << 2U) & 0x00000004U);
	/* initialize writting data */
	(void)memset( data, 0U, sizeof(data) );

	/* make preset data to the CL (DMAOL/DMAOST/DMAOFM) */
	data[0] = (((uint32_t)dmao_param->image_y << 16U) & 0x7FFF0000U) |
			  ( (uint32_t)dmao_param->image_x         & 0x000003FFU);
	data[1] = (((uint32_t)dmao_param->image_stride  ) & 0x0000FFFFU);
	data[2] = (((uint32_t)dmao_param->image_format  ) & 0x00000007U);
	/* fix the dest address */
	add		= (uint16_t)(R_ATMLIB_CNN_DMAOL / 4U);
	/* fix write size in CL (DMAOL/DMAOST/DMAOFM) */
	wpr_n	= 3U;
	/* add WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
	/* caliculate index of DMAOSA */
	dmaosa_index	= (uint32_t)(R_ATMLIB_CNN_DMAOSA / 4U);

	/* check the base address setting state */
    switch( cldata->cnn_data->baddr_info.baddr_dmao[0].baddr_state )
	{
	case R_ATMLIB_CNN_BADDRSTAT_UNSET:
		/* make preset data to the CL (DMAOSA) */
		data[0] = (uint32_t)(dmao_param->ptr[0]);
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMAOCO) */
			data[1] = (uint32_t)(dmao_param->channel_offset);
			/* fix write size in CL (DMAOSA/DMAOCO) */
			wpr_n = 2U;
		}
		else
		{
			/* fix write size in CL (DMAOSA) */
			wpr_n = 1U;
		}
		/* fix the dest address */
		add	= (uint16_t)(R_ATMLIB_CNN_DMAOSA / 4U);
		/* backup the current address of the CL */
		temp_cur_addr = cldata->cur_addr;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );

		/* check whether add WPR command and need notify application */
		if( (temp_cur_addr != cldata->cur_addr)	&&
			(clofs_dmao != NULL) )
		{
			/* set image address of clofs_dmao */
			clofs_dmao[0] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
		}
		break;

	case R_ATMLIB_CNN_BADDRSTAT_SET1ST:
		/* make preset data to the CL (DMAOSA) */
		data[0] = (uint32_t)(cldata->cnn_data->baddr_info.baddr_dmao[0].baddr);
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMAOCO) */
			data[1] = (uint32_t)(dmao_param->channel_offset);
			/* fix write size in CL (DMAOSA/DMAOCO) */
			wpr_n = 2U;
		}
		else
		{
			/* fix write size in CL (DMAOSA) */
			wpr_n = 1U;
		}
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMAOSA / 4U);
		/* backup the current address of the CL */
		temp_cur_addr	= cldata->cur_addr;
		/* add the WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
		/* check whether add WPR command and need notify application */
		if( (temp_cur_addr != cldata->cur_addr)	&&
			(clofs_dmao != NULL) )
		{
			/* set image address of clofs_dmao */
			clofs_dmao[0] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
		}

		/* make preset data to the CL (DMAOOA) */
		DMAOOA = (uint32_t)(dmao_param->ptr[0] - cldata->cnn_data->baddr_info.baddr_dmao[0].baddr);
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMAOOA / 4U);
		/* fix write size in CL (DMAOOA) */
		wpr_n	= 1U;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAOOA, wpr_n );

		/* update the current_reg */
		cldata->cnn_data->current_reg[dmaosa_index].value	= cldata->cnn_data->current_reg[dmaosa_index].value + DMAOOA;
		cldata->cnn_data->current_reg[dmaosa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
		/* update the base address setting state */
		cldata->cnn_data->baddr_info.baddr_dmao[0].baddr_state = R_ATMLIB_CNN_BADDRSTAT_SET2ND;
		break;

	case R_ATMLIB_CNN_BADDRSTAT_SET2ND:
		/* check if data_mode is R_ATMLIB_CNN_DATA_SCMP */
		if(dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP)
		{
			/* make preset data to the CL (DMAOCO) */
			data[0] = (uint32_t)(dmao_param->channel_offset);
			/* fix write size in CL (DMAOCO) */
			wpr_n = 1U;
			/* fix the dest address */
			add		= (uint16_t)(R_ATMLIB_CNN_DMAOCO / 4U);
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );
		}

		/* make preset data to the CL (DMAOOA) */
		DMAOOA = (uint32_t)(dmao_param->ptr[0] - cldata->cnn_data->current_reg[dmaosa_index].value);
		/* fix the dest address */
		add		= (uint16_t)(R_ATMLIB_CNN_DMAOOA / 4U);
		/* fix write size in CL (DMAOOA) */
		wpr_n	= 1U;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAOOA, wpr_n );

		/* update the current_reg */
		cldata->cnn_data->current_reg[dmaosa_index].value	= cldata->cnn_data->current_reg[dmaosa_index].value + DMAOOA;
		cldata->cnn_data->current_reg[dmaosa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
		break;

	default:
		/* DO NOTHING */
		break;
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupSlowDMAO
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmao_param						: DMAO parameters
				clofs_imgaddr					: Image address offset of writes in CL data
 [ Returns ]	none
 [ Function ]   Add DMAO channel setup information to the command list in slow setup mode.
 [ Note ]       none
******************************************************************************/
void atmlib_CNNSetupSlowDMAO(
	R_ATMLIB_CLData 			*cldata, 
	const R_ATMLIB_CNNDMAOParam *dmao_param, 
	uint32_t 					*clofs_dmao
)
{
	uint32_t				DMAOE;
	uint32_t				DMAOSA;
	uint32_t				DMAOOA;
	uint8_t					wpr_n;
	uint16_t				add;
	uint32_t				*temp_cur_addr;
	uint8_t					ch_num;
	uint16_t				dmaosa_index;
	uint32_t				data[3];
	uint8_t					loop_cnt;

	/* initialize local variables */
	DMAOE					= 0U;
	DMAOOA					= 0U;
	DMAOSA					= 0U;
	dmaosa_index			= 0U;
	wpr_n					= 0U;
	add						= 0U;
	ch_num					= 0U;
	loop_cnt				= 0U;

	/* update the channel_props for DMAO (DMAOS) */
	cldata->cnn_data->channel_props.DMAOS = (((uint32_t)dmao_param->data_mode)  		& 0x00000001U)	|
											(((uint32_t)0U						 << 1U) & 0x00000002U)	|
											(((uint32_t)0U			   			 << 2U) & 0x00000004U);
	/* initialize writting data */
	(void)memset( data, 0U, sizeof(data) );
	/* make preset data to the CL (DMAOL/DMAOST/DMAOFM) */
	data[0] = (((uint32_t)dmao_param->image_y << 16U) & 0x7FFF0000U) |
			  ( (uint32_t)dmao_param->image_x         & 0x000003FFU);
	data[1] = (((uint32_t)dmao_param->image_stride  ) & 0x0000FFFFU);
	data[2] = (((uint32_t)dmao_param->image_format  ) & 0x00000007U);

	/* fix the dest address */
	add = R_ATMLIB_CNN_DMAOL / 4U;
	/* fix write size in CL (DMAOL/DMAOST/DMAOFM) */
	wpr_n = 3U;
	/* add WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );

	/* loop for the ch_count of dmao_param */
	for( loop_cnt = 0; loop_cnt < dmao_param->ch_count; loop_cnt++ )
	{
		/* calculate the ch_num */
		ch_num = (uint8_t)(dmao_param->ch_id[loop_cnt] - R_ATMLIB_CNN_DMAO0);
		/* update the channel_props for DMAO (DMAOE) */
		DMAOE |= (uint32_t)(1U << ch_num);

		/* caliculate index of DMAOSA */
		dmaosa_index	= (uint16_t)((R_ATMLIB_CNN_DMAOSA_CH_BASE + (R_ATMLIB_CNN_DMAOSA_CH_OFST * ch_num)) / 4U);
		
		/* check the base address setting state */
		switch( cldata->cnn_data->baddr_info.baddr_dmao[ch_num].baddr_state )
		{
		case R_ATMLIB_CNN_BADDRSTAT_UNSET:
			/* make preset data to the CL (DMAOSA) */
			DMAOSA  = dmao_param->ptr[ch_num];
			/*fix write size in CL (DMAOSA) */
			wpr_n = 1U;
			/*fix the dest address */
			add		= dmaosa_index;

			/* backup the current address of the CL */
			temp_cur_addr = cldata->cur_addr;
			/* add WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAOSA, wpr_n );

			/* check whether add WPR command and need notify application */
			if( (temp_cur_addr != cldata->cur_addr)	&&
				(clofs_dmao != NULL) )
			{
				/* set image address of clofs_imgaddr->clofs_dmao */
				clofs_dmao[ch_num] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
			}
			break;

		case R_ATMLIB_CNN_BADDRSTAT_SET1ST:
			/* make preset data to the CL (DMAOSA) */
			DMAOSA  = (uint32_t)(cldata->cnn_data->baddr_info.baddr_dmao[ch_num].baddr);
			/*fix write size in CL (DMAOSA) */
			wpr_n   = 1U;
			/*fix the dest address */
			add		= dmaosa_index;

			/* backup the current address of the CL */
			temp_cur_addr	= cldata->cur_addr;
			/* add WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAOSA, wpr_n );

			/* check whether add WPR command and need notify application */
			if( (temp_cur_addr != cldata->cur_addr)	&&
				(clofs_dmao != NULL) )
			{
				/* set image address of clofs_imgaddr->clofs_dmao */
				clofs_dmao[ch_num] = (uint32_t)((temp_cur_addr + 1U) - cldata->top_addr);
			}

			/* make preset data to the CL (DMAOOA) */
			DMAOOA = (uint32_t)(dmao_param->ptr[ch_num] - cldata->cnn_data->baddr_info.baddr_dmao[ch_num].baddr);
			/* fix the dest address */     
			add		= (uint16_t)((R_ATMLIB_CNN_DMAOOA_CH_BASE + (R_ATMLIB_CNN_DMAOOA_CH_OFST * ch_num)) / 4U);
			/* fix write size in CL (DMAOOA) */
			wpr_n	= 1U;
			/* add WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAOOA, wpr_n );

			/* update the current_reg */
			cldata->cnn_data->current_reg[dmaosa_index].value	= cldata->cnn_data->current_reg[dmaosa_index].value + DMAOOA;
			cldata->cnn_data->current_reg[dmaosa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
			/* update the base address setting state */
			cldata->cnn_data->baddr_info.baddr_dmao[ch_num].baddr_state = R_ATMLIB_CNN_BADDRSTAT_SET2ND;
			break;

		case R_ATMLIB_CNN_BADDRSTAT_SET2ND:
			/* make preset data to the CL (DMAOOA) */
			DMAOOA  = (uint32_t)(dmao_param->ptr[ch_num] - cldata->cnn_data->current_reg[dmaosa_index].value); 
			/* fix the dest address */      
			add		= (uint16_t)((R_ATMLIB_CNN_DMAOOA_CH_BASE + (R_ATMLIB_CNN_DMAOOA_CH_OFST * ch_num)) / 4U);;
			/* fix write size in CL (DMAOOA) */
			wpr_n	= 1U;
			/* add WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, &DMAOOA, wpr_n );

			/* update the current_reg */
			cldata->cnn_data->current_reg[dmaosa_index].value	= cldata->cnn_data->current_reg[dmaosa_index].value + DMAOOA;
			cldata->cnn_data->current_reg[dmaosa_index].attr	|= R_ATMLIB_CNN_REGCACHE_SET;
			break;

		default:
			/* DO NOTHING */
			break;
		}
	}

	/* update the channel_props for DMAO (DMAOE) */
	cldata->cnn_data->channel_props.DMAOE = DMAOE;

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupARI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				ari_param						: ARI parameters
				channel_info					: Channel statistic information
 [ Returns ]	none
 [ Function ]	Add ARI setup information and channel statistic inforamtion to the command list.
 [ Note ]		none
******************************************************************************/
void atmlib_CNNSetupARI(
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNARIParam *ari_param,
	const R_ATMLIB_CNNChannelStatistic *channel_info
)
{
	uint32_t				ARIE;
	uint32_t				ARICSR;
	uint32_t				CPOOL_INFO;
	uint32_t                ARI_BND;
	uint32_t                data[2];
	uint16_t				add;
	uint8_t					ch_num;
	uint8_t					loop_cnt;

	/* initialize local variables */
	ARIE			= 0U;
	ARICSR			= 0U;
	CPOOL_INFO		= 0U;
	ARI_BND  		= 0x80007FFFU;
	add				= 0U;
	ch_num			= 0U;
	loop_cnt		= 0U;
	(void)memset( data, 0U, sizeof(data) );

	/* loop for the ari_cnt of the ari_param */
	for( loop_cnt = 0U; loop_cnt < ari_param->ari_cnt; loop_cnt++ )
	{
		/* calculate the ch_num */
		ch_num = (uint8_t)(ari_param->ch_id[loop_cnt] - R_ATMLIB_CNN_ARI0);
		/* update the channel_props for ARI (ARIE) */
		ARIE |= (uint32_t)(1U << ch_num);
	}
	/* update the channel_props for ARI (ARIE) */
	cldata->cnn_data->channel_props.ARIE = ARIE;
	/* update the channel_props for ARI (ARIFM) */
	cldata->cnn_data->ari_format = (uint32_t)ari_param->ari_format;
	/* update the channel_props for ARI (NEXT_ARIFM) */
	cldata->cnn_data->next_ari_format = (uint32_t)ari_param->next_ari_format;

	/* check if the cpool_mode is R_ATMLIB_CNN_CPOOL_MAX */
	if( ari_param->cpool_mode == R_ATMLIB_CNN_CPOOL_MAX )
	{
		/* set CPOOL_INFO to maxout_group */
		CPOOL_INFO = (uint32_t) ari_param->maxout_group;
	}
	else
	{
		/* set CPOOL_INFO to label_info */
		CPOOL_INFO = (uint32_t) ari_param->label_info;
	}

	/* check whether the channel_info is NULL */
	if( channel_info == NULL )
	{
		/* make preset data to the CL (ARICSR) */
		ARICSR	= 	(((uint32_t)ari_param->sftm_round_mode 		<< 26U) & 0x04000000U) |
					(((uint32_t)ari_param->cpool_ui           	<< 24U) & 0x01000000U) |
					(((uint32_t)ari_param->next_ari_format    	<< 21U) & 0x00600000U) |
					(((uint32_t)ari_param->lut_lin_int	    	<< 20U) & 0x00100000U) |
					(((uint32_t)ari_param->pad_mode   	    	<< 18U) & 0x000C0000U) |
					(((uint32_t)ari_param->ari_format		    << 16U) & 0x00030000U) |
					(((uint32_t)(ari_param->ypool - 1U)	    	<< 14U) & 0x0000C000U) |
					(((uint32_t)(ari_param->xpool - 1U)	    	<< 12U) & 0x00003000U) |
					(((uint32_t)ari_param->xypool_mode	    	<< 10U) & 0x00000C00U) |
					(((uint32_t)CPOOL_INFO				    	<< 7U)	& 0x00000380U) |
					(((uint32_t)ari_param->cpool_mode		    << 5U)	& 0x00000060U) |
					(((uint32_t)ari_param->acti_mode		    << 2U)	& 0x0000001CU) |
					(((uint32_t)ari_param->bias_en    	    	<< 1U)  & 0x00000002U);
	}
	else
	{
		/* make preset data to the CL (ARICSR) */
		ARICSR	= 	(((uint32_t)channel_info->channel_max_min 	<< 27U) & 0x08000000U) |
					(((uint32_t)ari_param->sftm_round_mode 		<< 26U) & 0x04000000U) |
					(((uint32_t)ari_param->cpool_ui           	<< 24U) & 0x01000000U) |
					(((uint32_t)channel_info->glob_sum        	<< 23U) & 0x00800000U) |
					(((uint32_t)ari_param->next_ari_format    	<< 21U) & 0x00600000U) |
					(((uint32_t)ari_param->lut_lin_int	    	<< 20U) & 0x00100000U) |
					(((uint32_t)ari_param->pad_mode   	    	<< 18U) & 0x000C0000U) |
					(((uint32_t)ari_param->ari_format		    << 16U) & 0x00030000U) |
					(((uint32_t)(ari_param->ypool - 1U)	    	<< 14U) & 0x0000C000U) |
					(((uint32_t)(ari_param->xpool - 1U)	    	<< 12U) & 0x00003000U) |
					(((uint32_t)ari_param->xypool_mode	    	<< 10U) & 0x00000C00U) |
					(((uint32_t)CPOOL_INFO				    	<< 7U)	& 0x00000380U) |
					(((uint32_t)ari_param->cpool_mode		    << 5U)	& 0x00000060U) |
					(((uint32_t)ari_param->acti_mode		    << 2U)	& 0x0000001CU) |
					(((uint32_t)ari_param->bias_en    	    	<< 1U)  & 0x00000002U);
	}

	/* update the channel_props for ARI (ARICSR) */
	cldata->cnn_data->channel_props.ARICSR = ARICSR;

	/* fix the dest address */
	add = R_ATMLIB_CNN_ARICSR / 4U;
	/* add the WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, &ARICSR, 1U );

	/* check if the acti_mode is BRELU/ELU */
	if( ari_param->acti_mode == R_ATMLIB_CNN_ACTFUNC_BRELU ||
		ari_param->acti_mode == R_ATMLIB_CNN_ACTFUNC_ELU
	)
	{
		/* make preset data to the CL (ARI_BND) */
		ARI_BND = (ARI_BND & 0xFFFF0000) | (((uint32_t)ari_param->upper_bound) & 0x0000FFFF);
		/* fix the dest address */
		add = R_ATMLIB_CNN_ARI_BND / 4U;
		/* add the WPR command */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, &ARI_BND, 1U );
	}
	/* check if the acti_mode is LRELU */
	else if( ari_param->acti_mode == R_ATMLIB_CNN_ACTFUNC_LRELU )
	{
		/* make preset data to the CL (ARI_BND) */
		ARI_BND = (((uint32_t)ari_param->lower_bound << 16U) & 0xFFFF0000U) |
			        (uint32_t)ari_param->upper_bound;
		/* fix the dest address */
		add = R_ATMLIB_CNN_ARI_BND / 4U;
		/* add the WPR command  */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, &ARI_BND, 1U );

		/* make preset data to the CL (MULT_POST_SFT/MULT_VAL) */
		data[0] = (uint32_t)ari_param->mult_frac;
		data[1] = (uint32_t)ari_param->mult_val;
		/* fix the dest address */
		add = R_ATMLIB_CNN_MULT_POST_SFT / 4U;
		/* add the WPR command */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, data, 2U );
	}
	/* check if the acti_mode is PRELU */
	else if( ari_param->acti_mode == R_ATMLIB_CNN_ACTFUNC_PRELU )
	{
		/* make preset data to the CL (ARI_BND) */
		ARI_BND = (((uint32_t)ari_param->lower_bound) << 16U & 0xFFFF0000) |
	               ((uint32_t)ari_param->upper_bound         & 0x0000FFFF);
		/* fix the dest address */
		add = R_ATMLIB_CNN_ARI_BND / 4U;
		/* add the WPR command */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, &ARI_BND, 1U );

		/* make preset data to the CL (MULT_POST_SFT/MULT_VAL) */
		data[0] = (uint32_t)ari_param->mult_frac;
		data[1] = (uint32_t)ari_param->mult_val;
		/* fix the dest address */
		add = R_ATMLIB_CNN_MULT_POST_SFT / 4U;
		/* add the WPR command */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, data, 2U );
	}
	/* check if the acti_mode is PELU */
	else if( ari_param->acti_mode == R_ATMLIB_CNN_ACTFUNC_PELU )
	{
		/* make preset data to the CL (ARI_BND) */
		ARI_BND = (ARI_BND & 0xFFFF0000) | (((uint32_t)ari_param->upper_bound) & 0x0000FFFF);
		/* fix the dest address */
		add = R_ATMLIB_CNN_ARI_BND / 4U;
		/* add the WPR command */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, &ARI_BND, 1U );

		/* make preset data to the CL (MULT_POST_SFT/MULT_VAL) */
		data[0] = (uint32_t)ari_param->mult_frac;
		data[1] = (uint32_t)ari_param->mult_val;
		/* fix the dest address */
		add = R_ATMLIB_CNN_MULT_POST_SFT / 4U;
		/* add the WPR command */
	    (void)atmlib_CNNAddCL_WPR( cldata, add, data, 2U );
	}
	else
	{
		/* DO NOTHING */
	}

	return;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupDMARF
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmarf_param    					: DMARF parameters
 [ Returns ]	none
 [ Function ]	Add DMARF setup information to the command list.
 [ Note ]		none
******************************************************************************/
void atmlib_CNNSetupDMARF( 
    R_ATMLIB_CLData *cldata, 
    const R_ATMLIB_CNNDMARFParam *dmarf_param
)
{
	uint32_t				data[4];
	uint16_t				add;
	uint8_t					wpr_n;

	/* initialize local variables */
	add				= 0U;
	wpr_n			= 0U;

	/* initilize the writing data */
	(void)memset( data, 0U, sizeof(data) );

	/* make preset data to CL (DMARFESA) */
	data[0] = (uint32_t)dmarf_param->external_sa;
	/* check if the format is SCMP */
	if( dmarf_param->data_mode == R_ATMLIB_CNN_DATA_SCMP )
	{
		/* make preset data to CL (DMARFISA) */
		data[1] = ((uint32_t)dmarf_param->internal_sa & 0x0000FFFFU);
		/* make preset data to CL (DMARFL) */
		data[2] = ( (uint32_t)dmarf_param->length & 0x000003FFU );
	}
	else
	{
		/* make preset data to CL (DMARFISA) */
		data[1] = (((uint32_t)dmarf_param->end_pos    << 3U) & 0x00000038U) |
			  	  ( (uint32_t)dmarf_param->start_pos         & 0x00000007U);
		/* make preset data to CL (DMARFL) */
		data[2] = (uint32_t)0U;
	}

	/* make preset data to CL (DMARFCTRL) */
	data[3]  =  (((uint32_t)1U             		   	  	<< 31U) & 0x80000000U) |
				(((uint32_t)(!(dmarf_param->data_mode)) << 3U ) & 0x00000008U) |
				(((uint32_t)dmarf_param->format) 				& 0x00000007U);

	/* fix the dest address */
	add = R_ATMLIB_CNN_DMARFESA / 4U;
	/* fix write size in CL (DMARFESA/DMARFISA/DMARFL/DMARFCTRL) */
	wpr_n = 4U;
	/* add WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, data, wpr_n );	
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupWeightsWPR
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				weight_info    					: Pointer to the R_ATMLIB_CNNWeightInfo structure
 [ Returns ]	none
 [ Function ]	Add weights information to the command list in WPR mode.
 [ Note ]		none
******************************************************************************/
void atmlib_CNNSetupWeightsWPR( 
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNWeightInfo *weight_info
)
{
	uint32_t loop_cnt;
	uint32_t loop_cnt2;
	uint8_t ch_in;
	uint8_t ch_out;
	uint16_t add;
	uint32_t weightbuffer_low[7];
	uint32_t weightbuffer_high[7];

	/* initialize the local variables */
	loop_cnt	= 0U;
	loop_cnt2	= 0U;
	ch_in		= 0U;
	ch_out		= 0U;
	(void)memset( weightbuffer_low,  0U, sizeof(weightbuffer_low)  );
	(void)memset( weightbuffer_high, 0U, sizeof(weightbuffer_high) );

	/* check if the ari_format is 8bit mode */
	if( cldata->cnn_data->ari_format <= R_ATMLIB_CNN_ARIFORMAT_8U)
	{
		/* loop for the ch_cnt of the weight_info */
		for( loop_cnt = 0U; loop_cnt < weight_info->ch_cnt ; loop_cnt++)
		{
			/* make preset data to CL(WB0IkOm_00_03 ~ WB0IkOm_24_27) */
			for( loop_cnt2 = 0U; loop_cnt2 < 6U; loop_cnt2++ )
			{
				weightbuffer_low[loop_cnt2] = ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 3U]) & 0x00FF) << 24U ) & 0xFF000000U)  | 
											  ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 2U]) & 0x00FF) << 16U ) & 0x00FF0000U)  |
											  ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 1U]) & 0x00FF) << 8U  ) & 0x0000FF00U)  | 
											  ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2]     ) & 0x00FF)        ) & 0x000000FFU);
			}

			weightbuffer_low[loop_cnt2] = 	  ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 2U]) & 0x00FF) << 16U ) & 0x00FF0000U)  |
											  ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 1U]) & 0x00FF) << 8U  ) & 0x0000FF00U)  | 
											  ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2]     ) & 0x00FF)        ) & 0x000000FFU);

			/* calculate the ch_in and ch_out */
			ch_in  = (uint8_t)(weight_info->ch_weight[loop_cnt].in_id  - R_ATMLIB_CNN_DMAI0);
			ch_out = (uint8_t)(weight_info->ch_weight[loop_cnt].out_id - R_ATMLIB_CNN_ARI0);

			/* fix the dest address */
			add = (uint16_t)((R_ATMLIB_CNN_WB0_BASE + (((uint16_t)ch_in * R_ATMLIB_CNN_WB_DMAI_CH_OFST) + ((uint16_t)ch_out * R_ATMLIB_CNN_WB_DMAO_CH_OFST))) / 4U);
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, weightbuffer_low, 7U );
		}
	}
	else
	{
		/* loop for the ch_cnt of the weight_info */
		for( loop_cnt = 0; loop_cnt < weight_info->ch_cnt ; loop_cnt++ )
		{
			/* make preset data to CL(WB0IkOm_00_03 ~ WB0IkOm_24_27) */
			for( loop_cnt2 = 0; loop_cnt2 < 6U; loop_cnt2++ )
			{
				weightbuffer_low[loop_cnt2] =   ( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 3U]) & 0x00FF) 	   << 24U ) & 0xFF000000U)  | 
									   			( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 2U]) & 0x00FF) 	   << 16U ) & 0x00FF0000U)  |
									   			( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 1U]) & 0x00FF) 	   << 8U  ) & 0x0000FF00U)  | 
									   			( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2]     ) & 0x00FF)        		  ) & 0x000000FFU);
				weightbuffer_high[loop_cnt2] =  ( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 3U]) & 0xFF00) >> 8U) << 24U ) & 0xFF000000U)  | 
									  			( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 2U]) & 0xFF00) >> 8U) << 16U ) & 0x00FF0000U)  |
									   			( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 1U]) & 0xFF00) >> 8U) << 8U  ) & 0x0000FF00U)  | 
								 	   			( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2]     ) & 0xFF00) >> 8U)        ) & 0x000000FFU);
			}
			weightbuffer_low[loop_cnt2] =   	( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 2U]) & 0x00FF) 	   << 16U ) & 0x00FF0000U)  |
									   			( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 1U]) & 0x00FF) 	   << 8U  ) & 0x0000FF00U)  | 
									   			( (((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2]     ) & 0x00FF)        		  ) & 0x000000FFU);
				
			weightbuffer_high[loop_cnt2] =  	( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 2U]) & 0xFF00) >> 8U) << 16U ) & 0x00FF0000U)  |
									   			( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2 + 1U]) & 0xFF00) >> 8U) << 8U  ) & 0x0000FF00U)  | 
								 	   			( ((((uint32_t)(weight_info->ch_weight[loop_cnt].value[4U*loop_cnt2]     ) & 0xFF00) >> 8U)        ) & 0x000000FFU);

			/* calculate the ch_in and ch_out */
			ch_in  = (uint8_t)(weight_info->ch_weight[loop_cnt].in_id  - R_ATMLIB_CNN_DMAI0);
			ch_out = (uint8_t)(weight_info->ch_weight[loop_cnt].out_id - R_ATMLIB_CNN_ARI0);

			/* fix the dest address */
			add = (uint16_t)((R_ATMLIB_CNN_WB0_BASE + (((uint16_t)(ch_in)      * R_ATMLIB_CNN_WB_DMAI_CH_OFST) + ((uint16_t)(ch_out) * R_ATMLIB_CNN_WB_DMAO_CH_OFST))) / 4U);
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, weightbuffer_low, 7U );
			/* fix the dest address */
			add = (uint16_t)((R_ATMLIB_CNN_WB0_BASE + (((uint16_t)(ch_in + 1U) * R_ATMLIB_CNN_WB_DMAI_CH_OFST) + ((uint16_t)(ch_out) * R_ATMLIB_CNN_WB_DMAO_CH_OFST))) / 4U);
			/* add the WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, weightbuffer_high, 7U );
		}
	}
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetupLUTWPR
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				lut_info    					: Pointer to the R_ATMLIB_CNNLUTInfo structure
 [ Returns ]	none
 [ Function ]	Add LUT tables to the command list in WPR mode.
 [ Note ]		none
******************************************************************************/
void atmlib_CNNSetupLUTWPR( 
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNLUTInfo *lut_info
)
{
	uint8_t loop_cnt;
	uint16_t add;
	uint32_t lut_table[64];

	/* initialize the local variables */
	loop_cnt	= 0U;
	add			= 0U;

	/* initialize the local variables */
	(void)memset( lut_table,  0U, sizeof(lut_table) );

	/* make preset data to CL (LUT_x_000_003 ~ LUT_x_252_255 ) */
	for( loop_cnt = 0U; loop_cnt < 64U; loop_cnt++ )
	{
		lut_table[loop_cnt] =  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 3U]) & 0x00FF) << 24U ) & 0xFF000000U)  | 
							   ( ((((uint32_t)lut_info->value[4U*loop_cnt + 2U]) & 0x00FF) << 16U ) & 0x00FF0000U)  |
							   ( ((((uint32_t)lut_info->value[4U*loop_cnt + 1U]) & 0x00FF) << 8U  ) & 0x0000FF00U)  | 
							   ( ((((uint32_t)lut_info->value[4U*loop_cnt]     ) & 0x00FF)        ) & 0x000000FFU);
	}

	/* fix the LUT table 0 dest address */
	add = (uint16_t)(R_ATMLIB_CNN_LUT_BASE) / 4U;
	/* add WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, lut_table, 64U );

	/* fix the LUT table 1 dest address */
	add = (uint16_t)( (R_ATMLIB_CNN_LUT_BASE + 1U*R_ATMLIB_CNN_LUT_OFST) / 4U );
	/* add WPR command */
	(void)atmlib_CNNAddCL_WPR( cldata, add, lut_table, 64U );
	
	/* when the ari format is 8bit */
	if ((cldata->cnn_data->ari_format == R_ATMLIB_CNN_ARIFORMAT_8S) ||
	    (cldata->cnn_data->ari_format == R_ATMLIB_CNN_ARIFORMAT_8U)
	)
	{
		/* fix the LUT table 2 dest address */
		add = (uint16_t)( (R_ATMLIB_CNN_LUT_BASE + 2U*R_ATMLIB_CNN_LUT_OFST) / 4U );
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, lut_table, 64U );

		/* fix the LUT table 3 dest address */
		add = (uint16_t)( (R_ATMLIB_CNN_LUT_BASE + 3U*R_ATMLIB_CNN_LUT_OFST) / 4U );
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, lut_table, 64U );
	}
	/* when ari format is 16 bits*/
	else
	{
		/* when Linear interpolation is enabled */
		if( (cldata->cnn_data->channel_props.ARICSR & 0x00100000U) != 0 )
		{
			/* make preset data to CL (LUT_x_000_003 ~ LUT_x_252_255 ) */
			for( loop_cnt = 0U; loop_cnt < 63U; loop_cnt++ )
			{
				lut_table[loop_cnt] = ( ((((uint32_t)lut_info->value[4U*loop_cnt + 4U]) & 0x00FF) << 24U ) & 0xFF000000U)  | 
									  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 3U]) & 0x00FF) << 16U ) & 0x00FF0000U)  |
									  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 2U]) & 0x00FF) << 8U  ) & 0x0000FF00U)  | 
									  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 1U]) & 0x00FF)        ) & 0x000000FFU);
			}
			lut_table[loop_cnt] = ( ((((uint32_t)lut_info->value[0U])               & 0x00FF) << 24U ) & 0xFF000000U)  | 
								  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 3U]) & 0x00FF) << 16U ) & 0x00FF0000U)  |
								  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 2U]) & 0x00FF) << 8U  ) & 0x0000FF00U)  | 
								  ( ((((uint32_t)lut_info->value[4U*loop_cnt + 1U]) & 0x00FF)        ) & 0x000000FFU);

			/* fix the LUT table 2 dest address */
			add = (uint16_t)( (R_ATMLIB_CNN_LUT_BASE + 2U*R_ATMLIB_CNN_LUT_OFST) / 4U );
			/* add WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, lut_table, 64U );
	
			/* fix the LUT table 3 dest address */
			add = (uint16_t)( (R_ATMLIB_CNN_LUT_BASE + 3U*R_ATMLIB_CNN_LUT_OFST) / 4U );
			/* add WPR command */
			(void)atmlib_CNNAddCL_WPR( cldata, add, lut_table, 64U );
		}
	}	
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNSetPerformCounter
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				perform_counter					: The pointer to the R_ATMLIB_CNNPerformCounter structure
 [ Returns ]	none 
 [ Function ]	Add performance counter value to the commandlist
 [ Note ]		none
******************************************************************************/
void atmlib_CNNSetPerformCounter( 
	R_ATMLIB_CLData *cldata, 
	const R_ATMLIB_CNNPerformCounter *perform_counter
)
{
	uint8_t  loop_cnt;
	uint32_t PERFCNTCTRL;
	uint32_t PERFCNT;
	uint16_t add;
	uint8_t	 wpr_n;

	/* initialize the local variables */
	loop_cnt		= 0U;
	PERFCNTCTRL		= 0U;
	PERFCNT			= 0U;
	add				= 0U;
	wpr_n			= 0U;

	for( loop_cnt = 0; loop_cnt < perform_counter->counter_cnt; loop_cnt++ )
	{
		/* make preset data to the CL (PPERFxCNT)) */
		PERFCNT =  perform_counter->counter_info[loop_cnt].counter_value;
		/* fix the dest address */
		add = (uint16_t)( (R_ATMLIB_CNN_PPERFCNT_BASE + ((uint16_t)(perform_counter->counter_info[loop_cnt].counter_id) * R_ATMLIB_CNN_PPERFCNT_OFST)) / 4U );
		/* fix write size in CL (PPERFxCNTL) */
		wpr_n = 1U;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &PERFCNT, wpr_n );

		/* make preset data to the CL (PERFCNTxCTRL)) */
		PERFCNTCTRL =   (((uint32_t)perform_counter->counter_info[loop_cnt].counter_en   << 31U) & 0x80000000) |
			 			(((uint32_t)perform_counter->counter_info[loop_cnt].stall_reason << 12U) & 0x00007000) |
			 			(((uint32_t)perform_counter->counter_info[loop_cnt].func         << 8U)  & 0x00000F00) |
			 			( (uint32_t)perform_counter->counter_info[loop_cnt].unit_num             & 0x0000007F) ;
		/* fix the dest address */
		add = (uint16_t)( (R_ATMLIB_CNN_PERFCNTCTRL_BASE + ((uint16_t)(perform_counter->counter_info[loop_cnt].counter_id) * R_ATMLIB_CNN_PERFCNTCTRL_OFST)) / 4U );
		/* fix write size in CL (PERFCNTxCTRL) */
		wpr_n = 1U;
		/* add WPR command */
		(void)atmlib_CNNAddCL_WPR( cldata, add, &PERFCNTCTRL, wpr_n );
	}

	return;
}

/******************************************************************************
 [ FuncName ]	atmlib_CNNSearchLabelName
 [ Params ]		label_info						: The pointer to the R_ATMLIB_CNNLabelInfo structure
				label_name						: Label name
 [ Returns ]	0 <=							: Index for the label information
				-1								: There is no applicable label in the label information
 [ Function ]	Search for the label name.
 [ Note ]		none
******************************************************************************/
int32_t atmlib_CNNSearchLabelName(
	const R_ATMLIB_CNNLabelInfo	*label_info,
	const char					*label_name
)
{
	int32_t		ret;
	uint32_t	loop_cnt;
	int32_t		cmp_ret;

	/* initialize local variables */
	ret			= -1;
	loop_cnt	= 0U;
	cmp_ret		= 0;

	/* check the duplicating label name against each label in label info */
	for( loop_cnt = 0U; loop_cnt < label_info->label_count; loop_cnt++ )
	{
		/* compare the 2nd argument to label name in label_info */
		cmp_ret = strncmp( (const char *)(label_info->label[loop_cnt].name), (const char *)label_name, R_ATMLIB_CNN_MAX_LABEL_LENGTH );

		/* check the result of strncmp() */
		if( cmp_ret == 0 )
		{
			/* set loop counter in return value */
			ret = (int32_t)loop_cnt;
			break;
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	atmlib_CNNCheckLabelName
 [ Params ]	    label_name						: Label name
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG					: label name is out of range [1 to 31 byte]
 [ Function ]	Check the label name.
 [ Note ]	none
******************************************************************************/
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckLabelName(
	const char	*label_name
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				name_len;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	name_len	= 0U;

	/* check the label name */
	name_len = (uint32_t)strlen( label_name );

	/* check the result of strlen() */
	if( (name_len == 0U)							||
		(name_len > R_ATMLIB_CNN_MAX_LABEL_LENGTH) )
	{
		/* set R_ATMLIB_E_NG in return value */
		ret = R_ATMLIB_E_NG;
	}

	return ret;
}
