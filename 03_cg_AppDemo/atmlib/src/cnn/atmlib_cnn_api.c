/******************************************************************************
    Atomic Library
     Copyright (C)  2019-2020 Renesas Electronics Corporation.All rights reserved.
	
    [File] atmlib_cnn_api.c

******************************************************************************/
#include <stdint.h>
#include <string.h>

#include "r_atmlib_prot.h"
#include "atmlib_cnn.h"
/******************************************************************************
 [ FuncName ]	r_atmlib_InitializeCNNCL
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				addr							: The address of CL memory
				size							: The size of CL memory
				cnn_data						: The pointer to the R_ATMLIB_CNNData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
				R_ATMLIB_E_NG_ARG2				: The argument addr is NULL
												  The argument addr is not 4byte size alignment
				R_ATMLIB_E_NG_ARG3				: The argument size is out of range [1 .. R_ATMLIB_MAX_CL_LENGTH]
				R_ATMLIB_E_NG_ARG4				: The argument cnn_data is NULL
 [ Function ]	Initialize the library for IMP CNN CL generation.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_InitializeCNNCL(
	R_ATMLIB_CLData		*cldata,
	uint32_t			*addr,
	uint32_t			size,
	R_ATMLIB_CNNData	*cnn_data
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initialize local variables */
	ret = R_ATMLIB_E_OK;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/* check the 2nd argument */
	else if( (addr == NULL) || (((uintptr_t)addr % 4U) != 0U) )
	{
		/* set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( (size < 1U) || (size > R_ATMLIB_MAX_CL_LENGTH) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/* check the 4th argument */
	else if( cnn_data == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* initialize the CL info */
		atmlib_CNNInitializeCL( cldata, addr, size, cnn_data );

		/* update the state of CL info */
		atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
	}

	return ret;
}



/******************************************************************************
 [ FuncName ]	r_atmlib_CNNReset
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Reset CNN data.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNReset(
	R_ATMLIB_CLData	*cldata
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initialize local variables */
	ret = R_ATMLIB_E_OK;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*   check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else if( ret != R_ATMLIB_E_OK )
		{
			/*  DO NOTHING */
		}
		else
		{
			/* initialize the CNN data */
			atmlib_CNNInitializeCnnData( cldata->cnn_data );

			/* update the state of CL info */
			atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNFlush
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	CNN registers cache is flushed.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNFlush(
	R_ATMLIB_CLData	*cldata
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/* initialize local variables */
	ret = R_ATMLIB_E_OK;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else if( ret != R_ATMLIB_E_OK )
		{
			/* DO NOTHING */
		}
		else
		{
			/* update attr of current_reg (all registers) */
			atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, 0, R_ATMLIB_CNN_REG_MAXNUM );

			/* update the state of CL info */
			atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNPictureProlog
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				ocfp							: Output Channel Fractional Position table (REG: OCFP_TABLE)
				dyn_ocfp_count					: Number of used dynamic ocfps [1..R_ATMLIB_MAX_DYN_FP_HANDLE]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument ocfp is out of range (case of ocfp is not NULL)
				R_ATMLIB_E_NG_ARG3				: The argument dyn_ocfp_count is out of range (case of ocfp is not NULL)
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append Picture Prolog to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNPictureProlog(
	R_ATMLIB_CLData				*cldata,
	const uint8_t				*ocfp,
	uint8_t						dyn_ocfp_count
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				size_total;
	uint32_t				size_wpr;
	uint32_t				size_cluvt;
	uint16_t				add;
	uint8_t					loop_cnt;
	uint32_t				data[R_ATMLIB_MAX_DYN_FP_HANDLE];

	/* initialize local variables */
	ret	= R_ATMLIB_E_OK;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 3rd argument */
	else if( (ocfp != NULL)															&&
			 ((dyn_ocfp_count < 1U) || (dyn_ocfp_count > R_ATMLIB_MAX_DYN_FP_HANDLE)) )
	{
		/*  set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* case of normal : calcurate the size of adding CL command elements */
			/* check whether ocfp setting is required */
			if( ocfp != NULL )
			{
				/* fix the size of WPR command */
				size_wpr	= 1U + (uint32_t)dyn_ocfp_count;	/* command size of WPR		*/
			}
			else
			{
				/* fix the size of WPR command */
				size_wpr	= 0U;								/* Do not add WPR			*/
			}
			/* calcurate total size of writting CL data */
			size_cluvt	= 1U;									/* command size of CLUVT	*/
			size_total	= size_wpr + size_cluvt;				/* command size of total	*/

			/* check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize( cldata, size_total );
			/* check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* check whether ocfp setting is required */
				if( ocfp != NULL )
				{
					/*  check the all parameters of ocfp */
					for( loop_cnt = 0U; loop_cnt < dyn_ocfp_count; loop_cnt++ )
					{
						/*  check the parameter of ocfp[n] */
						if( ocfp[loop_cnt + 1U] > R_ATMLIB_CNN_OCFP_RANGE )
						{
							/*  set R_ATMLIB_E_NG_ARG2 in return value */
							ret = R_ATMLIB_E_NG_ARG2;
							break;
						}
						/*  make the writting data to the CL */
						data[loop_cnt] = (uint32_t)(ocfp[loop_cnt + 1U]);
					}
				}

				/* check the return value */
				if( ret == R_ATMLIB_E_OK )
				{
					/* case of normal : add commands */
					/* check whether ocfp setting is required */
					if( ocfp != NULL )
					{
						/* add the WPR command (update OCFP Table) */
						add	= R_ATMLIB_CNN_OCFP_TABLE / 4U;
						(void)atmlib_CNNAddCL_WPR( cldata, add, data, dyn_ocfp_count );
					}

					/* /  add CLUVT (clear UV Table) */
					data[0]	= R_ATMLIB_CNN_OPC_CLUVT;
					(void)atmlib_CNNAddCLCommon( cldata, data, size_cluvt );
					
					/* update the state of CL info */
					atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
				}
				else
				{
					/* case of error : 2nd argument error (DO NOTHING) */
				}
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNLayerProlog
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				max_val_thres					: Unsigned MAX value threshold, same for each output channel (REG: MAX_VAL_THRES) [0x0..0xffff]
				min_val_thres					: Unsigned MIN value threshold, same for each output channel (REG: MIN_VAL_THRES) [0x0..0xffff]
				scres_mode						: SCRES mode [0..3]
				max_val_thres					: Unsigned MAX value threshold, same for each output channel (REG: MAX_VAL_THRES) [0x0..0xffff]
				min_val_thres					: Unsigned MIN value threshold, same for each output channel (REG: MIN_VAL_THRES) [0x0..0xffff]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG4				: The argument scres_mode is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append Layer Prolog to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNLayerProlog(
	R_ATMLIB_CLData				*cldata,
	uint32_t					maxsum_cnt_thres,
	uint32_t					minsum_cnt_thres,
	R_ATMLIB_CNNSCRESMode		scres_mode,
	uint16_t					max_val_thres,
	uint16_t					min_val_thres
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data[4];
	uint16_t				add;
	uint32_t				size_total;
	uint32_t				size_scres;
	uint8_t					size_wpr;

	/*  initialize local variables */
	ret		= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/* check the 4th argument */
	else if( scres_mode >= R_ATMLIB_CNN_SCRESMODE_END )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  calcurate total size of writting CL data */
			size_scres	= 1U;								/* command size of SCRES	*/
			size_wpr	= 4U;								/* command size of WPR		*/
			size_total	= size_scres + 1U + (uint32_t)size_wpr;

			/*  check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize( cldata, size_total );
			/*  check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* -check the scres_mode */
				switch( scres_mode )
				{
				case R_ATMLIB_CNN_SCRESMODE_0:
					/* make the writting data(mode0) to the CL */
					data[0]	= R_ATMLIB_CNN_OPC_SCRES;
					break;
				case R_ATMLIB_CNN_SCRESMODE_1:
					/* make the writting data(mode1) to the CL */
					data[0]	= R_ATMLIB_CNN_OPC_SCRES | 1U;
					break;
				case R_ATMLIB_CNN_SCRESMODE_2:
					/* make the writting data(mode2) to the CL */
					data[0]	= R_ATMLIB_CNN_OPC_SCRES | 2U;
					break;
				default:	/* case R_ATMLIB_CNN_SCRESMODE_3: */
					/*  make the writting data(mode3) to the CL */
					data[0]	= R_ATMLIB_CNN_OPC_SCRES | 3U;
					break;
				}

				/* add the SCRES command in the CL (Clear statistics counter information) */
				(void)atmlib_CNNAddCLCommon( cldata, data, size_scres );

				/* make CL data (MAX_VAL_THRES, MIN_VAL_THRES, MAXSUM_CNT_THRES & MINSUM_CNT_THRES) */
				add		= R_ATMLIB_CNN_MAX_VAL_THRES / 4U;
				data[0]	= (uint32_t)max_val_thres;
				data[1]	= (uint32_t)min_val_thres;
				data[2]	= maxsum_cnt_thres;
				data[3]	= minsum_cnt_thres;

				/**
					add tje WPR command in the CL
					(update MAX_VAL_THRES, MIN_VAL_THRES, MAXSUM_CNT_THRES & MINSUM_CNT_THRES)
				*/
				(void)atmlib_CNNAddCL_WPR( cldata, add, data, size_wpr );

				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHIHG */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetBaseAddress
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				ch_id							: The ID info  
                baddr							: The value of base address
				baddr_cnt						: Count of base address info
				ch_type                         : The channel type, DMAI/DMAC/DMAO
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument ch_id is NULL
				R_ATMLIB_E_NG_ARG3				: The argument baddr is NULL
				R_ATMLIB_E_NG_ARG4				: The argument baddr_cnt is error
				R_ATMLIB_E_NG_ARG5				: The channel type is out of range (>= R_ATMLIB_CNN_TYPE_END)
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_CH_ID             : Value of ch_id member is out of range 

[ Function ]	Set base address of image.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetBaseAddress(
	R_ATMLIB_CLData					*cldata,
	const R_ATMLIB_CNNChannelID  	*ch_id,
	const uint32_t					*baddr,
	uint8_t		                    baddr_cnt,
	R_ATMLIB_CNNChannelType 		ch_type
)
{
	R_ATMLIB_RETURN_VALUE		ret;
	uint8_t 					loop_cnt;
	uint8_t						ch_num;
	uint8_t				  		ch_cnt;
	uint8_t				  		ch_start_id;
	R_ATMLIB_CNNBaseAddrInfo	*baddr_dmax;

	/* initialize local variables */
	ret		 	 = R_ATMLIB_E_OK;
	loop_cnt 	 = 0U;
	ch_num 	 	 = 0U;
	ch_cnt	 	 = 0U;
	ch_start_id	 = 0U;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/* check the 2nd argument */
	else if( ch_id == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( baddr == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}	
	/* check the 5th argument */
	else if( ch_type >= R_ATMLIB_CNN_TYPE_END )
	{
		/* set R_ATMLIB_E_NG_ARG5 in return value */
		ret = R_ATMLIB_E_NG_ARG5;
	}
	/* check the 4th argument */
	else if( (baddr_cnt < 1U) ||
			 ((ch_type == R_ATMLIB_CNN_TYPE_DMAI) && (baddr_cnt > R_ATMLIB_CNN_DMAI_MAX_CH)) ||
			 ((ch_type == R_ATMLIB_CNN_TYPE_DMAC) && (baddr_cnt > R_ATMLIB_CNN_DMAC_MAX_CH)) ||
			 ((ch_type == R_ATMLIB_CNN_TYPE_DMAO) && (baddr_cnt > R_ATMLIB_CNN_DMAO_MAX_CH))
	)
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*record the dma base addr to cnn_data*/
			for( loop_cnt = 0; loop_cnt < baddr_cnt; loop_cnt++ )
			{
				/*check the channel type */
				switch( ch_type )
				{
					case R_ATMLIB_CNN_TYPE_DMAI:
						/* set the ch_cnt and ch_start_id */
						ch_cnt 	= R_ATMLIB_CNN_DMAI_MAX_CH;
						ch_start_id  = (uint8_t)R_ATMLIB_CNN_DMAI0;
						/*  get the base address information of DMAI */
						baddr_dmax	= cldata->cnn_data->baddr_info.baddr_dmai;
						ch_num = (uint8_t)(ch_id[loop_cnt] - R_ATMLIB_CNN_DMAI0);					
						break;
					case R_ATMLIB_CNN_TYPE_DMAO:
						/* set the ch_cnt and ch_start_id */
						ch_cnt = R_ATMLIB_CNN_DMAO_MAX_CH;
						ch_start_id  = (uint8_t)R_ATMLIB_CNN_DMAO0;
						/*  get the base address information of DMAO */
						baddr_dmax	= cldata->cnn_data->baddr_info.baddr_dmao;
						ch_num = (uint8_t)(ch_id[loop_cnt] - R_ATMLIB_CNN_DMAO0);
						break;
					default:	/* case R_ATMLIB_CNN_TYPE_DMAC: */
						/* set the ch_cnt and ch_start_id */
						ch_cnt = R_ATMLIB_CNN_DMAC_MAX_CH;
						ch_start_id  = (uint8_t)R_ATMLIB_CNN_DMAC0;
						/*  get the base address information of DMAC */
						baddr_dmax	= cldata->cnn_data->baddr_info.baddr_dmac;
						ch_num = (uint8_t)(ch_id[loop_cnt] - R_ATMLIB_CNN_DMAC0);
						break;
				}
				/* check the member ch_id of the baddr_config */
				if( (ch_id[loop_cnt] < ch_start_id)  || (ch_id[loop_cnt] >= (ch_start_id + ch_cnt)) )
				{
					/* set R_ATMLIB_E_NG_CH_ID in return value */
					ret = R_ATMLIB_E_NG_CH_ID;
				}
				else if( baddr[loop_cnt] == 0U )
				{
					/* set R_ATMLIB_E_NG_ARG3 in return value */
					ret = R_ATMLIB_E_NG_ARG3;
				}
				else
				{
					/*  store the base address information */
					baddr_dmax[ch_num].baddr		= baddr[loop_cnt];
					baddr_dmax[ch_num].baddr_state	= R_ATMLIB_CNN_BADDRSTAT_SET1ST;
				}
			}
			/* if the ret == R_ATMLIB_E_OK */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupChannelDMAI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmai_param						: DMAI parameters
			    clofs_dmai				        : The pointer to CL image address
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_ARG2				: 2nd argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_COUNT             : member of ch_count/param_count is out of range
				R_ATMLIB_E_NG_CH_ID 			: member id is out of range
				R_ATMLIB_E_NG_CH_CO         	: member channel_offset is out of range
				R_ATMLIB_E_NG_CH_PTR			: member ptr is out of range
				R_ATMLIB_E_NG_CH_IMG_X	   	    : member image_x is out of range
				R_ATMLIB_E_NG_CH_IMG_Y		    : member image_y is out of range
				R_ATMLIB_E_NG_CH_IMG_STR	    : member image_stride is out of range
				R_ATMLIB_E_NG_CH_IMG_FMT	    : member image_format is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE      : member data_mode is out of range
				R_ATMLIB_E_NG_CH_KERNEL_MODE    : member kernel_mode is out of range
				R_ATMLIB_E_NG_CH_DIL_CONV       : member dil_conv is out of range
				R_ATMLIB_E_NG_CH_START_POS      : member start_pos is out of range
				R_ATMLIB_E_NG_CH_STR_X		    : member stride_x is out of range
				R_ATMLIB_E_NG_CH_STR_Y		    : member stride_y is out of range								
				R_ATMLIB_E_NG_CH_PAD_T		    : member pad_t is out of range
				R_ATMLIB_E_NG_CH_PAD_B		    : member pad_b is out of range
				R_ATMLIB_E_NG_CH_PAD_L		    : member pad_l is out of range
				R_ATMLIB_E_NG_CH_PAD_R		    : member pad_r is out of range				
				R_ATMLIB_E_NG_CH_MAG_TYPE       : member mag_type is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_X	    : member repeat_skip_x is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_Y	    : member repeat_skip_y is out of range
                R_ATMLIB_E_NG_CONV_XLEN         : ARI channel width is out of range
				R_ATMLIB_E_NG_CONV_YLEN         : ARI channel height is out of range
 				R_ATMLIB_E_NG_COMB_CNT_SPMC		: Combination of param_count (>1) and data_mode (SPMC) is invalid
				R_ATMLIB_E_NG_COMB_CNT_SCMP_KM	: Combination of param_count (>1), data_mode (SCMP) & kernel_mode(4x1*1, 2x3*3,3x3*3) is invalid
				R_ATMLIB_E_NG_COMB_SPMC_K333	: Combination of data_mode (SPMC) & kernel_mode(3x3*3) is invalid
				R_ATMLIB_E_NG_COMB_KM_DIL		: combination of dil_conv and kern_mode is invalid
				R_ATMLIB_E_NG_COMB_SPMC_DATAMIX	: Combination of image_format and ari_fmt in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_DATAMIX	: Combination of image_format and ari_fmt in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_FM_ST	: Combination of image_format and image_stride in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SPMC_FM_ST	: Combination of image_format and image_stride in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_FM_IMGX		: Combination of image_format and image_x is invalid
				R_ATMLIB_E_NG_COMB_STPOS_PADL	: Combination of start_pos and pad_l is invalid
				R_ATMLIB_E_NG_COMB_KM_STX		: Combination of kernel_mode and stride_x is invalid
				R_ATMLIB_E_NG_COMB_KM_STY		: Combination of kernel_mode and stride_y is invalid
				R_ATMLIB_E_NG_COMB_KM_STPOS		: Combination of kernel_mode and start_pos is invalid
				R_ATMLIB_E_NG_COMB_KM_PADL		: Combination of kernel_mode and pad_l is invalid
				R_ATMLIB_E_NG_COMB_KM_PADR		: Combination of kernel_mode and pad_r is invalid
				R_ATMLIB_E_NG_COMB_KM_PADT		: Combination of kernel_mode and pad_t is invalid
				R_ATMLIB_E_NG_COMB_KM_PADB		: Combination of kernel_mode and pad_b is invalid
				R_ATMLIB_E_NG_COMB_KM_MAG		: Combination of kernel_mode and mag_type is invalid
 [ Function ]	Set the channel information passed by the argument .
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupChannelDMAI(
	R_ATMLIB_CLData					*cldata,
	const R_ATMLIB_CNNDMAIParam		*dmai_param,
	uint32_t 						*clofs_dmai
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				cl_size;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( dmai_param == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check the DMAI paramter info */
			ret = atmlib_CNNCheckParamDMAI( cldata, dmai_param );
			/* check the result of atmlib_CNNCheckDMAIParam() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* set default value of the setup mode to FAST */
				cldata->cnn_data->setup_mode_dmai = R_ATMLIB_CNN_CHSET_FAST;
				/* check whether SCMP & 1x5*5 & count >1, only in such case can be slow*/
				if( (dmai_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) && 
				    (dmai_param->kernel_mode == R_ATMLIB_CNN_KERNEL_1_5_5) && (dmai_param->param_count > 1U) )
				{
					/* judge the DMAI setup mode */
					atmlib_CNNJudgeSetupModeDMAI( cldata, dmai_param->dmai_ch, dmai_param->param_count );
				}

				/* combination check, for feature matrix */
				ret = atmlib_CNNCheckCombParamDMAI( cldata, dmai_param );
				/* check the result of atmlib_CNNCheckCOMBParamDMAI() */
				if( ret == R_ATMLIB_E_OK)
				{
					/* check the setup mode of DMAI */
					if( cldata->cnn_data->setup_mode_dmai == R_ATMLIB_CNN_CHSET_FAST )
					{
						/* calcurate the size of writting data */
						cl_size = 12U;
						/* check the remain size of the CL */
						ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
						/* check the result of atmlib_CNNCheckRemainCLSize() */
						if( ret == R_ATMLIB_E_OK)
						{
							/* setup DMAI in fast mode */
							atmlib_CNNSetupFastDMAI( cldata, dmai_param, clofs_dmai );
							/* update the state of CL info */
							atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
						} /* do nothing for else branch */
					}
					else
					{
						/* calcurate the size of writting data */
						cl_size = 59U + (uint32_t)(4U * dmai_param->ch_count);
						/* check the remain size of the CL */
						ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
						/* check the result of atmlib_CNNCheckRemainCLSize() */
						if( ret == R_ATMLIB_E_OK)
						{
							/* setup DMAI in slow mode */
							atmlib_CNNSetupSlowDMAI( cldata, dmai_param, clofs_dmai );
							/* update the state of CL info */
							atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
						} /* do nothing for else branch */
					}	
					
				} /* end of OK for atmlib_CNNCheckCombParamDMAI(), do nothing for else branch */								
			} /* end of OK for atmlib_CNNCheckParamDMAI(), do nothing for else branch */
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupChannelDMAC
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmac_param						: DMAC parameters
				clofs_dmac						: The pointer to CL image address
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_ARG2				: 2nd argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
			    R_ATMLIB_E_NG_COUNT             : member of ch_count is out of range
				R_ATMLIB_E_NG_CH_ID 			: member id is out of range
				R_ATMLIB_E_NG_CH_CO         	: member channel_offset is out of range
				R_ATMLIB_E_NG_CH_PTR			: member ptr is out of range
				R_ATMLIB_E_NG_CH_IMG_X	   	    : member image_x is out of range
				R_ATMLIB_E_NG_CH_IMG_Y		    : member image_y is out of range
				R_ATMLIB_E_NG_CH_IMG_STR	    : member image_stride is out of range
				R_ATMLIB_E_NG_CH_IMG_FMT	    : member image_format is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE      : member data_mode is out of range
				R_ATMLIB_E_NG_CH_MAG_TYPE       : member mag_type is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_X	    : member repeat_skip_x is out of range
				R_ATMLIB_E_NG_CH_RPT_SKP_Y	    : member repeat_skip_y is out of range
 				R_ATMLIB_E_NG_COMB_CNT_SPMC		: Combination of param_count(>1) and data_mode(SPMC) is invalid
				R_ATMLIB_E_NG_COMB_SPMC_DATAMIX	: Combination of image_format and ari_fmt in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_DATAMIX	: Combination of image_format and ari_fmt in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_SCMP_FM_ST	: Combination of image_format and image_stride in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SPMC_FM_ST	: Combination of image_format and image_stride in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_FM_IMGX		: Combination of image_format and image_x is invalid
 [ Function ]	Set the channel information passed by the argument .
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupChannelDMAC(
	R_ATMLIB_CLData					*cldata,
	const R_ATMLIB_CNNDMACParam		*dmac_param,
	uint32_t 						*clofs_dmac
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				cl_size;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( dmac_param == NULL) 
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
		    /* check the DMAC parameter info */
			ret = atmlib_CNNCheckParamDMAC( cldata, dmac_param );
			/* check the result of atmlib_CNNCheckDMACParam() */
			if( ret == R_ATMLIB_E_OK )
			{
		        /* set default value of the setup mode to FAST */
				cldata->cnn_data->setup_mode_dmac = R_ATMLIB_CNN_CHSET_FAST;
				/* check whether SCMP & channel count >1, only in such case can be slow*/
				if( (dmac_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) && (dmac_param->param_count > 1U) )
				{
					/* judge the DMAC setup mode */
					atmlib_CNNJudgeSetupModeDMAC( cldata, dmac_param->dmac_ch, dmac_param->param_count );
				}
				/* combination check for DMAC, feature matrix */
				ret = atmlib_CNNCheckCombParamDMAC( cldata, dmac_param );
				/* check the result of atmlib_CNNCheckCOMBParamDMAC() */
				if( ret == R_ATMLIB_E_OK )
				{
					/* check the setup mode of DMAC */
					if( cldata->cnn_data->setup_mode_dmac == R_ATMLIB_CNN_CHSET_FAST )
					{
						/* calcurate the size of writting data */
						cl_size = 10U;
						/* check the remain size of the CL */
						ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
						/* check the result of atmlib_CNNCheckRemainCLSize() */
						if( ret == R_ATMLIB_E_OK)
						{
							/* setup DMAC in fast mode*/
							atmlib_CNNSetupFastDMAC( cldata, dmac_param, clofs_dmac );
							/* update the state of CL info */
							atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
						} /* do nothing for else branch */
						
					}
					else
					{
						/* calcurate the size of writting data */
						cl_size = 80U + (uint32_t)(4U * dmac_param->ch_count);
						/* check the remain size of the CL */
						ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
						/* check the result of atmlib_CNNCheckRemainCLSize() */
						if( ret == R_ATMLIB_E_OK)
						{
							/* setup DMAC in slow mode*/
							atmlib_CNNSetupSlowDMAC( cldata, dmac_param, clofs_dmac );
							/* update the state of CL info */
							atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
						} /* do nothing for else branch */
					}					
				} /* end of OK for atmlib_CNNCheckCombParamDMAC(), do nothing for else branch */
				
			} /* end of OK for atmlib_CNNCheckParamDMAC(), do nothing for else branch */
			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupChannelDMAO
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmao_param						: DMAO parameters
				clofs_imgaddr					: The pointer to CL image address
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_ARG2				: 2nd argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
			    R_ATMLIB_E_NG_COUNT             : member of ch_count is out of range
				R_ATMLIB_E_NG_CH_ID 			: member id is out of range
				R_ATMLIB_E_NG_CH_PTR			: member ptr is out of range
				R_ATMLIB_E_NG_CH_IMG_X	   	    : member image_x is out of range
				R_ATMLIB_E_NG_CH_IMG_Y		    : member image_y is out of range
				R_ATMLIB_E_NG_CH_IMG_STR	    : member image_stride is out of range
				R_ATMLIB_E_NG_CH_IMG_FMT	    : member image_format is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE      : member data_mode is out of range
				R_ATMLIB_E_NG_CH_CO      		: member channel_offset is out of range
 				R_ATMLIB_E_NG_CH_KERNEL_MODE    : member kernel_mode is out of range
 				R_ATMLIB_E_NG_COMB_CNT_SPMC		: Combination of param_count(>1) and data_mode(SPMC) is invalid
				R_ATMLIB_E_NG_COMB_SCMP_FM_ST	: Combination of image_format and image_stride in SCMP mode is invalid
				R_ATMLIB_E_NG_COMB_SPMC_FM_ST	: Combination of image_format, ari_format and image_stride in SPMC mode is invalid
				R_ATMLIB_E_NG_COMB_FM_IMGX		: Combination of image_format and image_x is invalid
 [ Function ]	Set the channel information passed by the argument .
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupChannelDMAO(
	R_ATMLIB_CLData					*cldata,
	const R_ATMLIB_CNNDMAOParam		*dmao_param,
	uint32_t 						*clofs_dmao
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				cl_size;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( dmao_param == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check the DMAO parameters info */
			ret = atmlib_CNNCheckParamDMAO( cldata, dmao_param );
			/* check the result of atmlib_CNNCheckDMAIParam() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* set default value of the setup mode to FAST */
				cldata->cnn_data->setup_mode_dmao = R_ATMLIB_CNN_CHSET_FAST;
				/* check whether SCMP & parameter count >1, only in such case can be slow*/
				if( (dmao_param->data_mode == R_ATMLIB_CNN_DATA_SCMP) && (dmao_param->param_count > 1U) )
				{
					/* judge the setup mode of DMAO*/
					atmlib_CNNJudgeSetupModeDMAO( cldata, dmao_param->ptr, dmao_param->param_count );
				}
				/* combination check for DMAO, the feature matrix*/
				ret = atmlib_CNNCheckCombParamDMAO( cldata, dmao_param );
				/* check the result of atmlib_CNNCheckCOMBParamDMAO() */
				if( ret == R_ATMLIB_E_OK )
				{
					/* check the setup mode of DMAO*/
					if( cldata->cnn_data->setup_mode_dmao == R_ATMLIB_CNN_CHSET_FAST )
					{
						/* calcurate the size of writting data */
						cl_size = 9U;
						/* check the remain size of the CL */
						ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
						/* check the result of atmlib_CNNCheckRemainCLSize() */
						if( ret == R_ATMLIB_E_OK)
						{
							/* setup DMAO in fast mode*/
							atmlib_CNNSetupFastDMAO( cldata, dmao_param, clofs_dmao );
							/* update the state of CL info */
							atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
						} /* do nothing for else branch */
					}
					else
					{
						/* calcurate the size of writting data */
						cl_size = 4U + (uint32_t)(4U * dmao_param->ch_count);
						/* check the remain size of the CL */
						ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
						/* check the result of atmlib_CNNCheckRemainCLSize() */
						if( ret == R_ATMLIB_E_OK)
						{
							/* setup DMAO in slow mode*/
							atmlib_CNNSetupSlowDMAO( cldata, dmao_param, clofs_dmao );
							/* update the state of CL info */
							atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
						} /* do nothing for else branch */
					}
				} /* end of OK for atmlib_CNNCheckCombParamDMAO(), do nothing for else branch */
				
			} /* end of OK for atmlib_CNNCheckParamDMAO(), do nothing for else branch */
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NONTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupWeights
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmarf_param     				: Pointer to the DMARF parameter
				weight_info    					: Pointer to the weight information transfered by WPR
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_ARG3				: 3nd argument is error
				                                  weight info and dmarf_param are both NULL
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_RF_EXSA			: member external_sa is out of range
				R_ATMLIB_E_NG_RF_INSA			: member internal_sa is out of range
				R_ATMLIB_E_NG_RF_LEN			: member length is out of range
				R_ATMLIB_E_NG_RF_STAPOS			: member start_pos is out of range
				R_ATMLIB_E_NG_RF_ENDPOS			: member end_pos is out of range
				R_ATMLIB_E_NG_DMARFFM			: member format is out of range
				R_ATMLIB_E_NG_COUNT				: Member ch_cnt is out of range
				R_ATMLIB_E_NG_WB_FMT			: Member weight_format is out of range
				R_ATMLIB_E_NG_CH_DATA_MODE	    : Member data_mode is out of range
				R_ATMLIB_E_NG_CH_ID				: Member in_id/out_id is out of range
				R_ATMLIB_E_NG_WB_VALUE          : Member value is out of range
				R_ATMLIB_E_NG_COMB_RFFM_ARIFM	: Combination of next_ari_format and dmarf_param is invalid
				R_ATMLIB_E_NG_COMB_WBFM_ARIFM   : Combination of ari_format and weight_format is invalid
 [ Function ]	Setup  weights buffer.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupWeights(
	R_ATMLIB_CLData					*cldata,	
	const R_ATMLIB_CNNDMARFParam 	*dmarf_param, 
	const R_ATMLIB_CNNWeightInfo 	*weight_info
)
{
	/* check input parameters */
	R_ATMLIB_RETURN_VALUE	ret;
	uint16_t				add;
	uint32_t				cl_size;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( (dmarf_param == NULL) && (weight_info == NULL) )
	{
		/* set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			if( dmarf_param != NULL)
			{
				/* check the DMARF parameter */
				ret = atmlib_CNNCheckParamWeightDMARF( cldata, dmarf_param );
				/* check the result of atmlib_CNNCheckParamWeightDMARF() */
				if( ret == R_ATMLIB_E_OK )
				{
					/* calcurate the size of writting data */
					cl_size = 5U;
					/* check the remain size of the CL */
					ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
					/* check the result of atmlib_CNNCheckRemainCLSize() */
					if( ret == R_ATMLIB_E_OK)
					{
						/* setup weight by DMARF */
						atmlib_CNNSetupDMARF( cldata, dmarf_param );
						/* get the start address of weight buffer */
						add = R_ATMLIB_CNN_WB0_BASE / 4U;
						/*  update attr of current_reg */
						atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, add, R_ATMLIB_CNN_WB_REG_LEN );
				   		/* update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					} /* do nothing for else branch */
				} /* do nothing for else branch */				
				
			}/* end of if( dmarf_param != NULL )  */
			else
			{
				/* check the weight parameter */
				ret = atmlib_CNNCheckParamWeightWPR( cldata, weight_info );
				/* check the result of atmlib_CNNCheckParamWeight() */
				if( ret == R_ATMLIB_E_OK )
				{
					/* calcurate the size of writting data */
					if ( cldata->cnn_data->ari_format <= R_ATMLIB_CNN_ARIFORMAT_8U)
					{
						cl_size = (uint32_t)(8U * weight_info->ch_cnt);
					}
					else
					{
						cl_size = (uint32_t)(16U * weight_info->ch_cnt);
					}
					
					/* check the remain size of the CL */
					ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
					/* check the result of atmlib_CNNCheckRemainCLSize() */
					if( ret == R_ATMLIB_E_OK)
					{
						/* setup weights by WPR */
						atmlib_CNNSetupWeightsWPR( cldata, weight_info );
						/* update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					} /* do nothing for else branch */
				} /* do nothing for else branch */					
			}			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
		
	}

	return ret;

}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupLUT
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dmarf_param     				: Pointer to the DMARF parameter
				lut_info    					: Pointer to the LUT information transfered by WPR
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_ARG3				: 3rd argument is error
				                                  lut_info and dmarf_param are both NULL
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_DMARFFM			: Member format is out of range
				R_ATMLIB_E_NG_COMB_RFFM_ARIFM	: Combination of next_ari_format and dmarf_param is invalid
				R_ATMLIB_E_NG_COMB_LUTFM_ARIFM	: Combination of lut_format and ari_format is invalid
 				R_ATMLIB_E_NG_CH_DATA_MODE		: Member data_mode is out of range
				R_ATMLIB_E_NG_RF_EXSA			: Member external_sa is out of range
				R_ATMLIB_E_NG_RF_INSA			: Member internal_sa is out of range
				R_ATMLIB_E_NG_RF_LEN			: Member length is out of range
				R_ATMLIB_E_NG_LUT_FMT			: Member lut_format is out of range
				R_ATMLIB_E_NG_LUT_VALUE			: Member value is out of range

 [ Function ]	Setup  LUT.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupLUT(
	R_ATMLIB_CLData					*cldata,
	const R_ATMLIB_CNNDMARFParam 	*dmarf_param,
	const R_ATMLIB_CNNLUTInfo 		*lut_info
)
{
	/* check input parameters */
	R_ATMLIB_RETURN_VALUE	ret;
	uint16_t				add;
	uint32_t				cl_size;
	uint32_t				tmp_aricsr;
	uint32_t				opc;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;
	tmp_aricsr	= 0U;
	opc			= 0U;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( (dmarf_param == NULL) && (lut_info == NULL) )
	{
		/* set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			if( dmarf_param != NULL )
			{
				/* check the DMARF parameter */
				ret = atmlib_CNNCheckParamLUTDMARF( cldata, dmarf_param );
				/* check the result of atmlib_CNNCheckParamLUTDMARF() */
				if ( ret == R_ATMLIB_E_OK )
				{
					/* calcurate the size of writting data */
					cl_size = 10U;
					/* check the remain size of the CL */
					ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
					/* check the result of atmlib_CNNCheckRemainCLSize() */
					if( ret == R_ATMLIB_E_OK)
					{
						/* SYNCS DMARF at first, ensure the DMARF can be used */
						opc = R_ATMLIB_CNN_OPC_SYNCS | R_ATMLIB_CNN_SYNCS_DMARF;								
						/* add the SYNCS commands */
						(void)atmlib_CNNAddCLCommon( cldata, &opc, 1U );

						/* check whether the ari_format is equals to the next_ari_format */
						if(cldata->cnn_data->ari_format != cldata->cnn_data->next_ari_format)
						{
							/* chang ARICSR::NEXT_ARIFM bit info to ARI_FM */
							tmp_aricsr = (0xFF9FFFFFU) & cldata->cnn_data->channel_props.ARICSR;
							tmp_aricsr |= ((uint32_t)(cldata->cnn_data->ari_format << 21U) & 0x00600000U);
							/* fix the dest address */
							add = R_ATMLIB_CNN_ARICSR / 4U;
							/* add the WPR command */
							(void)atmlib_CNNAddCL_WPR( cldata, add, &tmp_aricsr, 1U );
						}
						/* setup LUT by DMARF */
						atmlib_CNNSetupDMARF( cldata, dmarf_param );
						/* check whether the ari_format is equals to the next_ari_format */
						if(cldata->cnn_data->ari_format != cldata->cnn_data->next_ari_format)
						{
							/* chang ARIFM to NEXT_ARIFM */
							tmp_aricsr = cldata->cnn_data->channel_props.ARICSR;
							/* fix the dest address */
							add = R_ATMLIB_CNN_ARICSR / 4U;
							/* add the WPR command */
							(void)atmlib_CNNAddCL_WPR( cldata, add, &tmp_aricsr, 1U );
						}
						/* get the start address of LUT Base address */
						add = R_ATMLIB_CNN_LUT_BASE / 4U;
						/*  update attr of current_reg */
						atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, add, R_ATMLIB_CNN_LUT_REG_LEN );
						/* update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					} /* do nothing for else branch */
				} /* do nothing for else branch */				
			} /* end of if( dmarf_param != NULL ) */
			else
			{
				/* check the LUT parameter */
				ret = atmlib_CNNCheckParamLUTWPR( cldata, lut_info );
				/* check the result of atmlib_CNNCheckParamLUT() */
				if ( ret == R_ATMLIB_E_OK )
				{
					/* calcurate the size of writting data,65: 1 WPR+64 LUT reg; 4: 4 LUT tables */
					cl_size = (uint32_t)(65U * 4U);
					/* check the remain size of the CL */
					ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
					/* check the result of atmlib_CNNCheckRemainCLSize() */
					if( ret == R_ATMLIB_E_OK)
					{
						/*setup LUT by WPR*/
						atmlib_CNNSetupLUTWPR( cldata, lut_info );
						/* update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					} /* do nothing for else branch */
				} /* do nothing for else branch */				
			}
			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
		
	}

	return ret;

}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupBiases
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				bias_vec						: Pointer to the bias vector
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument bias_vec is NULL
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Set the bias values.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupBiases (
	R_ATMLIB_CLData		*cldata,
	const int16_t 		*bias_vec
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint8_t 				loop_cnt;
	uint32_t 				BIAS_COMB[16];
	uint16_t 				add;
	uint32_t				cl_size;
	uint8_t 				bias_same_flag;
	uint8_t					size;

	/*  initialize local variables */
	ret				= R_ATMLIB_E_OK;
	cl_size			= 0U;
	bias_same_flag	= 0U;
	(void)memset( BIAS_COMB,  0U, sizeof(BIAS_COMB) );

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( bias_vec == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{			
			/* check whether all the bias values are same */
			for( loop_cnt = 1; loop_cnt < R_ATMLIB_CNN_ARI_MAX_CH ; loop_cnt++ )
			{
				if(bias_vec[loop_cnt] != bias_vec[0])
				{
					/* bias is not same */
					bias_same_flag = 0U;
					break;
				}
				else
				{
					/* bias is same */
					bias_same_flag = 1U;
				}
			}
			/* generate CL according the bias value */
			if ( bias_same_flag == 1U )
			{
				/* calcurate the size of writting data */
				cl_size = 2U;
				/* make preset data to the CL (BIAS)) */
				BIAS_COMB[0] = (uint32_t)bias_vec[0] & 0x0000FFFFU;
				/* fix the dest address */
				add = R_ATMLIB_CNN_BIAS / 4U;
			}
			else
			{
				/* calcurate the size of writting data */
				cl_size = 17U;
				/* loop for the R_ATMLIB_CNN_DMAO_MAX_CH */
				for( loop_cnt = 0; loop_cnt < (R_ATMLIB_CNN_ARI_MAX_CH / 2U) ; loop_cnt++ )
				{
					/* make preset data to the CL (BIAS00_01 ~ BIAS30_31)) */
					BIAS_COMB[loop_cnt] = ( ((uint32_t)bias_vec[loop_cnt*2U + 1U] << 16U ) & 0xFFFF0000U)  | 
										  ( ((uint32_t)bias_vec[loop_cnt*2U]             ) & 0x0000FFFFU);
				}

				/* fix the dest address */
				add = R_ATMLIB_CNN_BIAS00_01 / 4U;
			}

			/* check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
			/* check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK)
			{
				/* get the size*/
				size = (uint8_t)(cl_size-1U);
				/* add WPR command */
				(void)atmlib_CNNAddCL_WPR( cldata, add, BIAS_COMB, size );
				/*  update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			} /* do nothing for else branch */					
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupShifts
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
                shift_cfg	   				    : Pointer to shift configuration parameter
				shift_func						: User Shift function to use
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument shift_cfg is NULL
				R_ATMLIB_E_NG_ARG3				: The argument shift_func is NULL
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_SHIFTER           : shifter func error
 [ Function ]	Set the shifter values.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupShifts(
	R_ATMLIB_CLData					*cldata,
	const R_ATMLIB_CNNShiftConfig 	*shift_cfg,
	const R_ATMLIB_CNNShiftFunc		shift_func
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/*  initialize local variables */
	ret		= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( shift_cfg == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else if( shift_func == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  run the user shift function */
			ret = shift_func( cldata, shift_cfg );
			if (ret == R_ATMLIB_E_OK)
			{
				/*  update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			} 
			else
			{
				/* shifter function occur error */
				ret = R_ATMLIB_E_NG_SHIFTER;
			}
			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetupARI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				ari_param						: ARI parameters
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_ARG2				: 2nd argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
			    R_ATMLIB_E_NG_CONV_ARI_FMT		: Member ari_format of ari_param is out of range
				R_ATMLIB_E_NG_CONV_ACTI_MODE	: Member acti_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_LOWER_VAL	: Member lower_bound of ari_param is out of range
				R_ATMLIB_E_NG_CONV_MULT_VAL		: Member mult_val of ari_param is out of range
				R_ATMLIB_E_NG_CONV_LUT_LIN_INT	: Member lut_lin_int of ari_param is out of range
				R_ATMLIB_E_NG_CONV_PAD_MODE		: Member pad_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_BIAS			: Member bias_en is of ari_param out of range
				R_ATMLIB_E_NG_CONV_XYPOOL_MODE	: Member xypool_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_XPOOL		: Member xpool of ari_param is out of range
				R_ATMLIB_E_NG_CONV_YPOOL		: Member ypool of ari_param is out of range
				R_ATMLIB_E_NG_CONV_CPOOL_MODE	: Member cpool_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_LABEL_INFO	: Member label_info of ari_param is out of range
				R_ATMLIB_E_NG_CONV_MAXOUT_GROUP	: Member maxout_group of ari_param is out of range
				R_ATMLIB_E_NG_CONV_ARI_FMT		: Member next_ari_format of ari_param is out of range
				R_ATMLIB_E_NG_CONV_SFTMRM		: Member sftm_round_mode of ari_param is out of range
				R_ATMLIB_E_NG_CONV_CPOOLUI		: Member cpool_ui of ari_param is out of range
				R_ATMLIB_E_NG_COUNT				: Member ari_cnt of ari_param is out of range
				R_ATMLIB_E_NG_CH_ID				: Member ch_id of ari_param is out of range
				R_ATMLIB_E_NG_CONV_CHGLOBSUM	: Member glob_sum of channel_info is out of range
				R_ATMLIB_E_NG_CONV_CHMAXMIN		: Member channel_max_min of channel_info is out of range
 
 [ Function ]	Set the ARI information passed by the argument ari_param.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupARI(
	R_ATMLIB_CLData						*cldata,
	const R_ATMLIB_CNNARIParam 			*ari_param,
	const R_ATMLIB_CNNChannelStatistic 	*channel_info
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				cl_size;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if( ari_param == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check the ARI parameter info */
			ret = atmlib_CNNCheckParamARI( ari_param, channel_info );
			/* check the result of atmlib_CNNCheckParamARI() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* calcurate the size of writting data */
				cl_size = 7U;
				/* check the remain size of the CL */
				ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
				/* check the result of atmlib_CNNCheckRemainCLSize() */
				if( ret == R_ATMLIB_E_OK)
				{
					/* set up the ARI, generate CL */
					atmlib_CNNSetupARI( cldata, ari_param, channel_info );
					
					/* update the state of CL info */
					atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
				} /* do nothing for else branch */
			} /* do nothing for else branch */
			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;

}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNStartDMAI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Start DMAI.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartDMAI(
	R_ATMLIB_CLData *cldata 
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;
	uint16_t				add;
	uint32_t				cl_size;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;
	data		= 0x0000001F;

	/* check the argument */
	if( cldata == NULL )
	{
		/*set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* calcurate the size of writting data */
			cl_size = 5U;
			/* check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
			/* check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK)
			{
				/* check whether the DMIE/DMA3DCE changed or not */
				if ((cldata->cnn_data->channel_props.DMAIE != cldata->cnn_data->current_reg[R_ATMLIB_CNN_DMAIE/4U].value) ||
					(cldata->cnn_data->channel_props.DMA3DCE != cldata->cnn_data->current_reg[R_ATMLIB_CNN_DMA3DCE/4U].value)
				)
				{
					/* if DMAIE or DMA3DCE changed, SYNCS ARI */
					data = R_ATMLIB_CNN_SYNCS_ARI;
				}				
				
				/* SYNCS DMAI & DMA3DC */
				data = R_ATMLIB_CNN_OPC_SYNCS | (data & R_ATMLIB_CNN_SYNCS_DMAI & R_ATMLIB_CNN_SYNCS_DMAC);
								
				/* add the SYNCS commands */
				(void)atmlib_CNNAddCLCommon( cldata, &data, 1U );
				
				/* setting the DMAIS register */
				data	= cldata->cnn_data->channel_props.DMAIS;
				add = R_ATMLIB_CNN_DMAIS / 4U;

				/* WPR command in the CL */
				(void)atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );

				/* setting the DMAIE register */
				data	= cldata->cnn_data->channel_props.DMAIE;
				add = R_ATMLIB_CNN_DMAIE / 4U;

				/* WPR command in the CL */
				(void)atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );
				
				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			} /* do nothing for else branch */			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*DO NOTHING */
		}
	}

	return ret;

}



/******************************************************************************
 [ FuncName ]	r_atmlib_CNNStartDMAC
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Start DMAC.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartDMAC( 
	R_ATMLIB_CLData *cldata 
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;
	uint16_t				add;
	uint32_t				cl_size;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/* check the argument */
	if( cldata == NULL )
	{
		/*set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* calcurate the size of writting data */
			cl_size = 4U;
			/* check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
			/* check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK)
			{
				/* setting the DMA3DCS register*/
				data = cldata->cnn_data->channel_props.DMA3DCS;
				add = R_ATMLIB_CNN_DMA3DCS / 4U;

				/*WPR command in the CL*/
				(void)atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );

				/*setting the DMA3DCE register*/
				data = cldata->cnn_data->channel_props.DMA3DCE;
				add = R_ATMLIB_CNN_DMA3DCE / 4U;

				/*WPR command in the CL*/
				(void)atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );
				
				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			} /* do nothing for else branch */		
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNStartDMAO
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Start DMAO
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartDMAO( 
	R_ATMLIB_CLData *cldata 
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;
	uint16_t				add;
	uint32_t				cl_size;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/* check the argument */
	if( cldata == NULL )
	{
		/*set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* calcurate the size of writting data */
			cl_size = 5U;
			/* check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
			/* check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK)
			{
				/* SYNCS DMAO & DMARF */
				data = R_ATMLIB_CNN_OPC_SYNCS | (R_ATMLIB_CNN_SYNCS_DMAO & R_ATMLIB_CNN_SYNCS_DMARF);
							
				/* add the SYNCS commands */
				(void)atmlib_CNNAddCLCommon( cldata, &data, 1U );
				
				/* setting the DMAOS register*/
				data	= cldata->cnn_data->channel_props.DMAOS;
				add = R_ATMLIB_CNN_DMAOS / 4U;

				/*WPR command in the CL*/
				(void)atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );

				/*setting the DMA3OE register*/
				data	= cldata->cnn_data->channel_props.DMAOE;
				add = R_ATMLIB_CNN_DMAOE / 4U;

				/*WPR command in the CL*/
				(void)atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );
				
				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			} /* do nothing for else branch */
			
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNStartTile
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: 1st argument is error
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Start Tile convolution.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartTile( 
	R_ATMLIB_CLData *cldata 
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;
	uint16_t				add;
	uint32_t				cl_size;

	/* initialize local variables */
	ret			= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/* check the argument */
	if( cldata == NULL )
	{
		/*set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* calcurate the size of writting data */
			cl_size = 6U;
			/* check the remain size of the CL */
			ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
			/* check the result of atmlib_CNNCheckRemainCLSize() */
			if( ret == R_ATMLIB_E_OK)
			{
				/* setting the ARI_LEN register*/
				data	= cldata->cnn_data->channel_props.ARI_LEN;
				add = R_ATMLIB_CNN_ARI_LEN / 4U;
				/*WPR command in the CL*/
				atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );

				/* setting the ARIE register*/
				data	= cldata->cnn_data->channel_props.ARIE;
				add = R_ATMLIB_CNN_ARIE / 4U;
				/*WPR command in the CL*/
				atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );

				/* setting the SLSP register*/
				data	= 0x1;
				add = R_ATMLIB_CNN_SLSP / 4U;
				/*WPR command in the CL*/
				atmlib_CNNAddCL_WPR( cldata, add, &data, 1U );
				
				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			} /* do nothing for else branch */
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNLayerEpilog
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				uvt_index						: Update Value Table index of TUV transfer (REG: UV_TABLE[uvt_index]) [1..R_ATMLIB_MAX_DYN_FP_HANDLE]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument uvt_index is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append Layer epilog to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNLayerEpilog(
	R_ATMLIB_CLData	*cldata,
	uint8_t			uvt_index
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data[3];
	uint8_t					size;

	/*  initialize local variables */
	ret		= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (uvt_index < 1U) || (uvt_index > R_ATMLIB_MAX_DYN_FP_HANDLE) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			
			/* add SYNCS SCCHK and TUV2UVT */
			data[0]	= R_ATMLIB_CNN_OPC_SYNCS;									/* waits for completion of the CNN operation */
			data[1]	= R_ATMLIB_CNN_OPC_SCCHK;									/* check statistic counter			*/
			data[2]	= R_ATMLIB_CNN_OPC_TUV2UVT | ((uint32_t)uvt_index - 1U);	/* TUV value is transferred to UVT	*/
			size	= 3U;														/* size of the above commands		*/
			
			/* add the commands */
			ret = atmlib_CNNAddCLCommon( cldata, data, size );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNPictureEpilog
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dyn_ocfp_count					: The number of dynamic ocfps in the linear network
				frac_func						: The fraction adjusting function
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument dyn_ocfp_count is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append Picture epilog to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNPictureEpilog(
	R_ATMLIB_CLData							*cldata,
	uint8_t									dyn_ocfp_count,
	const R_ATMLIB_CNNFractionAdjustFunc	frac_func
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (dyn_ocfp_count < 1U) || (dyn_ocfp_count > R_ATMLIB_MAX_DYN_FP_HANDLE) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check whether calling frac_func is required */
			if( frac_func != NULL )
			{
				/* execute frac_func */
				ret = frac_func( cldata, dyn_ocfp_count );
			}

			/* check the result of frac_func() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNEndProcessing
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				trap_code						: Specify the status for TRAP identification
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_UNDEFINE_LABEL    : there is undefined label exists in the CL
 [ Function ]	Append the TRAP command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNEndProcessing(
	R_ATMLIB_CLData	*cldata,
	uint8_t			trap_code
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data[2];

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_E );
		/*  check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else if( ret != R_ATMLIB_E_OK )
		{
			/* DO NOTHING */
		}
		else
		{
			/*  check whether the labels are resolved */
			if( cldata->cnn_data->label_info.resolve_count == 0U )
			{
				/* make writting data to the CL */
				data[0] = R_ATMLIB_CNN_OPC_SYNCS;
				data[1] = R_ATMLIB_CNN_OPC_TRAP | trap_code;

				/* add the TRAP command */
				ret = atmlib_CNNAddCLCommon( cldata, data, 2U );
				/*  check the result of atmlib_CNNAddCLCommon() */
				if( ret == R_ATMLIB_E_OK )
				{
					/* update the state of CL info */
					atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_FINAL );
				}
			}
			else
			{
				/* case of error : there is the undefined labels... */
				/* set R_ATMLIB_E_NG_UNDEFINE_LABEL in return value */
				ret = R_ATMLIB_E_NG_UNDEFINE_LABEL;
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_ADD
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dst_start_add					: Start address of destination register 
				src_add							: Address of source register 
				dst_length						: Number of destination register
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument dst_start_add is out of range
				R_ATMLIB_E_NG_ARG3				: The argument src_add is out of range
				R_ATMLIB_E_NG_ARG4				: The argument dst_length is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append ADD command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_ADD(
	R_ATMLIB_CLData	*cldata,
	uint16_t		dst_start_add,
	uint16_t		src_add,
	uint8_t			dst_length
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (dst_start_add < R_ATMLIB_CNN_ADDR_MIN) || (dst_start_add > R_ATMLIB_CNN_ADDR_MAX) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( (src_add < R_ATMLIB_CNN_ADDR_MIN) || (src_add > R_ATMLIB_CNN_ADDR_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( (dst_length < R_ATMLIB_CNN_DSTLEN_MIN) || (dst_length > R_ATMLIB_CNN_DSTLEN_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  make writting data to the CL, offset 0x2000 is added by HW automaticly  */
			data	= R_ATMLIB_CNN_OPC_ADD | (((uint32_t)dst_length & 0x0000003FU) << 18U) | (((uint32_t)(dst_start_add - 0x800) & 0x000001FFU) << 9U) | ((uint32_t)(src_add - 0x800) & 0x000001FFU);
			
			/*  add the ADD / NOP command */
			ret = atmlib_CNNAddCLCommon( cldata, &data, 1U );
			/*  check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update attr of current_reg, the ADD function is using the address space starting from 0x2000 */
				atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, dst_start_add, dst_length );

				/* -update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_SUB
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dst_start_add					: Start address of destination register
				src_add							: Address of source register 
				dst_length						: Number of destination register
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument dst_start_add is out of range
				R_ATMLIB_E_NG_ARG3				: The argument src_add is out of range
				R_ATMLIB_E_NG_ARG4				: The argument dst_length is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append SUB command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SUB(
	R_ATMLIB_CLData *cldata,
	uint16_t dst_start_add,
	uint16_t src_add,
	uint8_t  dst_length
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (dst_start_add < R_ATMLIB_CNN_ADDR_MIN) || (dst_start_add > R_ATMLIB_CNN_ADDR_MAX) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( (src_add < R_ATMLIB_CNN_ADDR_MIN) || (src_add > R_ATMLIB_CNN_ADDR_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( (dst_length < R_ATMLIB_CNN_DSTLEN_MIN) || (dst_length > R_ATMLIB_CNN_DSTLEN_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  make writting data to the CL, offset 0x2000 is added by HW automaticly */
			data	= R_ATMLIB_CNN_OPC_SUB | (((uint32_t)dst_length & 0x0000003FU) << 18U) | (((uint32_t)(dst_start_add - 0x800) & 0x000001FFU) << 9U) | ((uint32_t)(src_add - 0x800) & 0x000001FFU);
			
			/*  add the SUB / NOP commands */
			ret = atmlib_CNNAddCLCommon( cldata, &data, 1U );
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update attr of current_reg, the SUB function is using the address space starting from 0x2000 */
				atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, dst_start_add, dst_length );

				/* -update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_WR
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dst_start_add					: Start address of destination register
				src_add							: Address of source register 
				dst_length						: Number of destination register 
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument dst_start_add is out of range
				R_ATMLIB_E_NG_ARG3				: The argument src_add is out of range
				R_ATMLIB_E_NG_ARG4				: The argument dst_length is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append WR command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WR(
	R_ATMLIB_CLData	*cldata,
	uint16_t		dst_start_add,
	uint16_t		src_add,
	uint8_t			dst_length
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (dst_start_add < R_ATMLIB_CNN_ADDR_MIN) || (dst_start_add > R_ATMLIB_CNN_ADDR_MAX) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( (src_add < R_ATMLIB_CNN_ADDR_MIN) || (src_add > R_ATMLIB_CNN_ADDR_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( (dst_length < R_ATMLIB_CNN_DSTLEN_MIN) || (dst_length > R_ATMLIB_CNN_DSTLEN_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  make writting data to the CL, offset 0x2000 is added by HW automaticly */
			opc	= R_ATMLIB_CNN_OPC_WR | (((uint32_t)dst_length & 0x0000003FU) << 18U) | (((uint32_t)(dst_start_add - 0x800) & 0x000001FFU) << 9U) | ((uint32_t)(src_add - 0x800) & 0x000001FFU);

			/*  add the WR command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/*  check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update attr of current_reg */
				atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, dst_start_add, dst_length );

				/* -update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_WRI
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dst_start_add					: Start address of destination register 
				imm								: Immediate value 
				dst_length						: Number of destination register
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument dst_start_add is out of range
				R_ATMLIB_E_NG_ARG3				: The argument src_add is out of range
				R_ATMLIB_E_NG_ARG4				: The argument dst_length is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append WRI command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WRI(
	R_ATMLIB_CLData	*cldata,
	uint16_t		dst_start_add,
	int16_t			imm,
	uint8_t			dst_length
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (dst_start_add < R_ATMLIB_CNN_ADDR_MIN) || (dst_start_add > R_ATMLIB_CNN_ADDR_MAX))
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( (imm < R_ATMLIB_CNN_IMM_MIN ) || (imm > R_ATMLIB_CNN_IMM_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( (dst_length < R_ATMLIB_CNN_DSTLEN_MIN) || (dst_length > R_ATMLIB_CNN_DSTLEN_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  add the WRI command, offset 0x2000 is added by HW automaticly  */
			ret = atmlib_CNNAddCL_WRI( cldata, dst_start_add, imm, dst_length );
			if( ret == R_ATMLIB_E_OK )
			{
				/* update status of CL */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* case of error : 1st argument error */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* case of error : return the result of atmlib_CNNCheckParamCL() */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_BRC
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				cond							: Branch condition
				dst								: Branch offset of program counter 
				src_add							: Address of reference register
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument cond is out of range
				R_ATMLIB_E_NG_ARG3				: The argument dst is out of range
				R_ATMLIB_E_NG_ARG4				: The argument src_add is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append BRC command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_BRC(
	R_ATMLIB_CLData		*cldata,
	R_ATMLIB_CNNBRCType	cond,
	int16_t				dst,
	uint16_t			src_add
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 1st argument */
	else if( cond >= R_ATMLIB_CNN_BRC_END )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3th argument */
	else if( (dst < R_ATMLIB_CNN_DST_MIN) || (dst > R_ATMLIB_CNN_DST_MAX) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( (src_add < R_ATMLIB_CNN_MIN_BRCADD) || (src_add >= R_ATMLIB_CNN_MAX_BRCADD) )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  check the branch condition */
			switch( cond )
			{
			case R_ATMLIB_CNN_BRC_EZ:
				/*  make writting data to the CL */
				opc = R_ATMLIB_CNN_OPC_BRC | 0x00000000U;
				break;
			case R_ATMLIB_CNN_BRC_GZ:
				/*  make writting data to the CL */
				opc = R_ATMLIB_CNN_OPC_BRC | 0x00400000U;
				break;
			case R_ATMLIB_CNN_BRC_LZ:
				/* -make writting data to the CL */
				opc = R_ATMLIB_CNN_OPC_BRC | 0x00800000U;
				break;
			default:	/* case R_ATMLIB_CNN_BRC_ALWAYS: */
				/* make writting data to the CL */
				opc = R_ATMLIB_CNN_OPC_BRC | 0x00C00000U;
				break;
			}
			/* make writting data to the CL, offset 0x2000 is added by HW automaticly  */
			opc |= (((uint32_t)dst & 0x00001FFFU) << 9U) | ((uint32_t)(src_add - 0x800) & 0x000001FFU);

			/* add the BRC command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/*  check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*  DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_CLUVT
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append CLUVT command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_CLUVT(
	R_ATMLIB_CLData	*cldata
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_CLUVT;

			/*  add the CLUVT command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_SCRES
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				mode							: clear mode [0..3]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument mode is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append SCRES command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SCRES(
	R_ATMLIB_CLData			*cldata,
	R_ATMLIB_CNNSCRESMode	mode
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( mode >= R_ATMLIB_CNN_SCRESMODE_END )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check the CLUVT mode */
			switch( mode )
			{
			case R_ATMLIB_CNN_SCRESMODE_0:
				/* make writting data to the CL */
				opc	= R_ATMLIB_CNN_OPC_SCRES | 0x00000000U;
				break;
			case R_ATMLIB_CNN_SCRESMODE_1:
				/* make writting data to the CL */
				opc	= R_ATMLIB_CNN_OPC_SCRES | 0x00000001U;
				break;
			case R_ATMLIB_CNN_SCRESMODE_2:
				/* make writting data to the CL */
				opc	= R_ATMLIB_CNN_OPC_SCRES | 0x00000002U;
				break;
			default:	/* case R_ATMLIB_CNN_SCRESMODE_3: */
				/*  make writting data to the CL */
				opc	= R_ATMLIB_CNN_OPC_SCRES | 0x00000003U;
				break;
			}

			/*  add the SCRES command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/*  check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_SCCHK
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append SCCHK command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SCCHK(
	R_ATMLIB_CLData	*cldata
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_SCCHK;

			/*  add the SCCHK command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_TUV2UVT
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				uvt_index						: Update Value Table index of TUV transfer (REG: UV_TABLE[uvt_index]) [1..R_ATMLIB_MAX_DYN_FP_HANDLE]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument uvt_index is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append TUV2UVT command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_TUV2UVT(
	R_ATMLIB_CLData	*cldata,
	uint8_t			uvt_index
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 1st argument */
	else if( (uvt_index <R_ATMLIB_CNN_UVT_INDEX_MIN) || (uvt_index > R_ATMLIB_MAX_DYN_FP_HANDLE) )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_TUV2UVT | ((uint32_t)uvt_index - 1U);

			/* add the TUV2UVT command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* case of error : 1st argument error */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* case of error : return the result of atmlib_CNNCheckParamCL() */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_CLW
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append CLW command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_CLW(
	R_ATMLIB_CLData		*cldata
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;
	uint16_t                add;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );

		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_CLW;
		
			/* add the CLW command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/*  check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* get the start address of weight buffer */
				add = R_ATMLIB_CNN_WB0_BASE / 4U;
				/*  update attr of current_reg */
				atmlib_CNNUnsetCurReg( cldata->cnn_data->current_reg, add, R_ATMLIB_CNN_WB_REG_LEN );
				/*  update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*  DO NOTHING */
		}
	}
	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_WPR
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				add								: Offset of writes register start address
				n								: Number of writes register
				data							: Pointer to the area where the data written in is stored
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument add is out of range
				R_ATMLIB_E_NG_ARG3				: The argument n is out of range
												  Value of the add + n is out of range
				R_ATMLIB_E_NG_ARG4				: The argument data is NULL
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append WPR command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WPR(
	R_ATMLIB_CLData	*cldata,
	uint16_t		add,
	uint8_t			n,
	const uint32_t	*data
)
{
	R_ATMLIB_RETURN_VALUE	ret;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( add > (R_ATMLIB_CNN_REG_MAXNUM - 1U) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( (n < R_ATMLIB_CNN_WPR_REGNUM_MIN) || ((add + (uint16_t)n) > R_ATMLIB_CNN_REG_MAXNUM) )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( data == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/*  add the WPR command */
			ret = atmlib_CNNAddCL_WPR( cldata, add, data, n );
			/*  check the result of atmlib_CNNAddCL_WPR() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_NOP
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				n								: Number of wait cycle [1..16]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument n is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append NOP command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_NOP(
	R_ATMLIB_CLData	*cldata,
	uint8_t			n
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (n < R_ATMLIB_CNN_NOP_NUM_MIN) || (n > R_ATMLIB_CNN_NOP_NUM_MAX) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_NOP | n;

			/* add the NOP command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*  DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_INT
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				code							: Interrupt code
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append INT command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_INT(
	R_ATMLIB_CLData	*cldata,
	uint8_t			code
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_INT | code;

			/*  add the INT command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_TRAP
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				code							: Trap code
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append TRAP command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_TRAP(
	R_ATMLIB_CLData	*cldata,
	uint8_t			code
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_TRAP | code;

			/*  add the TRAP command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_SYNCS
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
                code							: SYNC code
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: syncs_code is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append SYNCS command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SYNCS(
	R_ATMLIB_CLData	*cldata,
	uint8_t			syncs_code
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else if(syncs_code > R_ATMLIB_CNN_MAX_SYNCS_CODE)
	{
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_SYNCS | syncs_code;

			/*  add the SYNCS command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_GOSUB
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				paddr							: Physical address of jump destination [Other than 0]
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument paddr is out of range
												  The argument addr is not 4byte size alignment
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append GOSUB command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_GOSUB(
	R_ATMLIB_CLData	*cldata,
	uint32_t		paddr
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data[2];

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (paddr == 0U) || ((paddr % 4U) != 0U) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			data[0]	= R_ATMLIB_CNN_OPC_GOSUB;	/* num = 0	*/
			data[1]	= paddr;

			/* add the GOSUB command */
			ret = atmlib_CNNAddCLCommon( cldata, data, 2U );
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*  DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_RET
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append RET command to the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_RET(
	R_ATMLIB_CLData	*cldata
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				opc;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	else
	{
		/*  check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/*  check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			opc	= R_ATMLIB_CNN_OPC_RET;	/* num = 0	*/

			/*  add the TRAP command */
			ret = atmlib_CNNAddCLCommon( cldata, &opc, 1U );
			if( ret == R_ATMLIB_E_OK )
			{
				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_WUP
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				sync_id							: ID for core which IMP CNN wakeup 
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument sync_id is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append WUP command to the command list.
 [ Note ]		This function writes eight NOP commands between two WUP commands in the command list.
				However, if there are other commands between the two WUP commands,
				this function writes the deficient number of NOP commands.
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WUP(
	R_ATMLIB_CLData	*cldata,
	uint32_t		sync_id
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data[2];
	uint32_t				size;
	uint32_t				cycle_cnt;
	uint8_t					nop_n;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	nop_n 		= 0U;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( sync_id > R_ATMLIB_CNN_MAX_SYNC_ID )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check the WUP command in the CL */
			if( cldata->cnn_data->ofs_addr_wup != NULL )
			{
				/* check the position of previous WUP command  */
				cycle_cnt = (uint32_t)((uintptr_t)cldata->cur_addr - (uintptr_t)cldata->cnn_data->ofs_addr_wup) / 4U;
				if( 8U > cycle_cnt )
				{
					/* calc number of required NOP instructions */
					nop_n = (uint8_t)(8U - cycle_cnt);
				}
			}

			/*  check the required NOP instructions */
			if( nop_n != 0U )
			{
				/*  make writting data to the CL */
				data[0]	= R_ATMLIB_CNN_OPC_NOP | nop_n;
				data[1]	= R_ATMLIB_CNN_OPC_WUP | sync_id;
				size	= 2U;
			}
			else
			{
				/*  make writting data to the CL */
				data[0]	= R_ATMLIB_CNN_OPC_WUP | sync_id;
				size	= 1U;
			}

			/*  add the WUP / NOP commands */
			ret = atmlib_CNNAddCLCommon( cldata, data, size );
			/* -check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/* update the ofs_addr_wup */
				cldata->cnn_data->ofs_addr_wup = cldata->cur_addr;

				/* update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNN_SLP
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				sync_id							: ID for core which wakeup IMP CNN
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument sync_id is out of range
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
 [ Function ]	Append SLP command to the command list.
 [ Note ]		This function writes four SLP commands in the command list.
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SLP(
	R_ATMLIB_CLData	*cldata,
	uint32_t		sync_id
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				data;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( sync_id > R_ATMLIB_CNN_MAX_SYNC_ID )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* make writting data to the CL */
			data = R_ATMLIB_CNN_OPC_SLP | sync_id;
			

			/* -1010]add the SLP command */
			ret = atmlib_CNNAddCLCommon( cldata, &data, 1U );
			/* check the result of atmlib_CNNAddCLCommon() */
			if( ret == R_ATMLIB_E_OK )
			{
				/*  update state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/*  DO NOTHING */
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetLabel
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				name							: Label name
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument name is NULL
												  Length of the argument name is out of range [1 to 31 byte]
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_LABEL_SIZE		: Size of the label management area is insufficient.
				R_ATMLIB_E_NG_DUPLICATE_LABEL	: same label is registered already
 [ Function ]	Set the Label to current CL address.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetLabel(
	R_ATMLIB_CLData		*cldata,
	const char			*name
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	int32_t					l_index;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	l_index		= 0;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( name == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the label name */
		ret = atmlib_CNNCheckLabelName( name );
		/* check the result of atmlib_CNNCheckLableName() */
		if( ret != R_ATMLIB_E_OK )
		{
			/*  set R_ATMLIB_E_NG_ARG2 in return value (out of range [1 to 31 byte]) */
			ret = R_ATMLIB_E_NG_ARG2;
		}
		else
		{
			/*  check the CL info */
			ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
			/* check the result of atmlib_CheckParamCL() */
			if( ret == R_ATMLIB_E_NG )
			{
				/*  set R_ATMLIB_E_NG_ARG1 in return value */
				ret = R_ATMLIB_E_NG_ARG1;
			}
			else if( ret != R_ATMLIB_E_OK )
			{
				/* DO NOTHING */
			}
			else
			{
				/* search for the label name */
				l_index = atmlib_CNNSearchLabelName( &(cldata->cnn_data->label_info), name );
				/* check the result of atmlib_CNNSearchLabelName() */
				if( l_index >= 0 )
				{
					/*  check the label's offset address is set */
					if( cldata->cnn_data->label_info.label[l_index].addr != NULL )
					{
						/*  set R_ATMLIB_E_NG_DUPLICATE_LABEL in return value */
						ret = R_ATMLIB_E_NG_DUPLICATE_LABEL;
					}
					else
					{
						/*  set the label address */
						cldata->cnn_data->label_info.label[l_index].addr = cldata->cur_addr;

						/*  update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					}
				}
				else	/* no applicable label */
				{
					/* -check the free area of label info */
					if( cldata->cnn_data->label_info.label_count >= R_ATMLIB_CNN_MAX_LABELS )
					{
						/* set R_ATMLIB_E_NG_LABEL_SIZE in return value (insufficient space) */
						ret = R_ATMLIB_E_NG_LABEL_SIZE;
					}
					else
					{
						/* get new label index */
						l_index = (int32_t)(cldata->cnn_data->label_info.label_count);
						/* set the label name */
						(void)strncpy( cldata->cnn_data->label_info.label[l_index].name, name, R_ATMLIB_CNN_MAX_LABEL_LENGTH);

						/*  update label info */
						cldata->cnn_data->label_info.label[l_index].addr = cldata->cur_addr;
						cldata->cnn_data->label_info.label_count ++;

						/* update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					}
				}
			}
		}
	}

   	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNBRCtoLabel
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				cond							: Branch condition
				dst								: Destination label name
				src_add							: Source register address
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument cond is out of range [R_ATMLIB_CNN_BRC_EZ..R_ATMLIB_CNN_BRC_ALWAYS]
				R_ATMLIB_E_NG_ARG3				: The argument dst is NULL
												  Length of the argument dst is out of range [1 to 31 byte]
				R_ATMLIB_E_NG_ARG4				: The argument src_add is out of range [0xC0..0x140]
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_LABEL_SIZE		: Size of the label management area is insufficient.
 [ Function ]	Apend BRC command to the command list with the label name.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNBRCtoLabel(
	R_ATMLIB_CLData		*cldata,
	R_ATMLIB_CNNBRCType	cond,
	const char			*dst,
	uint16_t			src_add
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	int32_t					l_index;
	uint32_t				rslv_index;
	uint32_t				write_data;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	l_index		= 0;
	rslv_index	= 0;
	write_data	= 0;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( cond >= R_ATMLIB_CNN_BRC_END )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	/* check the 3rd argument */
	else if( dst == NULL )
	{
		/* set R_ATMLIB_E_NG_ARG3 in return value */
		ret = R_ATMLIB_E_NG_ARG3;
	}
	/*  check the 4th argument */
	else if( (src_add < R_ATMLIB_CNN_MIN_BRCADD) || (src_add >= R_ATMLIB_CNN_MAX_BRCADD) )
	{
		/* set R_ATMLIB_E_NG_ARG4 in return value */
		ret = R_ATMLIB_E_NG_ARG4;
	}
	else
	{
		/* check the label name */
		ret = atmlib_CNNCheckLabelName( dst );
		/* check the result of atmlib_CNNCheckLableName() */
		if( ret != R_ATMLIB_E_OK )
		{
			/* set R_ATMLIB_E_NG_ARG2 in return value (out of range [1 to 31 byte]) */
			ret = R_ATMLIB_E_NG_ARG3;
		}
		else
		{
			/* check the CL info */
			ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
			/*  check the result of atmlib_CheckParamCL() */
			if( ret == R_ATMLIB_E_NG )
			{
				/*  set R_ATMLIB_E_NG_ARG1 in return value */
				ret = R_ATMLIB_E_NG_ARG1;
			}
			else if( ret != R_ATMLIB_E_OK )
			{
				/*  DO NOTHING */
			}
			else
			{
				/*  search for the label name */
				l_index = atmlib_CNNSearchLabelName( &(cldata->cnn_data->label_info), dst );
				/*  check the result of atmlib_CNNSearchLabelName() */
				if( l_index < 0 )
				{
					/* -check the free area of label info  */
					if( cldata->cnn_data->label_info.label_count >= R_ATMLIB_CNN_MAX_LABELS )
					{
						/* set R_ATMLIB_E_NG_LABEL_SIZE in return value (insufficient space) */
						ret = R_ATMLIB_E_NG_LABEL_SIZE;
					}
					else
					{
						/* get new label index */
						l_index = (int32_t)(cldata->cnn_data->label_info.label_count);
						/* set the label name */
						(void)strncpy( cldata->cnn_data->label_info.label[l_index].name, dst, R_ATMLIB_CNN_MAX_LABEL_LENGTH);

						/*  update label info */
						cldata->cnn_data->label_info.label[l_index].addr = NULL;
						cldata->cnn_data->label_info.label_count ++;
					}
				}

				/* check the result of searching / setting label name */
				if( ret != R_ATMLIB_E_OK )
				{
					/* DO NOTHING (case of error : set label name) */
				}
				/* check the free area of resolve info */
				else if( cldata->cnn_data->label_info.resolve_count >= R_ATMLIB_CNN_MAX_LABEL_RESOLVES )
				{
					/*  set R_ATMLIB_E_NG_LABEL_SIZE in return value (insufficient space) */
					ret = R_ATMLIB_E_NG_LABEL_SIZE;
				}
				else
				{
					/*  make writting data to the CL (OPC & cond) */
					switch( cond )
					{
					case R_ATMLIB_CNN_BRC_EZ:
						/*   */
						write_data = R_ATMLIB_CNN_OPC_BRC | 0x00000000U;
						break;
					case R_ATMLIB_CNN_BRC_GZ:
						/*   */
						write_data = R_ATMLIB_CNN_OPC_BRC | 0x00400000U;
						break;
					case R_ATMLIB_CNN_BRC_LZ:
						/*   */
						write_data = R_ATMLIB_CNN_OPC_BRC | 0x00800000U;
						break;
					default:	/* case R_ATMLIB_CNN_BRC_ALWAYS: */
						/*   */
						write_data = R_ATMLIB_CNN_OPC_BRC | 0x00C00000U;
						break;
					}
					/*  make writting data to the CL (src_add), offset 0x2000 is added by HW automaticly  */
					write_data |= ((uint32_t)(src_add - 0x800) & 0x000001FFU);

					/* add the writting data to the CL */
					ret = atmlib_CNNAddCLCommon( cldata, &write_data, 1U );
					/* check the result of atmlib_CNNAddCLCommon() */
					if( ret == R_ATMLIB_E_OK )
					{
						/* update the resolve label info */
						rslv_index = cldata->cnn_data->label_info.resolve_count;
						cldata->cnn_data->label_info.resolve[rslv_index].addr		= cldata->cur_addr - 1U;
						cldata->cnn_data->label_info.resolve[rslv_index].label_num	= (uint32_t)l_index;
						cldata->cnn_data->label_info.resolve_count ++;

						/* update state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					}
				}
			}
		}
	}

	return ret;
}

/******************************************************************************
 [ FuncName ]	r_atmlib_CNNGOSUBtoLabel
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				dst								: Destination label name
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument dst is NULL
												  Length of the argument dst is out of range [1 to 31 byte]
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_LABEL_SIZE		: Size of the label management area is insufficient.
 [ Function ]	Apend GOSUB command to the command list with the label name.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNGOSUBtoLabel(
	R_ATMLIB_CLData	*cldata,
	const char		*dst
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	int32_t					l_index;
	uint32_t				rslv_index;
	uint32_t				write_data[2];

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	l_index		= 0;
	rslv_index	= 0;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( dst == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG21 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the label name */
		ret = atmlib_CNNCheckLabelName( dst );
		/* check the result of atmlib_CNNCheckLableName() */
		if( ret != R_ATMLIB_E_OK )
		{
			/*  set R_ATMLIB_E_NG_ARG2 in return value (out of range [1 to 31 byte]) */
			ret = R_ATMLIB_E_NG_ARG2;
		}
		else
		{
			/*  check the CL info */
			ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
			/* check the result of atmlib_CNNCheckParamCL() */
			if( ret == R_ATMLIB_E_NG )
			{
				/*  set R_ATMLIB_E_NG_ARG1 in return value */
				ret = R_ATMLIB_E_NG_ARG1;
			}
			else if( ret != R_ATMLIB_E_OK )
			{
				/* DO NOTHING */
			}
			else
			{
				/* search for the label name */
				l_index = atmlib_CNNSearchLabelName( &(cldata->cnn_data->label_info), dst );
				/* check the result of atmlib_CNNSearchLabelName() */
				if( l_index < 0 )
				{
					/*  check the free area of label info  */
					if( cldata->cnn_data->label_info.label_count >= R_ATMLIB_CNN_MAX_LABELS )
					{
						/*  set R_ATMLIB_E_NG_LABEL_SIZE in return value (insufficient space) */
						ret = R_ATMLIB_E_NG_LABEL_SIZE;
					}
					else
					{
						/*  get new label index */
						l_index = (int32_t)(cldata->cnn_data->label_info.label_count);

						/*  set the label name */
						(void)strncpy( cldata->cnn_data->label_info.label[l_index].name, dst, R_ATMLIB_CNN_MAX_LABEL_LENGTH);

						/* -update label info */
						cldata->cnn_data->label_info.label[l_index].addr = NULL;
						cldata->cnn_data->label_info.label_count ++;
					}
				}

				/* check the result of searching / setting label name */
				if( ret != R_ATMLIB_E_OK )
				{
					/* NOP (case of error : set label name) */
				}
				/* check the free area of resolve info  */
				else if( cldata->cnn_data->label_info.resolve_count >= R_ATMLIB_CNN_MAX_LABEL_RESOLVES )
				{
					/*  set R_ATMLIB_E_NG_LABEL_SIZE in return value (insufficient space) */
					ret = R_ATMLIB_E_NG_LABEL_SIZE;
				}
				else
				{
					/*  make writting data to the CL */
					write_data[0]	= R_ATMLIB_CNN_OPC_GOSUB;	/* num   = 0						*/
					write_data[1]	= 0;						/* paddr = 0 (temporarily settings)	*/

					/*  add the writting data to the CL */
					ret = atmlib_CNNAddCLCommon( cldata, write_data, 2U );
					/*  check the result of atmlib_CNNAddCLCommon() */
					if( ret == R_ATMLIB_E_OK )
					{
						/*  update the resolve label info */
						rslv_index = cldata->cnn_data->label_info.resolve_count;
						cldata->cnn_data->label_info.resolve[rslv_index].addr		= cldata->cur_addr - 2U;
						cldata->cnn_data->label_info.resolve[rslv_index].label_num	= (uint32_t)l_index;
						cldata->cnn_data->label_info.resolve_count ++;

						/*  update the state of CL info */
						atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
					}
				}
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNResolveLabels
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				paddr							: Physical Address of the command list top
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument paddr is NULL
												  The argument addr is not 4byte size alignment
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_UNDEFINE_LABEL	: number of write elements is bigger than number of remain cldata elements
				R_ATMLIB_E_NG_ADDR_RANGE		: offset addr between source and destination is out of range [0x180..0x1FF]
 [ Function ]	Resolve BRC/GOSUB label in the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNResolveLabels(
	R_ATMLIB_CLData	*cldata,
	uint32_t		paddr
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				loop_cnt;
	uint32_t				l_num;
	uint32_t				ope_code;
	uint32_t				dst_addr;
	int32_t					ofs_addr;

	/*  initialize local variables */
	ret			= R_ATMLIB_E_OK;
	loop_cnt	= 0U;
	l_num		= 0U;
	ope_code	= 0U;
	dst_addr	= 0U;
	ofs_addr	= 0;

	/*  check the 1st argument */
	if( cldata == NULL )
	{
		/*  set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/*  check the 2nd argument */
	else if( (paddr == 0U) || (((uintptr_t)paddr % 4U) != 0U) )
	{
		/*  set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_NG )
		{
			/*  set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else if( ret != R_ATMLIB_E_OK )
		{
			/*  DO NOTHING */
		}
		else
		{
			/* update label & resolve label info */
			for( loop_cnt = 0U; loop_cnt < cldata->cnn_data->label_info.resolve_count; loop_cnt++ )
			{
				/* check the undefined label  */
				l_num = cldata->cnn_data->label_info.resolve[loop_cnt].label_num;
				if( cldata->cnn_data->label_info.label[l_num].addr == NULL )
				{
					/* set R_ATMLIB_E_NG_UNDEFINED_LABEL in return value (undefined label is left) */
					ret = R_ATMLIB_E_NG_UNDEFINE_LABEL;
				}
				else
				{
					/* check the operation code */
					ope_code = *(cldata->cnn_data->label_info.resolve[loop_cnt].addr);
					if( (ope_code & R_ATMLIB_CNN_MASK_OPC) == R_ATMLIB_CNN_OPC_GOSUB )
					{
						/*  calc destination address (physical address) */
						dst_addr = paddr + (uint32_t)((uintptr_t)(cldata->cnn_data->label_info.label[l_num].addr) - (uintptr_t)(cldata->top_addr));

						/*  upadate GOSUB command in CL */
						*(cldata->cnn_data->label_info.resolve[loop_cnt].addr + 1U) = dst_addr;
					}
					else if( (ope_code & R_ATMLIB_CNN_MASK_OPC) == R_ATMLIB_CNN_OPC_BRC )
					{
						/*  calc offset address between src and dst */
						ofs_addr = (int32_t)(cldata->cnn_data->label_info.label[l_num].addr - cldata->cnn_data->label_info.resolve[loop_cnt].addr);

						/*  check the offset address */
						if( (ofs_addr < R_ATMLIB_CNN_DST_MIN) || (ofs_addr > R_ATMLIB_CNN_DST_MAX) )
						{
							/* -set R_ATMLIB_E_NG_ADDR_RANGE in return value (ofset address is invalid) */
							ret = R_ATMLIB_E_NG_ADDR_RANGE;
						}
						else
						{
							/* upadate BRC command in CL */
							*(cldata->cnn_data->label_info.resolve[loop_cnt].addr) |= (((uint32_t)ofs_addr << 9U) & 0x003FFE00U);
						}
					}
					else
					{
						/* DO NOTHING */
					}
				}

				/* check the result of resolving label */
				if( ret != R_ATMLIB_E_OK )
				{
					/* DO NOTHING */
					break;
				}
			}

			/*  check the result of resolving labels */
			if( ret == R_ATMLIB_E_OK )
			{
				/* clear resolve count */
				cldata->cnn_data->label_info.resolve_count = 0U;

				/* update the state of CL info */
				atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
			}
		}
	}

	return ret;
}


/******************************************************************************
 [ FuncName ]	r_atmlib_CNNSetPerformCounter
 [ Params ]		cldata							: The pointer to the R_ATMLIB_CLData structure
				perform_counter					: The pointer to R_ATMLIB_CNNPerformCounter structure
 [ Returns ]	R_ATMLIB_E_OK					: on success
				R_ATMLIB_E_NG_ARG1				: The argument cldata is NULL
												  Value of the members of the argument cldata is out of range
				R_ATMLIB_E_NG_ARG2				: The argument perform_counter is NULL
				R_ATMLIB_E_NG_CLTYPE			: cltype of cldata is error
				R_ATMLIB_E_NG_CLSIZE			: lack of CL area
				R_ATMLIB_E_NG_STATE				: state of cldata is error
				R_ATMLIB_E_NG_COUNTER_NUM	    : member counter_num is out of range
				R_ATMLIB_E_NG_COUNTER_ID		: member counter_id is out of range
 				R_ATMLIB_E_NG_COUNTER_CTRL   	: member counter_en is out of range
				R_ATMLIB_E_NG_COUNTER_STRSN		: member stall_reason is out of range
 				R_ATMLIB_E_NG_COUNTER_FUNC	    : member func is out of range
				R_ATMLIB_E_NG_COUNTER_UNITNO	: member unit_no is out of range
 				R_ATMLIB_E_NG_COM_UNITNO_FUNC	: member unit_no and func are out of combination range
				R_ATMLIB_E_NG_COM_FUNC_STRSN	: member func and stall_reason  are out of combination range
 [ Function ]	Resolve BRC/GOSUB label in the command list.
 [ Note ]		none
******************************************************************************/
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetPerformCounter(
	R_ATMLIB_CLData 					*cldata,  
    const R_ATMLIB_CNNPerformCounter 	*perform_counter
)
{
	R_ATMLIB_RETURN_VALUE	ret;
	uint32_t				cl_size;

	/*  initialize local variables */
	ret 		= R_ATMLIB_E_OK;
	cl_size		= 0U;

	/* check the 1st argument */
	if( cldata == NULL )
	{
		/*set R_ATMLIB_E_NG_ARG1 in return value */
		ret = R_ATMLIB_E_NG_ARG1;
	}
	/* check the 2nd argument */
	else if( perform_counter == NULL )
	{
		/*set R_ATMLIB_E_NG_ARG2 in return value */
		ret = R_ATMLIB_E_NG_ARG2;
	}
	else
	{
		/* check the CL info */
		ret = atmlib_CNNCheckParamCL( cldata, R_ATMLIB_CNNEVT_GROUP_B );
		/* check the result of atmlib_CNNCheckParamCL() */
		if( ret == R_ATMLIB_E_OK )
		{
			/* check the counter parameter info */
			ret = atmlib_CNNCheckParamCounter( perform_counter );
			/* check the result of atmlib_CNNCheckParamCounter() */
			if( ret == R_ATMLIB_E_OK )
			{
			    /* calcurate the size of writting data */
				cl_size = (uint32_t)(4 * perform_counter->counter_cnt);
				/* check the remain size of the CL */
				ret = atmlib_CNNCheckRemainCLSize(cldata, cl_size);
				/* check the result of atmlib_CNNCheckRemainCLSize() */
				if( ret == R_ATMLIB_E_OK)
				{
					/* set performanc counter configuration */
					atmlib_CNNSetPerformCounter( cldata, perform_counter );
					/* update the state of CL info */
					atmlib_CNNUpdateStatus( cldata, R_ATMLIB_CLSTATE_READY );
				} /* do nothing for else branch */
			} /* do nothing for else branch */		

		}
		else if( ret == R_ATMLIB_E_NG )
		{
			/* set R_ATMLIB_E_NG_ARG1 in return value */
			ret = R_ATMLIB_E_NG_ARG1;
		}
		else
		{
			/* DO NOTHING */
		}

	}
	
	return ret;
}

