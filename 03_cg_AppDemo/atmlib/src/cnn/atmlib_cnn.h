/******************************************************************************
    Atomic Library
     Copyright (C)  2019-2020 Renesas Electronics Corporation.All rights reserved.

    [File] atmlib_cnn.h

******************************************************************************/

#ifndef R_ATMLIB_CNN_H_
#define R_ATMLIB_CNN_H_

/************************************************
    Switching definitions
************************************************/
#ifndef R_ATMLIB_BUILD_UNIT_TEST
#define R_ATMLIB_CNN_STATIC static
#else
#define R_ATMLIB_CNN_STATIC
#endif

/************************************************
	Constant definitions
************************************************/
/* CNN Register Info (byte offset) */
#define R_ATMLIB_CNN_VCR0					    (0x0000U)
#define R_ATMLIB_CNN_SWRST					    (0x0008U)
#define R_ATMLIB_CNN_MPRST                      (0x000CU)
#define R_ATMLIB_CNN_SR						    (0x0010U)
#define R_ATMLIB_CNN_SRE					    (0x0014U)
#define R_ATMLIB_CNN_SRC					    (0x0018U)
#define R_ATMLIB_CNN_SRM                        (0x001CU)
#define R_ATMLIB_CNN_CLPC                       (0x0100U)
#define R_ATMLIB_CNN_SACL                       (0x0104U)
#define R_ATMLIB_CNN_SCLP                       (0x0108U)
#define R_ATMLIB_CNN_CLEIR                      (0x010CU)
#define R_ATMLIB_CNN_SLSP                       (0x0110U)
#define R_ATMLIB_CNN_MAX_CNT                    (0x0120U)
#define R_ATMLIB_CNN_MIN_CNT                    (0x01A0U)
#define R_ATMLIB_CNN_MAXSUM_CNT                 (0x0220U)
#define R_ATMLIB_CNN_MINSUM_CNT                 (0x0224U)
#define R_ATMLIB_CNN_MAX_VAL_THRES				(0x0228U)
#define R_ATMLIB_CNN_MIN_VAL_THRES				(0x022CU)
#define R_ATMLIB_CNN_MAXSUM_CNT_THRES			(0x0230U)
#define R_ATMLIB_CNN_MINSUM_CNT_THRES			(0x0234U)
#define R_ATMLIB_CNN_GLOB_SUM_CH_BASE  			(0x0840U)
#define R_ATMLIB_CNN_GLOB_SUM_CH_OFST           (0x0004U) /* Offset between GLOB_SUM_(m) and GLOB_SUM_(m+1) 	*/
#define R_ATMLIB_CNN_SYNCC00_03					(0x0900U)
#define R_ATMLIB_CNN_SYNCC04_07					(0x0904U)
#define R_ATMLIB_CNN_SYNCC08_11					(0x0908U)
#define R_ATMLIB_CNN_SYNCC12_15					(0x090CU)
#define R_ATMLIB_CNN_PERFCNTCTRL_BASE			(0x0980U)
#define R_ATMLIB_CNN_PERFCNTCTRL_OFST           (0x0004U) /* Offset between PERFCNT(n)CTRL and PERFCNT(n+1)CTRL */
#define R_ATMLIB_CNN_PPERFCNT_BASE			    (0x0990U)
#define R_ATMLIB_CNN_PPERFCNT_OFST              (0x0004U) /* Offset between PERFCNTn and PERFCNT(n+1) 			*/
#define R_ATMLIB_CNN_MAX_MIN_BASE               (0x0A00U)
#define R_ATMLIB_CNN_ARICSR                     (0x1000U)
#define R_ATMLIB_CNN_ARIE                       (0x1004U)
#define R_ATMLIB_CNN_PADD_STRIDE_STARTPOS       (0x1008U)
#define R_ATMLIB_CNN_BIAS                       (0x100CU)
#define R_ATMLIB_CNN_ARI_LEN                    (0x1010U)
#define R_ATMLIB_CNN_MULT_POST_SFT              (0x1018U)
#define R_ATMLIB_CNN_MULT_VAL                   (0x101CU)
#define R_ATMLIB_CNN_PADD00_01                  (0x1100U)
#define R_ATMLIB_CNN_PADD02_03                  (0x1104U)
#define R_ATMLIB_CNN_PADD04_05                  (0x1108U)
#define R_ATMLIB_CNN_PADD06_07                  (0x110CU)
#define R_ATMLIB_CNN_PADD08_09                  (0x1110U)
#define R_ATMLIB_CNN_PADD10_11                  (0x1114U)
#define R_ATMLIB_CNN_PADD12_13                  (0x1118U)
#define R_ATMLIB_CNN_PADD14_15                  (0x111CU)
#define R_ATMLIB_CNN_STRIDE00_03                (0x1120U)
#define R_ATMLIB_CNN_STRIDE04_07                (0x1124U)
#define R_ATMLIB_CNN_STRIDE08_11                (0x1128U)
#define R_ATMLIB_CNN_STRIDE12_15                (0x112CU)
#define R_ATMLIB_CNN_START_POS_00_07            (0x1130U)
#define R_ATMLIB_CNN_START_POS_08_15            (0x1134U)
#define R_ATMLIB_CNN_BIAS00_01                  (0x1138U)
#define R_ATMLIB_CNN_BIAS02_03                  (0x113CU)
#define R_ATMLIB_CNN_BIAS04_05                  (0x1140U)
#define R_ATMLIB_CNN_BIAS06_07                  (0x1144U)
#define R_ATMLIB_CNN_BIAS08_09                  (0x1148U)
#define R_ATMLIB_CNN_BIAS10_11                  (0x114CU)
#define R_ATMLIB_CNN_BIAS12_13                  (0x1150U)
#define R_ATMLIB_CNN_BIAS14_15                  (0x1154U)
#define R_ATMLIB_CNN_BIAS16_17                  (0x1158U)
#define R_ATMLIB_CNN_BIAS18_19                  (0x115CU)
#define R_ATMLIB_CNN_BIAS20_21                  (0x1160U)
#define R_ATMLIB_CNN_BIAS22_23                  (0x1164U)
#define R_ATMLIB_CNN_BIAS24_25                  (0x1168U)
#define R_ATMLIB_CNN_BIAS26_27                  (0x116CU)
#define R_ATMLIB_CNN_BIAS28_29                  (0x1170U)
#define R_ATMLIB_CNN_BIAS30_31                  (0x1174U)
#define R_ATMLIB_CNN_SFTI                       (0x2000U)
#define R_ATMLIB_CNN_SFTC                       (0x2004U)
#define R_ATMLIB_CNN_SFTB                       (0x2008U)
#define R_ATMLIB_CNN_SFTM                       (0x200CU)
#define R_ATMLIB_CNN_SFTI_CH_BASE               (0x2010U)
#define R_ATMLIB_CNN_SFTI_CH_OFST               (0x0004U) /* Offset between SFTI(k) and SFTI(k+1) 		*/
#define R_ATMLIB_CNN_SFTC_CH_BASE               (0x2050U)
#define R_ATMLIB_CNN_SFTC_CH_OFST               (0x0004U) /* Offset between SFTC(k) and SFTC(k+1) 		*/
#define R_ATMLIB_CNN_R0							(0x20D0U)
#define R_ATMLIB_CNN_R1							(0x20D4U)
#define R_ATMLIB_CNN_R2							(0x20D8U)
#define R_ATMLIB_CNN_ARI_BND                    (0x20DCU)
#define R_ATMLIB_CNN_LUT_POST_SFT               (0x20E0U)
#define R_ATMLIB_CNN_OCFP_TABLE                 (0x2100U)
#define R_ATMLIB_CNN_UV_TABLE                   (0x2300U)
#define R_ATMLIB_CNN_DMAISA                     (0x4000U)
#define R_ATMLIB_CNN_DMAICO                     (0x4004U)
#define R_ATMLIB_CNN_DMAIL                      (0x4008U)
#define R_ATMLIB_CNN_DMAIST                     (0x400CU)
#define R_ATMLIB_CNN_DMAIMAG                    (0x4010U)
#define R_ATMLIB_CNN_DMAIFM                     (0x4014U)
#define R_ATMLIB_CNN_DMAIOA                     (0x4018U)
#define R_ATMLIB_CNN_DMAIE                      (0x401CU)
#define R_ATMLIB_CNN_DMAIS                      (0x4020U)
#define R_ATMLIB_CNN_DMAISA_CH_BASE             (0x4100U) 
#define R_ATMLIB_CNN_DMAISA_CH_OFST             (0x0004U) /* Offset between DMAISA(k) and DMAISA(k+1) 		*/
#define R_ATMLIB_CNN_DMAIL_CH_BASE              (0X4140U)
#define R_ATMLIB_CNN_DMAIL_CH_OFST              (0x0004U) /* Offset between DMAIOL(k) and DMAIL(k+1)	 	*/
#define R_ATMLIB_CNN_DMAIST_CH_BASE             (0x4180U)
#define R_ATMLIB_CNN_DMAIST_CH_OFST             (0x0004U) /* Offset between DMAIST(k) and DMAIST(k+1) 		*/
#define R_ATMLIB_CNN_DMAIMAG00_03               (0x41C0U)
#define R_ATMLIB_CNN_DMAIMAG04_07               (0x41C4U)
#define R_ATMLIB_CNN_DMAIMAG08_11               (0x41C8U)
#define R_ATMLIB_CNN_DMAIMAG12_15               (0x41CCU)
#define R_ATMLIB_CNN_DMAIFM00_07                (0x41D0U)
#define R_ATMLIB_CNN_DMAIFM08_15                (0x41D4U)
#define R_ATMLIB_CNN_DMAIOA_CH_BASE             (0x4200U)
#define R_ATMLIB_CNN_DMAIOA_CH_OFST             (0x0004U) /* Offset between DMAIO(k) and DMAIOA_(k+1) 		*/
#define R_ATMLIB_CNN_DMA3DCSA                   (0x5000U)
#define R_ATMLIB_CNN_DMA3DCCO                   (0x5004U)
#define R_ATMLIB_CNN_DMA3DCL                    (0x5008U)
#define R_ATMLIB_CNN_DMA3DCST                   (0x500CU)
#define R_ATMLIB_CNN_DMA3DCMAG                  (0x5010U)
#define R_ATMLIB_CNN_DMA3DCFM                   (0x5014U)
#define R_ATMLIB_CNN_DMA3DCOA                   (0x5018U)
#define R_ATMLIB_CNN_DMA3DCE                    (0x501CU)
#define R_ATMLIB_CNN_DMA3DCS                    (0x5020U)
#define R_ATMLIB_CNN_DMA3DCSA_CH_BASE           (0x5100U)
#define R_ATMLIB_CNN_DMA3DCSA_CH_OFST           (0x0004U) /* Offset between DMA3DCSA(k) and DMA3DCSA(k+1) 	*/
#define R_ATMLIB_CNN_DMA3DCL_CH_BASE            (0x5180U)
#define R_ATMLIB_CNN_DMA3DCL_CH_OFST            (0x0004U) /* Offset between DMA3DCL(k) and DMA3DCL(k+1) 	*/
#define R_ATMLIB_CNN_DMA3DCST_CH_BASE           (0x5200U)
#define R_ATMLIB_CNN_DMA3DCST_CH_OFST           (0x0004U) /* Offset between DMA3DCST(k) and DMA3DCST(k+1) 	*/
#define R_ATMLIB_CNN_DMA3DCMAG00_03             (0x5280U)
#define R_ATMLIB_CNN_DMA3DCMAG04_04             (0x5284U)
#define R_ATMLIB_CNN_DMA3DCMAG08_11             (0x5288U)
#define R_ATMLIB_CNN_DMA3DCMAG12_15             (0x528CU)
#define R_ATMLIB_CNN_DMA3DCMAG16_19             (0x5290U)
#define R_ATMLIB_CNN_DMA3DCMAG20_23             (0x5294U)
#define R_ATMLIB_CNN_DMA3DCMAG24_27             (0x5298U)
#define R_ATMLIB_CNN_DMA3DCMAG28_31             (0x529CU)
#define R_ATMLIB_CNN_DMA3DCFM00_07			    (0x52A0U)
#define R_ATMLIB_CNN_DMA3DCFM08_15			    (0x52A4U)
#define R_ATMLIB_CNN_DMA3DCFM16_23			    (0x52A8U)
#define R_ATMLIB_CNN_DMA3DCFM24_31			    (0x52ACU)
#define R_ATMLIB_CNN_DMA3DCOA_CH_BASE           (0x5300U)
#define R_ATMLIB_CNN_DMA3DCOA_CH_OFST           (0x0004U) /* Offset between DMA3DCOA(k) and DMA3DCOA(k+1) 	*/
#define R_ATMLIB_CNN_DMAOSA                     (0x6000U)
#define R_ATMLIB_CNN_DMAOCO                     (0x6004U)
#define R_ATMLIB_CNN_DMAOL                      (0x6008U)
#define R_ATMLIB_CNN_DMAOST                     (0x600CU)
#define R_ATMLIB_CNN_DMAOFM                     (0x6010U)
#define R_ATMLIB_CNN_DMAOOA                     (0x6014U)
#define R_ATMLIB_CNN_DMAOE                      (0x6018U)
#define R_ATMLIB_CNN_DMAOS                      (0x601CU)
#define R_ATMLIB_CNN_DMAOSA_CH_BASE             (0x6100U)
#define R_ATMLIB_CNN_DMAOSA_CH_OFST             (0x0004U) /* Offset between DMAISA(k) and DMAISA(k+1) 		*/
#define R_ATMLIB_CNN_DMAOOA_CH_BASE             (0x6180U)
#define R_ATMLIB_CNN_DMAOOA_CH_OFST             (0x0004U) /* Offset between DMAIOA(k) and DMAIOA(k+1) 		*/
#define R_ATMLIB_CNN_DMARFESA                   (0x7000U)
#define R_ATMLIB_CNN_DMARFISA                   (0x7004U)
#define R_ATMLIB_CNN_DMARFL                     (0x7008U)
#define R_ATMLIB_CNN_DMARFCTRL                  (0x700CU)
#define R_ATMLIB_CNN_LUT_BASE                  	(0x7100U)
#define R_ATMLIB_CNN_LUT_OFST                  	(0x0100U) /* Offset between LUT_(I) and LUT_(I+1) 			*/
#define R_ATMLIB_CNN_WB0_BASE				    (0x8000U)
#define R_ATMLIB_CNN_WB_DMAI_CH_OFST      		(0x0400U) /* Offset between WB0I(k)O(m) and WB0I(k+1)O_m 	*/
#define R_ATMLIB_CNN_WB_DMAO_CH_OFST	        (0x0020U) /* Offset between WB0I(k)O(m) and WB0IkO(m+1) 	*/

/* CNN Operation Code*/
#define R_ATMLIB_CNN_OPC_ADD					(0x40000000U)
#define R_ATMLIB_CNN_OPC_SUB					(0x41000000U)
#define R_ATMLIB_CNN_OPC_WR						(0x42000000U)
#define R_ATMLIB_CNN_OPC_WRI					(0x43000000U)
#define R_ATMLIB_CNN_OPC_BRC					(0x44000000U)
#define R_ATMLIB_CNN_OPC_CLUVT					(0x48000000U)
#define R_ATMLIB_CNN_OPC_SCRES					(0x49000000U)
#define R_ATMLIB_CNN_OPC_SCCHK					(0x4A000000U)
#define R_ATMLIB_CNN_OPC_TUV2UVT				(0x4B000000U)
#define R_ATMLIB_CNN_OPC_CLW					(0x4C000000U)
#define R_ATMLIB_CNN_OPC_WPR					(0x55000000U)
#define R_ATMLIB_CNN_OPC_NOP					(0x80000000U)
#define R_ATMLIB_CNN_OPC_INT					(0x81000000U)
#define R_ATMLIB_CNN_OPC_TRAP					(0x88000000U)
#define R_ATMLIB_CNN_OPC_SYNCS					(0x8B000000U)
#define R_ATMLIB_CNN_OPC_GOSUB					(0x91000000U)
#define R_ATMLIB_CNN_OPC_RET					(0x99000000U)
#define R_ATMLIB_CNN_OPC_WUP					(0xA0000000U)
#define R_ATMLIB_CNN_OPC_SLP					(0xA1000000U)

/* CNN Operation Code Mask */
#define R_ATMLIB_CNN_MASK_OPC					(0xFF000000U)

/* SYNCS module */
#define R_ATMLIB_CNN_SYNCS_DMARF				(0x0000001EU)
#define R_ATMLIB_CNN_SYNCS_DMAI					(0x0000001DU)
#define R_ATMLIB_CNN_SYNCS_DMAC					(0x0000001BU)
#define R_ATMLIB_CNN_SYNCS_ARI					(0x00000017U)
#define R_ATMLIB_CNN_SYNCS_DMAO					(0x0000000FU)

/* Flag pattern of current register (registers cache) */
#define R_ATMLIB_CNN_REGCACHE_SET				((uint32_t)0x00000001U)		/* The register is set					*/
#define R_ATMLIB_CNN_REGCACHE_ENABLE			((uint32_t)0x00000002U)		/* The register can use cache			*/

/*The minimum image stride size*/
#define R_ATMLIB_CNN_MIN_IMG_STRIDE             (1U)

/*The maximum channel offset*/
#define R_ATMLIB_CNN_MAX_CH_OFST				(1024U)

/*The maximum padding value on the left and right side of the data*/
#define R_ATMLIB_CNN_MAX_PADLR                  (16U)
/*The maximum padding value on the top and bottom side of the data*/
#define R_ATMLIB_CNN_MAX_PADTB                  (7U)

/*The maximum and minimum convolution stride value*/
#define R_ATMLIB_CNN_MAX_CONV_STRIDE            (5U)
#define R_ATMLIB_CNN_MIN_CONV_STRIDE            (1U)

/*The maximum and minimum value for repeat/skip x and y*/
#define R_ATMLIB_CNN_MAX_RPT_SKP                (4U)
#define R_ATMLIB_CNN_MIN_RPT_SKP                (0U)

/*The maximum and minimum value for image width*/
#define R_ATMLIB_CNN_MAX_WIDTH                  (128U)
#define R_ATMLIB_CNN_MIN_WIDTH                  (1U)

/*The maximum and minimum value for image height*/
#define R_ATMLIB_CNN_MAX_HEIGHT                 (4095U)
#define R_ATMLIB_CNN_MIN_HEIGHT                 (1U)

/*The maximum and minimum value for  dilated convolution distance*/
#define R_ATMLIB_CNN_MAX_DILATED_DISTANCE       (3U)
#define R_ATMLIB_CNN_MIN_DILATED_DISTANCE       (0U)

/*The maximum and minimum start position in x direction*/
#define R_ATMLIB_CNN_MAX_START_POS              (7U)
#define R_ATMLIB_CNN_MIN_START_POS              (0U)

/*The maximum and minimum pooling length in x and y direction*/
#define R_ATMLIB_CNN_MAX_XYPOOL                 (4U)
#define R_ATMLIB_CNN_MIN_XYPOOL                 (1U)

/*The maximum and minimum DMARF length*/
#define R_ATMLIB_CNN_MAX_WEIGHT_DMARFL			(256U)
#define R_ATMLIB_CNN_MIN_WEIGHT_DMARFL			(1U)

/*The maximum and minimum LUT length*/
#define R_ATMLIB_CNN_MAX_LUT_DMARFL				(16U)
#define R_ATMLIB_CNN_MIN_LUT_DMARFL				(1U)

/*The maximum DMARF position in SPMC mode*/
#define R_ATMLIB_CNN_MAX_DMARF_SPMC_POS			(7U)

/*The maximum DMARF internal address for LUT and weight in SCMP mode*/
#define R_ATMLIB_CNN_MAX_LUT_ISA		 		(0x74C0U)
#define R_ATMLIB_CNN_MAX_WEIGHT_ISA				(0xBFC0U)

/*The maximum SYNCS code value*/
#define R_ATMLIB_CNN_MAX_SYNCS_CODE             (0x1FU)

/*The maximum and minimum value for source address*/
#define R_ATMLIB_CNN_MAX_BRCADD                 (0x940U)
#define R_ATMLIB_CNN_MIN_BRCADD                 (0x8C0U)

/*The maximum value for lower boundary in activation function*/
#define R_ATMLIB_CNN_MAX_LOWERBOUND             (0)

/*The maximum value for multiplier value*/
#define R_ATMLIB_CNN_MULT_VAL_INVALID           (0U)

/*The maximum value for SYNC ID, used in SLP and WUP*/
#define R_ATMLIB_CNN_MAX_SYNC_ID                0x0000FFFFU

/* Limit of destination address and destination length  for ADD & SUB OPCODE*/
#define R_ATMLIB_CNN_ADDR_MAX					(0x9FFU)
#define R_ATMLIB_CNN_ADDR_MIN					(0x800U)
#define R_ATMLIB_CNN_DSTLEN_MAX					(0x3FU)
#define R_ATMLIB_CNN_DSTLEN_MIN					(1U)


/*The maximum and minimum value for immediate data in WRI instruction*/
#define R_ATMLIB_CNN_IMM_MAX					(255)
#define R_ATMLIB_CNN_IMM_MIN					(-256)

/*The maximum and minimum value for branch destination in BRC instruction*/
#define R_ATMLIB_CNN_DST_MAX					(4095)
#define R_ATMLIB_CNN_DST_MIN					(-4096)

/*The maximum and minimum value for number of wait cycles in NOP instruction*/
#define R_ATMLIB_CNN_NOP_NUM_MAX				(16U)
#define R_ATMLIB_CNN_NOP_NUM_MIN				(1U)


/*The minimum number of writes register in WPR instruction*/
#define R_ATMLIB_CNN_WPR_REGNUM_MIN				(1U)

/*The minimum number of index in TUV2UVT instruction*/
#define R_ATMLIB_CNN_UVT_INDEX_MIN				(1U)

/*The length of the weight register*/
#define R_ATMLIB_CNN_WB_REG_LEN		         	(0x1000U)

/*The length of the LUT register*/
#define R_ATMLIB_CNN_LUT_REG_LEN		        (0x100U)

/*The length of the weight register*/
#define R_ATMLIB_CNN_OCFP_RANGE		            (0x3FU)


/************************************************
	Enum definitions
************************************************/
/* Event ID of CNN(Category of CNN API) */
typedef enum
{
	R_ATMLIB_CNNEVT_GROUP_A						= (0),						/* All status		-> Ready			*/
	R_ATMLIB_CNNEVT_GROUP_B,												/* Ready			-> Ready			*/
	R_ATMLIB_CNNEVT_GROUP_E,												/* Ready			-> Finalized		*/
	R_ATMLIB_CNNEVT_GROUP_COUNT												/* Number of Event group				*/
} R_ATMLIB_CNNEVT_GROUP;


/************************************************
	Structure definitions
************************************************/
/* initialize current_reg structure */
typedef struct {
	uint32_t			offset_addr;										/* Offset address of registers					*/
	uint32_t			num;												/* Number of registers(4 bytes unit)			*/
	uint32_t			initial_val;										/* Initial value								*/
} R_ATMLIB_CNNInitCurReg;

/* Disable current_reg structure */
typedef struct {
	uint32_t			offset_addr;										/* Offset address of registers					*/
	uint32_t			num;												/* Number of registers(4 bytes unit)			*/
} R_ATMLIB_CNNDisableCurReg;


/************************************************
	Function prototype definitions
************************************************/
void atmlib_CNNInitializeCL( R_ATMLIB_CLData *cldata, uint32_t *addr, uint32_t size, R_ATMLIB_CNNData *cnn_data );
void atmlib_CNNInitializeCnnData( R_ATMLIB_CNNData *cnn_data );
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckRemainCLSize( const R_ATMLIB_CLData *cldata, uint32_t size );
void atmlib_CNNUpdateStatus( R_ATMLIB_CLData *cldata, R_ATMLIB_CLState clstate );
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamCL( const R_ATMLIB_CLData *cldata, R_ATMLIB_CNNEVT_GROUP event_group );
void atmlib_CNNUnsetCurReg(R_ATMLIB_CNNCurrentReg *current_reg, uint16_t dst_start_pos, uint16_t dst_length );
R_ATMLIB_RETURN_VALUE atmlib_CNNAddCLCommon( R_ATMLIB_CLData *cldata, const uint32_t *data, uint32_t data_num );
R_ATMLIB_RETURN_VALUE atmlib_CNNAddCL_WPR( R_ATMLIB_CLData *cldata, uint16_t add, const uint32_t *data, uint8_t data_num );
R_ATMLIB_RETURN_VALUE atmlib_CNNAddCL_WRI( R_ATMLIB_CLData *cldata, uint16_t dst_start_add, int16_t imm, uint8_t dst_length );
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamDMAI(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAIParam *dmai_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamDMAC(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMACParam *dmac_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamDMAO(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAOParam *dmao_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamWeightDMARF(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMARFParam *dmarf_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamLUTDMARF(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMARFParam *dmarf_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamWeightWPR(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNWeightInfo *weight_info);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamLUTWPR(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNLUTInfo	*lut_info);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamCounter(const R_ATMLIB_CNNPerformCounter *perform_counter);
void atmlib_CNNJudgeSetupModeDMAI(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAIChParam *dmai_ch, uint8_t param_count);
void atmlib_CNNJudgeSetupModeDMAC(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMACChParam *dmac_ch, uint8_t param_count);
void atmlib_CNNJudgeSetupModeDMAO(R_ATMLIB_CLData *cldata, const uint32_t *ptr, uint8_t param_count);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckCombParamDMAI(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAIParam *dmai_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckCombParamDMAC(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMACParam *dmac_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckCombParamDMAO(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAOParam *dmao_param);
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckParamARI(const R_ATMLIB_CNNARIParam *ari_param, const R_ATMLIB_CNNChannelStatistic *channel_info);
void atmlib_CNNSetupFastDMAI(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAIParam *dmai_param, uint32_t *clofs_dmai);
void atmlib_CNNSetupSlowDMAI(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAIParam *dmai_param, uint32_t *clofs_dmai);
void atmlib_CNNSetupFastDMAC(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMACParam *dmac_param, uint32_t *clofs_dmac);
void atmlib_CNNSetupSlowDMAC(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMACParam *dmac_param, uint32_t *clofs_dmac);
void atmlib_CNNSetupFastDMAO(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAOParam *dmao_param, uint32_t *clofs_dmao);
void atmlib_CNNSetupSlowDMAO(R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAOParam *dmao_param, uint32_t *clofs_dmao);
void atmlib_CNNSetupARI( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNARIParam *ari_param, const R_ATMLIB_CNNChannelStatistic *channel_info );
void atmlib_CNNSetupDMARF( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMARFParam *dmarf_param);
void atmlib_CNNSetupWeightsWPR( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNWeightInfo *weight_info);
void atmlib_CNNSetupLUTWPR( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNLUTInfo *lut_info);
void atmlib_CNNSetPerformCounter( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNPerformCounter *perform_counter);
int32_t atmlib_CNNSearchLabelName( const R_ATMLIB_CNNLabelInfo *label_info, const char *label_name );
R_ATMLIB_RETURN_VALUE atmlib_CNNCheckLabelName( const char *label_name );

#endif /* R_ATMLIB_CNN_H_ */
