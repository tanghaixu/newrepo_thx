/******************************************************************************
    Atomic Library
     Copyright (C)  2019-2020 Renesas Electronics Corporation.All rights reserved.

    [File] r_atmlib_def.h

******************************************************************************/
#ifndef R_ATMLIB_DEF_H_
#define R_ATMLIB_DEF_H_

/************************************************
 Constant definitions
************************************************/
/* CL max size */
#define R_ATMLIB_MAX_CL_LENGTH			(26214400U)

/* CNN Max Channel */
#define R_ATMLIB_CNN_DMAI_MAX_CH        (16U)
#define R_ATMLIB_CNN_DMAO_MAX_CH		(32U)
#define R_ATMLIB_CNN_DMAC_MAX_CH		(32U)
#define R_ATMLIB_CNN_ARI_MAX_CH         (32U)

#define R_ATMLIB_CNN_DMA_ALL_CH			(R_ATMLIB_CNN_DMAI_MAX_CH + R_ATMLIB_CNN_DMAO_MAX_CH + R_ATMLIB_CNN_DMAC_MAX_CH)

/* Maximum channel of the weight */
#define R_ATMLIB_CNN_WEIGHT_MAX_CH      (512U)

/* performance counter number */
#define R_ATMLIB_CNN_PERFORMCNT_NUM     (4U)

/* Filter restriction */
#define R_ATMLIB_CNN_FILTER_SIZE        (27U)

/* LUT table size */
#define R_ATMLIB_CNN_LUTTABLE_SIZE      (256U)

/* Number of CNN registers */
#define R_ATMLIB_CNN_REG_MAXNUM			(0x3000U)

/* Maximum number of handles to the floating point setup of the channel */
#define R_ATMLIB_MAX_DYN_FP_HANDLE		(128U)

/* Invalid value of fp_handle (the channel is not in use) */
#define R_ATMLIB_CNN_CHANNEL_UNUSED		(0xFFFFFFFFU)

/* Memory attributes */
#define R_ATMLIB_CNN_MEM_INVALID        (0xFFFFFFFFU)

/* Label restrictions for BRC, GOSUB, LABEL */
#define R_ATMLIB_CNN_MAX_LABEL_LENGTH   (31U)
#define R_ATMLIB_CNN_MAX_LABELS         (1024U)
#define R_ATMLIB_CNN_MAX_LABEL_RESOLVES (4096U)

#endif /* R_ATMLIB_DEF_H_ */

