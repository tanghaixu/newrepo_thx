/******************************************************************************
    Atomic Library
     Copyright (C)  2019-2020 Renesas Electronics Corporation.All rights reserved.
    
	[File] r_atmlib_types.h

******************************************************************************/
#ifndef R_ATMLIB_TYPES_H_
#define R_ATMLIB_TYPES_H_

#include <stdint.h>
#include <stddef.h>

#include "r_atmlib_def.h"

/************************************************
	Enum definitions
************************************************/
/* Command List status */
typedef enum
{
	R_ATMLIB_CLSTATE_UNINIT			= 0,
	R_ATMLIB_CLSTATE_READY,
	R_ATMLIB_CLSTATE_CONFIG,
	R_ATMLIB_CLSTATE_FINAL,
	R_ATMLIB_CLSTATE_COUNT
} R_ATMLIB_CLState;

/* Return Value */
typedef enum
{
    R_ATMLIB_E_OK					= 0,							/* On success														*/		
    R_ATMLIB_E_NG					= -1,							/* Error occur														*/
    R_ATMLIB_E_NG_CLTYPE			= -2,							/* Cltype of cldata is error										*/
    R_ATMLIB_E_NG_CLSIZE			= -3,							/* Lack of CL area													*/
    R_ATMLIB_E_NG_STATE				= -4,							/* state of cldata is error											*/
	R_ATMLIB_E_NG_COUNT				= -5,							/* Count is out of range											*/
    R_ATMLIB_E_NG_ARG1				= -101,							/* 1st argument is error											*/
    R_ATMLIB_E_NG_ARG2				= -102,							/* 2nd argument is error											*/
    R_ATMLIB_E_NG_ARG3				= -103,							/* 3th argument is error											*/
    R_ATMLIB_E_NG_ARG4				= -104,							/* 4th argument is error											*/
    R_ATMLIB_E_NG_ARG5				= -105,							/* 5th argument is error											*/
    R_ATMLIB_E_NG_ARG6				= -106,							/* 6th argument is error											*/
    R_ATMLIB_E_NG_ARG7				= -107,							/* 7th argument is error											*/
    R_ATMLIB_E_NG_ARG8				= -108,							/* 8th argument is error											*/
	R_ATMLIB_E_NG_RF_EXSA           = -401,							/* DMARF external start address is out of range						*/
    R_ATMLIB_E_NG_RF_INSA           = -402,							/* DMARF internal start address is out of range						*/
    R_ATMLIB_E_NG_RF_LEN            = -403,							/* DMARF length is out of range										*/
    R_ATMLIB_E_NG_RF_STAPOS         = -404,							/* DMARF start position is out of range								*/
    R_ATMLIB_E_NG_RF_ENDPOS         = -405,							/* DMARF end position is out of range								*/
    R_ATMLIB_E_NG_DMARFFM           = -406,							/* DMARF format is out of range										*/
    R_ATMLIB_E_NG_COUNTER_NUM		= -501,							/* Performance counter number is out of range						*/
    R_ATMLIB_E_NG_COUNTER_ID		= -502,							/* Performance counter ID is out of range							*/
    R_ATMLIB_E_NG_COUNTER_CTRL		= -503,							/* Performance counter enable control is out of range				*/
    R_ATMLIB_E_NG_COUNTER_STRSN		= -504,							/* Performance counter stall reason is out of range					*/
    R_ATMLIB_E_NG_COUNTER_FUNC		= -505,							/* Performance counter function is out of range						*/
    R_ATMLIB_E_NG_COUNTER_UNITNO	= -506,							/* Performance counter unit number is out of range					*/
    R_ATMLIB_E_NG_CH_ID			    = -601,							/* DMA* channel ID is out of range									*/
    R_ATMLIB_E_NG_CH_CO      	    = -602,							/* DMA* channel offset is out of range								*/
    R_ATMLIB_E_NG_CH_PTR			= -603,							/* DMA* channel start address is out of range						*/
    R_ATMLIB_E_NG_CH_IMG_X		    = -604,							/* DMA* channel image width is out of range							*/
    R_ATMLIB_E_NG_CH_IMG_Y		    = -605,							/* DMA* channel image height is out of range						*/
    R_ATMLIB_E_NG_CH_IMG_FMT		= -606,							/* DMA* channel image format is out of range						*/
    R_ATMLIB_E_NG_CH_IMG_STR	    = -607,							/* DMA* channel image stride is out of range						*/
    R_ATMLIB_E_NG_CH_DATA_MODE	    = -608,							/* DMA* channel data mode is out of range							*/
    R_ATMLIB_E_NG_CH_KERNEL_MODE    = -609,							/* DMA* channel kernel mode is out of range							*/
    R_ATMLIB_E_NG_CH_DIL_CONV       = -610,							/* DMA* channel dilated convolution is out of range					*/
    R_ATMLIB_E_NG_CH_START_POS	    = -611,							/* DMAI channel start position is out of range						*/
    R_ATMLIB_E_NG_CH_STR_X		    = -612,							/* DMAI channel convolution stride x is out of range				*/
    R_ATMLIB_E_NG_CH_STR_Y		    = -613,							/* DMAI channel convolution stride y is out of range				*/
    R_ATMLIB_E_NG_CH_PAD_T		    = -614,							/* DMAI channel top padding(north padding) size is out of range		*/
    R_ATMLIB_E_NG_CH_PAD_B		    = -615,							/* DMAI channel bottom padding(south padding) size is out of range	*/
    R_ATMLIB_E_NG_CH_PAD_L		    = -616,							/* DMAI channel left padding(west padding) size is out of range		*/
    R_ATMLIB_E_NG_CH_PAD_R		    = -617,							/* DMAI channel right padding(east padding) size is out of range	*/
    R_ATMLIB_E_NG_CH_MAG_TYPE	    = -618,							/* DMA* channel magnification type convolution is out of range		*/
    R_ATMLIB_E_NG_CH_RPT_SKP_X	    = -619,							/* DMA* channel repeat/skip size in x direction is out of range		*/
    R_ATMLIB_E_NG_CH_RPT_SKP_Y	    = -620,							/* DMA* channel repeat/skip size in y direction is out of range		*/
    R_ATMLIB_E_NG_CONV_ARI_FMT      = -701,							/* ARI format is out of range										*/
    R_ATMLIB_E_NG_CONV_ACTI_MODE 	= -702,							/* Activation function mode out of range							*/
	R_ATMLIB_E_NG_CONV_MULT_VAL     = -703,							/* Multiplyer value is out of range									*/
    R_ATMLIB_E_NG_CONV_LOWER_VAL	= -704,							/* Lower boundary is out of range									*/
    R_ATMLIB_E_NG_CONV_LUT_LIN_INT	= -705,							/* LUT linear interpolation control is out of range					*/
    R_ATMLIB_E_NG_CONV_PAD_MODE	    = -706,							/* Padding mode is out of range										*/
    R_ATMLIB_E_NG_CONV_BIAS			= -707,							/* Bias control is out of range										*/
    R_ATMLIB_E_NG_CONV_XYPOOL_MODE  = -708,							/* XY pooling mode is out of range									*/
    R_ATMLIB_E_NG_CONV_XPOOL		= -709,							/* X pooling is out of range										*/
    R_ATMLIB_E_NG_CONV_YPOOL		= -710,							/* Y pooling is out of range										*/
    R_ATMLIB_E_NG_CONV_CPOOL_MODE	= -711,							/* Channel pooling mode is out of range								*/
    R_ATMLIB_E_NG_CONV_LABEL_INFO	= -712,							/* Label information bits is out of range							*/
    R_ATMLIB_E_NG_CONV_MAXOUT_GROUP	= -713,							/* Group information bits is out of range							*/
	R_ATMLIB_E_NG_CONV_SFTMRM       = -714,							/* Sftm rounding enable is out of range								*/
	R_ATMLIB_E_NG_CONV_CHGLOBSUM    = -715,							/* Channel global sum enable control is out of range				*/
	R_ATMLIB_E_NG_CONV_CHMAXMIN     = -716,							/* Channel max/min enable control is out of range					*/
	R_ATMLIB_E_NG_CONV_CPOOLUI      = -717,							/* User info for cpool is out of range								*/
	R_ATMLIB_E_NG_CONV_XLEN         = -718,							/* ARI x length is out of range										*/
	R_ATMLIB_E_NG_CONV_YLEN         = -719,							/* ARI y length is out of range										*/
    R_ATMLIB_E_NG_DUPLICATE_LABEL	= -801,							/* Label duplicate													*/
    R_ATMLIB_E_NG_LABEL_SIZE		= -802,							/* Label size is out of range										*/
    R_ATMLIB_E_NG_UNDEFINE_LABEL	= -803,							/* Undefined label 													*/
    R_ATMLIB_E_NG_ADDR_RANGE		= -804,							/* Address is out of range											*/
	R_ATMLIB_E_NG_LUT_FMT           = -805,							/* LUT format is out of range										*/
    R_ATMLIB_E_NG_SHIFTER           = -806,							/* Shifter error occur												*/
	R_ATMLIB_E_NG_WB_VALUE			= -807,							/* Weight value is out of range										*/
	R_ATMLIB_E_NG_WB_FMT			= -808,							/* Weight format is out of range									*/
	R_ATMLIB_E_NG_LUT_VALUE			= -809,							/* LUT value is out of range										*/
	R_ATMLIB_E_NG_COMB_CNT_SPMC     = -901,							/* Combination of count and SPMC mode  is invalid					*/
	R_ATMLIB_E_NG_COMB_CNT_SCMP_KM  = -902,							/* Combination of count, SCMP and kernel mode is invalid			*/
	R_ATMLIB_E_NG_COMB_UNITNO_FUNC  = -903,							/* Combination of unit number and function is invalid				*/
    R_ATMLIB_E_NG_COMB_FUNC_STRSN   = -904,							/* Combination of function and stall reason is invalid				*/
	R_ATMLIB_E_NG_COMB_SPMC_K333 	= -905,							/* Combination of kernel 3*3x3 and SPMC mode is invalid				*/
	R_ATMLIB_E_NG_COMB_SPMC_DATAMIX = -906,							/* Combination of SPMC mode and data mix is invalid					*/
	R_ATMLIB_E_NG_COMB_SCMP_DATAMIX = -907,							/* Combination of SPMC mode and data mix is invalid					*/
	R_ATMLIB_E_NG_COMB_SCMP_FM_ST   = -908,							/* Combination of format and stride in SCMP mode is invalid			*/
	R_ATMLIB_E_NG_COMB_SPMC_FM_ST   = -909,							/* Combination of format and stride in SPMC mode is invalid			*/
	R_ATMLIB_E_NG_COMB_STPOS_PADL   = -910,							/* Combination of start position and padding left is invalid		*/
	R_ATMLIB_E_NG_COMB_FM_IMGX      = -911,							/* Combination of image format and image width is invalid			*/
	R_ATMLIB_E_NG_COMB_RFFM_ARIFM   = -912,							/* Combination of DMARF format and next ARI format is invalid		*/
	R_ATMLIB_E_NG_COMB_WBFM_ARIFM	= -913,							/* Combination of weight value format and ARI format is invalid		*/
	R_ATMLIB_E_NG_COMB_LUTFM_ARIFM	= -914,							/* Combination of lut	 value format and ari_format is invalid		*/
	R_ATMLIB_E_NG_COMB_KM_STX       = -915,							/* Combination of kernel mode and stride x is invalid				*/
	R_ATMLIB_E_NG_COMB_KM_STY       = -916,							/* Combination of kernel mode and stride y is invalid				*/
	R_ATMLIB_E_NG_COMB_KM_STPOS     = -917,							/* Combination of kernel mode and start position is invalid			*/
	R_ATMLIB_E_NG_COMB_KM_PADL      = -918,							/* Combination of kernel mode and padding left is invalid			*/
	R_ATMLIB_E_NG_COMB_KM_PADR      = -919,							/* Combination of kernel mode and padding right is invalid			*/
	R_ATMLIB_E_NG_COMB_KM_PADT      = -920,							/* Combination of kernel mode and padding top is invalid			*/
	R_ATMLIB_E_NG_COMB_KM_PADB      = -921,							/* Combination of kernel mode and padding bottom is invalid			*/
	R_ATMLIB_E_NG_COMB_KM_DIL       = -922,							/* Combination of kernel mode and dilated convolution is invalid	*/
	R_ATMLIB_E_NG_COMB_KM_MAG       = -923,							/* Combination of kernel mode and magnification is invalid			*/
} R_ATMLIB_RETURN_VALUE;


/* Atomic Library Image Format */
typedef enum
{
	R_ATMLIB_IMG_4S			= 0U,
	R_ATMLIB_IMG_4U			= 1U,
	R_ATMLIB_IMG_8S			= 2U,
	R_ATMLIB_IMG_8U			= 3U,
	R_ATMLIB_IMG_16S		= 4U,
	R_ATMLIB_IMG_16U		= 5U,	
	R_ATMLIB_IMG_END		= 6U
} R_ATMLIB_CNNImageFormat;

/* Command List Type */
typedef enum
{
	R_ATMLIB_CLTYPE_IMP		= 1U,
	R_ATMLIB_CLTYPE_OCV		= 2U,
	R_ATMLIB_CLTYPE_DMA		= 4U,
	R_ATMLIB_CLTYPE_PSC		= 8U,
	R_ATMLIB_CLTYPE_CNN		= 16U
} R_ATMLIB_CLTYPE;

/* CNN DMAI/3DC/O ARI Channel IDs */
typedef enum
{
	R_ATMLIB_CNN_DMAI0				= (0),							/* DMA input channel   0 (V3U)				*/
	R_ATMLIB_CNN_DMAI1,												/* DMA input channel   1 (V3U)				*/
	R_ATMLIB_CNN_DMAI2,												/* DMA input channel   2 (V3U)				*/
	R_ATMLIB_CNN_DMAI3,												/* DMA input channel   3 (V3U)				*/
	R_ATMLIB_CNN_DMAI4,												/* DMA input channel   4 (V3U)				*/
	R_ATMLIB_CNN_DMAI5,												/* DMA input channel   5 (V3U)				*/
	R_ATMLIB_CNN_DMAI6,												/* DMA input channel   5 (V3U)				*/
	R_ATMLIB_CNN_DMAI7,												/* DMA input channel   7 (V3U)				*/
    R_ATMLIB_CNN_DMAI8,    			        						/* DMA input channel   8 (V3U)				*/
	R_ATMLIB_CNN_DMAI9,											  	/* DMA input channel   9 (V3U)				*/
	R_ATMLIB_CNN_DMAI10,											/* DMA input channel   10 (V3U)				*/
	R_ATMLIB_CNN_DMAI11,											/* DMA input channel   11 (V3U)				*/
	R_ATMLIB_CNN_DMAI12,											/* DMA input channel   12 (V3U)				*/
	R_ATMLIB_CNN_DMAI13,											/* DMA input channel   13 (V3U)				*/
	R_ATMLIB_CNN_DMAI14,											/* DMA input channel   14 (V3U)				*/
	R_ATMLIB_CNN_DMAI15,											/* DMA input channel   15 (V3U)				*/
	R_ATMLIB_CNN_DMAO0,												/* DMA output channel  0 (V3U)				*/
	R_ATMLIB_CNN_DMAO1,												/* DMA output channel  1 (V3U)				*/
	R_ATMLIB_CNN_DMAO2,												/* DMA output channel  2 (V3U)				*/
	R_ATMLIB_CNN_DMAO3,												/* DMA output channel  3 (V3U)				*/
	R_ATMLIB_CNN_DMAO4,												/* DMA output channel  4 (V3U)				*/
	R_ATMLIB_CNN_DMAO5,												/* DMA output channel  5 (V3U)				*/
	R_ATMLIB_CNN_DMAO6,												/* DMA output channel  6 (V3U)				*/
	R_ATMLIB_CNN_DMAO7,												/* DMA output channel  7 (V3U)				*/
	R_ATMLIB_CNN_DMAO8,												/* DMA output channel  8 (V3U)				*/
	R_ATMLIB_CNN_DMAO9,												/* DMA output channel  9 (V3U)				*/
	R_ATMLIB_CNN_DMAO10,											/* DMA output channel  10 (V3U)				*/
	R_ATMLIB_CNN_DMAO11,											/* DMA output channel  11 (V3U)				*/
	R_ATMLIB_CNN_DMAO12,											/* DMA output channel  12 (V3U)				*/
	R_ATMLIB_CNN_DMAO13,											/* DMA output channel  13 (V3U)				*/
	R_ATMLIB_CNN_DMAO14,											/* DMA output channel  14 (V3U)				*/
	R_ATMLIB_CNN_DMAO15,											/* DMA output channel  15 (V3U)				*/
	R_ATMLIB_CNN_DMAO16,											/* DMA output channel  16 (V3U)				*/
	R_ATMLIB_CNN_DMAO17,											/* DMA output channel  17 (V3U)				*/
	R_ATMLIB_CNN_DMAO18,											/* DMA output channel  18 (V3U)				*/
	R_ATMLIB_CNN_DMAO19,											/* DMA output channel  19 (V3U)				*/
	R_ATMLIB_CNN_DMAO20,											/* DMA output channel  20 (V3U)				*/
	R_ATMLIB_CNN_DMAO21,											/* DMA output channel  21 (V3U)				*/
	R_ATMLIB_CNN_DMAO22,											/* DMA output channel  22 (V3U)				*/
	R_ATMLIB_CNN_DMAO23,											/* DMA output channel  23 (V3U)				*/
	R_ATMLIB_CNN_DMAO24,											/* DMA output channel  24 (V3U)				*/
	R_ATMLIB_CNN_DMAO25,											/* DMA output channel  25 (V3U)				*/
	R_ATMLIB_CNN_DMAO26,											/* DMA output channel  26 (V3U)				*/
	R_ATMLIB_CNN_DMAO27,											/* DMA output channel  27 (V3U)				*/
	R_ATMLIB_CNN_DMAO28,											/* DMA output channel  28 (V3U)				*/
	R_ATMLIB_CNN_DMAO29,											/* DMA output channel  29 (V3U)				*/
	R_ATMLIB_CNN_DMAO30,											/* DMA output channel  30 (V3U)				*/
	R_ATMLIB_CNN_DMAO31,											/* DMA output channel  31 (V3U)				*/
	R_ATMLIB_CNN_DMAC0,												/* DMA 3Dcarry channel 0 (V3U)				*/
	R_ATMLIB_CNN_DMAC1,												/* DMA 3Dcarry channel 1 (V3U)				*/
	R_ATMLIB_CNN_DMAC2,												/* DMA 3Dcarry channel 2 (V3U)				*/
	R_ATMLIB_CNN_DMAC3,												/* DMA 3Dcarry channel 3 (V3U)				*/
	R_ATMLIB_CNN_DMAC4,												/* DMA 3Dcarry channel 4 (V3U)				*/
	R_ATMLIB_CNN_DMAC5,												/* DMA 3Dcarry channel 5 (V3U)				*/
	R_ATMLIB_CNN_DMAC6,												/* DMA 3Dcarry channel 6 (V3U)				*/
	R_ATMLIB_CNN_DMAC7,												/* DMA 3Dcarry channel 7 (V3U)				*/
	R_ATMLIB_CNN_DMAC8,												/* DMA 3Dcarry channel 8 (V3U)				*/
	R_ATMLIB_CNN_DMAC9,												/* DMA 3Dcarry channel 9 (V3U)				*/
	R_ATMLIB_CNN_DMAC10,											/* DMA 3Dcarry channel 10 (V3U)				*/
	R_ATMLIB_CNN_DMAC11,											/* DMA 3Dcarry channel 11 (V3U)				*/
	R_ATMLIB_CNN_DMAC12,											/* DMA 3Dcarry channel 12 (V3U)				*/
	R_ATMLIB_CNN_DMAC13,											/* DMA 3Dcarry channel 13 (V3U)				*/
	R_ATMLIB_CNN_DMAC14,											/* DMA 3Dcarry channel 14 (V3U)				*/
	R_ATMLIB_CNN_DMAC15,											/* DMA 3Dcarry channel 15 (V3U)				*/
	R_ATMLIB_CNN_DMAC16,											/* DMA 3Dcarry channel 16 (V3U)				*/
	R_ATMLIB_CNN_DMAC17,											/* DMA 3Dcarry channel 17 (V3U)				*/
	R_ATMLIB_CNN_DMAC18,											/* DMA 3Dcarry channel 18 (V3U)				*/
	R_ATMLIB_CNN_DMAC19,											/* DMA 3Dcarry channel 19 (V3U)				*/
	R_ATMLIB_CNN_DMAC20,											/* DMA 3Dcarry channel 20 (V3U)				*/
	R_ATMLIB_CNN_DMAC21,											/* DMA 3Dcarry channel 21 (V3U)				*/
	R_ATMLIB_CNN_DMAC22,											/* DMA 3Dcarry channel 22 (V3U)				*/
	R_ATMLIB_CNN_DMAC23,											/* DMA 3Dcarry channel 23 (V3U)				*/
	R_ATMLIB_CNN_DMAC24,											/* DMA 3Dcarry channel 24 (V3U)				*/
	R_ATMLIB_CNN_DMAC25,											/* DMA 3Dcarry channel 25 (V3U)				*/
	R_ATMLIB_CNN_DMAC26,											/* DMA 3Dcarry channel 26 (V3U)				*/
	R_ATMLIB_CNN_DMAC27,											/* DMA 3Dcarry channel 27 (V3U)				*/
	R_ATMLIB_CNN_DMAC28,									        /* DMA 3Dcarry channel 28 (V3U)				*/
	R_ATMLIB_CNN_DMAC29,											/* DMA 3Dcarry channel 29 (V3U)				*/
	R_ATMLIB_CNN_DMAC30,											/* DMA 3Dcarry channel 30 (V3U)				*/
	R_ATMLIB_CNN_DMAC31,											/* DMA 3Dcarry channel 31 (V3U)				*/
	R_ATMLIB_CNN_ARI0,												/* ARI channel 0 (V3U)				        */
	R_ATMLIB_CNN_ARI1,												/* ARI channel 1 (V3U)				        */
	R_ATMLIB_CNN_ARI2,												/* ARI channel 2 (V3U)				        */
	R_ATMLIB_CNN_ARI3,												/* ARI channel 3 (V3U)				        */
	R_ATMLIB_CNN_ARI4,												/* ARI channel 4 (V3U)				        */
	R_ATMLIB_CNN_ARI5,												/* ARI channel 5 (V3U)				        */
	R_ATMLIB_CNN_ARI6,												/* ARI channel 6 (V3U)				        */
	R_ATMLIB_CNN_ARI7,												/* ARI channel 7 (V3U)				        */
	R_ATMLIB_CNN_ARI8,												/* ARI channel 8 (V3U)				        */
	R_ATMLIB_CNN_ARI9,												/* ARI channel 9 (V3U)				        */
	R_ATMLIB_CNN_ARI10,												/* ARI channel 10 (V3U)				        */
	R_ATMLIB_CNN_ARI11,												/* ARI channel 11 (V3U)				        */
	R_ATMLIB_CNN_ARI12,												/* ARI channel 12 (V3U)				        */
	R_ATMLIB_CNN_ARI13,												/* ARI channel 13 (V3U)				        */
	R_ATMLIB_CNN_ARI14,												/* ARI channel 14 (V3U)				        */
	R_ATMLIB_CNN_ARI15,												/* ARI channel 15 (V3U)				        */
	R_ATMLIB_CNN_ARI16,												/* ARI channel 16 (V3U)				        */
	R_ATMLIB_CNN_ARI17,												/* ARI channel 17 (V3U)				        */
	R_ATMLIB_CNN_ARI18,												/* ARI channel 18 (V3U)				        */
	R_ATMLIB_CNN_ARI19,												/* ARI channel 19 (V3U)				        */
	R_ATMLIB_CNN_ARI20,												/* ARI channel 20 (V3U)				        */
	R_ATMLIB_CNN_ARI21,												/* ARI channel 21 (V3U)				        */
	R_ATMLIB_CNN_ARI22,												/* ARI channel 22 (V3U)				        */
	R_ATMLIB_CNN_ARI23,												/* ARI channel 23 (V3U)				        */
	R_ATMLIB_CNN_ARI24,												/* ARI channel 24 (V3U)				        */
	R_ATMLIB_CNN_ARI25,												/* ARI channel 25 (V3U)				        */
	R_ATMLIB_CNN_ARI26,												/* ARI channel 26 (V3U)				        */
	R_ATMLIB_CNN_ARI27,												/* ARI channel 27 (V3U)				        */
	R_ATMLIB_CNN_ARI28,												/* ARI channel 28 (V3U)				        */
	R_ATMLIB_CNN_ARI29,												/* ARI channel 29 (V3U)				        */
	R_ATMLIB_CNN_ARI30,												/* ARI channel 30 (V3U)				        */
	R_ATMLIB_CNN_ARI31,												/* ARI channel 31 (V3U)				        */
	R_ATMLIB_CNN_CH_END												/* The end of CNN channel IDs				*/
} R_ATMLIB_CNNChannelID;


/* Input/3DC magnification modes */
typedef enum
{
	R_ATMLIB_CNN_NOMAG			    = (0),				        	/* No Magnification						    */
	R_ATMLIB_CNN_SKP,									        	/* Skip mode							    */
	R_ATMLIB_CNN_RPT_ZERO,								        	/* Repeat mode, repeat with zero		    */
	R_ATMLIB_CNN_RPT_IDENTITY,							        	/* Repeat mode, repeat with identity values */
	R_ATMLIB_CNN_MAG_END								        	/* The end of magnification modes   	    */
} R_ATMLIB_CNNMAGType;        
        
/* Branch condition type */        
typedef enum        
{        
	R_ATMLIB_CNN_BRC_EZ			    = (0),				            /* Branc condition : equal to 0			    */
	R_ATMLIB_CNN_BRC_GZ,								        	/* Branc condition : greater than 0		    */
	R_ATMLIB_CNN_BRC_LZ,								        	/* Branc condition : less than 0		    */
	R_ATMLIB_CNN_BRC_ALWAYS,							        	/* Branc condition : always				    */
	R_ATMLIB_CNN_BRC_END								        	/* The end of branch conditions			    */
} R_ATMLIB_CNNBRCType;        
        
/* Enable / Disable */        
typedef enum        
{        
	R_ATMLIB_CNN_DISABLE		    = (0),					        /* Disable							        */
	R_ATMLIB_CNN_ENABLE,								        	/* Enable							        */
	R_ATMLIB_CNN_ENABLECONTROL_END						        	/* The end of enable control patterns       */
} R_ATMLIB_CNNEnableControl;        

/* Channel pool mode selection */        
typedef enum        
{        
	R_ATMLIB_CNN_CPOOL_NO		    = (0),					       	/* No channel pooling					    */
	R_ATMLIB_CNN_CPOOL_MAX,						                    /* Channel Max						        */
	R_ATMLIB_CNN_CPOOL_RANK,							        	/* Channel Rank						        */
	R_ATMLIB_CNN_CPOOL_LABEL,							        	/* Channel Label						    */
	R_ATMLIB_CNN_CPOOL_END								        	/* The end of channel pool mode			    */
} R_ATMLIB_CNNCPoolMode;

/* Label Bit information */
typedef enum
{
	R_ATMLIB_CNN_LABELBIT_0	        = (0),						    /* No bit used for label : 0			    */
	R_ATMLIB_CNN_LABELBIT_1,										/* Lower bit used for label info : 1	    */
	R_ATMLIB_CNN_LABELBIT_2,										/* Lower bit used for label info : 2	    */
	R_ATMLIB_CNN_LABELBIT_3,										/* Lower bit used for label info : 3	    */
	R_ATMLIB_CNN_LABELBIT_4,										/* Lower bit used for label info : 4	    */
	R_ATMLIB_CNN_LABELBIT_5,										/* Lower bit used for label info : 5	    */
	R_ATMLIB_CNN_LABELBIT_END									    /* The end of channel pool info		        */
} R_ATMLIB_CNNLabelBit;

/* Max group */
typedef enum
{
	R_ATMLIB_CNN_MAXGROUP_1	        = (0),					   	    /* Max value inside 1 group of 32 ARI chanenl (V3U)		                                           */
	R_ATMLIB_CNN_MAXGROUP_2,							    		/* Max value inside 2 groups of each 16 ARI chanenl  (V3U)	                                       */
	R_ATMLIB_CNN_MAXGROUP_4,							    		/* Max value inside 4 groups of each 8 ARI chanenl (V3U)	                                       */
	R_ATMLIB_CNN_MAXGROUP_8,							    		/* Max value inside 8 groups of each 4 ARI chanenl (V3U)	                                       */
	R_ATMLIB_CNN_MAXGROUP_16,							    		/* Max value inside 16 groups of each 2 ARI chanenl (V3U)	                                       */
	R_ATMLIB_CNN_MAXGROUP_32,							    		/* Max value inside 32 groups of each 1 ARI chanenl (V3U)	                                       */
	R_ATMLIB_CNN_MAXGROUP_END							    		/* The end of max group								                                               */
} R_ATMLIB_CNNMAXGroup;    
    
typedef enum
{
	R_ATMLIB_CNN_SCRESMODE_0	    = (0),				    	    /* SCRES mode : clear only MAX_CNT, MIN_CNT of the all channels	                                    */
	R_ATMLIB_CNN_SCRESMODE_1,								    	/* SCRES mode : clear MAX_CNT and MIN_CNT for all channels as well as MAXSUM_CNT and MINSUM_CNT.	*/
	R_ATMLIB_CNN_SCRESMODE_2,								    	/* SCRES mode : clear MAX_CNT and MIN_CNT for all channels as well as the flag TUV in SR register.	*/
	R_ATMLIB_CNN_SCRESMODE_3,								    	/**
															    		SCRES mode : clear MAX_CNT and MIN_CNT for all channels as well as
															    		MAXSUM_CNT and MINSUM_CNT and the flag TUV in SR register.
															    	*/
	R_ATMLIB_CNN_SCRESMODE_END								    	/* The end of SCRES modes				                                                            */
} R_ATMLIB_CNNSCRESMode;

/* Setting Base address states */
typedef enum
{
	R_ATMLIB_CNN_BADDRSTAT_UNSET    = (0),				         	/* State of unsetting base address			*/
	R_ATMLIB_CNN_BADDRSTAT_SET1ST,							     	/* State of setting base address (1st stage)*/
	R_ATMLIB_CNN_BADDRSTAT_SET2ND							     	/* State of setting base address (2nd stage)*/
} R_ATMLIB_CNNBaddrStatus;     
     
/* Channel types */     
typedef enum {     
	R_ATMLIB_CNN_TYPE_DMAI		    = (0),	     	                /* Channel type : DMAI						*/
	R_ATMLIB_CNN_TYPE_DMAO,									     	/* Channel type : DMAO						*/
	R_ATMLIB_CNN_TYPE_DMAC,									     	/* Channel type : DMAC						*/
	R_ATMLIB_CNN_TYPE_END									     	/* The end of Channel types					*/
} R_ATMLIB_CNNChannelType;

/* Activation function types */
typedef enum {
	R_ATMLIB_CNN_ACTFUNC_NO		    = (0),							/* Activation function : No					*/
	R_ATMLIB_CNN_ACTFUNC_RELU,										/* Activation function : RELU				*/
	R_ATMLIB_CNN_ACTFUNC_BRELU,										/* Activation function : BRELU				*/
	R_ATMLIB_CNN_ACTFUNC_ELU,										/* Activation function : ELU				*/
	R_ATMLIB_CNN_ACTFUNC_LRELU,										/* Activation function : LRELU				*/
	R_ATMLIB_CNN_ACTFUNC_PRELU,										/* Activation function : PRELU				*/
	R_ATMLIB_CNN_ACTFUNC_PELU,										/* Activation function : PELU				*/
	R_ATMLIB_CNN_ACTFUNC_SIGTANH,									/* Activation function : SIGTANH			*/
	R_ATMLIB_CNN_ACTFUNC_END										/* The end of activation function types		*/
} R_ATMLIB_CNNActFunc;

/* XY Pool Mode */
typedef enum {
	R_ATMLIB_CNN_XYPOOL_NO 		    = (0),							/* XY pooling mode : No						*/
	R_ATMLIB_CNN_XYPOOL_MAX,										/* XY pooling mode : MAX					*/
	R_ATMLIB_CNN_XYPOOL_ADD,										/* XY pooling mode : ADD					*/
	R_ATMLIB_CNN_XYPOOL_END											/* The end of XY pooling mode				*/
} R_ATMLIB_CNNXYPoolMode;

/* Kernel mode */
typedef enum {
	R_ATMLIB_CNN_KERNEL_1_5_5	    = (0),						    /* Kernel mode : 1x5*5						*/
	R_ATMLIB_CNN_KERNEL_4_1_1,										/* Kernel mode : 4x1*1						*/
	R_ATMLIB_CNN_KERNEL_2_3_3,										/* Kernel mode : 2x3*3						*/
	R_ATMLIB_CNN_KERNEL_3_3_3		= (4),							/* Kernel mode : 3x3*3						*/
	R_ATMLIB_CNN_KERNEL_END											/* The end of kernel mode					*/
} R_ATMLIB_CNNKernelMode;

/* Channel setup mode */
typedef enum {
	R_ATMLIB_CNN_CHSET_SLOW		    = (0),							/* Channel setup mode : slow				*/
	R_ATMLIB_CNN_CHSET_FAST,										/* Channel setup mode : fast				*/
} R_ATMLIB_CNNCHSetupMode;

/* Data transfer mode */
typedef enum {
	R_ATMLIB_CNN_DATA_SPMC 		    = (0),							/* Data transfer mode : SPMC				*/
	R_ATMLIB_CNN_DATA_SCMP ,										/* Data transfer mode : SCMP				*/
	R_ATMLIB_CNN_DATA_END											/* The end of data transfer mode			*/
} R_ATMLIB_CNNDataMode;

/* Padding mode setting */
typedef enum {
	R_ATMLIB_CNN_PAD_ZERO		    = (0),	    					/* Padding mode : zero						*/
	R_ATMLIB_CNN_PAD_IDENTITY,										/* Padding mode : identity					*/
	R_ATMLIB_CNN_PAD_MIRROR,										/* Padding mode : mirror					*/
	R_ATMLIB_CNN_PAD_END											/* The end of padding mode					*/
} R_ATMLIB_CNNPadMode;

/* ARI format setting */
typedef enum {
	R_ATMLIB_CNN_ARIFORMAT_8S		= (0),							/* ARI format : 8 bit signed				*/
	R_ATMLIB_CNN_ARIFORMAT_8U,										/* ARI format : 8 bit unsigned				*/
	R_ATMLIB_CNN_ARIFORMAT_16S,										/* ARI format : 16bit signed				*/
	R_ATMLIB_CNN_ARIFORMAT_16U,										/* ARI format : 16bit unsigned				*/
	R_ATMLIB_CNN_ARIFORMAT_END										/* The end of ARI format					*/
} R_ATMLIB_CNNARIFormat;

/* DATA format setting */
typedef enum {
	R_ATMLIB_CNN_DATAFM_16U		= (0),						    	/* DATA format : unpacked 16bit unsigned for weight			*/
	R_ATMLIB_CNN_DATAFM_16S,										/* DATA format : unpacked 16bit singed	 for weight			*/
	R_ATMLIB_CNN_DATAFM_8U,											/* DATA format : unpacked 8bit unsigned	 for weight and LUT	*/
	R_ATMLIB_CNN_DATAFM_8S,											/* DATA format : unpacked 8bit unsigned  for weight and LUT	*/
	R_ATMLIB_CNN_DATAFM_4U,											/* DATA format : packed 4bit unsigned 	 for weight			*/
	R_ATMLIB_CNN_DATAFM_4S,											/* DATA format : packed 4bit signed 	 for weight 		*/
	R_ATMLIB_CNN_DATAFM_END											/* The end of DATA format				 					*/
} R_ATMLIB_CNNDATAFormat;

/* Stall reason of performance counter */
typedef enum {
	R_ATMLIB_CNN_STALL_ALL		    = (0),							/* All stalls, only used for ARI							        */
	R_ATMLIB_CNN_STALL_DASINK,									    /* Stalls caused by wait as data sink						        */
	R_ATMLIB_CNN_STALL_DASRC_SYNCS,						            /* Stalls caused by wait as data source , or by SYNCS 				*/
    R_ATMLIB_CNN_STALL_DASINK_3DC,							        /* Stalls caused by wait as data sink for data from 3DC buffer		*/
	R_ATMLIB_CNN_STALL_END                                          /* The end of counter stall reason						            */
} R_ATMLIB_CNNStallReason;

/* Function of Performance counter */
typedef enum {
	R_ATMLIB_CNN_CNT_FUNC_DMAIB		= (0),						    /* DMAI busy cycles						                            */
	R_ATMLIB_CNN_CNT_FUNC_DMAIS,								    /* DMAI stall cycles						                        */
	R_ATMLIB_CNN_CNT_FUNC_DMAOB,			          			    /* DMAO busy cycles 							                    */
    R_ATMLIB_CNN_CNT_FUNC_DMAOS,			          			    /* DMAO stall cycles							                    */
	R_ATMLIB_CNN_CNT_FUNC_DMACB,			          			    /* DMA3DC busy cycles							                    */
    R_ATMLIB_CNN_CNT_FUNC_DMACS,			          			    /* DMA3DC stall cycles							                    */
	R_ATMLIB_CNN_CNT_FUNC_ARIB,				          		        /* ARI busy cycles							                        */
    R_ATMLIB_CNN_CNT_FUNC_ARIS,				          			    /* ARI stall cycles						                            */
	R_ATMLIB_CNN_CNT_FUNC_CLB,				          		        /* CL busy cycles 							                        */
    R_ATMLIB_CNN_CNT_FUNC_CLS,				          			    /* CL stall cycles						                            */
	R_ATMLIB_CNN_CNT_FUNC_DMARFB,			          			    /* DMARF busy cycles							                    */
    R_ATMLIB_CNN_CNT_FUNC_DMARFS,			          			    /* DMARF stall cycles						                        */
	R_ATMLIB_CNN_CNT_FUNC_END                                       /* The end of performance counter function		                    */
} R_ATMLIB_CNNCounterFunc;

/* Performance counter ID */
typedef enum {
	R_ATMLIB_CNN_COUNTER_ID_0       = (0),							/* Performance counter	0					*/
	R_ATMLIB_CNN_COUNTER_ID_1,									    /* Performance counter	1					*/
	R_ATMLIB_CNN_COUNTER_ID_2,						                /* Performance counter 2					*/
    R_ATMLIB_CNN_COUNTER_ID_3,							            /* Performance counter	3					*/
	R_ATMLIB_CNN_COUNTER_ID_END                                     /* The end of performance counter ID		*/
} R_ATMLIB_CNNCounterID;

/* User information to distinguish the 2 runs*/
typedef enum {
	R_ATMLIB_CNN_CPOOLUI_0		    = (0),						    /* First Run            					*/
	R_ATMLIB_CNN_CPOOLUI_1,										    /* Second run           					*/
	R_ATMLIB_CNN_CPOOLUI_END										/* The end of user information control		*/
} R_ATMLIB_CNNCPOOLUI;

/* User shifter approach */
typedef enum {
	R_ATMLIB_CNN_SHIFTER_FIXED		    = (0),						/* fixed approach          					*/
	R_ATMLIB_CNN_SHIFTER_STARIC,									/* static approach         					*/
	R_ATMLIB_CNN_SHIFTER_DYNAMIC,									/* dynamic approach        					*/
	R_ATMLIB_CNN_SHIFTER_END										/* The end of user information control		*/
} R_ATMLIB_CNNShifterApproach;


/************************************************
	Structure definitions 	
************************************************/
/* CNNData */
typedef struct R_ATMLIB_CNNData_ R_ATMLIB_CNNData;

typedef struct
{
	R_ATMLIB_CLTYPE                 cltype;
	uint32_t						size;
	R_ATMLIB_CLState				state;
	uint32_t						*cur_addr;
	uint32_t						*top_addr;
	R_ATMLIB_CNNData			    *cnn_data;
} R_ATMLIB_CLData;


/* Describe a CNN DMAI channel parameters */
typedef struct	
{
	uint8_t						    dmai_frac;						/* The fractional position  of the channel	             								*/
	uint32_t						ptr;					        /* Channel physical address					                                        	*/
	uint16_t						image_x;				        /* Channel width								                                        */
	uint16_t						image_y;				        /* Channel height								                                        */
	uint16_t						image_stride;			        /* Channel stride								                                        */
	R_ATMLIB_CNNImageFormat         image_format;			        /* Channel image format						                                        	*/
	R_ATMLIB_CNNMAGType			    mag_type;					    /* Magnification type for input channels	                                            */
	uint8_t							repeat_skip_x;					/* Pixel repeat/skip amount for DMAI                                        	*/
	uint8_t							repeat_skip_y;					/* Row repeat/skip amount for DMAI	                                        	*/
	uint8_t							start_pos;				     	/* Start position in x direction on left side											*/
	uint8_t							stride_x;						/* Convolution stride X for DMAI				                                		*/
	uint8_t							stride_y;						/* Convolution stride X for DMAI				                                		*/
	uint8_t							pad_t;					    	/* Channel top padding for DMAI					                                		*/
	uint8_t							pad_b;					    	/* Channel bottom padding for DMAI				                                		*/
	uint8_t							pad_l;					    	/* Channel left padding for DMAI				                                		*/
	uint8_t							pad_r;					    	/* Channel right padding for DMAI				                                		*/
} R_ATMLIB_CNNDMAIChParam;


/* Describe a CNN DMAC channel parameters */
typedef struct
{
	uint8_t						    dmac_frac;						/* The fractional position  of the channel	             								*/
	uint32_t						ptr;					        /* Channel physical address					                                        	*/
	uint16_t						image_x;				        /* Channel width								                                        */
	uint16_t						image_y;				        /* Channel height								                                        */
	uint16_t						image_stride;			        /* Channel stride								                                        */
	R_ATMLIB_CNNImageFormat         image_format;			        /* Channel image format						                                        	*/
	R_ATMLIB_CNNMAGType			    mag_type;					    /* Magnification type for input channels	                                            */
	uint8_t							repeat_skip_x;					/* Pixel repeat/skip amount for DMA3DC                                        	*/
	uint8_t							repeat_skip_y;					/* Row repeat/skip amount for DMA3DC	                                        	*/
} R_ATMLIB_CNNDMACChParam;


/* Describe a CNN DMAI parameters */
typedef struct
{
	uint8_t							param_count;						/* Count of parameter number														*/
	R_ATMLIB_CNNDMAIChParam         dmai_ch[R_ATMLIB_CNN_DMAI_MAX_CH];	/* Common channel params                                                            */
	uint8_t                         ch_count;                       	/* Channel count number                                                             */
	R_ATMLIB_CNNChannelID		    ch_id[R_ATMLIB_CNN_DMAI_MAX_CH];	/* Channel ID array																	*/
	R_ATMLIB_CNNDataMode            data_mode;                      	/* The arrangement of the data                                                      */
	R_ATMLIB_CNNKernelMode			kernel_mode;				    	/* Kernel mode setting																*/
	uint8_t							dil_conv;							/* Dilated convolution distance									                    */
	uint16_t                        channel_offset;                 	/* Channel offset value                                                             */
} R_ATMLIB_CNNDMAIParam;

/* Describe a CNN DMAO parameters */
typedef struct
{
	uint8_t							param_count;						/* Count of ptr array														*/
	uint32_t				        ptr[R_ATMLIB_CNN_DMAO_MAX_CH];   	/* Channel start physical addresses					                                        */
	uint8_t                         ch_count;                       	/* Channel count number                                                             */
	R_ATMLIB_CNNChannelID		    ch_id[R_ATMLIB_CNN_DMAO_MAX_CH];	/* Channel ID array																	*/
	uint16_t						image_x;				        	/* Channel width								                   					*/
	uint16_t						image_y;				        	/* Channel height								                   				    */
	uint16_t						image_stride;			        	/* Channel stride								                   	                */
	R_ATMLIB_CNNImageFormat         image_format;			        	/* Channel image format							                   				    */
	R_ATMLIB_CNNDataMode            data_mode;                      	/* The arrangement of the data                                                      */
	R_ATMLIB_CNNKernelMode			next_layer_kernel_mode;			    /* Kernel mode setting						                                        */
	uint16_t                        channel_offset;                 	/* Channel offset value                                                             */
} R_ATMLIB_CNNDMAOParam;


/* Describe a CNN DMAC parameters */
typedef struct
{
	uint8_t							param_count;						/* Count of parameter number														*/
	R_ATMLIB_CNNDMACChParam			dmac_ch[R_ATMLIB_CNN_DMAC_MAX_CH];	/* Common channel params			                                                */
	uint8_t                         ch_count;                       	/* Channel count number                                                             */
	R_ATMLIB_CNNChannelID		    ch_id[R_ATMLIB_CNN_DMAC_MAX_CH];	/* Channel ID array																	*/
	R_ATMLIB_CNNDataMode            data_mode;                      	/* The arrangement of the data                                                      */
	uint16_t                        channel_offset;                 	/* Channel offset value                                                             */
} R_ATMLIB_CNNDMACParam;


/* Describe Conv+ARI settings */
typedef struct
{
	R_ATMLIB_CNNChannelID		    ch_id[R_ATMLIB_CNN_ARI_MAX_CH];	/* Array of ARI IDs       						                                    	*/
	uint8_t		                    ari_cnt;					    /* Count number of ARI channel	                                            */
    R_ATMLIB_CNNARIFormat			ari_format;						/* ARI format 							                            	                */
	R_ATMLIB_CNNActFunc		        acti_mode;						/* Acitivation function						                        	    			*/
	R_ATMLIB_CNNEnableControl		lut_lin_int;					/* Enable for LUT piece-wise linear interprolation function                        	    */
	R_ATMLIB_CNNPadMode			    pad_mode;					    /* Pad mode					                        	    		                    */
	R_ATMLIB_CNNEnableControl   	bias_en;						/* Biasing enabled						                        	    				*/
	R_ATMLIB_CNNXYPoolMode			xypool_mode;					/* XY mode configuration				                        	    				*/
	uint8_t							xpool;							/* Pooling along x-axis					                        	    				*/
	uint8_t							ypool;							/* Pooling along y-axis				                        	    				    */
	R_ATMLIB_CNNCPoolMode			cpool_mode;						/* Channel pool mode						                        	    			*/
	R_ATMLIB_CNNLabelBit		    label_info;						/* Label bits info in Rank/label mode					                        	    */
	R_ATMLIB_CNNMAXGroup 			maxout_group;					/* Maxout Group for MAX operation		                        	    				*/
	R_ATMLIB_CNNARIFormat			next_ari_format;				/* NExt ARI format for weights preload by DMARF	                        	    	    */
    R_ATMLIB_CNNEnableControl       sftm_round_mode;                /* Enable rounding option for sftm down shifter                                         */
	R_ATMLIB_CNNCPOOLUI             cpool_ui;                       /* User information to distinguish the 2 runs                                           */
	uint16_t						upper_bound;					/* Upper boundary value					                        	    	 		    */
	int16_t						    lower_bound;					/* Lower boundary value						                        	    	   		*/
	uint8_t							mult_val;						/* Multiplier value of activation function					                        	*/
	uint8_t							mult_frac;						/* Fractional position of activation mult							                    */
} R_ATMLIB_CNNARIParam;


/* Describe DMARF settings */
typedef struct
{
	uint32_t			            external_sa;	                /* internal memory/DDR start address, the source address		                        */
	uint16_t			            internal_sa;	                /* internal start address, the destination address,only used for SCMP mode              */
	uint16_t			            length;                         /* the transfer length of DMARF, only used for SCMP mode                                */
	uint8_t			                start_pos;	                    /* Start position, only used for SPMC mode of weight                                    */
	uint8_t			                end_pos;	                    /* End position, only used for SPMC mode of weight                                      */
	R_ATMLIB_CNNDATAFormat			format;			                /* DMARF transfer mode						                                            */
	R_ATMLIB_CNNDataMode			data_mode;                      /* Data mode SPMC/SCMP */
} R_ATMLIB_CNNDMARFParam;

/* label information for BRC, GOSUB, JUMP */
typedef struct
{
	uint32_t						*addr;							/* Address of BRC / GOSUB instruction	                                                */
	uint32_t						label_num;						/* Index of R_ATMLIB_CNNLabel			                                                */
} R_ATMLIB_CNNRelolveLabel;

typedef struct
{
	char							name[R_ATMLIB_CNN_MAX_LABEL_LENGTH + 1U];
	uint32_t						*addr;							/* Address of label						                                                */
} R_ATMLIB_CNNLabel;

typedef struct
{
	uint32_t						label_count;					/* Count of R_ATMLIB_CNNLabel			                                                */
	uint32_t						resolve_count;					/* Count of R_ATMLIB_CNNRelolveLabel	                                                */
	R_ATMLIB_CNNLabel				label[R_ATMLIB_CNN_MAX_LABELS];
	R_ATMLIB_CNNRelolveLabel		resolve[R_ATMLIB_CNN_MAX_LABEL_RESOLVES];
} R_ATMLIB_CNNLabelInfo;


/* Describe base address information and status */
typedef struct
{
	uint32_t						baddr;							/* Base address							                                                */
	R_ATMLIB_CNNBaddrStatus			baddr_state;					/* Enable flag of setting base address	                                                */
} R_ATMLIB_CNNBaseAddrInfo;

/* Describe the DMAI/3DC/O base address information and status */
typedef struct
{
	R_ATMLIB_CNNBaseAddrInfo		baddr_dmai[R_ATMLIB_CNN_DMAI_MAX_CH];	/* Base address information of DMAI	                                            */
	R_ATMLIB_CNNBaseAddrInfo		baddr_dmao[R_ATMLIB_CNN_DMAO_MAX_CH];	/* Base address information of DMAO	                                            */
	R_ATMLIB_CNNBaseAddrInfo		baddr_dmac[R_ATMLIB_CNN_DMAC_MAX_CH];	/* Base address information of DMAC	                                            */
} R_ATMLIB_CNNBaseAddr_DMA;

/* Describe the register attribute and register */
typedef struct
{
	uint32_t						value;							/* Value of register					                                                */
	uint32_t						attr;							/* Attribute of register				                                                */
} R_ATMLIB_CNNCurrentReg;

/* Register which have plural channel information */
typedef struct
{
	uint32_t						DMAIS;
	uint32_t						DMAIE;
	uint32_t						DMA3DCS;
	uint32_t						DMA3DCE;
    uint32_t						DMAOS;
	uint32_t						DMAOE;
	uint32_t						ARI_LEN;
	uint32_t                        ARIE;
	uint32_t						ARICSR;
} R_ATMLIB_CNNChannelProps;

/* CNN Data */
struct R_ATMLIB_CNNData_
{
	R_ATMLIB_CNNARIFormat			ari_format;			            		/* ARI Format				                                		            */
	R_ATMLIB_CNNARIFormat			next_ari_format;						/* Next ARI format for weights preload by DMARF	                        	    */
    R_ATMLIB_CNNCHSetupMode         setup_mode_dmai;						/* Channel setup mode for DMAI						                            */
	R_ATMLIB_CNNCHSetupMode         setup_mode_dmac;						/* Channel setup mode for DMAC						                            */
	R_ATMLIB_CNNCHSetupMode         setup_mode_dmao;						/* Channel setup mode for DMAO						                            */
	uint8_t							dmai_frac[R_ATMLIB_CNN_DMAI_MAX_CH];	/* Fractional position array of input DMAI     	                            	*/
	uint8_t							dmac_frac[R_ATMLIB_CNN_DMAC_MAX_CH];	/* Fractional position array of input DMA3DC     	                           	*/
	R_ATMLIB_CNNChannelProps        channel_props;							/* Register which have plural channel information	                            */
	R_ATMLIB_CNNCurrentReg          current_reg[R_ATMLIB_CNN_REG_MAXNUM];	/* current register value		                                                */
	R_ATMLIB_CNNLabelInfo           label_info;								/* label information for BRC, GOSUB, JUMP			                            */
	uint32_t                        *ofs_addr_wup;							/* Offset Address of WUP Command					                            */
	R_ATMLIB_CNNBaseAddr_DMA        baddr_info;								/* information of patching and address increment	                            */										
};

/* Single performance counter informantion */
typedef struct
{
	R_ATMLIB_CNNCounterID		    counter_id;						/* ID number of the performance counter		                                            */
	R_ATMLIB_CNNEnableControl	    counter_en;						/* Performance counter enable	                                                        */
	R_ATMLIB_CNNStallReason		    stall_reason;               	/* Stall reason of performance counter		                                            */
	R_ATMLIB_CNNCounterFunc		    func;                       	/* Function fo performance counter			                                            */
	uint8_t                         unit_num;                   	/* Unit number of performance counter		                                            */
	uint32_t                        counter_value;              	/* Value of the performance counter                                                     */
} R_ATMLIB_CNNCounterInfo;

/* Performance counter informantion */
typedef struct
{
	R_ATMLIB_CNNCounterInfo		    counter_info[R_ATMLIB_CNN_PERFORMCNT_NUM];	/* Performance counter information			                                */
	uint8_t		                    counter_cnt;								/* Count number of R_ATMLIB_CNNCounterInfo	                                */
}  R_ATMLIB_CNNPerformCounter;

/* Channel statistic control */
typedef struct
{
	R_ATMLIB_CNNEnableControl		glob_sum;						/* Enable/Disable the global channel sum generation                                     */
	R_ATMLIB_CNNEnableControl		channel_max_min;				/* Enable/Disable the channel Max/Min generation	                                    */
}  R_ATMLIB_CNNChannelStatistic;

/* Weights information structure for one channel  */
typedef struct
{
	R_ATMLIB_CNNChannelID           in_id;                          		/* Input channel ID 						                                    */
	R_ATMLIB_CNNChannelID		    out_id;                        			/* Output channel ID                                                            */
	int32_t                         value[R_ATMLIB_CNN_FILTER_SIZE];		/* weights value                                             					*/
}  R_ATMLIB_CNNChWeight;

/* Weights information structure for all channels */
typedef struct
{
	R_ATMLIB_CNNChWeight            ch_weight[R_ATMLIB_CNN_WEIGHT_MAX_CH]; 	/* Array of weights value detail information			            	        */
	uint32_t				        ch_cnt;                         		/* Channel count of the Weights (count of input-output channel)                 */
	R_ATMLIB_CNNDATAFormat			weight_format;							/* Weight Format				                                		            */
}  R_ATMLIB_CNNWeightInfo;


/* LUT information structure for all tables of one tile */
typedef struct
{
	int16_t                         value[R_ATMLIB_CNN_LUTTABLE_SIZE];		/* Values of one LUT table														*/
   	R_ATMLIB_CNNDATAFormat			lut_format;								/* LUT Format				                                		            */
}  R_ATMLIB_CNNLUTInfo;


/* Shift configuration structure for one tile */
typedef struct
{
	uint8_t                         dmao_frac;			            	/* Fractional position of output DMAO, only uesed when shifter appproach is fixed   */
	uint8_t							weight_frac;						/* Fractional position of weights						                           	*/
	uint8_t							bias_frac;							/* Fractional position of bias							                          	*/
	uint8_t							lut_frac;							/* Fractional position in LUT operation							                    */
}  R_ATMLIB_CNNShiftConfig;

/* Shift function type */
typedef R_ATMLIB_RETURN_VALUE (*R_ATMLIB_CNNShiftFunc)(
	R_ATMLIB_CLData					*cldata,						/* The pointer of R_ATMLIB_CLData structure				                                */
	const R_ATMLIB_CNNShiftConfig	*shift_cfg
);

/* Fraction Adjust function type */
typedef R_ATMLIB_RETURN_VALUE (*R_ATMLIB_CNNFractionAdjustFunc)(
	R_ATMLIB_CLData					*cldata,						/* The pointer of R_ATMLIB_CLData structure				                                */
	uint8_t							dyn_ocfp_count					/* The number of dynamic ocfps in the linear network	                                */
);

#endif /* R_ATMLIB_TYPES_H_ */

