/******************************************************************************
    Atomic Library
     Copyright (C)  2019-2020 Renesas Electronics Corporation.All rights reserved.
    
    [File] r_atmlib_prot.h

******************************************************************************/
#ifndef R_ATMLIB_PROT_H_
#define R_ATMLIB_PROT_H_

#include "r_atmlib_types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* CNN */
R_ATMLIB_RETURN_VALUE r_atmlib_InitializeCNNCL( R_ATMLIB_CLData *cldata, uint32_t *addr, uint32_t size, R_ATMLIB_CNNData *cnn_data );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNEndProcessing( R_ATMLIB_CLData *cldata, uint8_t trap_code );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNPictureProlog( R_ATMLIB_CLData *cldata, const uint8_t *ocfp, uint8_t dyn_ocfp_count);
R_ATMLIB_RETURN_VALUE r_atmlib_CNNPictureEpilog( R_ATMLIB_CLData *cldata, uint8_t dyn_ocfp_count, const R_ATMLIB_CNNFractionAdjustFunc frac_func );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNLayerProlog( R_ATMLIB_CLData *cldata, uint32_t maxsum_cnt_thres, uint32_t minsum_cnt_thres, R_ATMLIB_CNNSCRESMode scres_mode, uint16_t max_val_thres, uint16_t min_val_thres );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNLayerEpilog( R_ATMLIB_CLData *cldata, uint8_t uvt_index );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNReset( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNFlush( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetBaseAddress( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNChannelID *ch_id, const uint32_t *baddr, uint8_t baddr_cnt, R_ATMLIB_CNNChannelType ch_type );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupChannelDMAI( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAIParam *dmai_param, uint32_t *clofs_dmai );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupChannelDMAC( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMACParam *dmac_param, uint32_t *clofs_dmac );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupChannelDMAO( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMAOParam *dmao_param, uint32_t *clofs_dmao );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupARI( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNARIParam *ari_param, const R_ATMLIB_CNNChannelStatistic *channel_info );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupWeights( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMARFParam *dmarf_param, const R_ATMLIB_CNNWeightInfo *weight_info );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupLUT( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNDMARFParam *dmarf_param, const R_ATMLIB_CNNLUTInfo *lut_info );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupBiases ( R_ATMLIB_CLData *cldata, const int16_t *bias_vec);
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetupShifts( R_ATMLIB_CLData *cldata, const R_ATMLIB_CNNShiftConfig *shift_cfg, const R_ATMLIB_CNNShiftFunc shift_func );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartDMAI( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartDMAC( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartDMAO( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNStartTile( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_ADD( R_ATMLIB_CLData *cldata, uint16_t dst_start_add, uint16_t src_add, uint8_t dst_length );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SUB( R_ATMLIB_CLData *cldata, uint16_t dst_start_add, uint16_t src_add, uint8_t dst_length );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WR( R_ATMLIB_CLData *cldata, uint16_t dst_start_add, uint16_t src_add, uint8_t dst_length );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WRI( R_ATMLIB_CLData *cldata, uint16_t dst_start_add, int16_t imm, uint8_t dst_length );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_BRC( R_ATMLIB_CLData *cldata, R_ATMLIB_CNNBRCType cond, int16_t dst, uint16_t src_add );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_CLUVT( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SCRES( R_ATMLIB_CLData *cldata, R_ATMLIB_CNNSCRESMode mode );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SCCHK( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_TUV2UVT( R_ATMLIB_CLData *cldata, uint8_t uvt_index );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_CLW( R_ATMLIB_CLData *cldata);
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WPR( R_ATMLIB_CLData *cldata, uint16_t add, uint8_t n, const uint32_t *data );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_NOP( R_ATMLIB_CLData *cldata, uint8_t n );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_INT( R_ATMLIB_CLData *cldata, uint8_t code );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_TRAP( R_ATMLIB_CLData *cldata, uint8_t code );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SYNCS( R_ATMLIB_CLData *cldata, uint8_t syncs_code);
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_GOSUB( R_ATMLIB_CLData *cldata, uint32_t paddr );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_RET( R_ATMLIB_CLData *cldata );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_WUP( R_ATMLIB_CLData *cldata, uint32_t sync_id );
R_ATMLIB_RETURN_VALUE r_atmlib_CNN_SLP( R_ATMLIB_CLData *cldata, uint32_t sync_id );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetLabel( R_ATMLIB_CLData *cldata, const char *name );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNBRCtoLabel( R_ATMLIB_CLData *cldata, R_ATMLIB_CNNBRCType cond, const char *dst, uint16_t src_add );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNGOSUBtoLabel( R_ATMLIB_CLData *cldata, const char *dst );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNResolveLabels( R_ATMLIB_CLData *cldata, uint32_t paddr );
R_ATMLIB_RETURN_VALUE r_atmlib_CNNSetPerformCounter(R_ATMLIB_CLData *cldata,  const R_ATMLIB_CNNPerformCounter *perform_counter);


#ifdef __cplusplus
}
#endif

#endif /* R_ATMLIB_PROT_H_ */

