/******************************************************************************
    Command Generator
     Copyright (C)  2020 Renesas Electronics Corporation.All rights reserved.
    
    [File] cg_cnn_sub.h

******************************************************************************/
#include "r_atmlib_prot.h"
#include "r_cg_cnn.h"
#include "cg_cnn.h"

#define CG_ATMLIB_ASSERT_OK(func)                                                                               \
    {                                                                                                           \
        R_ATMLIB_RETURN_VALUE ret = func;                                                                       \
        if (R_ATMLIB_E_OK != ret)                                                                               \
        {                                                                                                       \
            printf("%d \"[  FAILED  ] == %s\" failed with %i!\n", __LINE__, #func, (int)ret);					\
            return ret;																							\
        }                                                                                                       \
		else                                                                               						\
        {                                                                                                       \
            printf("%d \"[    OK    ] == %s\" \n", __LINE__, #func);											\
        }                                                                                                       \
    }
/************************************************
 Static function prototype definitions
************************************************/
R_CG_CNN_STATIC void cg_CNNParseFlowCtrl( const R_CG_CNNNodeConfig* cnnNodeConfig, CG_CNNFlowContrl* cgCNNFlowCtrl );
R_CG_CNN_STATIC void cg_CNNParseDMAIParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMAIParam* dmaiParam);
R_CG_CNN_STATIC void cg_CNNParseDMACParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMACParam* dmacParam);
R_CG_CNN_STATIC void cg_CNNParseDMAOParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMAOParam* dmaoParam);
R_CG_CNN_STATIC void cg_CNNParseARIFormat(const R_ATMLIB_CNNDATAFormat inputFM, const R_ATMLIB_CNNDATAFormat outputFM, const R_ATMLIB_CNNDATAFormat weightFM, R_ATMLIB_CNNARIFormat* ariFormat);
R_CG_CNN_STATIC void cg_CNNParseARIParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNARIParam* ariParam);
R_CG_CNN_STATIC void cg_CNNParseWeightWPRParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNWeightInfo* weightInfo);
R_CG_CNN_STATIC void cg_CNNParseWeightDMARFParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMARFParam* weightDMARF);
R_CG_CNN_STATIC void cg_CNNPareLUTWPRParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNLUTInfo* lutInfo);
R_CG_CNN_STATIC void cg_CNNPareLUTDMARFParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMARFParam* lutDMARF);
R_CG_CNN_STATIC R_ATMLIB_RETURN_VALUE cg_CNNUserShiftFunc( R_ATMLIB_CLData* cldata, const R_ATMLIB_CNNShiftConfig *shift_cfg);

/******************************************************************************
 [ FuncName ]	cg_CNNCheckNodeConfig
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
 [ Returns ]	R_CG_OK                         : On success
                R_CG_NG_CNN_NODE_TYPE           : nodeType is out of range
                                                :       case -- nodeTyep > R_CG_CNN_LAYER_EPILOG_NODE;
                R_CG_NG_CNN_COREID              : coreID is out of range
                                                :       case -- coreID > R_CG_CNN_CORE3;
                R_CG_NG_CNN_INPUT_TYPE          : inputTyp is out of range
                                                :       case -- inputType > R_CG_CNN_DMAI_AND_DMAC;
                R_CG_NG_CNN_CH_MODE             : channel mode is out of range
                                                :       case 1 -- if inputType =  R_CG_CNN_DMAI / R_CG_CNN_DMAI_AND_DMAC, dmaiChannelMode > R_ATMLIB_CNN_DATA_END;
                                                        case 2 -- if inputType =  R_CG_CNN_DMAC / R_CG_CNN_DMAI_AND_DMAC, dmacChannelMode > R_ATMLIB_CNN_DATA_END;
                                                        case 3 -- dmaoChannelMode > R_ATMLIB_CNN_DATA_END;
                R_CG_CN_CNN_CH_CNT              : channel count is out of range
                                                :       case 1 -- if inputType =  R_CG_CNN_DMAI / R_CG_CNN_DMAI_AND_DMAC, dmaiMemoryInfo.channelCnt > R_ATMLIB_CNN_DMAI_MAX_CH;
                                                        case 2 -- if inputType =  R_CG_CNN_DMAC / R_CG_CNN_DMAI_AND_DMAC, dmacMemoryInfo.channelCnt > R_ATMLIB_CNN_DMAC_MAX_CH;
                                                        case 3 -- dmaoMemoryInfo.channelCnt > R_ATMLIB_CNN_DMAO_MAX_CH;
                R_CG_NG_CNN_DAMOMODE_N411SPMC   : combination of DMAO channel mode and next layer kernel 4*1x1 and SPMC
                                                :       case -- dmaoChannelMode = R_ATMLIB_CNN_DATA_SCMP && nextLayerSPMC411 = true
                R_CG_NG_CNN_BIASDATA_NULL       : bias data poniter is NULL
                                                :       case -- if enableBias = true, bias.data = NULL;
                R_CG_NG_CNN_BIASDATA_LEN        : bias data arrary length is out of range
                                                :       case 1 -- if enableBias = true, bias data array length dataLen > R_ATMLIB_CNN_ARI_MAX_CH;
                                                :       case 2 -- if enableBias = true, bias data array length dataLen < 1;
                R_CG_NG_CNN_TRANSFER_MODE       : weightTransferMode / lutTransferMode transfer mode is out of range
                                                :       case 1 -- weightTransferMode > R_CG_CNN_DMARF;
                                                :       case 2 -- lutTransferMode > R_CG_CNN_DMARF;
                R_CG_NG_CNN_WEIHGTDATA_NULL     : weight data pointer is NULL
                                                :       case   -- if inputType =  R_CG_CNN_DMAI / R_CG_CNN_DMAI_AND_DMAC and  weightTransferMode = R_CG_CNN_WPR, the weightWPRInfo.data = NULL;
                R_CG_NG_CNN_WEIHGTDATA_LEN      : weight data arrary length is out of range
                                                :       case 1 -- if inputType =  R_CG_CNN_DMAI / R_CG_CNN_DMAI_AND_DMAC and  weightTransferMode = R_CG_CNN_WPR, the weightWPRInfo.dataLen > 0x3600U;
                                                :       case 2 -- if inputType =  R_CG_CNN_DMAI / R_CG_CNN_DMAI_AND_DMAC and  weightTransferMode = R_CG_CNN_WPR, the weightWPRInfo.dataLen < 1;
                R_CG_NG_CNN_WEIHGTDATA_FM       : weight data format is out of range
                                                :       case   -- if inputType =  R_CG_CNN_DMAI / R_CG_CNN_DMAI_AND_DMAC and  weightTransferMode = R_CG_CNN_WPR, the weightWPRInfo.format > R_ATMLIB_CNN_DATAFM_END;
                R_CG_NG_CNN_NEXT_DMAIFM         : next tile dmai format is out of range
                                                :       case  -- if weightTransferMode = R_CG_CNN_DMARF, nextTileDMAIFM >= R_ATMLIB_CNN_DATAFM_END;
                R_CG_NG_CNN_NEXT_DMAOFM         : next tile dmao format is out of range
                                                :       case  -- if weightTransferMode = R_CG_CNN_DMARF, nextTileDMAOFM >= R_ATMLIB_CNN_DATAFM_END;
                R_CG_NG_CNN_NEXT_WEIGHTFM       : next tile weight format is out of range
                                                :       case  -- if weightTransferMode = R_CG_CNN_DMARF, nextTileWeightFM >= R_ATMLIB_CNN_DATAFM_END; 
                R_CG_NG_CNN_CUR_WEIGHTFM        : current tile weight format is out of range
                                                :       case  -- if weightTransferMode = R_CG_CNN_DMARF, curTileFormat >= R_ATMLIB_CNN_DATAFM_END; 
 [ Function ]   Check the cnnNodeConfig parameters
 [ Note ]		None
******************************************************************************/
R_CG_RETURN_VALUE cg_CNNCheckNodeConfig( const R_CG_CNNNodeConfig* cnnNodeConfig )
{
    R_CG_RETURN_VALUE ret;
    ret = R_CG_OK;
 
    if(cnnNodeConfig->cnnNodeType > R_CG_CNN_LAYER_EPILOG_NODE)
    {
        ret = R_CG_NG_CNN_NODE_TYPE;
    }
    else if(cnnNodeConfig->cnnCoreID > R_CG_CNN_CORE3 )
    {
        ret = R_CG_NG_CNN_COREID;
    }
    else if(cnnNodeConfig->inputType > R_CG_CNN_DMAI_AND_DMAC)
    {
        ret = R_CG_NG_CNN_INPUT_TYPE;
    }
    else if( (cnnNodeConfig->inputType == R_CG_CNN_DMAI || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) &&
             (cnnNodeConfig->dmaiChannelMode > R_ATMLIB_CNN_DATA_END )
    )
    {
        ret = R_CG_NG_CNN_CH_MODE;
    }
    else if( (cnnNodeConfig->inputType == R_CG_CNN_DMAC || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) &&
             (cnnNodeConfig->dmacChannelMode > R_ATMLIB_CNN_DATA_END )
    )
    {
        ret = R_CG_NG_CNN_CH_MODE;
    }
    else if(cnnNodeConfig->dmaoChannelMode > R_ATMLIB_CNN_DATA_END )
    {
        ret = R_CG_NG_CNN_CH_MODE;
    }
    else if((cnnNodeConfig->inputType == R_CG_CNN_DMAI || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) &&
            (cnnNodeConfig->dmaiMemoryInfo.channelCnt > R_ATMLIB_CNN_DMAI_MAX_CH)
    )
    {
        ret = R_CG_CN_CNN_CH_CNT;
    }
    else if((cnnNodeConfig->inputType == R_CG_CNN_DMAC || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) &&
            (cnnNodeConfig->dmacMemoryInfo.channelCnt > R_ATMLIB_CNN_DMAC_MAX_CH)
    )
    {
        ret = R_CG_CN_CNN_CH_CNT;
    }
    else if(cnnNodeConfig->dmaoMemoryInfo.channelCnt > R_ATMLIB_CNN_DMAO_MAX_CH)
    {
        ret = R_CG_CN_CNN_CH_CNT;
    }
    else if ((cnnNodeConfig->dmaoChannelMode == R_ATMLIB_CNN_DATA_SCMP) && 
             (cnnNodeConfig->nextLayerSPMC411 == true)
    )
    {
        ret = R_CG_NG_CNN_DAMOMODE_N411SPMC;
    }   
    else if((cnnNodeConfig->enableBias == true) && 
            (cnnNodeConfig->bias.data == NULL)
    )
    {
        ret = R_CG_NG_CNN_BIASDATA_NULL;
    }
    else if((cnnNodeConfig->enableBias == true) && 
            (cnnNodeConfig->bias.dataLen > R_ATMLIB_CNN_ARI_MAX_CH || cnnNodeConfig->bias.dataLen < 1U )
    )
    {
        ret = R_CG_NG_CNN_BIASDATA_LEN;
    }
    else if(cnnNodeConfig->lutTransferMode > R_CG_CNN_DMARF)
    {
        ret = R_CG_NG_CNN_TRANSFER_MODE;
    }
    else if(cnnNodeConfig->weightTransferMode > R_CG_CNN_DMARF )
    {
        ret = R_CG_NG_CNN_TRANSFER_MODE;
    }
    else
    {
        if(cnnNodeConfig->weightTransferMode == R_CG_CNN_WPR)
        {
            if((cnnNodeConfig->inputType == R_CG_CNN_DMAI || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) && 
                (cnnNodeConfig->weightWPRInfo.data == NULL)
            )
            {
                ret = R_CG_NG_CNN_WEIHGTDATA_NULL;
            }
            else if((cnnNodeConfig->inputType == R_CG_CNN_DMAI || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) && 
                    (cnnNodeConfig->weightWPRInfo.dataLen > 0x3600U || cnnNodeConfig->weightWPRInfo.dataLen < 1U)
            )
            {
                ret = R_CG_NG_CNN_WEIHGTDATA_LEN;
            }
            else if((cnnNodeConfig->inputType == R_CG_CNN_DMAI || cnnNodeConfig->inputType == R_CG_CNN_DMAI_AND_DMAC) && 
                    (cnnNodeConfig->weightWPRInfo.format > R_ATMLIB_CNN_DATAFM_END)
            )
            {
                ret = R_CG_NG_CNN_WEIHGTDATA_FM;
            }
            else
            {

            }
        }
        else
        {
            if(cnnNodeConfig->weightDMARFInfo.nextTileDMAOFM >= R_ATMLIB_CNN_DATAFM_END)
            {
                ret = R_CG_NG_CNN_NEXT_DMAOFM;
            }
            else if(cnnNodeConfig->weightDMARFInfo.nextTileDMAIFM >= R_ATMLIB_CNN_DATAFM_END)
            {
                ret = R_CG_NG_CNN_NEXT_DMAIFM;
            }
            else if(cnnNodeConfig->weightDMARFInfo.nextTileWeightFM >= R_ATMLIB_CNN_DATAFM_END)
            {
                ret = R_CG_NG_CNN_NEXT_WEIGHTFM;
            }
            else if(cnnNodeConfig->weightDMARFInfo.curTileFormat >= R_ATMLIB_CNN_DATAFM_END)
            {
                ret = R_CG_NG_CNN_CUR_WEIGHTFM;
            }
            else
            {

            }
        }
    }

    return ret;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseFlowCtrl
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                flowControl                     : output parameter -- The pointer to the CG_CNNFlowContrl structure
 [ Returns ]	None
 [ Function ]   Parse the flow control parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseFlowCtrl( const R_CG_CNNNodeConfig* cnnNodeConfig, CG_CNNFlowContrl* flowControl )
{
    flowControl->nodeType = cnnNodeConfig->cnnNodeType;
    flowControl->coreID = cnnNodeConfig->cnnCoreID;

    switch(cnnNodeConfig->inputType)
    {
    case R_CG_CNN_DMAI:
        flowControl->enableDMAI = true;
        flowControl->enableDMAC = false;
        break;
    case R_CG_CNN_DMAC:
        flowControl->enableDMAI = false;
        flowControl->enableDMAC = true;
        break;
    default:
        flowControl->enableDMAI = true;
        flowControl->enableDMAC = true;
        break;
    }

    flowControl->enableBias = cnnNodeConfig->enableBias;

    if (cnnNodeConfig->actiMode == R_ATMLIB_CNN_ACTFUNC_PELU ||
        cnnNodeConfig->actiMode == R_ATMLIB_CNN_ACTFUNC_ELU  ||
        cnnNodeConfig->actiMode == R_ATMLIB_CNN_ACTFUNC_SIGTANH
    )
    {
        flowControl->enableLUT =  true;        
    }
    else
    {
        flowControl->enableLUT = false;
    }
    flowControl->lutTransMode = (cnnNodeConfig->lutTransferMode == R_CG_CNN_WPR)? false : true;
    flowControl->weightTransMode = (cnnNodeConfig->weightTransferMode == R_CG_CNN_WPR)? false : true;

    flowControl->enableBaseaddr = false;

    return;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseDMAIParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                dmaiParam                       : output parameter -- The pointer to the R_ATMLIB_CNNDMAIParam structure
 [ Returns ]	None
 [ Function ]   Parse the dmai parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseDMAIParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMAIParam* dmaiParam)
{
    uint8_t loopCnt;
    loopCnt = 0U;

    dmaiParam->kernel_mode = cnnNodeConfig->kernelMode;
    dmaiParam->dil_conv = cnnNodeConfig->dilation;
    dmaiParam->data_mode = cnnNodeConfig->dmaiChannelMode;
    dmaiParam->ch_count = cnnNodeConfig->dmaiMemoryInfo.channelCnt;
    for( loopCnt = 0U; loopCnt < dmaiParam->ch_count; loopCnt++) // 濞夈劍鍓板Λ锟介弻顧﹉annel count 閸︺劍娓舵稉锟藉锟芥慨锟�
    {
        dmaiParam->ch_id[loopCnt] = R_ATMLIB_CNN_DMAI0 + loopCnt;
    }

    if (cnnNodeConfig->dmaiChannelMode == R_ATMLIB_CNN_DATA_SPMC)
    {
        dmaiParam->param_count = 1U;
    }
    else
    {
        dmaiParam->param_count = cnnNodeConfig->dmaiMemoryInfo.channelCnt;
        if(dmaiParam->param_count > 1U)
        {
            dmaiParam->channel_offset = dmaiParam->dmai_ch[1].ptr - dmaiParam->dmai_ch[0].ptr;
        }
    }

    for( loopCnt = 0U; loopCnt < dmaiParam->param_count; loopCnt++)
    {
        dmaiParam->dmai_ch[loopCnt].image_x         = cnnNodeConfig->dmaiMemoryInfo.imageWidth;
        dmaiParam->dmai_ch[loopCnt].image_y         = cnnNodeConfig->dmaiMemoryInfo.imageHeigh;
        dmaiParam->dmai_ch[loopCnt].image_stride    = cnnNodeConfig->dmaiMemoryInfo.imageStride;
        dmaiParam->dmai_ch[loopCnt].image_format    = cnnNodeConfig->dmaiMemoryInfo.format;
        dmaiParam->dmai_ch[loopCnt].dmai_frac       = cnnNodeConfig->dmaiMemoryInfo.frac;
        dmaiParam->dmai_ch[loopCnt].ptr             = cnnNodeConfig->dmaiMemoryInfo.imageStartAddr[loopCnt];
        dmaiParam->dmai_ch[loopCnt].pad_t           = cnnNodeConfig->padding.n;
        dmaiParam->dmai_ch[loopCnt].pad_b           = cnnNodeConfig->padding.s;
        dmaiParam->dmai_ch[loopCnt].pad_l           = cnnNodeConfig->padding.w;
        dmaiParam->dmai_ch[loopCnt].pad_r           = cnnNodeConfig->padding.e;
        dmaiParam->dmai_ch[loopCnt].repeat_skip_x   = cnnNodeConfig->dmaiSkipRepeat.x;
        dmaiParam->dmai_ch[loopCnt].repeat_skip_y   = cnnNodeConfig->dmaiSkipRepeat.y;
        dmaiParam->dmai_ch[loopCnt].mag_type        = cnnNodeConfig->dmaiSkipRepeat.type;
        dmaiParam->dmai_ch[loopCnt].start_pos       = cnnNodeConfig->startPosition;
        dmaiParam->dmai_ch[loopCnt].stride_x        = cnnNodeConfig->convolutionStrideX;
        dmaiParam->dmai_ch[loopCnt].stride_y        = cnnNodeConfig->convolutionStrideY;
    }

    return;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseDMACParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                dmacParam                       : output parameter -- The pointer to the R_ATMLIB_CNNDMACParam structure
 [ Returns ]	None
 [ Function ]   Parse the dmac parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseDMACParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMACParam* dmacParam)
{
    uint8_t loopCnt;
    loopCnt = 0U;

    dmacParam->data_mode = cnnNodeConfig->dmacChannelMode;
    dmacParam->ch_count = cnnNodeConfig->dmaiMemoryInfo.channelCnt;
    for( loopCnt = 0U; loopCnt < dmacParam->ch_count; loopCnt++) // 濞夈劍鍓板Λ锟介弻顧﹉annel count 閸︺劍娓舵稉锟藉锟芥慨锟�
    {
        dmacParam->ch_id[loopCnt] = R_ATMLIB_CNN_DMAI0 + loopCnt;
    }

    if (cnnNodeConfig->dmaiChannelMode == R_ATMLIB_CNN_DATA_SPMC)
    {
        dmacParam->param_count = 1U;
    }
    else
    {
        dmacParam->param_count = cnnNodeConfig->dmaiMemoryInfo.channelCnt;
        if(dmacParam->param_count > 1U)
        {
            dmacParam->channel_offset = dmacParam->dmac_ch[1].ptr - dmacParam->dmac_ch[0].ptr;
        }
    }

    for( loopCnt = 0U; loopCnt < dmacParam->param_count; loopCnt++)
    {
        dmacParam->dmac_ch[loopCnt].image_x         = cnnNodeConfig->dmacMemoryInfo.imageWidth;
        dmacParam->dmac_ch[loopCnt].image_y         = cnnNodeConfig->dmacMemoryInfo.imageHeigh;
        dmacParam->dmac_ch[loopCnt].image_stride    = cnnNodeConfig->dmacMemoryInfo.imageStride;
        dmacParam->dmac_ch[loopCnt].image_format    = cnnNodeConfig->dmacMemoryInfo.format;
        dmacParam->dmac_ch[loopCnt].dmac_frac       = cnnNodeConfig->dmacMemoryInfo.frac;
        dmacParam->dmac_ch[loopCnt].ptr             = cnnNodeConfig->dmacMemoryInfo.imageStartAddr[loopCnt];
        dmacParam->dmac_ch[loopCnt].repeat_skip_x   = cnnNodeConfig->dmacSkipRepeat.x;
        dmacParam->dmac_ch[loopCnt].repeat_skip_y   = cnnNodeConfig->dmacSkipRepeat.y;
        dmacParam->dmac_ch[loopCnt].mag_type        = cnnNodeConfig->dmacSkipRepeat.type;
    }

    return;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseDMAOParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                dmaoParam                       : output parameter -- The pointer to the R_ATMLIB_CNNDMAOParam structure
 [ Returns ]	None
 [ Function ]   Parse the dmao parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseDMAOParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMAOParam* dmaoParam)
{
    uint8_t loopCnt;
    loopCnt = 0U;

    dmaoParam->data_mode       = cnnNodeConfig->dmacChannelMode;
    dmaoParam->image_x         = cnnNodeConfig->dmaoMemoryInfo.imageWidth;
    dmaoParam->image_y         = cnnNodeConfig->dmaoMemoryInfo.imageHeigh;
    dmaoParam->image_stride    = cnnNodeConfig->dmaoMemoryInfo.imageStride;
    dmaoParam->image_format    = cnnNodeConfig->dmaoMemoryInfo.format;

    dmaoParam->ch_count = cnnNodeConfig->dmaoMemoryInfo.channelCnt;
    for( loopCnt = 0U; loopCnt < dmaoParam->ch_count; loopCnt++) // 濞夈劍鍓板Λ锟介弻顧﹉annel count 閸︺劍娓舵稉锟藉锟芥慨锟�
    {
        dmaoParam->ch_id[loopCnt] = R_ATMLIB_CNN_DMAI0 + loopCnt;
    }

    if (cnnNodeConfig->dmaiChannelMode == R_ATMLIB_CNN_DATA_SPMC)
    {
        dmaoParam->param_count = 1U;
    }
    else
    {
        dmaoParam->param_count = cnnNodeConfig->dmaoMemoryInfo.channelCnt;
        if(dmaoParam->param_count > 1U)
        {
            dmaoParam->channel_offset = dmaoParam->ptr[1] - dmaoParam->ptr[0];
        }
    }

    for( loopCnt = 0U; loopCnt < dmaoParam->param_count; loopCnt++)
    {
        dmaoParam->ptr[loopCnt] = cnnNodeConfig->dmaoMemoryInfo.imageStartAddr[loopCnt];
    }

    dmaoParam->next_layer_kernel_mode = (cnnNodeConfig->nextLayerSPMC411 == true ) ? R_ATMLIB_CNN_KERNEL_4_1_1 : R_ATMLIB_CNN_KERNEL_1_5_5;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseARIFormat
 [ Params ]		inputFM					: input parameter -- The dmai / dmac format
                outputFM                : input parameter -- The damo format 
                weightFM                : input parameter -- The weight format
                ariFM                   : output parameter -- The ari format
 [ Returns ]	None
 [ Function ]   Parse the ari format parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseARIFormat(  const R_ATMLIB_CNNDATAFormat inputFM, 
                                            const R_ATMLIB_CNNDATAFormat outputFM,
                                            const R_ATMLIB_CNNDATAFormat weightFM,
                                            R_ATMLIB_CNNARIFormat* ariFM
)
{
    uint8_t 				inputFM_index;
	uint8_t 				weightFM_index;

    if(inputFM == R_ATMLIB_CNN_DATAFM_4S)
    {
        *ariFM = R_ATMLIB_CNN_DATAFM_8S;
    }
    else if(inputFM == R_ATMLIB_CNN_DATAFM_8S)
    {
        *ariFM = R_ATMLIB_CNN_DATAFM_8S;
    }
    else if(inputFM == R_ATMLIB_CNN_DATAFM_16S)
    {
        *ariFM = R_ATMLIB_CNN_DATAFM_16S;
    }
    else if(inputFM == R_ATMLIB_CNN_DATAFM_16U)
    {
        *ariFM = R_ATMLIB_CNN_DATAFM_16U;
    }
    else if(weightFM == R_ATMLIB_CNN_DATAFM_16S)
    {
        *ariFM = R_ATMLIB_CNN_DATAFM_16S;
    }
    else if(weightFM == R_ATMLIB_CNN_DATAFM_16U)
    {
        *ariFM = R_ATMLIB_CNN_DATAFM_16U;
    }
    else
    {
        R_CG_CNN_STATIC const R_ATMLIB_CNNARIFormat cgCNNARIFormatTbl[2][4][6] =
        {   /* input 4U */
            {   /* ouputFM:16U                   ouputFM:16S                  ouputFM:8U                ouputFM:8S                  ouputFM: 4U                ouputFM: 4S */    
                {  R_ATMLIB_CNN_ARIFORMAT_16U,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8U, R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8U, R_ATMLIB_CNN_ARIFORMAT_8S },  /* weightFM: 8U */
                {  R_ATMLIB_CNN_ARIFORMAT_16S,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8S },  /* weightFM: 8S */
                {  R_ATMLIB_CNN_ARIFORMAT_16U,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8U, R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8U, R_ATMLIB_CNN_ARIFORMAT_8S },  /* weightFM: 4U */
                {  R_ATMLIB_CNN_ARIFORMAT_16S,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8S, R_ATMLIB_CNN_ARIFORMAT_8S },  /* weightFM: 4S */
            },          
            /* input 8U */
            {	/* ouputFM:16U                   ouputFM:16S                  ouputFM:8U                  ouputFM:8S                   ouputFM: 4U                 ouputFM: 4S */    
                {  R_ATMLIB_CNN_ARIFORMAT_16U,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8U,  R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8U,  R_ATMLIB_CNN_ARIFORMAT_16S },  /* weightFM: 8U */
                {  R_ATMLIB_CNN_ARIFORMAT_16S,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_16S, R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_16S, R_ATMLIB_CNN_ARIFORMAT_16S },  /* weightFM: 8S */
                {  R_ATMLIB_CNN_ARIFORMAT_16U,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8U,  R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_8U,  R_ATMLIB_CNN_ARIFORMAT_16S },  /* weightFM: 4U */
                {  R_ATMLIB_CNN_ARIFORMAT_16S,   R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_16S, R_ATMLIB_CNN_ARIFORMAT_16S,  R_ATMLIB_CNN_ARIFORMAT_16S, R_ATMLIB_CNN_ARIFORMAT_16S },  /* weightFM: 4S */
            }
        };

        inputFM_index = (inputFM == R_ATMLIB_CNN_DATAFM_4U)? 0U : 1U;

        if(weightFM == R_ATMLIB_CNN_DATAFM_8U )
            weightFM_index = 0U;
        else if(weightFM == R_ATMLIB_CNN_DATAFM_8S)
            weightFM_index = 1U;
        else if(weightFM == R_ATMLIB_CNN_DATAFM_4U)
            weightFM_index = 2U;
        else
            weightFM_index = 3U;

        *ariFM = cgCNNARIFormatTbl[inputFM_index][weightFM_index][outputFM];
    }
}
/******************************************************************************
 [ FuncName ]	cg_CNNParseARIParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                ariParam                        : output parameter -- The pointer to the R_ATMLIB_CNNARIParam
 [ Returns ]	None
 [ Function ]   Parse the ari parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseARIParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNARIParam* ariParam)
{
    R_ATMLIB_CNNDATAFormat inputFormat;
    R_ATMLIB_CNNDATAFormat weightFormat;

    ariParam->ari_cnt = cnnNodeConfig->convChannels;

    for(uint8_t i = 0; i < ariParam->ari_cnt; i++)
    {
        ariParam->ch_id[i] = R_ATMLIB_CNN_ARI0 + i;
    }

    inputFormat = (cnnNodeConfig->inputType == R_CG_CNN_DMAI)? cnnNodeConfig->dmaiMemoryInfo.format : cnnNodeConfig->dmacMemoryInfo.format;
    weightFormat = (cnnNodeConfig->weightTransferMode == R_CG_CNN_WPR)? cnnNodeConfig->weightWPRInfo.format : cnnNodeConfig->weightDMARFInfo.curTileFormat;

    cg_CNNParseARIFormat( inputFormat,
                          cnnNodeConfig->dmaoMemoryInfo.format,
                          weightFormat,
                          &ariParam->ari_format);

    if(cnnNodeConfig->weightTransferMode == R_CG_CNN_DMARF)
    {
        cg_CNNParseARIFormat( cnnNodeConfig->weightDMARFInfo.nextTileDMAIFM,
                              cnnNodeConfig->weightDMARFInfo.nextTileDMAOFM,
                              cnnNodeConfig->weightDMARFInfo.nextTileWeightFM,
                              &ariParam->next_ari_format);
    }

    ariParam->acti_mode         = cnnNodeConfig->actiMode;
    ariParam->lut_lin_int       = cnnNodeConfig->lutLinearInter;
    ariParam->pad_mode          = cnnNodeConfig->padding.mode;
    ariParam->bias_en           = (cnnNodeConfig->enableBias == true) ?  R_ATMLIB_CNN_ENABLE : R_ATMLIB_CNN_DISABLE;
    ariParam->xypool_mode       = cnnNodeConfig->xyPool.mode;
    ariParam->xpool             = cnnNodeConfig->xyPool.x;
    ariParam->ypool             = cnnNodeConfig->xyPool.y;
    ariParam->cpool_mode        = cnnNodeConfig->channelPool.mode;
    ariParam->label_info        = cnnNodeConfig->channelPool.labelNo;
    ariParam->maxout_group      = cnnNodeConfig->channelPool.maxoutGroup;
    ariParam->sftm_round_mode   = R_ATMLIB_CNN_DISABLE;
    ariParam->cpool_ui          = 0U;
    ariParam->upper_bound       = cnnNodeConfig->upperBound;				
    ariParam->lower_bound       = cnnNodeConfig->lowerBound;
    ariParam->mult_val          = cnnNodeConfig->multValue;					
    ariParam->mult_frac         = cnnNodeConfig->multFrac;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseWeightWPRParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                weightInfo                      : output parameter -- The pointer to the R_ATMLIB_CNNWeightInfo structure
 [ Returns ]	None
 [ Function ]   Parse the weight WPR parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseWeightWPRParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNWeightInfo* weightInfo)
{
    return;
}
/******************************************************************************
 [ FuncName ]	cg_CNNParseWeightDMARFParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                weightDMARF                     : output parameter -- The pointer to the R_ATMLIB_CNNDMARFParam
 [ Returns ]	None
 [ Function ]   Parse the weight DMARF parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNParseWeightDMARFParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMARFParam* weightDMARF)
{
    return;
}
/******************************************************************************
 [ FuncName ]	cg_CNNPareLUTWPRParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                lutInfo                         : output parameter -- The pointer to the R_ATMLIB_CNNLUTInfo structure
 [ Returns ]	None
 [ Function ]   Parse the LUT WPR parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNPareLUTWPRParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNLUTInfo* lutInfo)
{
    return;
}
/******************************************************************************
 [ FuncName ]	cg_CNNPareLUTDMARFParam
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                lutDMARF                        : output parameter -- The pointer to the R_ATMLIB_CNNDMARFParam structure
 [ Returns ]	None
 [ Function ]   Parse the LUT DMARF parameters
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC void cg_CNNPareLUTDMARFParam(const R_CG_CNNNodeConfig* cnnNodeConfig, R_ATMLIB_CNNDMARFParam* lutDMARF)
{
    return;
}

/******************************************************************************
 [ FuncName ]	cg_CNNUserShiftFunc
 [ Params ]		cldata					    : output parameter -- The pointer to the R_ATMLIB_CLData structure
                shift_cfg                   : input parameter -- The pointer to the R_ATMLIB_CNNShiftConfig
 [ Returns ]	R_ATMLIB_E_OK	            : On success
                R_ATMLIB_E_NG	            : Error occur			    
                                            :       case 1 -- (mac_shift < 0) (mac_shift = (int16_t)(shift_cfg->weight_frac + cldata->cnn_data->dmai_frac[0] - shift_cfg->dmao_frac)
                                            :       case 2 -- (carry_shift < 0) (mac_shift + shift_cfg->dmao_frac - cldata->cnn_data->dmac_frac[0])
                                            :       case 3 -- (bias_shift < 0) (shift_cfg->weight_frac + cldata->cnn_data->dmai_frac[0] - shift_cfg->bias_frac)		
                R_ATMLIB_E_NG_ARG1			: NULL pointer errror
											:		case 1 --  The argument cldata is NULL
													case 2 --  Value of the members of the argument cldata is out of range (cur_addr or top_addr or cnn_data is NULL)
				R_ATMLIB_E_NG_ARG2			: The argument dst_start_add is out of range
											:		case	-- The argument  dst_start_add is out of range (<0x800 or > 0x9FF)
				R_ATMLIB_E_NG_ARG3			: The argument src_add is out of range
											:		case	-- The argument imm is out of range  (<-256  or  >255)
				R_ATMLIB_E_NG_ARG4			: The argument dst_length is out of range
											:		case	-- The argument dst_length is out of range  ( <1  or  > 0x3F )
				R_ATMLIB_E_NG_CLTYPE		: cltype of cldata is error
											:		case	-- Value of the member cltype of the argument cldata is not R_ATMLIB_CLTYPE_CNN
				R_ATMLIB_E_NG_STATE			: state of cldata is error
											:		case	-- The state of this library is Uninitialized State or Finalized State
				R_ATMLIB_E_NG_CLSIZE		: lack of CL area
											:		case1	-- Value of the member clsize of the argument cldata is out of range(<1 or >R_ATMLIB_MAX_CL_LENGTH)
    										:		case2	-- Number of write elements is bigger than number of remain cldata elements"	
 [ Function ]   CNN User Shift Function
 [ Note ]		None
******************************************************************************/
R_CG_CNN_STATIC R_ATMLIB_RETURN_VALUE cg_CNNUserShiftFunc( R_ATMLIB_CLData* cldata, const R_ATMLIB_CNNShiftConfig *shift_cfg)
{
    R_ATMLIB_RETURN_VALUE ret;
    ret = R_ATMLIB_E_OK;

    int16_t mac_shift = (int16_t)(shift_cfg->weight_frac + cldata->cnn_data->dmai_frac[0] - shift_cfg->dmao_frac);
    int16_t carry_shift = (int16_t)(mac_shift + shift_cfg->dmao_frac - cldata->cnn_data->dmac_frac[0]);
    int16_t bias_shift = (int16_t)(shift_cfg->weight_frac + cldata->cnn_data->dmai_frac[0] - shift_cfg->bias_frac);

    if ((mac_shift < 0) || (carry_shift < 0) || (bias_shift < 0))
    {
        ret = R_ATMLIB_E_NG;
    }
    else
    {
        ret = r_atmlib_CNN_WRI(cldata, 0x200CU / 4U, mac_shift, 1);
        if(ret == R_ATMLIB_E_OK)
        {
            ret = r_atmlib_CNN_WRI(cldata, 0x2004U / 4U, carry_shift, 1);
            if(ret == R_ATMLIB_E_OK)
            {
                ret = r_atmlib_CNN_WRI(cldata, 0x2008U / 4U, bias_shift, 1);
            }
        }
    }

    return ret;
}

/******************************************************************************
 [ FuncName ]	cg_CNNParseNode
 [ Params ]		cnnNodeConfig					: input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                cnnipConfig                     : output parameter -- The pointer to the CG_CNNIPConfig structure
 [ Returns ]	None
 [ Function ]   Parse the cnnNodeConfig into the cnnipConfig
 [ Note ]		None
******************************************************************************/
void cg_CNNParseNode( const R_CG_CNNNodeConfig* cnnNodeConfig, CG_CNNIPConfig* cnnipConfig )
{
    uint8_t loopCnt;

    loopCnt = 0U;

    cg_CNNParseFlowCtrl(cnnNodeConfig, &cnnipConfig->flowControl);

    if(cnnipConfig->flowControl.enableDMAI == true)
    {
        cg_CNNParseDMAIParam(cnnNodeConfig, &cnnipConfig->dmaiParam);
    }

    if(cnnipConfig->flowControl.enableDMAC == true)
    {
        cg_CNNParseDMACParam(cnnNodeConfig, &cnnipConfig->dmacParam);
    }

    if( (cnnNodeConfig->weightTransferMode == R_CG_CNN_WPR) && 
        (cnnipConfig->flowControl.enableDMAI == true)
    )
    {
        cg_CNNParseWeightWPRParam(cnnNodeConfig, &cnnipConfig->weightInfo);
        cnnipConfig->shiftConfig.weight_frac = cnnNodeConfig->weightWPRInfo.frac;
    }

    if(cnnNodeConfig->weightTransferMode == R_CG_CNN_DMARF)
    {
        cg_CNNParseWeightDMARFParam(cnnNodeConfig,&cnnipConfig->weightDMARF);
        cnnipConfig->shiftConfig.weight_frac = cnnNodeConfig->weightDMARFInfo.frac;
    }

    cg_CNNParseDMAOParam(cnnNodeConfig, &cnnipConfig->dmaoParam);
    cnnipConfig->shiftConfig.dmao_frac   = cnnNodeConfig->dmaoMemoryInfo.frac;

    cg_CNNParseARIParam(cnnNodeConfig, &cnnipConfig->ariParam);

    if(cnnipConfig->flowControl.enableBias == true)
    {
        for(loopCnt = 0; loopCnt < cnnNodeConfig->bias.dataLen; loopCnt++)
        {
            cnnipConfig->biasData[loopCnt] = cnnNodeConfig->bias.data[loopCnt];
        }
        cnnipConfig->shiftConfig.bias_frac = cnnNodeConfig->bias.frac;
    }

    if(cnnipConfig->flowControl.enableLUT == true)
    {
        if(cnnipConfig->flowControl.lutTransMode == false)
        {
            cg_CNNPareLUTWPRParam(cnnNodeConfig, &cnnipConfig->lutInfo);
            cnnipConfig->shiftConfig.lut_frac = cnnNodeConfig->lutWPRInfo.frac;
        }
        else
        {
            cg_CNNPareLUTDMARFParam(cnnNodeConfig, &cnnipConfig->lutDMARF);
            cnnipConfig->shiftConfig.lut_frac = cnnNodeConfig->lutDMARFInfo.frac;
        }
    }

    cnnipConfig->shiftFunc = cg_CNNUserShiftFunc;

    return;
}

/******************************************************************************
* [ FuncName ]	cg_CNNProcessNode
* [ Params ]	cnnipConfig					  : input parameter -- The pointer to the CG_CNNIPConfig structure
*               clData                        : output parameter -- The pointer to the R_ATMLIB_CLData structure
* [ Returns ]	Please refer to the atmlib return value
* [ Function ]  Process the node and call the atmlib APIs to generate the command list
* [ Note ]		None
******************************************************************************/
R_ATMLIB_RETURN_VALUE cg_CNNProcessNode( const CG_CNNIPConfig* cnnipConfig, R_ATMLIB_CLData* clData)
{
	R_ATMLIB_RETURN_VALUE atmlibRet;
	atmlibRet = R_ATMLIB_E_OK;

    if(cnnipConfig->flowControl.nodeType == R_CG_CNN_PICTURE_PROLOG_NODE)
    {
    	/*! r_atmlib_InitializeCNNCL */
        CG_ATMLIB_ASSERT_OK(r_atmlib_InitializeCNNCL(clData, clData->top_addr, clData->size, clData->cnn_data));
        /*! r_atmlib_CNNPictureProlog */
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNPictureProlog(clData, NULL, 0U));
       	/*! r_atmlib_CNNLayerProlog */
       	CG_ATMLIB_ASSERT_OK(r_atmlib_CNNLayerProlog(clData ,0U ,0U ,R_ATMLIB_CNN_SCRESMODE_0 ,0U ,0U));
    }

    /*! @brief 1st Tile*/
    if(cnnipConfig->flowControl.nodeType == R_CG_CNN_PICTURE_PROLOG_NODE)
    {
        /*! @brief Transfer weight by DMARF*/
        if(cnnipConfig->flowControl.weightTransMode == true)
        {
            /*! r_atmlib_CNNSetupARI */
            CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupARI( clData, &cnnipConfig->ariParam, NULL));
        	//printf("[   Transfer weight by DMARF  ]\n");
            /*! r_atmlib_CNNSetupWeights */
            CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupWeights( clData ,&cnnipConfig->weightDMARF ,NULL));
        }
    }
    /*! r_atmlib_CNNSetupARI */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupARI( clData ,&cnnipConfig->ariParam ,NULL));

    if(cnnipConfig->flowControl.enableDMAI == true)
    {
        /*! r_atmlib_CNNSetupChannelDMAI*/
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupChannelDMAI( clData ,&cnnipConfig->dmaiParam ,cnnipConfig->clofsDMAI));
    }

    if(cnnipConfig->flowControl.enableDMAC == true)
    {
        /*! r_atmlib_CNNSetupChannelDMAI */
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupChannelDMAC( clData ,&cnnipConfig->dmacParam ,cnnipConfig->clofsDMAC));
    }

    /*! @brief Transfer weight by WPR*/
    if(cnnipConfig->flowControl.weightTransMode == false)
    {
    	//printf("[   Transfer weight by WPR  ]\n");
        /*! r_atmlib_CNNSetupWeights */
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupWeights( clData, NULL, &cnnipConfig->weightInfo));
    }

    if(cnnipConfig->flowControl.enableBias == true)
    {
        /*! r_atmlib_CNNSetupBiases */
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupBiases( clData ,cnnipConfig->biasData));
    }

    /*! r_atmlib_CNNSetupShifts */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupShifts( clData ,&cnnipConfig->shiftConfig ,cnnipConfig->shiftFunc));


    if((cnnipConfig->flowControl.enableLUT == true)&&(cnnipConfig->flowControl.lutTransMode == true))
    {
        /*! r_atmlib_CNNSetupLUT DMARF*/
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupLUT( clData, &cnnipConfig->lutDMARF, NULL));
    }
    else
    {
    	/*! r_atmlib_CNNSetupLUT WPR*/
    	CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupLUT( clData, NULL, &cnnipConfig->lutInfo));
    }

    /*! r_atmlib_CNNStartDMAI */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNStartDMAI(clData));

    /*! r_atmlib_CNNStartDMAC */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNStartDMAC(clData));

    /*! r_atmlib_CNNSetupChannelDMAO */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupChannelDMAO( clData ,&cnnipConfig->dmaoParam ,cnnipConfig->clofsDMAO));

    /*! r_atmlib_CNNStartDMAO */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNStartDMAO(clData));

    /*! r_atmlib_CNNStartTile */
    CG_ATMLIB_ASSERT_OK(r_atmlib_CNNStartTile(clData));

    if(cnnipConfig->flowControl.nodeType != R_CG_CNN_PICTURE_EPILOG_NODE)
    {
        /*! @brief Transfer weight by DMARF*/
        if(cnnipConfig->flowControl.weightTransMode == true)
        {
        	//printf("[   Transfer weight by DMARF  ]\n");
            /*! r_atmlib_CNNSetupWeights */
            CG_ATMLIB_ASSERT_OK(r_atmlib_CNNSetupWeights( clData ,&cnnipConfig->weightDMARF, NULL));
        }
    }
    
    if( cnnipConfig->flowControl.nodeType == R_CG_CNN_PICTURE_EPILOG_NODE)
    {
    	/*! r_atmlib_CNNLayerEpilog */
    	CG_ATMLIB_ASSERT_OK(r_atmlib_CNNLayerEpilog( clData, 1U ));
    	/*! r_atmlib_CNNPictureEpilog */
    	CG_ATMLIB_ASSERT_OK(r_atmlib_CNNPictureEpilog( clData, 1U, NULL));
    	/*! r_atmlib_CNNEndProcessing */
        CG_ATMLIB_ASSERT_OK(r_atmlib_CNNEndProcessing( clData, 0U ));
    }

    return atmlibRet;
}

/******************************************************************************
 [ FuncName ]	cg_CNNWriteCL
 [ Params ]		coreID                : input parameter -- cnn core ID cnn0/cnn1/cnn2/cnn3 
                clData                : input parameter -- The pointer to the R_ATMLIB_CLData structure
 [ Returns ]	R_CG_NG_OPEN_FILE     : fail to open file
                                      : case -- fopen() return error.
                R_CG_NG_WRITE_FILE    : fail to write file   
                                      : case -- fwirte() return error.
                R_CG_NG_CLOSE_FILE    : fail to close file  
                                      : case -- fclose() return error.        
 [ Function ]   Write the command list to the binary file.
 [ Note ]		None
******************************************************************************/
R_CG_RETURN_VALUE cg_CNNWriteCL( const R_CG_CNNCoreID coreID, const R_ATMLIB_CLData* clData)
{
    R_CG_RETURN_VALUE ret;
    FILE* fp;
    FILE* fp_txt;

    ret = R_CG_OK;

    switch (coreID)
    {
    case R_CG_CNN_CORE0 :
        fp = fopen("cl_cnn0.bin", "wb");
        fp_txt = fopen("cl_cnn0.txt", "w");
        break;
    case R_CG_CNN_CORE1 :
        fp = fopen("cl_cnn1.bin", "wb");
        fp_txt = fopen("cl_cnn1.txt", "w");
        break;
    case R_CG_CNN_CORE2 :
        fp = fopen("cl_cnn2.bin", "wb");
        fp_txt = fopen("cl_cnn2.txt", "w");
        break;
    case R_CG_CNN_CORE3 :
        fp = fopen("cl_cnn3.bin", "wb");
        fp_txt = fopen("cl_cnn3.txt", "w");
        break;
    default:
        break;
    }
    if((NULL == fp)||(NULL == fp_txt))
    {
    	ret = R_CG_NG_OPEN_FILE;
    }
    else
    {
    	/* (NULL != fp)||(NULL != fp_txt) */
        if (fp)
        {
            if(fwrite(clData->top_addr, ((uintptr_t)(clData->cur_addr) - (uintptr_t)(clData->top_addr)), 1, fp) < 0)
            {
            	ret = R_CG_NG_WRITE_FILE;
            }
            if(fclose(fp) < 0)
            {
            	ret = R_CG_NG_CLOSE_FILE;
            }
        }

        if (fp_txt)
        {
            uint32_t* ptr = clData->top_addr;
            do
            {
            	if(fprintf(fp_txt, "0x%08x\n", (unsigned int)*ptr++) < 0)
            	{
            		ret = R_CG_NG_WRITE_FILE;
            	}
            } while (ptr <= clData->cur_addr);
            if(fclose(fp_txt) < 0)
            {
            	ret = R_CG_NG_CLOSE_FILE;
            }
        }

    }

    return ret;
}
