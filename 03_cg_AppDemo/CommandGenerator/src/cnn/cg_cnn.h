/******************************************************************************
    Atomic Library
     Copyright (C)  2020 Renesas Electronics Corporation.All rights reserved.
    
    [File] cg_cnn.h

******************************************************************************/
#ifndef CG_CNN_H_
#define CG_CNN_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "r_atmlib_types.h"

/************************************************
    Switching definitions
************************************************/
#ifndef R_CG_BUILD_UNIT_TEST
#define R_CG_CNN_STATIC static
#else
#define R_CG_CNN_STATIC
#endif

/************************************************
	Structure definitions 	
************************************************/

typedef struct
{
	R_CG_CNNNodeType    		nodeType;                                	/* CNN node type                                        */              
    R_CG_CNNCoreID	            coreID;		                             	/* CNN core type                                        */
	bool						enableDMAI;                              	/* enable DMAI                                          */  
	bool						enableDMAC;                              	/* enable DMAC                                          */  
	bool						enableBias;                              	/* enable BIAS                                          */  
	bool						enableBaseaddr;                          	/* enable base address                                  */      
	bool						enableLUT;                               	/* enable lut table                                     */
	bool						lutTransMode;	    	                 	/* lut transfer mode: false: WPR true: DMARF            */              
	bool						weightTransMode;	                     	/* weight transfer mode: false WPR true: DMARF          */          
}CG_CNNFlowContrl;                                                     

typedef struct
{
	CG_CNNFlowContrl			flowControl;								/* flow control information                             */
	R_ATMLIB_CNNDMAIParam       dmaiParam;                             		/* DMAI parameters                                      */
	uint32_t					clofsDMAI[R_ATMLIB_CNN_DMAI_MAX_CH];   		/* The pointer to CL DMAI image address                 */
	R_ATMLIB_CNNDMACParam       dmacParam;                             		/* DMAC parameters                                      */
	uint32_t					clofsDMAC[R_ATMLIB_CNN_DMAC_MAX_CH];   		/* The pointer to CL DMAC image address                 */
	R_ATMLIB_CNNDMAOParam      	dmaoParam;                              	/* DMAO parameters                                      */
	uint32_t					clofsDMAO[R_ATMLIB_CNN_DMAO_MAX_CH];    	/* The pointer to CL DMAO image address                 */
	R_ATMLIB_CNNARIParam        ariParam;                              		/* ARI parameters                                       */
	R_ATMLIB_CNNWeightInfo      weightInfo;                            		/* weight information by WPR                            */
	R_ATMLIB_CNNDMARFParam     	weightDMARF;                           		/* weight information by DMARF                          */
	R_ATMLIB_CNNShiftConfig		shiftConfig;                           		/* Pointer to shift configuration parameter             */
	R_ATMLIB_CNNShiftFunc 		shiftFunc; 	                           		/* User Shift function to use                           */
	int16_t						biasData[R_ATMLIB_CNN_ARI_MAX_CH];     		/* bias data r_atmlib_CNNSetupBiases() input parameters */
	R_ATMLIB_CNNLUTInfo         lutInfo;                               		/* lut information by WPR                               */
	R_ATMLIB_CNNDMARFParam     	lutDMARF;                              		/* lut information by DMARF                             */
}CG_CNNIPConfig;

/************************************************
	Function prototype definitions
************************************************/
R_CG_RETURN_VALUE     cg_CNNCheckNodeConfig( const R_CG_CNNNodeConfig* cnnNodeConfig );
void 			      cg_CNNParseNode( const R_CG_CNNNodeConfig* cnnNodeConfig, CG_CNNIPConfig* cnnipConfig );
R_ATMLIB_RETURN_VALUE cg_CNNProcessNode( const CG_CNNIPConfig* cnnipConfig, R_ATMLIB_CLData* clData);
R_CG_RETURN_VALUE     cg_CNNWriteCL(const R_CG_CNNCoreID core_type, const R_ATMLIB_CLData* clData);

#endif /* CG_CNN_H_ */
