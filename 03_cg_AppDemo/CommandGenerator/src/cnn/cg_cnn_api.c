/******************************************************************************
    Command Generator
     Copyright (C)  2020 Renesas Electronics Corporation.All rights reserved.
	
    [File] cg_cnn_api.c

******************************************************************************/
#include "r_cg_cnn.h"
#include "cg_cnn.h"

/******************************************************************************
 [ FuncName ]	r_cg_CNNGenNodeCL
 [ Params ]		cnnNodeConfig   				 : input parameter -- The pointer to the R_CG_CNNNodeConfig structure
                clData                           : input parameter -- The pointer to the R_ATMLIB_CLData structure
 [ Returns ]    R_CG_OK 					     ：On success
                R_CG_NG_NULL_POINTER		     : input parameter is NULL pointer                                       
                R_CG_NG_CNN_NODE_TYPE            : nodeType is out of range                                              
                R_CG_NG_CNN_COREID               : coreID is out of range                                                
                R_CG_NG_CNN_INPUT_TYPE           : inputTyp is out of range                                              
                R_CG_NG_CNN_CH_MODE              : channel mode is out of range                                          
                R_CG_CN_CNN_CH_CNT               : channel count is out of range                                         
                R_CG_NG_CNN_DAMOMODE_N411SPMC    : combination of DMAO channel mode and next layer kernel 4*1x1 and SPMC 
                R_CG_NG_CNN_BIASDATA_NULL        : bias data poniter is NULL                                             
                R_CG_NG_CNN_BIASDATA_LEN         : bias data arrary length is out of range                               
                R_CG_NG_CNN_TRANSFER_MODE        : weightTransferMode / lutTransferMode transfer mode is out of range    
                R_CG_NG_CNN_WEIHGTDATA_NULL      : weight data pointer is NULL                                           
                R_CG_NG_CNN_WEIHGTDATA_LEN       : weight data arrary length is out of range                             
                R_CG_NG_CNN_WEIHGTDATA_FM        : weight data format is out of range                                    
                R_CG_NG_CNN_NEXT_DMAIFM          : next tile dmai format is out of range                                 
                R_CG_NG_CNN_NEXT_DMAOFM          : next tile dmao format is out of range                                 
                R_CG_NG_CNN_NEXT_WEIGHTFM        : next tile weight format is out of range                               
                R_CG_NG_CNN_CUR_WEIGHTFM         : current tile weight format is out of range                            
                R_CG_NG_OPEN_FILE                : fail to open file                                                     
                R_CG_NG_WRITE_FILE               : fail to write file                                                    
                R_CG_NG_CLOSE_FILE               : fail to close file                                                    
 [ Function ]	generate the commandlist.
 [ Note ]		none
******************************************************************************/
int32_t r_cg_CNNGenNodeCL(const R_CG_CNNNodeConfig *cnnNodeConfig, R_ATMLIB_CLData *clData)
{
    int32_t ret;
    CG_CNNIPConfig cnnipConfig;

    ret = R_CG_OK;
    memset(&cnnipConfig, 0U, sizeof(CG_CNNIPConfig));

    if (cnnNodeConfig == NULL || clData == NULL)
    {
        ret = R_CG_NG_NULL_POINTER;
    }
    else
    {
        ret = cg_CNNCheckNodeConfig(cnnNodeConfig);
        if (ret == R_CG_OK)
        {
            cg_CNNParseNode(cnnNodeConfig, &cnnipConfig);
            ret = cg_CNNProcessNode(&cnnipConfig, clData);
            if (ret == R_CG_OK && cnnNodeConfig->cnnNodeType == R_CG_CNN_PICTURE_EPILOG_NODE)
            {
                ret = cg_CNNWriteCL(cnnNodeConfig->cnnCoreID, clData);
            }
        }
    }

    return ret;
}
