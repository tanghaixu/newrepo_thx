/******************************************************************************
    Command Generator
     Copyright (C)  2020 Renesas Electronics Corporation.All rights reserved.
    
    [File] r_cg_cnn.h

******************************************************************************/
#ifndef R_CG_CNN_H_
#define R_CG_CNN_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#include "r_atmlib_types.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/************************************************
	Enum definitions
************************************************/
/* Return Value */
typedef enum
{
	R_CG_OK 					    = 0,                /* On success                                                               */
    R_CG_NG_NULL_POINTER		    = -1001,            /* input parameter is NULL pointer                                          */
	R_CG_NG_CNN_NODE_TYPE           = -1002,            /* nodeType is out of range                                                 */
	R_CG_NG_CNN_COREID              = -1003,            /* coreID is out of range                                                   */
	R_CG_NG_CNN_INPUT_TYPE          = -1004,            /* inputTyp is out of range                                                 */
    R_CG_NG_CNN_CH_MODE             = -1005,            /* channel mode is out of range                                             */
    R_CG_CN_CNN_CH_CNT              = -1006,            /* channel count is out of range                                            */
    R_CG_NG_CNN_DAMOMODE_N411SPMC   = -1007,            /* combination of DMAO channel mode and next layer kernel 4*1x1 and SPMC    */
    R_CG_NG_CNN_BIASDATA_NULL       = -1008,            /* bias data poniter is NULL                                                */
    R_CG_NG_CNN_BIASDATA_LEN        = -1009,            /* bias data arrary length is out of range                                  */
    R_CG_NG_CNN_TRANSFER_MODE       = -1010,            /* weightTransferMode / lutTransferMode transfer mode is out of range       */
    R_CG_NG_CNN_WEIHGTDATA_NULL     = -1011,            /* weight data pointer is NULL                                              */
    R_CG_NG_CNN_WEIHGTDATA_LEN      = -1012,            /* weight data arrary length is out of range                                */
    R_CG_NG_CNN_WEIHGTDATA_FM       = -1013,            /* weight data format is out of range                                       */
    R_CG_NG_CNN_NEXT_DMAIFM         = -1014,            /* next tile dmai format is out of range                                    */
    R_CG_NG_CNN_NEXT_DMAOFM         = -1015,            /* next tile dmao format is out of range                                    */
    R_CG_NG_CNN_NEXT_WEIGHTFM       = -1016,            /* next tile weight format is out of range                                  */
    R_CG_NG_CNN_CUR_WEIGHTFM        = -1017,            /* current tile weight format is out of range                               */
    R_CG_NG_OPEN_FILE               = -1018,            /* fail to open file                                                        */
    R_CG_NG_WRITE_FILE              = -1019,            /* fail to write file                                                       */
    R_CG_NG_CLOSE_FILE              = -1020             /* fail to close file                                                       */
} R_CG_RETURN_VALUE;

/* CNN node type */
typedef enum 
{
    R_CG_CNN_COMMON_NODE,							    /* common node 				                    */
    R_CG_CNN_PICTURE_PROLOG_NODE,					    /* first node of the picture                    */
    R_CG_CNN_PICTURE_EPILOG_NODE,					    /* last  node of the picture                    */
    R_CG_CNN_LAYER_PROLOG_NODE,						    /* first node of the layer	                    */
    R_CG_CNN_LAYER_EPILOG_NODE						    /* last  node of the layer	                    */
}R_CG_CNNNodeType;

/* CNN core type */
typedef enum 
{
    R_CG_CNN_CORE0,   								    /* CNN core0                                    */
    R_CG_CNN_CORE1,   								    /* CNN core1                                    */
    R_CG_CNN_CORE2,   								    /* CNN core2                                    */
    R_CG_CNN_CORE3,   								    /* CNN core3                                    */
}R_CG_CNNCoreID;

/* CNN Input type */
typedef enum 
{
    R_CG_CNN_DMAI,                          		    /* only DMAI is used 			                */
    R_CG_CNN_DMAC,                        			    /* only DMAC is used			                */
    R_CG_CNN_DMAI_AND_DMAC    						    /* Both DMAI and DMAC are used 	                */
}R_CG_CNNInputType;

/* CNN LUT/Weight transfer mode */
typedef enum 
{
    R_CG_CNN_WPR,      								    /* data transfer mode is WPR	                */
    R_CG_CNN_DMARF     								    /* data transfer mode is DMARF	                */
}R_CG_CNNTransMode;

/************************************************
	Structure definitions 	
************************************************/
/* XY pooling information */
typedef struct  
{
    R_ATMLIB_CNNXYPoolMode      mode;       			 /* xy pooling mode			                            */
    uint8_t                     x;            			 /* x direction pooling value (range [1,4])             */
    uint8_t                     y;            			 /* y direction pooling value (range [1,4])             */
}R_CG_CNNXYPoolInfo;

/* Channel pooling information */
typedef struct 
{
    R_ATMLIB_CNNCPoolMode       mode;     		   	     /* channel pooling mode                                */
    R_ATMLIB_CNNMAXGroup        maxoutGroup;   		   	 /* max group info		                                */
    R_ATMLIB_CNNLabelBit        labelNo;       		   	 /* label info			                                */
}R_CG_CNNChannelPoolInfo;

/* CNN magnification information */
typedef struct 
{
    R_ATMLIB_CNNMAGType         type;          	         /* magnification type			                         */
    uint8_t        		        x;    		             /* x direction repeat skip value (range [0,4])          */
    uint8_t        		        y;             		     /* y direction repeat skip value (range [0,4])          */
}R_CG_CNNSkipRepeatInfo;

/* CNN padding information */
typedef struct  
{
    R_ATMLIB_CNNPadMode  	    mode;       			 /* padding mode                                          */
    uint8_t          	        n;          			 /* top padding value	(range [0,7])	                  */
    uint8_t          	        s;          			 /* bottom padding value(range [0,7])	                  */
    uint8_t          	        w;          			 /* left padding mode	(range [0,16])	                  */
    uint8_t          	        e;          			 /* right padding mode	(range [0,16])	                  */
}R_CG_CNNPadInfo;

/* Weight WPR information */
typedef struct 
{
    int32_t*			        data;				     /* weight data  	 		                                                           */
    uint16_t			        dataLen;		         /** length of the weight data: 
                                                             eg. kernel mode 333 & 16x32, the length is 3*3*3*16*32 = 0x3600 range[1, 0x3600]  */
    R_ATMLIB_CNNDATAFormat	    format;			         /* weight format 			                                                           */
    uint8_t				        frac;				     /* weight fractional position                                                         */
}R_CG_CNNWeightWPRInfo;

/* Bias data information */
typedef struct
{
    int16_t*			        data;				     /* bias data 		 	                                                         */
    uint8_t				        dataLen;			     /* length of the bias data, same with convolution channel counts (range [1,32]) */
    uint8_t				        frac;				     /* bias fractional position                                                     */
}R_CG_CNNBiasInfo;

/* Lut WPR information */
typedef struct 
{
    int16_t				        data[256];			     /* Lut table data for all lut tables  		           */
    R_ATMLIB_CNNDATAFormat	    format;				     /* Lut tabel format 						           */
    uint8_t				        frac;				     /* Lut data fractional position		               */
}R_CG_CNNLutWPRInfo;

/* Weight DMARF information */
typedef struct 
{
    uint32_t                    extStartAddr;	         /* internal memory/DDR start address, the source address    */
    uint16_t                    length;     		     /* the transfer length of DMARF, only used for SCMP mode    */
    uint8_t                     startPosition;	         /* Start position, only used for DMARF SPMC mode of weight  */
    uint8_t                     endPositio;		         /* End position, only used for DMARF  SPMC mode of weight   */
    R_ATMLIB_CNNDATAFormat	    curTileFormat;	         /* current tile weight data format							 */
    uint8_t                     frac;				     /* weight data fractional position							 */
    R_ATMLIB_CNNDataMode        dataMode;			     /* weight data mode : SPMC/SCMP		    				 */
	R_ATMLIB_CNNDATAFormat      nextTileDMAIFM;       	 /* next tile DMAI format									 */
    R_ATMLIB_CNNDATAFormat      nextTileDMAOFM;          /* next tile DMAO format									 */
    R_ATMLIB_CNNDATAFormat      nextTileWeightFM;        /* next tiel weight format                                  */
}R_CG_CNNWeightDMARFInfo;

/* Lut DMARF information */
typedef struct 
{
    uint32_t			        extStartAddr;  	        /* internal memory/DDR start address, the source address    */
    uint16_t			        length;      		    /* the transfer length of DMARF						        */                        
    R_ATMLIB_CNNDATAFormat      format;      		    /* lut data format  								  	    */
    uint8_t 			        frac;             	    /* lut data fractional position					            */
}R_CG_CNNLutDMARFInfo;

/* Memory information */
typedef struct 
{
	uint8_t			            channelCnt; 			/** channel count, for SCMP mode : DMAI range [1,16], DMAC/DMAO (range [1,32]);             
                                                                           for SPMC mode,  DMAI/DMAC/DMAO (range [1,1])                  */ 
    R_ATMLIB_CNNDATAFormat	    format;		            /* format (s4/u4/s8/u8/s16/u16)	          e.g. uint16.8 the format is u16        */
    uint8_t                     frac;                   /* fractional positon (range[0x00, 0xFF]) e.g. uint16.8 the frac is 8            */
	uint16_t				    imageWidth;			    /* feature map width in pixel                                                    */
    uint16_t                    imageHeigh;             /* feature map heigth in pixel                                                   */
    uint16_t                    imageStride;            /* feature map stride in pixel                                                   */
    uint32_t*                   imageStartAddr;         /** feature map start address in DDR or internal memory      
                                                            Allocate the area for the number of channelCnt                               */              
}R_CG_CNNMemoryInfo;

/* Describe a CNN node parameters */
typedef struct
{
	R_CG_CNNNodeType 		    cnnNodeType;			/* CNN node type    									                            */
	R_CG_CNNCoreID 		        cnnCoreID;	    		/* CNN core ID: cnn0/cnn1/cnn2/cnn3    									            */
	R_CG_CNNInputType		    inputType;				/* input memory has only DMAI/only DMAC/ DMAI and DMAC	                            */
	R_ATMLIB_CNNKernelMode      kernelMode;             /* kernel mode : 1*5x5 / 2*3x3 /3*3x3 / 4*1x1                                       */
    uint8_t					    dilation;				/* dilated convolution distance                 (range[0,3])                        */
	uint8_t					    startPosition;			/* start position in x direction on left side	(range[0,7])                        */
	uint8_t					    convolutionStrideX;		/* convolution stride in x direction            (range[1,5])                        */
	uint8_t					    convolutionStrideY;		/* convolution stride in y direction            (range[1,5])                        */
	R_CG_CNNPadInfo			    padding;				/* padding information 	: padding mode and padding value                            */
	R_ATMLIB_CNNDataMode	    dmaiChannelMode;		/* DMAI Data transfer mode : SPMC / SCMP				                            */
	R_CG_CNNSkipRepeatInfo	    dmaiSkipRepeat;			/* DMAI skip repeat information: magnification type,x/y direction repeat skip value */
	R_CG_CNNMemoryInfo		    dmaiMemoryInfo;			/* DMAI memory infomation 								                            */
	R_ATMLIB_CNNDataMode	    dmacChannelMode;		/* DMAC data transfer mode : SPMC / SCMP				                            */
	R_CG_CNNSkipRepeatInfo	    dmacSkipRepeat;			/* DMAC skip repeat information: magnification type,x/y direction repeat skip value */
	R_CG_CNNMemoryInfo		    dmacMemoryInfo;			/* DMAC memory infomation								                            */
	R_ATMLIB_CNNDataMode	    dmaoChannelMode;		/* DMAO Data transfer mode : SPMC / SCMP				                            */
	R_CG_CNNMemoryInfo		    dmaoMemoryInfo;			/* DMAO memory infomation								                            */
	bool					    nextLayerSPMC411;		/* whether next layer is SPMC & 1_4x4					                            */
	bool					    enableBias;				/* bias enable control									                            */
	R_CG_CNNBiasInfo		    bias;					/* bias data information : bias array bias frac and aray length                     */
	uint8_t					    convChannels;			/* Count number of convolution channles	       (range[1,32])                        */
	R_CG_CNNTransMode		    weightTransferMode;		/* Weight transfer mode	: DMARF or WPR					                            */
	R_CG_CNNWeightWPRInfo       weightWPRInfo;			/* Weight transferred by WPR, the weights information 	                            */
	R_ATMLIB_CNNActFunc 	    actiMode;				/* CNN activiation mode	: NO, RELU, BRELU,ELU,LRELU,PRELU,PELU,SIGTAHH              */
	uint16_t				    upperBound;				/* Upper boundary value for activ BRELU, ELU, LRELU, PELU (range[0x0000,0x7FFF])    */
	int16_t					    lowerBound;				/* Lower boundary value for activ LRELU, PRELU	          (range[0x8000,0xFFFF])    */
	uint8_t					    multValue;				/* Multiplier value of activ ELU, PELU, Sigmoid, TanH     (range[0x01, 0xFF])       */
	uint8_t					    multFrac;				/* Fractional position of multValue                       (range[0x00, 0xFF])       */
	R_CG_CNNTransMode		    lutTransferMode;		/* Lut transferred by DMARF or WPR						                            */
    bool				        lutLinearInter; 		/* Enable for LUT piece-wise linear interprolation function                         */
	R_CG_CNNLutWPRInfo		    lutWPRInfo;				/* Lut is transferred by WPR information				                            */
	R_CG_CNNXYPoolInfo  	    xyPool;					/* XY pooling information, xy pooling mode and pooling value                        */
	R_CG_CNNChannelPoolInfo     channelPool;			/* Channel pooling information, channel pooling mode and pooling value              */
	R_CG_CNNWeightDMARFInfo     weightDMARFInfo;		/* Weight transferred by DMARF, the DMARF information	                            */
	R_CG_CNNLutDMARFInfo	    lutDMARFInfo;			/* Lut transferred by DMARF, the DMARF information		                            */
} R_CG_CNNNodeConfig;

/************************************************
	Function prototype definitions
************************************************/
int32_t r_cg_CNNGenNodeCL( const R_CG_CNNNodeConfig* CNNNodeConfig, R_ATMLIB_CLData* clData);

#ifdef __cplusplus
}
#endif

#endif /* R_CG_CNN_H_ */
