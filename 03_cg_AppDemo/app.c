/*!
* @name        : cg_test.c
* @Author      : REL
* @Version     : Copyright
* @note		   : Atmlib CNN Demo
* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "r_cg_cnn.h"
#include "cg_cnn.h"

R_ATMLIB_RETURN_VALUE UserShiftFunc(
	R_ATMLIB_CLData					*cldata,						/* The pointer of R_ATMLIB_CLData structure				                                */
	const R_ATMLIB_CNNShiftConfig	*shift_cfg					    /* The pointer of shift configuration structure for one tile	                        */
)
{
	return R_ATMLIB_E_OK;
}

void ConfigureSetupCNNARI(CG_CNNIPConfig* cnn_config)
{
	cnn_config->ariParam.ari_cnt            = 32         					 ;
	cnn_config->ariParam.ari_format         = R_ATMLIB_CNN_ARIFORMAT_8U      ;
	cnn_config->ariParam.acti_mode          = R_ATMLIB_CNN_ACTFUNC_ELU       ;
	cnn_config->ariParam.lut_lin_int        = R_ATMLIB_CNN_DISABLE           ;
	cnn_config->ariParam.pad_mode           = R_ATMLIB_CNN_PAD_ZERO          ;
	cnn_config->ariParam.bias_en            = R_ATMLIB_CNN_ENABLE            ;
	cnn_config->ariParam.xypool_mode        = R_ATMLIB_CNN_XYPOOL_NO         ;
	cnn_config->ariParam.xpool              = 1                              ;
	cnn_config->ariParam.ypool              = 1                              ;
	cnn_config->ariParam.cpool_mode         = R_ATMLIB_CNN_CPOOL_NO          ;
	cnn_config->ariParam.label_info         = R_ATMLIB_CNN_LABELBIT_0        ;
	cnn_config->ariParam.maxout_group       = R_ATMLIB_CNN_MAXGROUP_1        ;
	cnn_config->ariParam.next_ari_format    = R_ATMLIB_CNN_ARIFORMAT_8U      ;
	cnn_config->ariParam.sftm_round_mode    = R_ATMLIB_CNN_ENABLE            ;
	cnn_config->ariParam.cpool_ui           = R_ATMLIB_CNN_CPOOLUI_0         ;
	cnn_config->ariParam.upper_bound        = 5                              ;
	cnn_config->ariParam.lower_bound        = -1                             ;
	cnn_config->ariParam.mult_val           = 1                              ;
	cnn_config->ariParam.mult_frac          = 0                              ;

	for(int i = 0; i < cnn_config->ariParam.ari_cnt ; i++)
	{
		cnn_config->ariParam.ch_id[i] = R_ATMLIB_CNN_ARI0 + i;
	}
}

void ConfigureSetupCNNWeights(CG_CNNIPConfig* cnn_config)
{
	const R_ATMLIB_CNNChannelID   in_id[16]  = { R_ATMLIB_CNN_DMAI0,R_ATMLIB_CNN_DMAI1,R_ATMLIB_CNN_DMAI2,R_ATMLIB_CNN_DMAI3,R_ATMLIB_CNN_DMAI4,R_ATMLIB_CNN_DMAI5,R_ATMLIB_CNN_DMAI6,R_ATMLIB_CNN_DMAI7,R_ATMLIB_CNN_DMAI8,R_ATMLIB_CNN_DMAI9,R_ATMLIB_CNN_DMAI10,R_ATMLIB_CNN_DMAI11,R_ATMLIB_CNN_DMAI12,R_ATMLIB_CNN_DMAI13,R_ATMLIB_CNN_DMAI14,R_ATMLIB_CNN_DMAI15 };
	const R_ATMLIB_CNNChannelID   out_id[32] = { R_ATMLIB_CNN_ARI0,R_ATMLIB_CNN_ARI1,R_ATMLIB_CNN_ARI2,R_ATMLIB_CNN_ARI3,R_ATMLIB_CNN_ARI4,R_ATMLIB_CNN_ARI5,R_ATMLIB_CNN_ARI6,R_ATMLIB_CNN_ARI7,R_ATMLIB_CNN_ARI8,R_ATMLIB_CNN_ARI9,R_ATMLIB_CNN_ARI10,R_ATMLIB_CNN_ARI11,R_ATMLIB_CNN_ARI12,R_ATMLIB_CNN_ARI13,R_ATMLIB_CNN_ARI14,R_ATMLIB_CNN_ARI15,R_ATMLIB_CNN_ARI16,R_ATMLIB_CNN_ARI17,R_ATMLIB_CNN_ARI18,R_ATMLIB_CNN_ARI19,R_ATMLIB_CNN_ARI20,R_ATMLIB_CNN_ARI21,R_ATMLIB_CNN_ARI22,R_ATMLIB_CNN_ARI23,R_ATMLIB_CNN_ARI24,R_ATMLIB_CNN_ARI25,R_ATMLIB_CNN_ARI26,R_ATMLIB_CNN_ARI27,R_ATMLIB_CNN_ARI28,R_ATMLIB_CNN_ARI29,R_ATMLIB_CNN_ARI30,R_ATMLIB_CNN_ARI31};
	int32_t                       value_test[27] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,127,126,125,124,123,122,121,120,119,0,0};

	if(cnn_config->flowControl.weightTransMode == true) /* true: DMARF */
	{
		cnn_config->weightDMARF.external_sa   = 0x40000000;
		cnn_config->weightDMARF.internal_sa   = 0x8000;
		cnn_config->weightDMARF.length        = 16 * 32 * 32/64;
		cnn_config->weightDMARF.start_pos     = 1;
		cnn_config->weightDMARF.end_pos       = 1;
		cnn_config->weightDMARF.format        = R_ATMLIB_CNN_DATAFM_8U;
		cnn_config->weightDMARF.data_mode     = R_ATMLIB_CNN_DATA_SCMP;
	}
	else   /* false: WPR */
	{
		cnn_config->weightInfo.weight_format  = R_ATMLIB_CNN_DATAFM_8U;
		cnn_config->weightInfo.ch_cnt 		  = cnn_config->dmaiParam.ch_count * cnn_config->ariParam.ari_cnt;

		for(int i = 0; i < cnn_config->dmaiParam.ch_count ; i++) /* input channel cnt */
		{
			for(int j = 0; j < cnn_config->ariParam.ari_cnt ; j++) /* output channel cnt */
			{
				cnn_config->weightInfo.ch_weight[(i*32)+(j)].in_id  =  in_id[i];
				cnn_config->weightInfo.ch_weight[(i*32)+(j)].out_id = out_id[j];

				for(int k = 0; k < R_ATMLIB_CNN_FILTER_SIZE; k++)
				{
					cnn_config->weightInfo.ch_weight[(i*32)+(j)].value[k] = value_test[k];
				}
			}
		}

	}
}

void ConfigureSetupCNNBiases(CG_CNNIPConfig* cnn_config)
{
    const int16_t bias_same_value[32] = { 32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767,	32767 };

	for(int loop_cnt = 0; loop_cnt < 32; loop_cnt++)
	{
		cnn_config->biasData[loop_cnt] = bias_same_value[loop_cnt];
	}
}

void ConfigureSetupCNNShifts(CG_CNNIPConfig* cnn_config)
{
	cnn_config->shiftConfig.dmao_frac		= 0         ;
	cnn_config->shiftConfig.weight_frac	    = 0         ;
	cnn_config->shiftConfig.bias_frac		= 0         ;
	cnn_config->shiftConfig.lut_frac		= 0         ;
	cnn_config->shiftFunc = UserShiftFunc;

}

void ConfigureSetupCNNLUT(CG_CNNIPConfig* cnn_config)
{
	/* lut transfer mode: false: WPR true: DMARF */
	if(cnn_config->flowControl.lutTransMode == true)
	{
		cnn_config->lutDMARF.external_sa = 0x30000000 ;
		cnn_config->lutDMARF.internal_sa = 0x7100	  ;
		cnn_config->lutDMARF.length      = 4 * 256/64 ;
		cnn_config->lutDMARF.format      = R_ATMLIB_CNN_DATAFM_8U ;
		cnn_config->lutDMARF.data_mode   = R_ATMLIB_CNN_DATA_SCMP ;
	}
	else
	{
		cnn_config->lutInfo.lut_format   = R_ATMLIB_CNN_DATAFM_8U;
		for(int loop_cnt = 0; loop_cnt < 256 ; loop_cnt++)
		{
			cnn_config->lutInfo.value[loop_cnt]= 1;
		}
	}
}

void ConfigureSetupCNNChannelDMAI(CG_CNNIPConfig* cnn_config)
{
	cnn_config->dmaiParam.param_count = 1;
	cnn_config->dmaiParam.ch_count = 16;

	for(int i = 0; i < cnn_config->dmaiParam.ch_count; i++)
	{
		cnn_config->dmaiParam.ch_id[i] = R_ATMLIB_CNN_DMAI0 + i;
	}

	for(int i = 0; i < cnn_config->dmaiParam.param_count; i++)
	{
		cnn_config->dmaiParam.data_mode = R_ATMLIB_CNN_DATA_SCMP;
		cnn_config->dmaiParam.kernel_mode = R_ATMLIB_CNN_KERNEL_1_5_5;
		cnn_config->dmaiParam.dil_conv = 0;
		cnn_config->dmaiParam.channel_offset = 1;
	}

	cnn_config->dmaiParam.dmai_ch[0].dmai_frac 			= 0;
	cnn_config->dmaiParam.dmai_ch[0].ptr				= 0x10000000;
	cnn_config->dmaiParam.dmai_ch[0].image_stride 		= 64;
	cnn_config->dmaiParam.dmai_ch[0].image_x			= 64;
	cnn_config->dmaiParam.dmai_ch[0].image_y 			= 64;
	cnn_config->dmaiParam.dmai_ch[0].image_format 		= R_ATMLIB_IMG_4U;
	cnn_config->dmaiParam.dmai_ch[0].mag_type 			= R_ATMLIB_CNN_SKP;
	cnn_config->dmaiParam.dmai_ch[0].repeat_skip_x 		= 0;
	cnn_config->dmaiParam.dmai_ch[0].repeat_skip_y 		= 4;
	cnn_config->dmaiParam.dmai_ch[0].start_pos 			= 0;
	cnn_config->dmaiParam.dmai_ch[0].stride_x 			= 1;
	cnn_config->dmaiParam.dmai_ch[0].stride_y 			= 1;
	cnn_config->dmaiParam.dmai_ch[0].pad_b				= 2;
	cnn_config->dmaiParam.dmai_ch[0].pad_l				= 2;
	cnn_config->dmaiParam.dmai_ch[0].pad_t				= 2;
	cnn_config->dmaiParam.dmai_ch[0].pad_r				= 2;

}

void ConfigureSetupCNNChannelDMAC(CG_CNNIPConfig* cnn_config)
{
	cnn_config->dmacParam.param_count								= 1					    ;
    cnn_config->dmacParam.ch_count									= 1					    ;
    cnn_config->dmacParam.data_mode                         		= R_ATMLIB_CNN_DATA_SPMC;
	cnn_config->dmacParam.ch_id[0] 									= R_ATMLIB_CNN_DMAC0	;
	cnn_config->dmacParam.dmac_ch[0].dmac_frac                  	= 0					    ;
	cnn_config->dmacParam.dmac_ch[0].ptr                        	= 0x20000000		    ;
	cnn_config->dmacParam.dmac_ch[0].image_x                    	= 64             		;
	cnn_config->dmacParam.dmac_ch[0].image_y                    	= 64             		;
	cnn_config->dmacParam.dmac_ch[0].image_stride               	= 64             		;
	cnn_config->dmacParam.dmac_ch[0].image_format               	= R_ATMLIB_IMG_8U       ;
	cnn_config->dmacParam.dmac_ch[0].mag_type                   	= R_ATMLIB_CNN_NOMAG    ;
	cnn_config->dmacParam.dmac_ch[0].repeat_skip_x              	= 0           			;
	cnn_config->dmacParam.dmac_ch[0].repeat_skip_y              	= 0          			;
}

void ConfigureSetupCNNChannelDMAO(CG_CNNIPConfig* cnn_config)
{
	cnn_config->dmaoParam.param_count 			 = 1				        ;
	cnn_config->dmaoParam.ch_count    			 = R_ATMLIB_CNN_DMAO_MAX_CH ;

	cnn_config->dmaoParam.ptr[0]                 = 0x80000000;
    cnn_config->dmaoParam.image_x                = 64U;
    cnn_config->dmaoParam.image_y                = 64U;
    cnn_config->dmaoParam.image_stride           = 64U;
    cnn_config->dmaoParam.image_format           = R_ATMLIB_IMG_4U;
    cnn_config->dmaoParam.data_mode              = R_ATMLIB_CNN_DATA_SCMP;
    cnn_config->dmaoParam.next_layer_kernel_mode = R_ATMLIB_CNN_KERNEL_1_5_5;
    cnn_config->dmaoParam.channel_offset         = 10U;

	for(int i = 0; i < cnn_config->dmaoParam.ch_count; i++)
	{
		cnn_config->dmaoParam.ch_id[i] = R_ATMLIB_CNN_DMAO0 + i;
	}
}

R_ATMLIB_RETURN_VALUE FlowContrlDMARF(CG_CNNIPConfig* cnn_config,R_ATMLIB_CLData* cnn_cldata)
{
	R_ATMLIB_RETURN_VALUE ret;
	ret = R_ATMLIB_E_OK;

	cnn_config->flowControl.enableDMAI      = true;
	cnn_config->flowControl.enableBias      = true;
	cnn_config->flowControl.enableDMAC      = true;
	cnn_config->flowControl.enableLUT       = true;
	cnn_config->flowControl.lutTransMode    = true;  /* lut transfer mode: false: WPR true: DMARF            */
	cnn_config->flowControl.weightTransMode = true;  /* weight transfer mode: false WPR true: DMARF          */

	ConfigureSetupCNNARI(cnn_config);
	ConfigureSetupCNNChannelDMAI(cnn_config);
	ConfigureSetupCNNChannelDMAC(cnn_config);
	ConfigureSetupCNNWeights(cnn_config);
	if(cnn_config->flowControl.enableBias) ConfigureSetupCNNBiases(cnn_config);
	ConfigureSetupCNNShifts(cnn_config);
	if(cnn_config->flowControl.enableLUT)  ConfigureSetupCNNLUT(cnn_config);
	ConfigureSetupCNNChannelDMAO(cnn_config);

    printf("[   First Node by DMARF  ]\n");
    cnn_config->flowControl.nodeType = R_CG_CNN_PICTURE_PROLOG_NODE ;
    ret = cg_CNNProcessNode( cnn_config, cnn_cldata);
    cnn_config->flowControl.nodeType = R_CG_CNN_COMMON_NODE ;
    printf("[   Common Node by DMARF ]\n");
    ret = cg_CNNProcessNode( cnn_config, cnn_cldata);
    printf("[   Last  Node by DMARF  ]\n");
    cnn_config->flowControl.nodeType = R_CG_CNN_PICTURE_EPILOG_NODE ;
    ret = cg_CNNProcessNode( cnn_config, cnn_cldata);

	return ret;
}

R_ATMLIB_RETURN_VALUE FlowContrlWPR(CG_CNNIPConfig* cnn_config,R_ATMLIB_CLData* cnn_cldata)
{
	R_ATMLIB_RETURN_VALUE ret;
	ret = R_ATMLIB_E_OK;

	cnn_config->flowControl.enableDMAI      = true;
	cnn_config->flowControl.enableBias      = true;
	cnn_config->flowControl.enableDMAC      = true;
	cnn_config->flowControl.enableLUT       = true;
	cnn_config->flowControl.lutTransMode    = false;  /* lut transfer mode: false: WPR true: DMARF            */
	cnn_config->flowControl.weightTransMode = false;  /* weight transfer mode: false WPR true: DMARF          */

	ConfigureSetupCNNARI(cnn_config);
	ConfigureSetupCNNChannelDMAI(cnn_config);
	ConfigureSetupCNNChannelDMAC(cnn_config);
	ConfigureSetupCNNWeights(cnn_config);
	if(cnn_config->flowControl.enableBias) ConfigureSetupCNNBiases(cnn_config);
	ConfigureSetupCNNShifts(cnn_config);
	if(cnn_config->flowControl.enableLUT)  ConfigureSetupCNNLUT(cnn_config);
	ConfigureSetupCNNChannelDMAO(cnn_config);

    printf("[   First Node by WPR    ]\n");
    cnn_config->flowControl.nodeType = R_CG_CNN_PICTURE_PROLOG_NODE ;
    ret = cg_CNNProcessNode( cnn_config, cnn_cldata);
    cnn_config->flowControl.nodeType = R_CG_CNN_COMMON_NODE ;
    printf("[   Common Node by WPR   ]\n");
    ret = cg_CNNProcessNode( cnn_config, cnn_cldata);
    printf("[   Last  Node by WPR    ]\n");
    cnn_config->flowControl.nodeType = R_CG_CNN_PICTURE_EPILOG_NODE ;
    ret = cg_CNNProcessNode( cnn_config, cnn_cldata);

	return ret;
}

int main(void) {
	puts("!!!This is Sprint0 review meeting demo!!!");
   	printf("     [==========] Running Sprint0 demo()!\n");

   	R_ATMLIB_RETURN_VALUE	   ret;
    ret = R_CG_OK;
    //R_CG_CNN_CORE0
    R_ATMLIB_CLData* p_cldata0 = (R_ATMLIB_CLData*)malloc(sizeof(R_ATMLIB_CLData));
    p_cldata0->size = R_ATMLIB_MAX_CL_LENGTH;
    p_cldata0->top_addr = (uint32_t*)malloc(p_cldata0->size * sizeof(uint32_t));
    p_cldata0->cnn_data = (R_ATMLIB_CNNData*)malloc(sizeof(R_ATMLIB_CNNData));

    CG_CNNIPConfig* ipConfig0 = (CG_CNNIPConfig*)malloc(sizeof(CG_CNNIPConfig));
    memset(ipConfig0, 0x0, sizeof(CG_CNNIPConfig));

    //R_CG_CNN_CORE1
    R_ATMLIB_CLData* p_cldata1 = (R_ATMLIB_CLData*)malloc(sizeof(R_ATMLIB_CLData));
    p_cldata1->size = R_ATMLIB_MAX_CL_LENGTH;
    p_cldata1->top_addr = (uint32_t*)malloc(p_cldata1->size * sizeof(uint32_t));
    p_cldata1->cnn_data = (R_ATMLIB_CNNData*)malloc(sizeof(R_ATMLIB_CNNData));

    CG_CNNIPConfig* ipConfig1 = (CG_CNNIPConfig*)malloc(sizeof(CG_CNNIPConfig));
    memset(ipConfig1, 0x0, sizeof(CG_CNNIPConfig));

    printf("!*****************  CORE0 Transfer weights by DMARF! *****************!\n");
    ret = FlowContrlDMARF(ipConfig0, p_cldata0);
    if(R_ATMLIB_E_OK != ret)
    {
    	printf("(CORE0)Failed to process CNN Node in DMARF mode\n");
    }
    else
    {
    	ret = cg_CNNWriteCL( R_CG_CNN_CORE0, p_cldata0); /* DMARF mode write CL */
        if(R_ATMLIB_E_OK != ret)
        {
        	printf("(CORE0)Failed to write R_CG_CNN_CORE0 CL Data in DMARF mode\n");
        }
    }
    printf("!*****************  CORE1 Transfer weights by WPR!   *****************!\n");
    ret = FlowContrlWPR(ipConfig1, p_cldata1);
    if(R_ATMLIB_E_OK != ret)
    {
    	printf("(CORE1)Failed to process CNN Node in WPR mode\n");
    }
    else
    {
    	ret = cg_CNNWriteCL( R_CG_CNN_CORE1, p_cldata1); /* WPR mode write CL */
        if(R_ATMLIB_E_OK != ret)
        {
        	printf("(CORE1)Failed to write R_CG_CNN_CORE0 CL Data in WPR mode\n");
        }
    }
    free(p_cldata0->cnn_data);
    free(p_cldata0->top_addr);
    free(p_cldata0);
    free(ipConfig0);

    free(p_cldata1->cnn_data);
    free(p_cldata1->top_addr);
    free(p_cldata1);
    free(ipConfig1);
    printf("     [==========] Successfully write CL!\n");

	return EXIT_SUCCESS;
}
